//
//  searchMedicationCell.swift
//  infoSage
//
//  Created by Kunal MAC1 on 29/07/15.
//  Copyright (c) 2015 Pramod shirsath. All rights reserved.
//

import UIKit

class searchMedicationCell: UITableViewCell {

    @IBOutlet weak var btnSelectMedication: UIButton!
    @IBOutlet weak var lblMedicationName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
