

import UIKit
import Alamofire
import Foundation
import SwiftyJSON

class homePage : UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate{
    
    @IBOutlet weak var labelcollectionView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var btHideNote: UIButton!
    @IBOutlet weak var logUserView: UIView!
    @IBOutlet weak var logUserName: UILabel!
    @IBOutlet weak var logUserProPic: UIImageView!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var tbAlert: UITableView!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var processIndicator: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var noteCount: UILabel!
    @IBOutlet weak var notificationView: UIView!
    
    var keystoneProPic:[String] = []
    var keystoneName:[String] = []
    var keyIDArr:[String] = []
    var updatekeystoneProPic:[String] = []
    var pos = 0
    var alertUserName:[String] = []
    var alertUserMessage:[String] = []
    var alertUserProPic:[String] = []
    var alertUserID:[String] = []
    var alertNotResolvedFlag:[String] = []
    var resolvedStatus:[String] = []
    var forwordLinkId:[String] = []
    var checkUpdates:[String] = []
    var alertNoteDate:[String] = []
    var unResolvedNotfID:[String] = []
    var indexNotification = 0
    var unresolvedCount = "0"
    
    var isMedicationVisible : String = String()
    
    var  alertHeight:CGFloat = CGFloat()
    var  btHideNoteY :CGFloat = CGFloat()
    
    let defaults = NSUserDefaults.standardUserDefaults()
    let delObj = UIApplication.sharedApplication().delegate as! AppDelegate
    var connURL:String = String()
    var alertconnURL:String = String()
    let vc : personInformation = personInformation()
    
    @IBOutlet weak var headerView: UIView!
    var name: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("name")
        }
        set{
            NSUserDefaults.standardUserDefaults().setObject(newValue!, forKey: "name")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    var proPic: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("proPic")
        }
        set{
            NSUserDefaults.standardUserDefaults().setObject(newValue!, forKey: "proPic")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    var keystoneID: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("keyID")
        }
        set{
            NSUserDefaults.standardUserDefaults().setObject(newValue!, forKey: "keyID")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    var stringOne = ""
    var proxyUser = ""
    var logedUserProPic = ""
    var logedUserName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(homePage.methodOfReceivedNotification(_:)), name:"NotificationIdentifier", object: nil)
        
        alertHeight = alertView.frame.size.height
        btHideNoteY  = btHideNote.frame.origin.y
        self.isMedicationVisible = "false"
        tbAlert.tableFooterView = UIView(frame: CGRectZero)
        self.lblTitle.text = "Where would you like to go?"
        self.navigationController?.navigationBar.hidden = true
        
        self.navigationController?.interactivePopGestureRecognizer!.enabled = false;
        self.navigationController?.interactivePopGestureRecognizer!.delegate = self;
        //Getting local storage
        proxyUser = defaults.valueForKey("PROXY") as! String
        logedUserProPic = defaults.valueForKey("logUserProPic") as! String
        logedUserName = defaults.valueForKey("logUserName") as! String
        
        stringOne = defaults.valueForKey("UID") as! String
        glob_logUserID = stringOne
        
        if(logedUserProPic == self.delObj.baseImageURL + "w168xh168-2/images/default_profile_image.png")
        {
            logedUserProPic =  delObj.baseImageURL + "w100xh100-2/images/default_profile_image.png"
        }
        else
        {
            
        }
        
        logUserName.text = logedUserName
        
        let headers = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
        
        Alamofire.request(.GET, logedUserProPic, headers: headers).response() {
            (request, _, data, _) in
            //("image request\(request)")
            let image = UIImage(data: data! as NSData)
            self.logUserProPic.image = image
            
        }
        
        //check if user is proxy to set title as per condition-----------------------------
        
        if(proxyUser == "PROXY")
        {
            
            let viewY = labelcollectionView.frame.origin.y + logUserView.frame.size.height
            labelcollectionView.frame = CGRectMake(labelcollectionView.frame.origin.x, viewY, labelcollectionView.frame.size.width, labelcollectionView.frame.size.height-logUserView.frame.size.height)
            collectionView.frame.size.height = labelcollectionView.frame.size.height - subTitleLabel.frame.size.height
            
            lblTitle.text = "Whose account would you like to log in to?"
            subTitleLabel.text = "You are designated as a proxy user for the following accounts:"
        }else
        {
            lblTitle.text = "Where would you like to go?"
            subTitleLabel.text = "You are subscribed to the following keystone users."
            
            
            let device = UIDevice.currentDevice().name
            let screenSize = self.defaults.valueForKey("screenSize") as! String
            let OS = self.defaults.valueForKey("OS") as! String
            
            let alertController = UIAlertController(title: "Device Info", message: "\(UIDevice.currentDevice().name)  \(screenSize) \(OS)  @home", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
            
            //self.presentViewController(alertController, animated: true, completion: nil)
            
            let parameters = ["string":"home"]
            
            var url = self.delObj.baseURL+"users/\(stringOne)/elasticLog/pageview/proxiedUser/\(stringOne)/mobile?operatingSystem=\(OS)&deviceType=\(device)&screenSize=\(screenSize)"
            url = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            
            //print("\(parameters) \(url)")
            //print("user \(stringOne)")
            
            Alamofire.request(.POST, url,parameters: parameters,headers: headers,encoding: .JSON)
                .responseJSON {
                    response in
                    if(response.result.isSuccess)
                    {
                        //print(response?.statusCode)
                        //print("Success")
                        
                    }else if(response.result.isFailure){
                        
                        //print(response?.statusCode)
                        //print("failure")
                        
                    }else{
                        
                        self.delObj.networkConnError();
                        
                    }
            }
            
            
        }
        
        headerView.layer.masksToBounds = true
        headerView.layer.borderColor = UIColor.grayColor().CGColor;
        headerView.layer.borderWidth = 0.5;
        alertView.layer.masksToBounds = true
        alertView.layer.borderColor = UIColor.grayColor().CGColor;
        alertView.layer.borderWidth = 0.5;
        self.navigationController?.navigationBarHidden = true
        
        loadKeystone(stringOne)
        
        let tmpID : AnyObject = keystoneID!
        
        //print("userID in Home uri:: \(tmpID)")
        
        if((tmpID as! String) != ""){
            
            // Check privileges for user
            
            Alamofire.Manager.sharedInstance
                .request(.GET, delObj.baseURL + "users/"+(tmpID as! String)+"/privileges/users/" + stringOne, parameters: nil, headers: headers, encoding: .JSON)
                .responseJSON {
                   response in
                    
                    if(response.result.isSuccess)
                    {
                        
                        var json1 = JSON(response.result.value!)
                        
                        let count: Int? = json1.array?.count
                        if(count != 0)
                        {
                            
                            
                            let isConnected =  json1["CONNECTED"].stringValue as String
                            if(isConnected == "true"){
                                
                                self.isMedicationVisible = json1["VIEW_MEDICATIONS"].stringValue as String
                                
                            }else{
                                print("called from Home line :225")
                                self.LogoutForProxy()
                                
                                
                            }
                        }
                    }
            }
        }else{
            //print("Null userID")
            
        }
        
        
    }
    
    
    
    
    // Refresh Notification on every Push notifications
    
    func methodOfReceivedNotification(notification: NSNotification){
        //Take Action on Notification
        stringOne = defaults.valueForKey("UID") as! String
        alertsShow(stringOne)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Unused method
    //loged user(self) goes home page---------------------------------------
    @IBAction func selfUserHomePage(sender: AnyObject) {
        
        defaults.setValue("", forKey:"PROXY")
        
        
        delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
        
        let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idHomePage") as! homePage
        self.navigationController?.pushViewController(nextView, animated: true)
    }
    
    //shows user alerts ----------------------------------------------------
    func alertsShow(stringOne: String)
    {
        alertUserName = []
        alertUserMessage = []
        alertUserProPic = []
        alertUserID = []
        resolvedStatus = []
        forwordLinkId = []
        checkUpdates = []
        alertNoteDate = []
        self.tbAlert.reloadData()
        
        let headers = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
        
        self.notificationView.hidden = true
        alertconnURL = delObj.baseURL + "users/"+self.stringOne+"/notifications"
        
        
        Alamofire.Manager.sharedInstance
            .request(.GET, alertconnURL, parameters: nil ,headers: headers, encoding: .JSON)
            .responseJSON {
              response in
                //print("notifications home url1: \(request)")
                
                if(response.result.isSuccess)
                {    //background thread
                    let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
                    dispatch_async(dispatch_get_global_queue(priority, 0)) {
                        let json1 = JSON(response.result.value!)
                        
                        let count1: Int? = json1["notifications"].array?.count
                        if(count1 != 0)
                        {
                            
                            var indexCount = 0
                            
                            if let ct = count1 {
                                for  index in 0...ct-1 {
                                    
                                    if(!json1["notifications"][index]["resolved"].boolValue)
                                    {  self.unResolvedNotfID.insert(json1["notifications"][index]["id"].stringValue, atIndex: indexCount)
                                    }
                                    else{ self.unResolvedNotfID.insert("0", atIndex: indexCount)
                                    }
                                    
                                    let dateString:NSTimeInterval =  json1["notifications"][index]["date"].doubleValue / 1000
                                    let  dateformat : NSDateFormatter = NSDateFormatter()
                                    dateformat.setLocalizedDateFormatFromTemplate("EEE, dd MMM yyyy hh:mm a")
                                    
                                    let stringDate = dateformat.stringFromDate(NSDate(timeIntervalSince1970: dateString))
                                    self.alertNoteDate.insert("\(stringDate)", atIndex: index)
                                    //self.alertNoteDate.insert(json1["notifications"][index]["date"].stringValue, atIndex: indexCount)
                                    self.alertUserName.insert(json1["notifications"][index]["name"].stringValue , atIndex: indexCount)
                                    
                                    self.resolvedStatus.insert(json1["notifications"][index]["resolved"].stringValue , atIndex: indexCount)
                                    
                                    self.alertUserMessage.insert(json1["notifications"][index]["text"].stringValue , atIndex: indexCount)
                                    
                                    
                                    let linkID = json1["notifications"][index]["forwardLink"].stringValue
                                    
                                    let str : NSCharacterSet = NSCharacterSet(charactersInString: "/public/")
                                    //print(str)
                                    let stringlength = (linkID.characters.count)
                                    //  var ierror: NSError?
                                    
                                    let regex = try! NSRegularExpression(pattern: "([-]|[ /]|[A-Z]|[a-z])",
                                        options: [.CaseInsensitive])
                                    
                                    
                                    //  var regex:NSRegularExpression = NSRegularExpression(pattern: "([-]|[ /]|[A-Z]|[a-z])", options: NSRegularExpressionOptions.CaseInsensitive, error: &ierror)!
                                    /* regEx.stringByReplacingMatchesInString
                                     (self, options: NSMatchingOptions(),
                                     range: NSMakeRange(0, countElements(self)),
                                     withTemplate:stop)*/
                                    
                                    let modString = regex.stringByReplacingMatchesInString(linkID, options: NSMatchingOptions(), range: NSMakeRange(0, stringlength), withTemplate: "")
                                    
                                    self.forwordLinkId.insert(modString, atIndex: indexCount)
                                    
                                    
                                    
                                    if(json1["notifications"][index]["image"]["path"].stringValue == "")
                                    {
                                        self.alertUserProPic.insert(self.self.delObj.baseImageURL + "w50xh50-2/images/default_profile_image.png", atIndex: indexCount)
                                        
                                    }else
                                    {
                                        let userProPic = self.delObj.baseImageURL + "w50xh50-2/images/"+(json1["notifications"][index]["image"]["path"].stringValue)
                                        self.alertUserProPic.insert(userProPic, atIndex: indexCount)
                                    }
                                    
                                    self.alertUserID.insert(json1["notifications"][index]["id"].stringValue, atIndex: indexCount)
                                    //}
                                    indexCount += 1
                                    
                                }
                                let count1 = json1["numUnresolved"].stringValue
                                
                                
                                //main
                                dispatch_async(dispatch_get_main_queue()) {
                                    if(count1 != "0")
                                    {
                                        self.unresolvedCount = count1
                                        self.notificationView.hidden = false
                                        self.noteCount.text = count1 }
                                    else{
                                        self.notificationView.hidden = true
                                    }
                                    
                                    self.tbAlert.reloadData()
                                }//end main
                            }
                        }
                    }//end background thread
                    
                }
                    //
                else{
                    self.delObj.networkConnError();
                }
        }
        
    }
    func resolveNotification()
    {
        if(self.unresolvedCount != "0")
        {
            if(unResolvedNotfID.count > indexNotification)
            {
                if(unResolvedNotfID[indexNotification] != "0")
                {
                    alertconnURL = delObj.baseURL+"users/"+stringOne+"/notifications/"+unResolvedNotfID[indexNotification]
                    
                    let headers = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
                    Alamofire.Manager.sharedInstance
                        .request(.PUT, alertconnURL, parameters: nil ,headers: headers, encoding: .JSON)
                        .responseJSON{
                           response in
                            self.indexNotification = self.indexNotification + 1
                            self.resolveNotification()
                    }
                }else{
                    if(unResolvedNotfID.count > indexNotification)
                    {self.indexNotification = self.indexNotification + 1
                        resolveNotification()}
                }
            }
        }
    }
    @IBAction func btNotification(sender: AnyObject) {
        
        if(alertUserName.count != 0)
        {
            resolveNotification()
            self.notificationView.hidden = true
            
            if(pos == 0)
            {
                pos = 1
                UIView.animateWithDuration(0.3, animations: {
                    self.alertView.frame.origin.y = self.headerView.frame.origin.y
                    self.alertView.hidden = false
                    }, completion:nil)
            }else
            {
                pos = 0
                UIView.animateWithDuration(0.3, animations: {
                    
                    self.alertView.frame.origin.y = -self.alertView.frame.size.height
                    self.alertView.hidden = true
                    }, completion:nil)
            }
            
        }else
        {
            let alertController = UIAlertController(title: "You have no Notifications", message: "", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func btHideNotification(sender: AnyObject) {
        pos = 0
        
        UIView.animateWithDuration(0.3, animations: {
            
            self.alertView.frame.origin.y = -self.alertView.frame.size.height
            self.alertView.hidden = true
            }, completion:nil)
    }
    //-----------------------------------------------------------------
    
    //load all logged user keystone ---------------------------------------
    func loadKeystone(let stringOne: String)
    {
        alertsShow(stringOne)
        
        // Load all keystones as Homepage
        if(proxyUser != "PROXY")
        {
            
            glob_viewAsProxy = false
            
            connURL = delObj.baseURL+"users/"+stringOne+"/connections/"
            
            let headers = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
            Alamofire.Manager.sharedInstance
                .request(.GET, connURL, parameters: nil , headers: headers,encoding: .JSON)
                .responseJSON {
                    response in
                    
                    
                    if(response.result.isSuccess)
                    {
                        //background thread
                        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
                        dispatch_async(dispatch_get_global_queue(priority, 0)) {
                            
                            let json1 = JSON(response.result.value!)
                            ////print(json1)
                            if(json1.count != 0)
                            {
                                if(json1["userMessage"].stringValue != "You must be logged in to do that.")
                                {
                                    let count: Int? = json1.array?.count
                                    
                                    if(count != 0)
                                    {
                                        
                                        var indexCount = 0
                                        
                                        //Logged in User is Keystone
                                        
                                        if(json1[0]["owner"]["isElder"].boolValue)
                                        {
                                            indexCount = 1
                                            
                                            glob_loginUserIsKeystone = true
                                            
                                            self.keystoneName.insert(json1[0]["owner"]["firstName"].stringValue + " " + json1[0]["owner"]["lastName"].stringValue, atIndex: 0)
                                            
                                            if(json1[0]["owner"]["profileImage"]["image"]["path"].stringValue == "")
                                            {
                                                self.keystoneProPic.insert(self.delObj.baseImageURL + "w168xh168-2/images/default_profile_image.png", atIndex: 0)
                                                
                                            }else
                                            {
                                                let keyProPic = self.delObj.baseImageURL + "w168xh168-2/images/"+(json1[0]["owner"]["profileImage"]["image"]["path"].stringValue)
                                                self.keystoneProPic.insert(keyProPic, atIndex: 0)
                                            }
                                            
                                            self.keyIDArr.insert(json1[0]["owner"]["id"].stringValue, atIndex: 0)
                                            
                                        }
                                        
                                        //Logged in User is not Keystone and has Single Keystone
                                        
                                        if(count == 1 && (!json1[0]["owner"]["isElder"].boolValue) )
                                        {
                                            glob_loginUserIsKeystone = false
                                            
                                            if(json1[0]["target"]["isElder"].boolValue)
                                            {
                                                
                                                self.name = json1[0]["target"]["firstName"].stringValue + " " + json1[0]["target"]["lastName"].stringValue
                                                
                                                if(json1[0]["target"]["profileImage"]["image"]["path"].stringValue == "")
                                                {
                                                    self.proPic = self.delObj.baseImageURL + "w168xh168-2/images/default_profile_image.png"
                                                    
                                                }else
                                                {
                                                    self.proPic = self.delObj.baseImageURL + "w168xh168-2/images/"+(json1[0]["target"]["profileImage"]["image"]["path"].stringValue)
                                                    
                                                }
                                                
                                                
                                                self.keystoneID = json1[0]["target"]["id"].stringValue
                                                self.defaults.setValue(count, forKey:"getOneKeystone" )
                                                self.defaults.setValue(self.name, forKey:"name" )
                                                self.defaults.setValue(self.proPic, forKey:"proPic" )
                                                self.defaults.setValue(self.keystoneID, forKey:"keystoneID" )
                                                
                                                glob_keystoneID = json1[0]["target"]["id"].stringValue
                                                
                                                //main
                                                dispatch_async(dispatch_get_main_queue()) {
                                                    let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idPersonalInforamation") as! personInformation
                                                    self.navigationController?.pushViewController(nextView, animated: false)
                                                }//end main
                                            }else{
                                                //main
                                                dispatch_async(dispatch_get_main_queue()) {
                                                    let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idPersonalInforamation") as! personInformation
                                                    self.navigationController?.pushViewController(nextView, animated: true)         }                       } //end main
                                            
                                        }
                                        else
                                        {
                                            if let ct = count {
                                                for  index in 0...ct-1 {
                                                    if(json1[index]["target"]["isElder"].boolValue  && (json1[index]["subscribed"].boolValue))
                                                    {
                                                        
                                                        self.keystoneName.insert(json1[index]["target"]["firstName"].stringValue + " " + json1[index]["target"]["lastName"].stringValue, atIndex: indexCount)
                                                        
                                                        if(json1[index]["target"]["profileImage"]["image"]["path"].stringValue == "")
                                                        {
                                                            self.keystoneProPic.insert(self.delObj.baseImageURL + "w168xh168-2/images/default_profile_image.png", atIndex: indexCount)
                                                            
                                                        }else
                                                        {
                                                            let keyProPic = self.delObj.baseImageURL + "w168xh168-2/images/"+(json1[index]["target"]["profileImage"]["image"]["path"].stringValue)
                                                            self.keystoneProPic.insert(keyProPic, atIndex: indexCount)
                                                        }
                                                        
                                                        self.keyIDArr.insert(json1[index]["target"]["id"].stringValue, atIndex: indexCount)
                                                        
                                                        indexCount += 1
                                                    }
                                                }
                                                //main
                                                dispatch_async(dispatch_get_main_queue()) {
                                                    self.collectionView.reloadData()
                                                    self.processIndicator.hidden = true
                                                }// end main
                                            }
                                        }
                                    }
                                    
                                    
                                }else
                                {
                                    self.delObj.checkLogin()
                                }
                            }
                            else if(json1.count == 0 )
                            {
                                self.LogoutForProxy()
                                 print("called from Home line :615")
                                
                            }
                            else
                            {
                                self.processIndicator.hidden = true
                                let alertController = UIAlertController(title: "You have no Keystone in your network", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
                                
                                self.presentViewController(alertController, animated: true, completion: nil)
                            }
                        }
                    }else
                    {
                        self.processIndicator.hidden = true
                        self.delObj.networkConnError();
                    }
            }
        }// Load only ProxyKeystone
        else
        {
            glob_viewAsProxy = true
            connURL = delObj.baseURL+"users/"+stringOne+"/privileges/PROXY/outgoing"
            let headers = delObj.headersAll
            Alamofire.Manager.sharedInstance
                .request(.GET, connURL, parameters: nil ,headers: headers, encoding: .JSON)
                .responseJSON {
                    response in
                    
                    //print("Privilege @Home URL:\(request)")
                    
                    if(response.result.isSuccess)
                    {
                        let json1 = JSON(response.result.value!)
                        if(json1["userMessage"].stringValue != "You must be logged in to do that."){
                            let count: Int? = json1.array?.count
                            if(count != 0)
                            {
                                var indexCount = 0
                                if let ct = count {
                                    for  index in 0...ct-1 {
                                        
                                        self.keystoneName.insert(json1[index]["firstName"].stringValue + " " + json1[index]["lastName"].stringValue, atIndex: indexCount)
                                        
                                        if(json1[index]["profileImage"]["image"]["path"].stringValue == "")
                                        {
                                            self.keystoneProPic.insert(self.delObj.baseImageURL + "w168xh168-2/images/default_profile_image.png", atIndex: indexCount)
                                            
                                        }else
                                        {
                                            let keyProPic = self.delObj.baseImageURL + "w168xh168-2/images/"+(json1[index]["profileImage"]["image"]["path"].stringValue)
                                            self.keystoneProPic.insert(keyProPic, atIndex: indexCount)
                                        }
                                        
                                        self.keyIDArr.insert(json1[index]["id"].stringValue, atIndex: indexCount)
                                        
                                        indexCount += 1
                                    }
                                    
                                    self.collectionView.reloadData()
                                    self.processIndicator.hidden = true
                                    }
                            }
                            else{
                                
                                // If no proxy availble due to Privilege change
                                print("called from Home line :683")
                                self.LogoutForProxy()
                            }
                        }else
                        {
                            self.delObj.checkLogin()
                        }
                    }else{
                        
                        self.processIndicator.hidden = true
                        self.delObj.networkConnError();
                    }
            }
        }
    }
    
    //logout loged user ---------------------------------------
    @IBAction func btLogout(sender: AnyObject)
    {
        let alert = UIAlertController(title: "Confirm to Logout", message: "Are you sure you want to Logout?", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler:nil))
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler:{
            (UIAlertAction)in Void.self
            var tokanid = self.defaults.valueForKey("deviceToken") as? String
            
            if(tokanid == nil){
                tokanid = "h433897fg77873456fg868389"
            }
            let headers = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
            Alamofire.request(.POST, self.delObj.baseURL + "users/" + self.stringOne + "/push-notifications/apns/unregister", parameters: ["string": tokanid!],headers: headers, encoding: .JSON)
                .responseJSON {
                    response in
                    
                    //print("Unregister: \(request)")
                    
                    let loginmanager:FBSDKLoginManager=FBSDKLoginManager()
                    let signIn = GPPSignIn.sharedInstance()
                    loginmanager.logOut()
                    signIn?.signOut()
                    self.defaults.setValue("", forKey:"PROXY" )
                    self.defaults.setValue("", forKey:"UserName" )
                    self.defaults.setValue("", forKey:"UID" )
                    self.defaults.setValue("", forKey:"Password" )
                    self.defaults.setValue("Logout", forKey:"fbG+accessToken" )
                    
                    
                    
                    Alamofire.request(.POST, self.delObj.baseURL+"logout/"+self.stringOne,headers: headers)
                        .responseJSON {
                            response in
                            
                            //print("Logout: \(request)")
                            
                            ////print(response!.statusCode)
                            
                            let st = response.response?.statusCode
                            
                            if(st == 204)
                            {
                                self.delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
                                
                                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idLoginViewController") as! ViewController
                                self.navigationController?.pushViewController(nextView, animated: true)
                            }else
                            {
                                self.showLogoutAlert()
                            }
                            
                    }
                    
            }
            
            
        }))
        self.presentViewController(alert, animated: true, completion: { })
        
    }
    //Function for Logout user
    
    func LogoutForProxy()
    {
        let alert = UIAlertController(title: "Your privileges have been modified for your Keystone(s). You will be re-logged in now.", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:
            {
                (UIAlertAction)in Void.self
                
                var tokanid = self.defaults.valueForKey("deviceToken") as? String
                
                if(tokanid == nil){
                    tokanid = "h433897fg77873456fg868389"
                }
                
                //Clear defaults
                
                defer{
                    self.vc.doLogout(tokanid!, stringUserID: self.stringOne);
                }
                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idLoginViewController") as! ViewController
                self.navigationController?.pushViewController(nextView, animated: true)
                
                
        }))
        
        dispatch_async(dispatch_get_main_queue(), {
            // code here
            
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
        //print("Your Keystone status has changed. You will be re-logged in now.")
        
        
    }
    
    
    func showLogoutAlert() {
        let alertController1 = UIAlertController(title: "Logout Failed..!!", message: "InfoSAGE failed to logout. Please try after some time.", preferredStyle: UIAlertControllerStyle.Alert)
        alertController1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController1, animated: true, completion: {})
    }
    
    //all collection view methos and delegets and load keystone ------------------
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        //print("Keystone: \(keystoneName.count)")
        
        return keystoneName.count
        
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as! myViewCell
        
        let headers = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
        
        Alamofire.request(.GET, keystoneProPic[indexPath.row],headers: headers).response() {
            (_, _, data, _) in
            
            let image = UIImage(data: data! as NSData)
            cell.imgView.image = image
            
        }
        cell.keystoneName.text = keystoneName[indexPath.row]
        return cell
    }
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        proPic = keystoneProPic[indexPath.row]
        name = keystoneName[indexPath.row]
        keystoneID = keyIDArr[indexPath.row]
        
        glob_keystoneID = keyIDArr[indexPath.row]
        
        //print("Selected keystone \(glob_keystoneID) by >> \(glob_logUserID) user");
        
        if(proxyUser == "PROXY")
        {
            delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": keyIDArr[indexPath.row]]
            
        }else
        {
            delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat
    {
        return 10
    }
    //-------------------------------------------------------------------------------------
    
    
    //all tables methos and delegets and load all notifiaction ---------------------------
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return alertUserName.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:myAlertViewCell = myAlertViewCell()
        cell = tbAlert.dequeueReusableCellWithIdentifier("alertCell") as! myAlertViewCell
        cell.alrtUserName.text = alertUserName[indexPath.row]
        cell.alrtUserMessage.text = alertUserMessage[indexPath.row]
        cell.lblNoteDate.text = alertNoteDate[indexPath.row]
        if(resolvedStatus[indexPath.row] == "true")
        {
            cell.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 1)
        }else
        {
            cell.backgroundColor = UIColor(red: 223/255, green: 240/255, blue: 238/255, alpha: 1)
        }
        Alamofire.request(.GET, alertUserProPic[indexPath.row]).response() {
            (_, _, data, _) in
            
            let image = UIImage(data: data! as NSData)
            cell.alrtUserImage.image = image
            
        }
        /*if(alertUserName.count < 5 && alertUserName.count-1 == indexPath.row && pos == 0)
         {
         alertView.frame.size.height = cell.contentView.frame.size.height * 7 + btHideNote.frame.size.height * 2
         btHideNote.frame.origin.y = alertView.frame.size.height - btHideNote.frame.size.height
         }else{
         alertView.frame.size.height = alertHeight
         btHideNote.frame.origin.y = btHideNoteY
         }*/
        
        
        return cell
        
    }
    
    
    //Selection of notification to redirect user.
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        var alert = UIAlertController()
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        //(keyIDArr.count)
        
        if(keyIDArr.count == 0 )
        {
            alert = UIAlertController(title: "You are not connected to this user.", message: nil ,preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Cancel, handler:{ (UIAlertAction)in Void()  }))
            self.presentViewController(alert, animated: true, completion: {})
        }
        else
        {
            
            if keyIDArr.contains(self.forwordLinkId[indexPath.row])
            {
                //("yes")
                
                alertconnURL = delObj.baseURL+"users/"+stringOne+"/notifications/"+alertUserID[indexPath.row]
                
                for index in 0...keyIDArr.count-1
                {
                    if(self.forwordLinkId[indexPath.row] == keyIDArr[index])
                    {
                        self.name = keystoneName[index]
                        self.proPic = keystoneProPic[index]
                    }
                }
                
                //self.keystoneName[indexPath.row]
                
                // Notification redirection
                self.keystoneID = self.forwordLinkId[indexPath.row]
                let id = self.forwordLinkId[indexPath.row]
                let statePost = self.alertUserName[indexPath.row].rangeOfString("post")
                let statePost1 = self.alertUserName[indexPath.row].rangeOfString("posts")
                let medState = self.alertUserName[indexPath.row].rangeOfString("medication")
                let medState1 = self.alertUserName[indexPath.row].rangeOfString("medications")
                
                let isElder = self.defaults.valueForKey("isElderStatus") as! String
                
                let headers = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
                
                Alamofire.Manager.sharedInstance
                    .request(.PUT, alertconnURL, parameters: nil , headers: headers, encoding: .JSON)
                    .responseJSON {
                       response in
                        //print("notifications home url2: \(request)")
                        if(response.result.isSuccess)
                        {
                            self.pos = 0
                            self.alertsShow(self.stringOne)
                            if(id != "")
                            {
                                if(id != self.stringOne || isElder == "true")
                                {
                                    if(statePost != nil || statePost1 != nil)
                                    {
                                        self.defaults.setValue("true", forKey:"POST" )
                                    }else
                                    {
                                        self.defaults.setValue("false", forKey:"POST" )
                                    }
                                    if((medState != nil || medState1 != nil) && self.isMedicationVisible == "true")
                                    {
                                        self.defaults.setValue("true", forKey:"MADE" )
                                    }else
                                    {
                                        self.defaults.setValue("false", forKey:"MADE" )
                                    }
                                    
                                    let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idPersonalInforamation") as! personInformation
                                    
                                    self.navigationController?.pushViewController(nextView, animated: true)
                                }
                            }else
                            {
                                self.defaults.setValue("", forKey:"PROXY" )
                                
                                self.delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
                                
                                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idHomePage") as! homePage
                                self.navigationController?.pushViewController(nextView, animated: true)
                            }
                            UIView.animateWithDuration(0.3, animations: {
                                
                                self.alertView.frame.origin.y = -self.alertView.frame.size.height
                                self.alertView.hidden = true
                                }, completion:nil)
                            
                        }
                        else{
                            self.delObj.networkConnError();
                        }
                }
                
            }
            else if((self.forwordLinkId[indexPath.row]) == self.stringOne)
            {
                //("yes")
                
                alertconnURL = delObj.baseURL+"users/"+stringOne+"/notifications/"+alertUserID[indexPath.row]
                
                for index in 0...keyIDArr.count-1
                {
                    if(self.forwordLinkId[indexPath.row] == keyIDArr[index])
                    {
                        self.name = keystoneName[index]
                        self.proPic = keystoneProPic[index]
                    }
                }
                
                //self.keystoneName[indexPath.row]
                self.keystoneID = self.forwordLinkId[indexPath.row]
                let id = self.forwordLinkId[indexPath.row]
                let statePost = self.alertUserName[indexPath.row].rangeOfString("post")
                let statePost1 = self.alertUserName[indexPath.row].rangeOfString("posts")
                let medState = self.alertUserName[indexPath.row].rangeOfString("medication")
                let medState1 = self.alertUserName[indexPath.row].rangeOfString("medications")
                
                let isElder = self.defaults.valueForKey("isElderStatus") as! String
                let headers = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
                Alamofire.Manager.sharedInstance
                    .request(.PUT, alertconnURL, parameters: nil , headers:headers, encoding: .JSON)
                    .responseJSON {
                        response in
                        
                        //print("notifications home url3: \(request)")
                        
                        if(response.result.isSuccess)
                        {
                            self.pos = 0
                            self.alertsShow(self.stringOne)
                            if(id != "")
                            {
                                if(id != self.stringOne || isElder == "true")
                                {
                                    if(statePost != nil || statePost1 != nil)
                                    {
                                        self.defaults.setValue("true", forKey:"POST" )
                                    }else
                                    {
                                        self.defaults.setValue("false", forKey:"POST" )
                                    }
                                    if((medState != nil || medState1 != nil) && self.isMedicationVisible == "true")
                                    {
                                        self.defaults.setValue("true", forKey:"MADE" )
                                    }else
                                    {
                                        self.defaults.setValue("false", forKey:"MADE" )
                                    }
                                    
                                    let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idPersonalInforamation") as! personInformation
                                    
                                    self.navigationController?.pushViewController(nextView, animated: true)
                                }
                            }
                            else
                            {
                                self.defaults.setValue("", forKey:"PROXY" )
                                /* Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]*/
                                self.delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
                                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idHomePage") as! homePage
                                self.navigationController?.pushViewController(nextView, animated: true)
                            }
                            UIView.animateWithDuration(0.3, animations: {
                                
                                self.alertView.frame.origin.y = -self.alertView.frame.size.height
                                self.alertView.hidden = true
                                }, completion:nil)
                            
                        }
                        else{
                            self.delObj.networkConnError();
                        }
                }
                
                
            }
            else{
                alert = UIAlertController(title: "You are not connected to this user.", message: nil ,preferredStyle: UIAlertControllerStyle.Alert)
                
                alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Cancel, handler:{ (UIAlertAction)in Void()  }))
                self.presentViewController(alert, animated: true, completion: {})
                ////("No Contains")
            } //End of Inner else
            
        }
        
    }
    
    //    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
    //        let deleteAction = UITableViewRowAction(style: .Default, title: "Delete") { (action, indexPath) -> Void in
    //        }
    //        return [deleteAction]
    //    }
    //
    //    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {return true
    //    }
    //----------------------------------------------------------------------------
}
