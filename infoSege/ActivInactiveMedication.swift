//
//  ActivInactiveMedication.swift
//  infoSage
//
//  Created by Kunal MAC1 on 22/07/15.
//  Copyright (c) 2015 Pramod shirsath. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

extension UIView {
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.CGPath
        self.layer.mask = mask
    }
}
class ActivInactiveMedication: UIViewController,UITableViewDelegate,UITableViewDataSource, CustomIOS7AlertViewDelegate, MBProgressHUDDelegate {

    @IBOutlet weak var nullMessage: UILabel!
    @IBOutlet weak var processBar: UIActivityIndicatorView!
    @IBOutlet weak var segIndex: UISegmentedControl!
    @IBOutlet weak var seg1: ADVSegmentedControl!
    @IBOutlet weak var tblMedicationlist: UITableView!
    @IBOutlet weak var btnEditMedication: UIButton!
    @IBOutlet var container: UIView!
    let delObj = UIApplication.sharedApplication().delegate as! AppDelegate
    var HUD:MBProgressHUD = MBProgressHUD()
    let defaults = NSUserDefaults.standardUserDefaults()
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var arrMedicationName:[String] = []
    var arrMedicationID:[String] = []
    var arrDosageName:[String] = []
    var arrFrequencyName:[String] = []
    var arrSymptomName:[String] = []
    var arrRouteName:[String] = []
    var arrTypeName:[String] = []
    var arrMedicationNotes:[String] = []
    var arrCreatedon:[String] = []
    var arrEditedOn:[String] = []
    var arrProxyUserID:[String] = []
    var proxyUser: String =  String()
    var logUserId = "" ; var stringOne = ""
    var medStatus:NSString = "(Active) \n"
    var medFlag = 0
    let vc : personInformation = personInformation()
     var proxyIDForAnalytics = ""
    
    var isMedicationVisible : String = String()
    
    var isProxyUser : String = String()
    
     var kID: String =  String()
    var keystoneID: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("keyID")
        }
        
    }
    var name: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("name")
        }
        set{
            NSUserDefaults.standardUserDefaults().setObject(newValue!, forKey: "name")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    var discableView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.seg1.enabled = false
        self.seg1.unselectedLabelColor = UIColor.whiteColor()
        self.seg1.backgroundColor = UIColor(red: 0.1, green: 0.4, blue: 0.6, alpha: 0.55)
        
        segIndex.clipsToBounds = true
        segIndex.setNeedsLayout()
        segIndex.setNeedsDisplay()
        proxyUser = defaults.valueForKey("PROXY") as! String!
        stringOne = defaults.valueForKey("UID") as! String
        kID =  keystoneID as! String
        if(kID == "")
        {
            kID = stringOne
        }
        tblMedicationlist.layer.borderColor = UIColor.grayColor().CGColor;
        tblMedicationlist.layer.borderWidth = 0.5;
        tblMedicationlist.layer.cornerRadius = 5
        tblMedicationlist.userInteractionEnabled = true
        tblMedicationlist.tableFooterView = UIView(frame: CGRectZero)
     
         let headers = delObj.headersAll
        
        
//        Alamofire.Manager.sharedInstance
//            .request(.GET, delObj.baseURL + "users/" + kID + "/privileges/users/" + stringOne, parameters: nil, encoding: .JSON, headers:headers)
//            .responseJSON {
//                (request, response, result) -> Void in
//                
//                //print("Privilege @Profile URL:\(request)")
//                
//                if(result.isSuccess)
//                {
//                    
//                    var json1 = JSON(result.value!)
//                    
//                    let count: Int? = json1.array?.count
//                    if(count != 0)
//                    {
//                        self.isMedicationVisible = json1["VIEW_MEDICATIONS"].stringValue as String
//                        self.isProxyUser = json1["PROXY"].stringValue as String
//                        
//                        if(json1["VIEW_MEDICATIONS"].stringValue == "false")
//                        {
//                            self.LogoutForProxy()
//                        }
//                    }
//                }else{
//                    
//                }
//        }

         getAllMedication("true")
       
        //check loged user have x-proxy user or not
      
        let connURL = appDelegate.baseURL+"users/"+stringOne+"/privileges/PROXY/outgoing"
       
        if(self.proxyUser == "PROXY" || self.kID == self.stringOne)
        {
            self.btnEditMedication.hidden = false
        }else
        {
            self.btnEditMedication.hidden = true
        }
      
        if(self.proxyUser == "PROXY"){
            self.proxyIDForAnalytics = self.kID
        }else{
        
            self.proxyIDForAnalytics = self.stringOne
        }
        
        Alamofire.request(.GET, connURL, parameters: nil ,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                
              //print("Privileges : Key: \(self.kID) LogIn: \(self.logUserId) ")
              //print("Privilege Medication URL:\(request)")
                
                if(response.result.isSuccess)
                {
                    let json1 = JSON(response.result.value!)
                    
                    let count: Int? = json1.array?.count
                    if(count != 0)
                    {
                        if let ct = count {
                            
                            //(json1)
                            for index in 0...ct-1 {
                             self.arrProxyUserID.insert(json1[index]["id"].stringValue, atIndex: index)
                            }
                            
                            if(self.arrProxyUserID.contains(self.kID) || self.proxyUser == "PROXY" || self.kID == self.stringOne)
                            {
                                self.btnEditMedication.hidden = false
                            }else
                            {
                                self.btnEditMedication.hidden = true
                            }
                        }
                        
                    }
                }else{
                    self.processBar.hidden = true
                    self.appDelegate.networkConnError();
                }
        }
        
        
        //Analytics
        
        
        let deviceType = self.defaults.valueForKey("deviceType") as! String
        
        let device = UIDevice.currentDevice().name
        let screenSize = self.defaults.valueForKey("screenSize") as! String
        let OS = self.defaults.valueForKey("OS") as! String
        
        let alertController = UIAlertController(title: "Device Info", message: "\(UIDevice.currentDevice().name)  \(screenSize) \(OS)  @home", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
        
        //self.presentViewController(alertController, animated: true, completion: nil)
        
        let parameters = ["string":"medications"]
        
        var url = self.delObj.baseURL+"users/\(stringOne)/elasticLog/pageview/proxiedUser/\(self.proxyIDForAnalytics)/mobile?operatingSystem=\(OS)&deviceType=\(device)&screenSize=\(screenSize)"
        url = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        //print("\(parameters) \(url)")
        //print("user \(stringOne)")
               
        Alamofire.request(.POST, url,parameters: parameters,headers: headers,encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    //print(response?.statusCode)
                    //print("Success")
                    
                }else if(response.result.isFailure){
                    
                    //print(response?.statusCode)
                    //print("failure")
                    
                }else{
                    
                    self.delObj.networkConnError();
                    
                }
        }

       
      
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        
        if(arrMedicationName.isEmpty && self.nullMessage.text == "")
        {
            getAllMedication("true")
            self.tblMedicationlist.reloadData()
            
        }
        
        

    }
    
    func LogoutForProxy()
    {
        let alert = UIAlertController(title: "Your privileges have been modified for your Keystone(s). You will be re-logged in now.", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:
            {
                (UIAlertAction)in Void.self
                
                var tokanid = self.defaults.valueForKey("deviceToken") as? String
                
                if(tokanid == nil){
                    tokanid = "h433897fg77873456fg868389"
                }
                
                //Clear defaults
                
                defer{
                    self.delObj.checkLogin();
                }
                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idLoginViewController") as! ViewController
                self.navigationController?.pushViewController(nextView, animated: true)
                
                
        }))
        
//        alert.addAction(UIAlertAction (title: "Close", style: .Destructive, handler: { (UIAlertAction) in
//            
//        }))
        
      
        dispatch_async(dispatch_get_main_queue(), {
            // code here
            
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
        //print("Your Keystone status has changed. You will be re-logged in now.")
        
        
    }
  
    func getAllMedication(isActive:String)
    {
        let headers = delObj.headersAll
        defer{
        Alamofire.Manager.sharedInstance
            .request(.GET, delObj.baseURL + "users/"+glob_keystoneID+"/privileges/users/" + glob_logUserID, parameters: nil, headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    
                    var json1 = JSON(response.result.value!)
                    
                    let count: Int? = json1.array?.count
                    if(count != 0)
                    {
                        self.isMedicationVisible = json1["VIEW_MEDICATIONS"].stringValue as String
                        self.isProxyUser = json1["PROXY"].stringValue as String
                        let isConnected = json1["CONNECTED"].stringValue
                        
                        self.defaults.setValue(self.isProxyUser, forKey:"isProxyUser")
                        
                        if(isConnected == "true"){
                            if(self.isProxyUser == "true"){
                                
                                print("good to go")
                                self.btnEditMedication.hidden = false
                                
                            }else{
                                
                                self.btnEditMedication.hidden = true
                                
                                if(self.isMedicationVisible == "false")
                                {
                                    self.LogoutForProxy()
                                }
                            }
                            
                        }else{
                            
                            
                            self.LogoutForProxy()
                        }
                    }
                }
        }
        }
        
        Alamofire.request(.GET, appDelegate.baseURL+"users/" + kID + "/drugImo", parameters: nil ,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    let json1 = JSON(response.result.value!)
                    if(json1["userMessage"].stringValue != "You must be logged in to do that."){
                        let count: Int? = json1.array?.count
                        if(count != 0)
                        {
                            var flagIndex = 0
                            if let ct = count {
                                
                                //(json1)
                                self.arrMedicationName = []
                                self.arrMedicationID = []
                                self.arrMedicationName = []
                                self.arrDosageName = []
                                self.arrFrequencyName = []
                                self.arrSymptomName = []
                                self.arrTypeName = []
                                self.arrMedicationNotes = []
                                self.arrCreatedon = []
                                
                                for index in 0...ct-1 {
                                    if(json1[index]["isPublic"].stringValue == "true")
                                    {
                                        if(json1[index]["isActive"].stringValue == isActive)
                                        {
                                            self.arrMedicationID.insert(json1[index]["id"].stringValue, atIndex: flagIndex)
                                            self.arrMedicationName.insert(json1[index]["name"].stringValue, atIndex: flagIndex)
                                            self.arrDosageName.insert(json1[index]["dosage"].stringValue, atIndex: flagIndex)
                                            self.arrFrequencyName.insert(json1[index]["drugFrequency"]["name"].stringValue, atIndex: flagIndex)
                                            self.arrSymptomName.insert(json1[index]["drugSymptom"]["name"].stringValue, atIndex: flagIndex)
                                            self.arrTypeName.insert(json1[index]["type"].stringValue, atIndex: flagIndex)
                                            self.arrRouteName.insert(json1[index]["route"].stringValue, atIndex: flagIndex)
                                            self.arrMedicationNotes.insert(json1[index]["otherNotes"].stringValue, atIndex: flagIndex)
                                            
                                            let dateString:NSTimeInterval =  json1[index]["creationDate"].doubleValue / 1000
                                            let  dateformat : NSDateFormatter = NSDateFormatter()
                                            dateformat.setLocalizedDateFormatFromTemplate("EEE, dd MMM yyyy hh:mm a")
                                            
                                            let stringDate = dateformat.stringFromDate(NSDate(timeIntervalSince1970: dateString))
                                            
                                            self.arrCreatedon.insert(stringDate, atIndex: flagIndex)
                                            
                                            
                                            let dateString1:NSTimeInterval =  json1[index]["editDate"].doubleValue / 1000
                                            dateformat.setLocalizedDateFormatFromTemplate("EEE, dd MMM yyyy hh:mm a")
                                            
                                            let stringDate1 = dateformat.stringFromDate(NSDate(timeIntervalSince1970: dateString1))
                                            
                                            self.arrEditedOn.insert(stringDate1, atIndex: flagIndex)
                                            
                                            
                                            flagIndex += 1
                                        }else
                                        {
                                            self.seg1.enabled = true
                                            self.seg1.backgroundColor = UIColor(red: 0.1, green: 0.4, blue: 0.6, alpha: 0.8)
                                            // self.segIndex.setEnabled(true, forSegmentAtIndex: 1)
                                        }
                                    }
                                }
                                self.tblMedicationlist.reloadData()
                                if(self.arrMedicationID.count == 0)
                                {
                                    self.nullMessage.hidden = false
                                    self.processBar.hidden = true
                                    
                                    if(self.proxyUser == "PROXY")
                                    {
                                        self.nullMessage.text = "You have no active medications listed."
                                    }else if(self.kID == self.stringOne){
                                        self.nullMessage.text = "You have no active medications listed."
                                    }else
                                    {
                                        self.nullMessage.text = (self.name as? String)! + " has no active medications listed."
                                    }
                                    
                                    
                                }else{}
                                
                                self.processBar.hidden = true
                            }
                            
                        }
                        else
                        {
                            self.nullMessage.hidden = false
                            self.processBar.hidden = true
                            
                            if(self.proxyUser == "PROXY")
                            {
                                self.nullMessage.text = "You have no active medications listed."
                            }else if(self.kID == self.stringOne){
                                self.nullMessage.text = "You have no active medications listed."
                            }else
                            {
                                self.nullMessage.text = (self.name as? String)! + " has no active medications listed."
                            }
                        } }else
                    {
                        self.appDelegate.checkLogin()
                    }
                    
                    if(json1["userMessage"].stringValue == "You don't have permission to do that."){
                    
                        self.processBar.hidden = true
                        self.LogoutForProxy()
                    }
                    
                    
                }else{
                    self.processBar.hidden = true
                    if(self.medFlag == 0)
                    {  self.medFlag = 1
                        self.appDelegate.networkConnError();
                    }
                }
        }
        
    }
    
    //MARK: Edit Medicatiobn
    
    //code for after mark as non keystone attempting to edit medication :2/9/2016
    func keystoneChange()
    {
        
        let headers = delObj.headersAll
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL+"users/" + self.kID + "/profile", parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
              response in
                if(response.result.isSuccess)
                {
                    
                    let json1 = JSON(response.result.value!)
                    print("json1 line ActINActMed 474: \(json1)")
                    if(json1["userMessage"].stringValue == "You don't have permission to do that."){
                        //print("called from line :586")
                        self.LogoutForProxy()
                    }else{
                        
                        if(json1["user"]["isElder"].boolValue) {
                            print("Line  ActINActMed 481")
                            
                        }else{
                            print("called from ActINActMed line :484")
                            
                            // self.LogoutForProxy()
                            self.LogoutForKeystoneStatusChange()
                            
                        }
                        
                        
                    }
                    
                }
                //println(self.userData)
        }
        
    }
    
    func LogoutForKeystoneStatusChange()
    {
        
        let alert = UIAlertController(title: "Your Keystone status has changed. You will be re-logged in now.", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:
            {
                (UIAlertAction)in Void.self
                
                var tokanid = self.defaults.valueForKey("deviceToken") as? String
                
                if(tokanid == nil){
                    tokanid = "h433897fg77873456fg868389"
                }
                
                
                defer{
                    self.doLogout(tokanid!, stringUserID: self.stringOne);
                }
                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idLoginViewController") as! ViewController
                self.navigationController?.pushViewController(nextView, animated: true)
                
                
        }))
        
        dispatch_async(dispatch_get_main_queue(), {
            
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
        
    }
    
    
    func doLogout(let tokanid:String, stringUserID:String)
    {
        
        delObj.checkLogin();
        
    }
    
//EndJ
    
    @IBAction func btnEVEditMedication(sender: AnyObject) {
        keystoneChange()
        let headers = delObj.headersAll
        
        Alamofire.Manager.sharedInstance
            .request(.GET, delObj.baseURL + "users/"+glob_keystoneID+"/privileges/users/" + glob_logUserID, parameters: nil, headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    
                    var json1 = JSON(response.result.value!)
                    
                    let count: Int? = json1.array?.count
                    if(count != 0)
                    {
                        self.isMedicationVisible = json1["VIEW_MEDICATIONS"].stringValue as String
                        self.isProxyUser = json1["PROXY"].stringValue as String
                        let isConnected = json1["CONNECTED"].stringValue
                        
                        self.defaults.setValue(self.isProxyUser, forKey:"isProxyUser")
                        
                        if(isConnected == "true"){
                        if(self.isProxyUser == "true"){
                            
                            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("idMedicationList") as! MedicationList
                            
                            self.addChildViewController(vc)
                            vc.view.frame = self.container.frame
                            self.container.addSubview(vc.view)
                            vc.didMoveToParentViewController(self)
                        }else{
                                self.LogoutForProxy()
                        }
                    
                        }else{
                        
                        
                            self.LogoutForProxy()
                        }
                    }
                }
        }
        
       
        
    }
    
    @IBAction func btnSegmentEvent(sender: AnyObject) {
        if(seg1.selectedIndex == 0)
        {
            processBar.hidden = false
            self.nullMessage.hidden = true
            segIndex.tag = 0
            getAllMedication("true")
            medStatus = "(Active) \n"
           
        }else
        {
            processBar.hidden = false
            self.nullMessage.hidden = true
            segIndex.tag = 1
            getAllMedication("false")
            medStatus = "(Inactive) \n"
            
        }
    }
    func showMessage(sender: UIButton)
    {
        let indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        
        var cell:activInactiveMedicationCell = activInactiveMedicationCell()
        cell = self.tblMedicationlist.cellForRowAtIndexPath(indexPath) as! activInactiveMedicationCell
        
      let style = NSMutableParagraphStyle()
        style.lineBreakMode = NSLineBreakMode.ByWordWrapping
        let size = arrMedicationName[indexPath.row].sizeWithAttributes([NSFontAttributeName: cell.lblMedicationName.font, NSParagraphStyleAttributeName: style])
        let textWidth = size.width
        
        if(textWidth > cell.lblMedicationName.frame.size.width)
        {
            /*self.HUD.customView = UIImageView(image: UIImage(named:""))
            self.HUD.mode = MBProgressHUDMode.CustomView
            self.HUD.detailsLabelText = self.arrMedicationName[sender.tag]
            self.view.addSubview(self.HUD)
            self.HUD.show(true)
            self.HUD.hide(true, afterDelay: 4)*/
            let height = UIScreen.mainScreen().bounds.size.height
            let alertController = UIAlertController(title: "", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
            alertController.setValue(setMutableSringTitle(arrMedicationName[sender.tag], str2: "", width1: height/40, width2: height/40), forKey: "attributedTitle")
            let EditButton = UIAlertAction(title: "Close", style: .Cancel) { (action) -> Void in
               }
            
            alertController.addAction(EditButton)
            alertController.popoverPresentationController?.sourceView = cell.contentView
            alertController.popoverPresentationController?.sourceRect = cell.contentView.frame
            
        presentViewController(alertController, animated: true, completion: nil)
        }
        
       
        
        
    }
    func showInformation(sender: AnyObject)
    {
        let indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        
        var cell:activInactiveMedicationCell = activInactiveMedicationCell()
        cell = self.tblMedicationlist.cellForRowAtIndexPath(indexPath) as! activInactiveMedicationCell
       
        var height = UIScreen.mainScreen().bounds.size.height
        var width = UIScreen.mainScreen().bounds.size.width
        if(height == 1024)
        {
            height = 736
            width  = 414
        }
        
        var arr1:[String] = []
        var arr2:[String] = []
        var notes = "None"
        if(arrMedicationNotes[sender.tag] != "")
        {
            notes = arrMedicationNotes[sender.tag]
        }
        let myMutableString = NSMutableAttributedString()
        if(arrSymptomName[sender.tag] == "N/A")
        {
            if(arrEditedOn[sender.tag] != arrCreatedon[sender.tag])
            {
                arr1 = ["Dosage","Frequency","Route","Type","Notes","Created on","Edited on"]
                arr2 = [arrDosageName[sender.tag],arrFrequencyName[sender.tag],arrRouteName[sender.tag],arrTypeName[sender.tag],notes,arrCreatedon[sender.tag],arrEditedOn[sender.tag]]
            }else
            {
                arr1 = ["Dosage","Frequency","Route","Type","Notes","Created on"]
                arr2 = [arrDosageName[sender.tag],arrFrequencyName[sender.tag],arrRouteName[sender.tag],arrTypeName[sender.tag],notes,arrCreatedon[sender.tag]]
            }
        }else
        {
            if(arrEditedOn[sender.tag] != arrCreatedon[sender.tag])
            {
                arr1 = ["Dosage","Frequency","Symptom","Route","Type","Notes","Created on","Edited on"]
                arr2 = [arrDosageName[sender.tag],arrFrequencyName[sender.tag],arrSymptomName[sender.tag],arrRouteName[sender.tag],arrTypeName[sender.tag],notes,arrCreatedon[sender.tag],arrEditedOn[sender.tag]]
            }else
            {
                arr1 = ["Dosage","Frequency","Symptom","Route","Type","Notes","Created on"]
                arr2 = [arrDosageName[sender.tag],arrFrequencyName[sender.tag],arrSymptomName[sender.tag],arrRouteName[sender.tag],arrTypeName[sender.tag],notes,arrCreatedon[sender.tag]]
            }
        }
        
        for index in 0...arr1.count-1
        {
            myMutableString.appendAttributedString(setMutableSring(arr1[index]+": ", str2: arr2[index]+" \n"))
        }
        
        let alertView = CustomIOS7AlertView()
        
        // Set the button titles array
        alertView.buttonTitles = ["Close"]
        
        // Set a custom container view
        alertView.containerView1 = createContainerView(sender.tag,mutableString: myMutableString)
        
        alertView.delegate = self
        alertView.onButtonTouchUpInside = { (alertView: CustomIOS7AlertView, buttonIndex: Int) -> Void in
            
        }
        alertView.show()
        
    }
    func setMutableSringTitle(str1: NSString, str2:NSString, width1:CGFloat, width2:CGFloat) -> NSMutableAttributedString
    {
        var myMutableString = NSMutableAttributedString()
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.Left
        
        myMutableString = NSMutableAttributedString(string: (str1 as String) + (str2 as String), attributes: [NSFontAttributeName:UIFont(name: "Helvetica Neue", size: width1)!])
        
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGrayColor(), range: NSRange(location:str1.length, length:str2.length))
        
        myMutableString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica Neue", size: width2)!, range: NSRange(location: str1.length,length: str2.length))
        
        return myMutableString
        
    }
    func setMutableSring(str1: NSString, str2:NSString) -> NSMutableAttributedString
    {
        var myMutableString = NSMutableAttributedString()
        var height = UIScreen.mainScreen().bounds.size.height
        var width = UIScreen.mainScreen().bounds.size.width
        if(height == 1024)
        {
            height = 736
            width  = 414
        }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.Left
        
        myMutableString = NSMutableAttributedString(string: (str1 as String) + (str2 as String), attributes: [NSParagraphStyleAttributeName: paragraphStyle, NSFontAttributeName:UIFont(name: "Helvetica Neue", size: width/23)!])
        
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 20/255, green: 20/255, blue: 20/255, alpha: 1), range: NSRange(location:str1.length, length:str2.length))
        
        myMutableString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica Neue", size: width/25)!, range: NSRange(location: str1.length,length: str2.length))
        
        return myMutableString
        
    }
    func createContainerView(flag:Int,mutableString:NSMutableAttributedString) -> UIScrollView {
        var height = UIScreen.mainScreen().bounds.size.height
        var width = UIScreen.mainScreen().bounds.size.width
        
        if(height == 1024)
        {
            height = 736
            width  = 550
        }
        let composedMedNameSatus = arrMedicationName[flag] + "(Private)" + "(Inactive)"
        
        let size1 = composedMedNameSatus.boundingRectWithSize(
            CGSizeMake(width/1.07 - 5, CGFloat.infinity),
            options: NSStringDrawingOptions.UsesLineFragmentOrigin,
            attributes: [NSFontAttributeName: UIFont.systemFontOfSize(width/23)],
            context: nil).size
        
        let numberofline = size1.height / UIFont.systemFontOfSize(width/23).lineHeight
        
        let text = mutableString.string
        let size2 = text.boundingRectWithSize(
            CGSizeMake(width/1.07 - (width/75 * 2), CGFloat.infinity),
            options: NSStringDrawingOptions.UsesLineFragmentOrigin,
            attributes: [NSFontAttributeName: UIFont.systemFontOfSize(width/25)],
            context: nil).size
        let numberofline1 = size2.height / UIFont.systemFontOfSize(width/25).lineHeight
        
        let i1 = height/32 * numberofline
        var i2 = height/32 * numberofline1
        var i3:CGFloat = 0
        if(i2 > height*0.6)
        {
            i2 = height/33 * 15
        }
        if(height == 480)
        {
            i3 = (height/33)+15
        }
        let containerView = UIScrollView(frame: CGRectMake(0, 0, width/1.07, i1+i2+i3))
        
        let lblTitle = UILabel(frame: CGRectMake(width/73,height/83, containerView.frame.width - 5, height/33+i1))
        
        let lineView = UIView(frame: CGRectMake(0,height/33+i1, containerView.frame.width, 0.5))
        
        lineView.backgroundColor = UIColor.grayColor()
       
        let lblTitle1 = UITextView(frame: CGRectMake(width/75,height/33.35+i1, containerView.frame.width - (width/75 * 2), i2))
        
        lblTitle.attributedText = setMutableSring(arrMedicationName[flag] + " ", str2: medStatus)
        lblTitle1.attributedText = mutableString
        lblTitle.lineBreakMode = NSLineBreakMode.ByWordWrapping
        lblTitle.numberOfLines = Int(numberofline + 1)
        lblTitle1.editable = false
        
        containerView.addSubview(lblTitle1)
        containerView.addSubview(lineView)
        containerView.addSubview(lblTitle)
        
        containerView.backgroundColor = UIColor.whiteColor()
        containerView.layer.cornerRadius = 7
        
        return containerView
    }
    func customIOS7AlertViewButtonTouchUpInside(alertView: CustomIOS7AlertView, buttonIndex: Int) {
        alertView.close()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return arrMedicationName.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:activInactiveMedicationCell = activInactiveMedicationCell()
        cell = tblMedicationlist.dequeueReusableCellWithIdentifier("activeInactiveCell") as! activInactiveMedicationCell
        cell.lblMedicationName.text = arrMedicationName[indexPath.row]
        cell.btnInformation.tag = indexPath.row
        cell.btnShowFullName.tag = indexPath.row
        cell.btnInformation.addTarget(self, action: #selector(ActivInactiveMedication.showInformation(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        cell.btnShowFullName.addTarget(self, action: #selector(ActivInactiveMedication.showMessage(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        return cell
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //("click on row")
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   

}
