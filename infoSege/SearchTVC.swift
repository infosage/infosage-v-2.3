//
//  SearchTVC.swift
//  infoSage
//
//  Created by Vijay Mac on 18/04/16.
//  Copyright © 2016 Pramod shirsath. All rights reserved.
//

import UIKit

class SearchTVC: UITableViewCell {

    
    @IBOutlet var btnOpenLink: UIButton!
    @IBOutlet var lblSnippet: UITextView!
    @IBOutlet var lblDisplayLink: UILabel!
    
    @IBOutlet var btnTitle: UILabel!
    
    @IBOutlet var dataView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
