//
//  MedicationList.swift
//  infoSage
//
//  Created by Kunal MAC1 on 22/07/15.
//  Copyright (c) 2015 Pramod shirsath. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

extension NSObject {
    
    func callSelectorAsync(selector: Selector, object: AnyObject?, delay: NSTimeInterval) -> NSTimer {
        
        let timer = NSTimer.scheduledTimerWithTimeInterval(delay, target: self, selector: selector, userInfo: object, repeats: false)
        return timer
    }
    
    func callSelector(selector: Selector, object: AnyObject?, delay: NSTimeInterval) {
        
        let delay = delay * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        dispatch_after(time, dispatch_get_main_queue(), {
            NSThread.detachNewThreadSelector(selector, toTarget:self, withObject: object)
        })
    }
}

class MedicationList: UIViewController, CustomIOS7AlertViewDelegate, MBProgressHUDDelegate {
    
    @IBOutlet weak var tblMedicationlist: UITableView!
    @IBOutlet weak var container: UIView!
    
    var HUD:MBProgressHUD = MBProgressHUD()
    @IBOutlet weak var nullMessage: UILabel!
    @IBOutlet weak var processBar: UIActivityIndicatorView!
    
    @IBOutlet weak var btnAddNewMedication: UIButton!
    let defaults = NSUserDefaults.standardUserDefaults()
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var arrMedicationName:[String] = []
    var arrMedicationID:[String] = []
    var arrDosageName:[String] = []
    var arrFrequencyName:[String] = []
    var arrSymptomName:[String] = []
    var arrFrequencyObject:[AnyObject] = []
    var arrSymptomObject:[AnyObject] = []
    var arrRouteName:[String] = []
    var arrTypeName:[String] = []
    var arrMedicationNotes:[String] = []
    var arrCreatedon:[String] = []
    var arrEditedOn:[String] = []
    var arrActiveStatus: [String] = []
    var arrSharedStatus: [String] = []
    var arrDrugCode: [String] = []
    var arrUserObject: [AnyObject] = []
    var kID: String =  String()
    var userData:JSON = JSON("")
    var proxyUser: String =  String()
    var logUserId = ""
    var medStatus:NSString = ""
    var alertController = UIAlertController()
    let vc : personInformation = personInformation()
    var stringOne = ""
    
    var isMedicationVisible : String = String()
    
    var isProxyUser : String = String()
    
    
    var keystoneID: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("keyID")
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        stringOne = defaults.valueForKey("UID") as! String
        
        proxyUser = defaults.valueForKey("PROXY") as! String!
        
        kID =  keystoneID as! String
        
        if(proxyUser == "PROXY")
        {
            logUserId = keystoneID as! String
        }
        else
        {
            logUserId = defaults.valueForKey("UID") as! String
        }
        
        if(kID == "")
        {
            kID = logUserId
        }
        
        
        
        tblMedicationlist.layer.borderColor = UIColor.grayColor().CGColor;
        tblMedicationlist.layer.borderWidth = 0.5;
        tblMedicationlist.layer.cornerRadius = 5
        tblMedicationlist.tableFooterView = UIView(frame: CGRectZero)
        btnAddNewMedication.layer.cornerRadius = 5
        
        
    }
    override func viewWillAppear(animated: Bool) {
        if(arrMedicationID.count == 0 && self.nullMessage.text != "You have no recorded medications.")
        {
            getAllMedication()
        }
        
        let headers = appDelegate.headersAll
        
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL+"users/" + kID + "/profile", parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    
                    let json1 = JSON(response.result.value!)
                    
                    if(json1["userMessage"].stringValue == "You don't have permission to do that."){
                        //print("called from line :586")
                        self.LogoutForProxy()
                    }else{
                        
                        if(json1["user"]["isElder"].boolValue) {
                            
                            
                        }else{
                            print("called from todo line :287")
                            
                            self.LogoutForProxy()
                            //self.LogoutForKeystoneStatusChange()
                            
                        }
                        
                        
                    }
                    
                }
                //println(self.userData)
        }
        
        
        //Check participants user rights
        
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL + "users/" + kID + "/privileges/users/" + logUserId, parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
              response in
                if(response.result.isSuccess)
                {
                    var json1 = JSON(response.result.value!)
                    
                    
                    let count: Int? = json1.array?.count
                    if(count != 0)
                    {
                        
                       let isProxyInResponse = json1["PROXY"].stringValue
                       let isConnected = json1["CONNECTED"].stringValue
                        
                        if(isConnected == "false" )
                        {
                            self.LogoutForProxy()
                            
                        }
                        
                        
                            if(isProxyInResponse == "false" )
                            {
                                //print("Not Proxy Account")
                                
                                self.LogoutForProxy()
                                
                            }
                        
                        
                   }
                }
                
                
        }
        

    }
    
    func getAllMedication()
    {
        self.processBar.hidden = false
        arrMedicationID.removeAll(keepCapacity: true)
        arrMedicationName.removeAll(keepCapacity: true)
        arrDosageName.removeAll(keepCapacity: true)
        arrFrequencyName.removeAll(keepCapacity: true)
        arrTypeName.removeAll(keepCapacity: true)
        arrMedicationNotes.removeAll(keepCapacity: true)
        arrCreatedon.removeAll(keepCapacity: true)
        arrEditedOn.removeAll(keepCapacity: true)
        arrActiveStatus.removeAll(keepCapacity: true)
        arrSharedStatus.removeAll(keepCapacity: true)
        arrSymptomName.removeAll(keepCapacity: true)
        arrUserObject.removeAll(keepCapacity: true)
        arrDrugCode.removeAll(keepCapacity: true)
        
        let headers = appDelegate.headersAll
        
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL+"users/" + kID + "/drugImo", parameters: nil , headers: headers,encoding: .JSON)
            .responseJSON {
              response in
                if(response.result.isSuccess)
                {   let json1 = JSON(response.result.value!)
                    if(json1["userMessage"].stringValue != "You must be logged in to do that."){
                        let count: Int? = json1.array?.count
                        if(count != 0)
                        {
                            if let ct = count {
                                for index in 0...ct-1 {
                                    
                                    self.arrMedicationID.insert(json1[index]["id"].stringValue, atIndex: index)
                                    
                                    let paramsString = json1[index]["user"].rawString( NSUTF8StringEncoding)
                                    let str2 : NSCharacterSet = NSCharacterSet(charactersInString: "\\")
                                    var str3 = paramsString!.stringByTrimmingCharactersInSet(str2)
                                    
                                    self.userData = json1[index]["user"]
                                    self.arrUserObject.insert(json1[index]["user"].object, atIndex: index)
                                    
                                    self.arrActiveStatus.insert(json1[index]["isActive"].stringValue, atIndex: index)
                                    self.arrDrugCode.insert(json1[index]["drugImoCode"].stringValue, atIndex: index)
                                    var temp = json1[index]["isPublic"].stringValue
                                    ////println("**1\(temp)")
                                    self.arrSharedStatus.insert(json1[index]["isPublic"].stringValue, atIndex: index)
                                    
                                    self.arrMedicationName.insert(json1[index]["name"].stringValue, atIndex: index)
                                    self.arrDosageName.insert(json1[index]["dosage"].stringValue, atIndex: index)
                                    let paramsString1 = json1[index]["drugFrequency"].rawString(NSUTF8StringEncoding)
                                    let str4 : NSCharacterSet = NSCharacterSet(charactersInString: "\\")
                                    var str5 = paramsString1!.stringByTrimmingCharactersInSet(str4)
                                    self.arrFrequencyObject.insert(json1[index]["drugFrequency"].object, atIndex: index)
                                    self.arrFrequencyName.insert(json1[index]["drugFrequency"]["name"].stringValue, atIndex: index)
                                    let paramsString2 = json1[index]["drugSymptom"].rawString(NSUTF8StringEncoding)
                                    let str : NSCharacterSet = NSCharacterSet(charactersInString: "\\")
                                    var str1 = paramsString2!.stringByTrimmingCharactersInSet(str)
                                    self.arrSymptomObject.insert(json1[index]["drugSymptom"].object, atIndex: index)
                                    
                                    self.arrSymptomName.insert(json1[index]["drugSymptom"]["name"].stringValue, atIndex: index)
                                    
                                    self.arrTypeName.insert(json1[index]["type"].stringValue, atIndex: index)
                                    self.arrRouteName.insert(json1[index]["route"].stringValue, atIndex: index)
                                    self.arrMedicationNotes.insert(json1[index]["otherNotes"].stringValue, atIndex: index)
                                    
                                    let dateString:NSTimeInterval =  json1[index]["creationDate"].doubleValue / 1000
                                    let  dateformat : NSDateFormatter = NSDateFormatter()
                                    dateformat.setLocalizedDateFormatFromTemplate("EEE, dd MMM yyyy hh:mm a")
                                    
                                    let stringDate = dateformat.stringFromDate(NSDate(timeIntervalSince1970: dateString))
                                    
                                    self.arrCreatedon.insert(stringDate, atIndex: index)
                                    
                                    let dateString1:NSTimeInterval =  json1[index]["editDate"].doubleValue / 1000
                                    dateformat.setLocalizedDateFormatFromTemplate("EEE, dd MMM yyyy hh:mm a")
                                    
                                    let stringDate1 = dateformat.stringFromDate(NSDate(timeIntervalSince1970: dateString1))
                                    
                                    self.arrEditedOn.insert(stringDate1, atIndex: index)
                                }
                                
                                if(self.arrMedicationID.count == 0)
                                {
                                    self.nullMessage.hidden = false
                                    self.processBar.hidden = true
                                    self.nullMessage.text = "You have no recorded medications."
                                    
                                }
                                self.tblMedicationlist.reloadData()
                                self.processBar.hidden = true
                                
                            }
                            
                        }
                        else
                        {
                            self.nullMessage.hidden = false
                            self.processBar.hidden = true
                            self.nullMessage.text = "You have no recorded medications."
                            self.tblMedicationlist.reloadData()
                        }
                    }else
                    {
                        self.appDelegate.checkLogin()
                    }
                    if(json1["userMessage"].stringValue == "You don't have permission to do that."){
                        
                        self.processBar.hidden = true
                        self.LogoutForProxy()
                    }
                    
                    
                    
                }else{
                    self.processBar.hidden = true
                    self.appDelegate.networkConnError();
                }
        }
    }
    func LogoutForProxy()
    {
        let alert = UIAlertController(title: "Your privileges have been modified for your Keystone(s). You will be re-logged in now.", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:
            {
                (UIAlertAction)in Void.self
                
                var tokanid = self.defaults.valueForKey("deviceToken") as? String
                
                if(tokanid == nil){
                    tokanid = "h433897fg77873456fg868389"
                }
                
                //Clear defaults
                
                defer{
                self.appDelegate.checkLogin();
                }
                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idLoginViewController") as! ViewController
                self.navigationController?.pushViewController(nextView, animated: true)
                
                
        }))
        
//        alert.addAction(UIAlertAction (title: "Close", style: .Destructive, handler: { (UIAlertAction) in
//            
//        }))
        
        dispatch_async(dispatch_get_main_queue(), {
            // code here
            
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
        //print("Your Keystone status has changed. You will be re-logged in now.")
        
        
    }
    
    @IBAction func btnSendMedicatioForPrint(sender: AnyObject) {
        let alrtSendMedication = UIAlertView(title: "infoSAGE", message: "Send all medication to entered email address.", delegate: self, cancelButtonTitle: "Cancel")
        alrtSendMedication.addButtonWithTitle("Send")
        alrtSendMedication.alertViewStyle = UIAlertViewStyle.PlainTextInput
        let textField = alrtSendMedication.textFieldAtIndex(0)
        textField?.placeholder = "Email Address"
        textField?.keyboardType = UIKeyboardType.EmailAddress
        
        alrtSendMedication.show()
    }
    
    func alertView(View: UIAlertView!, clickedButtonAtIndex buttonIndex: Int)
    {
        if(buttonIndex > 0)
        {
            
        }
    }
    
    
    //MARK: Add/Update Medicatiobn
    
    @IBAction func btnAddNewMedication(sender: AnyObject) {
      
        self.defaults.setValue([], forKey:"AllForEditData" )
        
        
        let headers = appDelegate.headersAll
        
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL + "users/"+glob_keystoneID+"/privileges/users/" + glob_logUserID, parameters: nil, headers: headers, encoding: .JSON)
            .responseJSON {
               response in
                //print("Privilege @Home URL:\(request)")
                //print(result)
                if(response.result.isSuccess)
                {
                    
                    var json1 = JSON(response.result.value!)
                    
                    let count: Int? = json1.array?.count
                    if(count != 0)
                    {
                        self.isMedicationVisible = json1["VIEW_MEDICATIONS"].stringValue as String
                        self.isProxyUser = json1["PROXY"].stringValue as String
                        let isConnected = json1["CONNECTED"].stringValue
                        
                        
                        self.defaults.setValue(self.isProxyUser, forKey:"isProxyUser")
                        
                        if(isConnected == "true"){
                            
                            if(self.isProxyUser == "true"){
                                
                                //Go to add/edit medication
                                
                                let vc = self.storyboard?.instantiateViewControllerWithIdentifier("idAddNewMedication") as! AddNewMedicationViewController
                                
                                self.addChildViewController(vc)
                                vc.view.frame = self.container.frame
                                self.container.addSubview(vc.view)
                                vc.didMoveToParentViewController(self)
                            }else{
                                self.LogoutForProxy()
                            }
                        }else{
                            
                            self.LogoutForProxy()
                        }
                    }
                }
        }
        
        
    }
    
    func showInformation(sender: AnyObject)
    {
        medStatus = ""
        let indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        
        var cell:MedicationCell = MedicationCell()
        
        cell = self.tblMedicationlist.cellForRowAtIndexPath(indexPath) as! MedicationCell
        
        if(cell.btnShared.currentTitle == "No")
        {
            medStatus = "(Private)"
        }
        if(cell.btnActive.currentTitle == "No")
        {
            medStatus = (medStatus as String) + " " +  "(Inactive)"
        }
        
        var height = UIScreen.mainScreen().bounds.size.height
        var width = UIScreen.mainScreen().bounds.size.width
        
        if(height == 1024)
        {
            height = 736
            width  = 414
        }
        
        var arr1:[String] = []
        var arr2:[String] = []
        var notes = "None"
        
        if(arrMedicationNotes[sender.tag] != "")
        {
            notes = arrMedicationNotes[sender.tag]
        }
        
        let myMutableString = NSMutableAttributedString()
        if(arrSymptomName[sender.tag] == "N/A")
        {
            if(arrEditedOn[sender.tag] != arrCreatedon[sender.tag])
            {
                arr1 = ["Dosage","Frequency","Route","Type","Notes","Created on","Edited on"]
                arr2 = [arrDosageName[sender.tag],arrFrequencyName[sender.tag],arrRouteName[sender.tag],arrTypeName[sender.tag],notes,arrCreatedon[sender.tag],arrEditedOn[sender.tag]]
            }else
            {
                arr1 = ["Dosage","Frequency","Route","Type","Notes","Created on"]
                arr2 = [arrDosageName[sender.tag],arrFrequencyName[sender.tag],arrRouteName[sender.tag],arrTypeName[sender.tag],notes,arrCreatedon[sender.tag]]
            }
        }else
        {
            if(arrEditedOn[sender.tag] != arrCreatedon[sender.tag])
            {
                arr1 = ["Dosage","Frequency","Symptom","Route","Type","Notes","Created on","Edited on"]
                arr2 = [arrDosageName[sender.tag],arrFrequencyName[sender.tag],arrSymptomName[sender.tag],arrRouteName[sender.tag],arrTypeName[sender.tag],notes,arrCreatedon[sender.tag],arrEditedOn[sender.tag]]
            }else
            {
                arr1 = ["Dosage","Frequency","Symptom","Route","Type","Notes","Created on"]
                arr2 = [arrDosageName[sender.tag],arrFrequencyName[sender.tag],arrSymptomName[sender.tag],arrRouteName[sender.tag],arrTypeName[sender.tag],notes,arrCreatedon[sender.tag]]
            }
        }
        
        for index in 0...arr1.count-1
        {
            myMutableString.appendAttributedString(setMutableSring(arr1[index]+": ", str2: arr2[index]+" \n"))
        }
        
        let alertView = CustomIOS7AlertView()
        
        // Set the button titles array
        alertView.buttonTitles = ["Close"]
        
        // Set a custom container view
        alertView.containerView1 = createContainerView(sender.tag,mutableString: myMutableString)
        
        alertView.delegate = self
        alertView.onButtonTouchUpInside = { (alertView: CustomIOS7AlertView, buttonIndex: Int) -> Void in
            
        }
        alertView.show()
        
        /* let alert = UIAlertController(title: "", message: "",  preferredStyle: .Alert)
         alert.setValue(myMutableString, forKey: "attributedMessage")
         alert.setValue(setMutableSringTitle(arrMedicationName[sender.tag]+" ", str2: medStatus, width1: width/22, width2:width/25), forKey: "attributedTitle")
         
         let cancelAction = UIAlertAction(title: "Close",
         style: .Default) { (action: UIAlertAction!) -> Void in
         }
         
         alert.addAction(cancelAction)
         presentViewController(alert,
         animated: true,
         completion: nil)*/
    }
    func setMutableSringTitle(str1: NSString, str2:NSString, width1:CGFloat, width2:CGFloat) -> NSMutableAttributedString
    {
        var myMutableString = NSMutableAttributedString()
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.Left
        
        myMutableString = NSMutableAttributedString(string: (str1 as String) + (str2 as String), attributes: [NSFontAttributeName:UIFont(name: "Helvetica Neue", size: width1)!])
        
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor.darkGrayColor(), range: NSRange(location:str1.length, length:str2.length))
        
        myMutableString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica Neue", size: width2)!, range: NSRange(location: str1.length,length: str2.length))
        
        return myMutableString
        
    }
    func setMutableSring(str1: NSString, str2:NSString) -> NSMutableAttributedString
    {
        var myMutableString = NSMutableAttributedString()
        var height = UIScreen.mainScreen().bounds.size.height
        var width = UIScreen.mainScreen().bounds.size.width
        if(height == 1024)
        {
            height = 736
            width  = 414
        }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.Left
        
        myMutableString = NSMutableAttributedString(string: (str1 as String) + (str2 as String), attributes: [NSParagraphStyleAttributeName: paragraphStyle, NSFontAttributeName:UIFont(name: "Helvetica Neue", size: width/23)!])
        
        myMutableString.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 20/255, green: 20/255, blue: 20/255, alpha: 1), range: NSRange(location:str1.length, length:str2.length))
        
        myMutableString.addAttribute(NSFontAttributeName, value: UIFont(name: "Helvetica Neue", size: width/25)!, range: NSRange(location: str1.length,length: str2.length))
        
        return myMutableString
        
    }
    func createContainerView(flag:Int,mutableString:NSMutableAttributedString) -> UIScrollView {
        
        var height = UIScreen.mainScreen().bounds.size.height
        var width = UIScreen.mainScreen().bounds.size.width
        
        if(height == 1024)
        {
            height = 736
            width  = 550
        }
        
        let composedMedNameSatus = arrMedicationName[flag] + "(Private)" + "(Inactive)"
        
        let size1 = composedMedNameSatus.boundingRectWithSize(
            CGSizeMake(width/1.07 - 5, CGFloat.infinity),
            options: NSStringDrawingOptions.UsesLineFragmentOrigin,
            attributes: [NSFontAttributeName: UIFont.systemFontOfSize(width/23)],
            context: nil).size
        
        let numberofline = size1.height / UIFont.systemFontOfSize(width/23).lineHeight
        
        let text = mutableString.string
        
        let size2 = text.boundingRectWithSize(
            CGSizeMake(width/1.07 - (width/75 * 2), CGFloat.infinity),
            options: NSStringDrawingOptions.UsesLineFragmentOrigin,
            attributes: [NSFontAttributeName: UIFont.systemFontOfSize(width/25)],
            context: nil).size
        let numberofline1 = size2.height / UIFont.systemFontOfSize(width/25).lineHeight
        
        let i1 = height/32 * numberofline
        var i2 = height/32 * numberofline1
        var i3:CGFloat = 0
        if(i2 > height*0.6)
        {
            i2 = height/33 * 15
        }
        if(height == 480)
        {
            i3 = (height/33)+15
        }
        let containerView = UIScrollView(frame: CGRectMake(0, 0, width/1.07, i1+i2+i3))
        
        let lblTitle = UILabel(frame: CGRectMake(width/73,height/83, containerView.frame.width - 5, height/33+i1))
        
        let lineView = UIView(frame: CGRectMake(0,height/33+i1, containerView.frame.width, 0.5)) // CHNGE 33 ->> 26
        
        lineView.backgroundColor = UIColor.grayColor()
        
        let lblTitle1 = UITextView(frame: CGRectMake(width/75,height/33.35+i1, containerView.frame.width - (width/75 * 2), i2)) // CHANGE 33.35 -> 25
        
        lblTitle.attributedText = setMutableSring(arrMedicationName[flag] + " ", str2: medStatus)
        lblTitle.lineBreakMode = NSLineBreakMode.ByWordWrapping
        lblTitle.numberOfLines = Int(numberofline + 1)
        
        lblTitle1.attributedText = mutableString
        lblTitle1.editable = false
        
        containerView.addSubview(lblTitle1) // med Data
        containerView.addSubview(lineView)  // Line separator
        containerView.addSubview(lblTitle) // Med Name
        
        containerView.backgroundColor = UIColor.whiteColor()
        containerView.layer.cornerRadius = 7
        
        return containerView
    }
    
    func customIOS7AlertViewButtonTouchUpInside(alertView: CustomIOS7AlertView, buttonIndex: Int) {
        alertView.close()
    }
    func changeStatus(status:String, index:Int, flag:Int)
    {
        let indexPath = NSIndexPath(forRow: index, inSection: 0)
        
        var cell:MedicationCell = MedicationCell()
        cell = self.tblMedicationlist.cellForRowAtIndexPath(indexPath) as! MedicationCell
        if(self.proxyUser == "PROXY")
        {
            
            self.appDelegate.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
        }
        
        let headers = appDelegate.headersAll
        
        Alamofire.Manager.sharedInstance
            .request(.PUT, appDelegate.baseURL+"users/" + kID + "/drugImo/" + self.arrMedicationID[index] + "/" + status, parameters: nil ,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                ////println(response)
                if(response.result.isSuccess)
                {
                    if(response.response?.statusCode == 200){
                    if(status == "active")
                    {
                        if(flag == 0)
                        {
                            cell.proForActive.hidden = true
                            self.arrActiveStatus[index] = "false"
                            cell.btnActive.setTitle("No", forState:  UIControlState.Normal)
                        }
                        else
                        {
                            cell.proForActive.hidden = true
                            self.arrActiveStatus[index] = "true"
                            cell.btnActive.setTitle("Yes", forState:  UIControlState.Normal)
                        }
                    }
                    else
                    {
                        if(flag == 0)
                        {
                            cell.proForShare.hidden = true
                            self.arrSharedStatus[index] = "false"
                            cell.btnShared.setTitle("No", forState:  UIControlState.Normal)
                        }
                        else
                        {
                            cell.proForShare.hidden = true
                            self.arrSharedStatus[index] = "true"
                            cell.btnShared.setTitle("Yes", forState:  UIControlState.Normal)
                        }
                    }
                    }else if(response.response?.statusCode == 403){
                    
                     self.LogoutForProxy()
                    }
                        
                }else{
                    self.processBar.hidden = true
                    self.appDelegate.networkConnError();
                }
        }
        
    }
    
    func changeActive(sender: UIButton)
    {
        //processBar.hidden = false
        let indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        
        var cell:MedicationCell = MedicationCell()
        cell = self.tblMedicationlist.cellForRowAtIndexPath(indexPath) as! MedicationCell
        if(cell.btnActive.currentTitle == "Yes")
        {
            cell.btnActive.setTitle("", forState:  UIControlState.Normal)
            cell.proForActive.hidden = false
            changeStatus("active",index: sender.tag,flag: 0)
        }else
        {
            cell.btnActive.setTitle("", forState:  UIControlState.Normal)
            cell.proForActive.hidden = false
            changeStatus("active",index: sender.tag,flag: 1)
        }
    }
    func changeShare(sender: UIButton)
    {
        //processBar.hidden = false
        let indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        
        var cell:MedicationCell = MedicationCell()
        cell = self.tblMedicationlist.cellForRowAtIndexPath(indexPath) as! MedicationCell
        if(cell.btnShared.currentTitle == "Yes")
        {
            cell.btnShared.setTitle("", forState:  UIControlState.Normal)
            cell.proForShare.hidden = false
            changeStatus("public",index: sender.tag,flag: 0)
        }else
        {
            cell.btnShared.setTitle("", forState:  UIControlState.Normal)
            cell.proForShare.hidden = false
            changeStatus("public",index: sender.tag,flag: 1)
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMedicationName.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:MedicationCell = MedicationCell()
        cell = tblMedicationlist.dequeueReusableCellWithIdentifier("MedicationCell") as! MedicationCell
        cell.lblMedicationName.text = arrMedicationName[indexPath.row]
        cell.btnInformation.tag = indexPath.row
        cell.btnShared.tag = indexPath.row
        cell.btnActive.tag = indexPath.row
        cell.btnShowMessage.tag = indexPath.row
        cell.btnInformation.addTarget(self, action: #selector(MedicationList.showInformation(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        if(arrSharedStatus[indexPath.row] == "true"){
            cell.btnShared.setTitle("Yes", forState:  UIControlState.Normal)
        }else{
            cell.btnShared.setTitle("No", forState:  UIControlState.Normal)
        }
        if(arrActiveStatus[indexPath.row] == "true"){
            
            cell.btnActive.setTitle("Yes", forState:  UIControlState.Normal)
        }else{
            
            cell.btnActive.setTitle("No", forState:  UIControlState.Normal)
        }
        cell.btnShared.layer.borderColor = UIColor.grayColor().CGColor
        cell.btnShared.layer.borderWidth = 0.5
        cell.btnShared.layer.cornerRadius = 5
        cell.btnShared.layer.masksToBounds = true
        cell.btnActive.layer.borderColor = UIColor.grayColor().CGColor
        cell.btnActive.layer.borderWidth = 0.5
        cell.btnActive.layer.cornerRadius = 5
        cell.btnActive.layer.masksToBounds = true
        cell.btnShared.addTarget(self, action: #selector(MedicationList.changeShare(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        cell.btnActive.addTarget(self, action: #selector(MedicationList.changeActive(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        cell.btnShowMessage.addTarget(self, action: #selector(MedicationList.showMessage(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        return cell
        
    }
    func showMessage(sender: UIButton)
    {
        let indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        
        var cell:MedicationCell = MedicationCell()
        cell = self.tblMedicationlist.cellForRowAtIndexPath(indexPath) as! MedicationCell
        
        /* let style = NSMutableParagraphStyle()
         style.lineBreakMode = NSLineBreakMode.ByWordWrapping
         let size = arrMedicationName[indexPath.row].sizeWithAttributes([NSFontAttributeName: cell.lblMedicationName.font, NSParagraphStyleAttributeName: style])
         var textWidth = size.width
         
         if(textWidth > cell.lblMedicationName.frame.size.width)
         {
         self.HUD.customView = UIImageView(image: UIImage(named:""))
         self.HUD.mode = MBProgressHUDMode.CustomView
         self.HUD.detailsLabelText = self.arrMedicationName[sender.tag]
         self.view.addSubview(self.HUD)
         self.HUD.show(true)
         self.HUD.hide(true, afterDelay: 4)
         }*/
        
        let height = UIScreen.mainScreen().bounds.size.height
        alertController = UIAlertController(title: "", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        alertController.setValue(setMutableSringTitle(arrMedicationName[sender.tag], str2: "", width1: height/40, width2: height/40), forKey: "attributedTitle")
        let closure = { (buttonIndex: Int) in
            {
                (action: UIAlertAction!) -> Void in
                
            }
        }
        let EditButton = UIAlertAction(title: "Edit", style: .Default) { (action) -> Void in
            self.getEditDate(sender.tag) }
        let buttonDelete = UIAlertAction(title: "Delete", style: .Destructive) { (action) -> Void in
            let alert = UIAlertController(title: "Confirm to Delete", message: "Are you sure that you want to delete this medication record?", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler:{(UIAlertAction)in Void.self
                self.tblMedicationlist.reloadData()}))
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler:{
                (UIAlertAction)in Void.self
                if(self.proxyUser == "PROXY")
                {
                    self.appDelegate.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
                    
                }
                let headers = self.appDelegate.headersAll
                Alamofire.Manager.sharedInstance
                    .request(.DELETE, self.appDelegate.baseURL+"users/" + self.kID + "/drugImo/" + self.arrMedicationID[sender.tag], parameters: nil , headers: headers, encoding: .JSON)
                    .responseJSON {
                    response in
                        
                        if(response.response?.statusCode == 403){
                            
                            self.LogoutForProxy()
                            
                        }else{
                        ////println(response)
                        
                        let image : UIImage = UIImage(named:"37x-Checkmark")!
                        self.HUD.customView = UIImageView(image: image)
                        
                        self.HUD.mode = MBProgressHUDMode.CustomView
                        self.HUD.detailsLabelText = self.arrMedicationName[sender.tag] + " has been deleted"
                        self.view.addSubview(self.HUD)
                        self.HUD.show(true)
                        self.HUD.hide(true, afterDelay: 3)
                        self.getAllMedication()
                        }
                }
            }))
            self.presentViewController(alert, animated: true, completion: { })
        }
        let CancelButton = UIAlertAction(title: "Cancel", style: .Cancel) { (action) -> Void in
        }
        alertController.addAction(EditButton)
        alertController.addAction(buttonDelete)
        alertController.addAction(CancelButton)
        
        alertController.popoverPresentationController?.sourceView = cell.contentView
        alertController.popoverPresentationController?.sourceRect = cell.contentView.frame
        
        presentViewController(alertController, animated: true, completion: nil)
        
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
    }
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]? {
        let delete = UITableViewRowAction(style: .Default, title: "") { action, index in
            let alert = UIAlertController(title: "Confirm to Delete", message: "Are you sure that you want to delete this medication record?", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler:{(UIAlertAction)in Void.self
                self.tblMedicationlist.reloadData()}))
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler:{
                (UIAlertAction)in Void.self
                if(self.proxyUser == "PROXY")
                {
                    //Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
                    
                    self.appDelegate.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
                }
                let headers = self.appDelegate.headersAll
                Alamofire.Manager.sharedInstance
                    .request(.DELETE, self.appDelegate.baseURL+"users/" + self.kID + "/drugImo/" + self.arrMedicationID[indexPath.row], parameters: nil , headers: headers, encoding: .JSON)
                    .responseJSON {
                        response in
                        ////println(response)
                        let image : UIImage = UIImage(named:"37x-Checkmark")!
                        self.HUD.customView = UIImageView(image: image)
                        
                        self.HUD.mode = MBProgressHUDMode.CustomView
                        self.HUD.detailsLabelText = self.arrMedicationName[indexPath.row] + " has been deleted"
                        self.view.addSubview(self.HUD)
                        self.HUD.show(true)
                        self.HUD.hide(true, afterDelay: 3)
                        self.getAllMedication()
                }
            }))
            self.presentViewController(alert, animated: true, completion: { })
        }
        let done = UITableViewRowAction(style: .Default, title: "") { action, index in
            
            self.getEditDate(indexPath.row)
        }
        //done.backgroundColor = UIColor.whiteColor()
        //var isSelected = UIImage(named: "check40")!
        delete.backgroundColor = UIColor(patternImage: UIImage(named: "deleteMed")!)
        done.backgroundColor = UIColor(patternImage: UIImage(named: "editMed")!)
        //done.setImage(self.isSelected, forState: UIControlState.Normal)
        
        return [delete, done]
    }
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        return false
    }
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    func getEditDate(index:Int)
    {
        let headers = appDelegate.headersAll
        
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL + "users/"+glob_keystoneID+"/privileges/users/" + glob_logUserID, parameters: nil, headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                //print("Privilege @Home URL:\(request)")
                //print(result)
                if(response.result.isSuccess)
                {
                    
                    var json1 = JSON(response.result.value!)
                    
                    let count: Int? = json1.array?.count
                    if(count != 0)
                    {
                        self.isMedicationVisible = json1["VIEW_MEDICATIONS"].stringValue as String
                        self.isProxyUser = json1["PROXY"].stringValue as String
                        let isConnected = json1["CONNECTED"].stringValue
                        
                        
                        self.defaults.setValue(self.isProxyUser, forKey:"isProxyUser")
                        
                        if(isConnected == "true"){
                            
                            if(self.isProxyUser == "true"){
                                
                                //Go to add/edit medication
                                
                                let myData = NSKeyedArchiver.archivedDataWithRootObject(self.arrUserObject[index])
                                let freData = NSKeyedArchiver.archivedDataWithRootObject(self.arrFrequencyObject[index])
                                let SympData = NSKeyedArchiver.archivedDataWithRootObject(self.arrSymptomObject[index])
                                
                                let allArrayValues = [/*0*/self.arrMedicationID[index],/*1*/self.arrMedicationName[index],/*2*/self.arrRouteName[index],/*3*/self.arrTypeName[index],/*4*/self.arrDosageName[index],/*5*/self.arrFrequencyName[index],/*6*/self.arrSymptomName[index],/*7*/self.arrSharedStatus[index],/*8*/self.arrMedicationNotes[index],/*9*/myData,/*10*/self.arrDrugCode[index],/*11*/freData,/*12*/SympData]
                                self.defaults.setObject(allArrayValues, forKey:"AllForEditData" )
                                let vc = self.storyboard?.instantiateViewControllerWithIdentifier("idAddNewMedication") as! AddNewMedicationViewController
                                self.addChildViewController(vc)
                                vc.view.frame = self.container.frame
                                self.container.addSubview(vc.view)
                                vc.didMoveToParentViewController(self)

                            }else{
                                self.LogoutForProxy()
                            }
                        }else{
                            
                            self.LogoutForProxy()
                        }
                    }
                }
        }

        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

