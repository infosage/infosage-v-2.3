//
//  personInformation.swift
//  infoSege
//
//  Created by Pramod shirsath on 3/5/15.
//  Copyright (c) 2015 Pramod shirsath. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


extension String {
    func trunc(length: Int, trailing: String? = "") -> String {
        if self.characters.count > length {
            return self.substringToIndex(self.startIndex.advancedBy(length))
        } else {
            return self
        }
    }
}

class toDoList: UIViewController, UITableViewDataSource, UITableViewDelegate, SMSegmentViewDelegate,UITextViewDelegate, UITextFieldDelegate,MBProgressHUDDelegate,CustomIOS7AlertViewDelegate{
  
    //weak var delegate: CustomIOS7AlertViewDelegate?
    let delObj = UIApplication.sharedApplication().delegate as! AppDelegate
    @IBOutlet weak var btCancelUnassignTask: UIButton!
    @IBOutlet weak var lblTaskCount: UILabel!
    @IBOutlet weak var btUnassignTask: UIButton!
    @IBOutlet var mainSeg: UISegmentedControl!
    @IBOutlet var segmentView: SMSegmentView!
    @IBOutlet weak var addNewTaskView: UIView!
    
    @IBOutlet weak var tbUserName: UITableView!
    @IBOutlet weak var viewUserName: UIView!
    @IBOutlet weak var viewUserTable: UIView!
    @IBOutlet weak var processIndiactor: UIActivityIndicatorView!
    @IBOutlet weak var nullMessage: UILabel!
    @IBOutlet weak var txtFullMsg: UILabel!
    @IBOutlet weak var btTaskCountandDelete: UIButton!
    
    @IBOutlet weak var textViewDuration: UITextField!
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var txtAddNewTask: UITextField!
    @IBOutlet var tdSelectIcon: UIImageView!
    @IBOutlet var upSelectIcon: UIImageView!
    @IBOutlet weak var seg1: ADVSegmentedControl!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var todoListView1: UIView!
    @IBOutlet weak var toDolistTableCell: myTableViewCell!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet weak var btAddNewTask: UIButton!
    
    @IBOutlet  var txtHeaderText: UILabel!
    
    var HUD:MBProgressHUD = MBProgressHUD()
    @IBOutlet weak var newImgView: UIImageView!
    @IBOutlet weak var tbToDoLists: UITableView!
    
    var txtTaskDetail = UITextView()
    var otherNotesTextView = UITextView()
    var lblMessage = UILabel()
    var containerView = UIView(frame: CGRectMake(0, 0, 0, 0))
    var lblNotes = UILabel(frame: CGRectMake(0,0, 0, 0))
    var lblTitle = UILabel(frame: CGRectMake(0,0, 0, 0))
    
    let vc : personInformation = personInformation()
    
    // Arrays variable name  declare
    var arr:[String] = []
    var fbtArr:[String] = []
    var sbtArr:[String] = []
    var taskId:[String] = []
    var taskOtherNote:[String] = []
    var taskComplete:[String] = []
    var assignuserData:[AnyObject] = []
    var assignuserData1:[AnyObject] = []
    var oldTaskMsg = ""
    var otherNotes = ""
   // let delObj = UIApplication.sharedApplication().delegate as! AppDelegate
    //check participants rights variable declearation
    var CGDeleted = "false"
    var canUploadPhotos = "false"
    var isParticipant = "false"

    var isEditablePTS = ""
    var isCalenderPTS = ""
    var isProxyInResponse = ""
    var isConnected = ""
    var isMedicationVisible = ""
    
    var timeDuration:[String] = []
    var dateTimeArr:[String] = []
    var endDateTime:[String] = []
    var startDateTime:[String] = []
    var keystoneProPic:[String] = []
    var assignUserName:[String] = []
    var assignUserId:[String] = []
    var setAssignUserId:[String] = []
    var index1 = 0
    var actualPos = CGFloat()
    var tabPos = CGFloat()
    var locButton : NSIndexPath = NSIndexPath()
    var locButtonDate : NSIndexPath = NSIndexPath()
    var textFeild : UITextField = UITextField (frame:CGRectMake(0, 0, 10, 30));
    var indexPath :NSIndexPath!
    var userFirstName = ""
    var userDataAssign:JSON = JSON("")
    var userData:JSON = JSON("")
    var assignUser:JSON = JSON("")
    var indexC = 0
    var alUser = 0
    var editPos = 0
    var flagAlert = 0
    let alertView = CustomIOS7AlertView()
    var isSelected: UIImage = UIImage()
    var unselected: UIImage = UIImage()
    let defaults = NSUserDefaults.standardUserDefaults()
    var proxyIDForAnalytics = ""
    
    var stringOne = ""
    
    var name: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("name")
        }
    }
    var proPic: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("proPic")
        }
    }
    var keystoneID: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("keyID")
        }
    }
    var buttons = [
        "Cancel",
        "Edit"
    ]
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var kID: String =  String()
    var proxyUser: String =  String()
    var logUserId = ""
    
    //set as schedule, Volunteer and Assign button images---------------
    var bgAssign: UIImage = UIImage()
    var btScedule: UIImage = UIImage()
    var btVolunteer: UIImage = UIImage()
    //---------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tbToDoLists.tableFooterView = UIView()
    
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
           stringOne = defaults.valueForKey("UID") as! String
        
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: "methodOfReceivedNotification", name:"NotificationIdentifier", object: nil)
        
        btAddNewTask.layer.masksToBounds = true
        btAddNewTask.titleLabel!.textColor = UIColor.blueColor()
        btAddNewTask.setTitleColor(UIColor(red: 21/255, green: 120/255, blue: 217/255, alpha: 1.0), forState: UIControlState.Normal)
        //btAddNewTask.setTitleColor(UIColor.blueColor(), forState: UIControlState.Normal)
        alertView.delegate = self
        alertView.show()
        alertView.close()
        
        proxyUser = defaults.valueForKey("PROXY") as! String!
        kID =  keystoneID as! String
        logUserId = defaults.valueForKey("UID") as! String
        
        if(kID == logUserId)
        {
            proxyUser = "PROXY"
        }
        actualPos = self.tbToDoLists.contentOffset.y
        tabPos = self.secondView.frame.origin.y
        
        if(kID == "")
        {
            kID = logUserId
        }
        // if you login failed at this line, throwing any warning, reset content of your simulator
        
        let userLogObject = defaults.valueForKey("USEROBJECT") as! NSObject
        let freData: AnyObject =  NSKeyedUnarchiver.unarchiveObjectWithData(userLogObject as! NSData)!
        
     //  //print("user object is : \(freData)")
        
        self.userData = JSON(freData)
        if (appDelegate.height == 568 || appDelegate.height == 480) {
            isSelected = UIImage(named: "checked")!
            unselected = UIImage(named: "uncheck")!
        }
        else{
            isSelected = UIImage(named: "check_iPad")!
            unselected = UIImage(named: "uncheck_iPad")!
        }
        
        if(proxyUser == "PROXY")
        {
            delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
            self.proxyIDForAnalytics = self.kID
            
        }else{
            delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
        
            self.proxyIDForAnalytics = self.logUserId
        }
        //get Assignable users
        let headers = delObj.headersAll
        
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL+"users/" + kID + "/connections", parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    var json1 = JSON(response.result.value!)
                    
                    let count: Int? = json1.array?.count
                    var index1 = 0
                    if(count != 0)
                    {
                        self.userData = json1[0]["owner"]
                        if let ct = count {
                            for index in 0...ct-1 {
                                
                                if(json1[index]["subscribed"].stringValue == "false")
                                {
                                    let logID = self.defaults.valueForKey("UID") as? String
                                    self.assignUser = json1[index]
                                    
                                    ////println(self.assignUser)
                                    self.assignuserData.insert(self.assignUser.object , atIndex: index1)
                                    
                                    if(json1[index]["target"]["id"].stringValue == logID)
                                    {
                                        self.alUser = index1
                                    }
                                    self.assignUserName.insert(json1[index]["target"]["firstName"].stringValue + " " + json1[index]["target"]["lastName"].stringValue, atIndex: index1)
                                    self.assignUserId.insert(json1[index]["target"]["id"].stringValue, atIndex: index1)
                                    if(json1[index]["target"]["profileImage"]["image"]["path"].stringValue == "")
                                    {
                                        self.keystoneProPic.insert(self.appDelegate.baseImageURL + "w50xh50-2/images/default_profile_image.png", atIndex: index1)
                                        
                                    }else
                                    {
                                        let keyProPic = self.appDelegate.baseImageURL + "w50xh50-2/images/"+(json1[index]["target"]["profileImage"]["image"]["path"].stringValue)
                                        self.keystoneProPic.insert(keyProPic, atIndex: index1)
                                    }
                                    index1 += 1
                                }
                            }
                        }
                         self.tbUserName.reloadData()
                    }
                }else{
                    // self.appDelegate.networkConnError();
                }
                
        }
        
        Alamofire.Manager.sharedInstance
            .request(.GET, delObj.baseURL+"users/" + kID + "/profile", parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    
                    let json1 = JSON(response.result.value!)
                    
                    if(json1["userMessage"].stringValue == "You don't have permission to do that."){
                        //print("called from line :586")
                        self.LogoutForProxy()
                    }else{
                        
                        if(json1["user"]["isElder"].boolValue) {
                            
                            
                        }else{
                            print("called from todo line :287")
                            
                            //self.LogoutForProxy()
                            self.LogoutForKeystoneStatusChange()
                            
                        }
                        
                        
                    }
                    
                }
                //println(self.userData)
        }

        
        //Check participants user rights
        
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL + "users/" + kID + "/privileges/users/" + logUserId, parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    var json1 = JSON(response.result.value!)
                   
                    
                    let count: Int? = json1.array?.count
                    if(count != 0)
                    {
                        
                        self.isProxyInResponse = json1["PROXY"].stringValue
                        self.isEditablePTS = json1["EDIT_TASKS"].stringValue
                        self.isCalenderPTS = json1["VIEW_CALENDAR"].stringValue
                        self.isConnected = json1["CONNECTED"].stringValue
                        
                        
                        if(self.isConnected == "false" )
                        {
                           self.LogoutForProxy()
                            
                        }
                        
                         if(self.proxyUser == "PROXY")
                         {
                          if(self.isProxyInResponse == "false" )
                          {
                             //print("Not Proxy Account")
                            
                              self.LogoutForProxy()
                            
                          }
                         }
                        
                        if(self.isEditablePTS == "false")
                        {
                            self.isParticipant = "true"
                            self.btAddNewTask.enabled = false
                            self.btAddNewTask.alpha = 0.6
                        }
                        else{
                            self.isParticipant = "false"
                        }
                        
                        self.getTask(0)
                    }
                }
               
                
          }
        
        
        //custom created segment bar as per devise size
        
        if (appDelegate.height == 480) {
            self.bgAssign = UIImage(named: "assign.png")!
            self.btScedule = UIImage(named: "calendar-icon.png")!
            self.btVolunteer = UIImage(named: "hand-icon.png")!
            
            self.segmentView = SMSegmentView(frame: CGRect(x: 0, y: 0, width: 240, height: 29), separatorColour: UIColor(white: 0.95, alpha: 0.3), separatorWidth: 0.5,
                
                segmentProperties: [keySegmentTitleFont: UIFont.systemFontOfSize(11.0), keySegmentOnSelectionColour: UIColor.whiteColor(), keySegmentOffSelectionColour: UIColor(red: 229.0/255.0, green: 229.0/255.0, blue: 229.0/255.0, alpha: 1.0), keyContentVerticalMargin: 0.0, keySegmentOnSelectionTextColour:UIColor(red: 7.0/255.0, green: 58.0/255.0, blue: 79.0/255.0, alpha: 1.0)])
            
        }
        else if (appDelegate.height == 568) {
                self.bgAssign = UIImage(named: "assign.png")!
                self.btScedule = UIImage(named: "calendar-icon.png")!
                self.btVolunteer = UIImage(named: "hand-icon.png")!
                
                self.segmentView = SMSegmentView(frame: CGRect(x: 0, y: 0, width: 235, height: 29), separatorColour: UIColor(white: 0.95, alpha: 0.3), separatorWidth: 0.5,
                    
                    segmentProperties: [keySegmentTitleFont: UIFont.systemFontOfSize(11.0), keySegmentOnSelectionColour: UIColor.whiteColor(), keySegmentOffSelectionColour: UIColor(red: 229.0/255.0, green: 229.0/255.0, blue: 229.0/255.0, alpha: 1.0), keyContentVerticalMargin: 0.0, keySegmentOnSelectionTextColour:UIColor(red: 7.0/255.0, green: 58.0/255.0, blue: 79.0/255.0, alpha: 1.0)])
                
            }
            else if (appDelegate.height == 667) {
                bgAssign = UIImage(named: "assign-iphone6.png")!
                btScedule = UIImage(named: "calendar-icon-iphone6.png")!
                btVolunteer = UIImage(named: "hand-icon-iphone6.png")!
                
                self.segmentView = SMSegmentView(frame: CGRect(x: 0, y: 0, width: 280, height: 35), separatorColour: UIColor(white: 0.95, alpha: 0.3), separatorWidth: 0.5,
                    
                    segmentProperties: [keySegmentTitleFont: UIFont.systemFontOfSize(13.0), keySegmentOnSelectionColour: UIColor.whiteColor(), keySegmentOffSelectionColour: UIColor(red: 229.0/255.0, green: 229.0/255.0, blue: 229.0/255.0, alpha: 1.0), keyContentVerticalMargin: 0.0, keySegmentOnSelectionTextColour:UIColor(red: 7.0/255.0, green: 58.0/255.0, blue: 79.0/255.0, alpha: 1.0)])
                
            }
            else{  if (appDelegate.height == 1024) {
                bgAssign = UIImage(named: "assign-ipad.png")!
                btScedule = UIImage(named: "calendar-icon-ipad.png")!
                btVolunteer = UIImage(named: "hand-icon-ipad.png")!
            
            
                
                self.segmentView = SMSegmentView(frame: CGRect(x: 0, y: 0, width: 600, height: 50), separatorColour: UIColor(white: 0.95, alpha: 0.3), separatorWidth: 0.5,
                    
                    segmentProperties: [keySegmentTitleFont: UIFont.systemFontOfSize(21.0), keySegmentOnSelectionColour: UIColor.whiteColor(), keySegmentOffSelectionColour: UIColor(red: 229.0/255.0, green: 229.0/255.0, blue: 229.0/255.0, alpha: 1.0), keyContentVerticalMargin: 0.0, keySegmentOnSelectionTextColour:UIColor(red: 7.0/255.0, green: 58.0/255.0, blue: 79.0/255.0, alpha: 1.0)])
            }else{
                bgAssign = UIImage(named: "assign-iphone6.png")!
                btScedule = UIImage(named: "calendar-icon-iphone6.png")!
                btVolunteer = UIImage(named: "hand-icon-iphone6.png")!
                
                self.segmentView = SMSegmentView(frame: CGRect(x: 0, y: 0, width: 325, height: 35), separatorColour: UIColor(white: 0.95, alpha: 0.3), separatorWidth: 0.5,
                    
                    segmentProperties: [keySegmentTitleFont: UIFont.systemFontOfSize(13.0), keySegmentOnSelectionColour: UIColor.whiteColor(), keySegmentOffSelectionColour: UIColor(red: 229.0/255.0, green: 229.0/255.0, blue: 229.0/255.0, alpha: 1.0), keyContentVerticalMargin: 0.0, keySegmentOnSelectionTextColour:UIColor(red: 7.0/255.0, green: 58.0/255.0, blue: 79.0/255.0, alpha: 1.0)])
                }
                
        }
        
        if((glob_logUserID == glob_keystoneID) || (glob_viewAsProxy))
        {
              txtHeaderText.text = "Help List"
        }else{
            txtHeaderText.text = "Can You Help With This?"
        }
        // Add segments
        self.segmentView.addSegmentWithTitle("All", onSelectionImage: nil, offSelectionImage: nil)
        self.segmentView.addSegmentWithTitle("Unassigned", onSelectionImage: nil, offSelectionImage: nil)
        self.segmentView.addSegmentWithTitle("Upcoming", onSelectionImage: nil, offSelectionImage: nil)
        
        segmentView.selectSegmentAtIndex(0)
        secondView.addSubview(self.segmentView)
        
        self.segmentView.delegate = self
        
        //set as view border color and corner raddius -------------------
        self.segmentView.layer.cornerRadius = 5.0
        self.segmentView.layer.borderColor = UIColor.grayColor().CGColor;
        self.segmentView.layer.borderWidth = 0.5
        
        self.datePickerView.layer.cornerRadius = 5.0
        self.datePickerView.layer.borderColor = UIColor.grayColor().CGColor;
        self.datePickerView.layer.borderWidth = 0.3
        
        btTaskCountandDelete.layer.cornerRadius = 5.0
        btTaskCountandDelete.layer.borderColor = UIColor.grayColor().CGColor;
        btTaskCountandDelete.layer.borderWidth = 0.3
        
        self.viewUserTable.layer.cornerRadius = 5.0
        self.viewUserTable.layer.borderColor = UIColor.grayColor().CGColor;
        self.viewUserTable.layer.borderWidth = 0.5
        
        datePicker.backgroundColor = UIColor.whiteColor()
        
        self.txtAddNewTask.layer.cornerRadius = 5
        self.txtAddNewTask.layer.borderColor = UIColor.grayColor().CGColor;
        self.txtAddNewTask.layer.borderWidth = 0.5;
        
        tbToDoLists.layer.borderColor = UIColor.grayColor().CGColor;
        tbToDoLists.layer.borderWidth = 0.5;
        tbToDoLists.layer.cornerRadius = 5
        //----------------------------------------------------------------------
        
        //call func for get all keystone data(tasks)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(toDoList.handleSingleTap(_:)))
        tapRecognizer.numberOfTapsRequired = 1
        //self.tbToDoLists.addGestureRecognizer(tapRecognizer)
        
        
        //Analytics
        
        
        //let deviceType = self.defaults.valueForKey("deviceType") as! String
        
        let device = UIDevice.currentDevice().name
        let screenSize = self.defaults.valueForKey("screenSize") as! String
        let OS = self.defaults.valueForKey("OS") as! String
        
        let alertController = UIAlertController(title: "Device Info", message: "\(UIDevice.currentDevice().name)  \(screenSize) \(OS)  @home", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
        
        //self.presentViewController(alertController, animated: true, completion: nil)
        
        let parameters = ["string":"tasks"]
        
        var url = self.delObj.baseURL+"users/\(logUserId)/elasticLog/pageview/proxiedUser/\(proxyIDForAnalytics)/mobile?operatingSystem=\(OS)&deviceType=\(device)&screenSize=\(screenSize)"
        url = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
       //print("\(parameters) \(url)")
       //print("user \(stringOne)")
        
        Alamofire.request(.POST, url,parameters: parameters,headers: headers,encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                   //print(response?.statusCode)
                   //print("Success")
                    
                }else if(response.result.isFailure){
                    
                   //print(response?.statusCode)
                   //print("failure")
                    
                }else{
                    
                    self.delObj.networkConnError();
                    
                }
        }
    }
    
    func LogoutForProxy()
    {
        let alert = UIAlertController(title: "Your privileges have been modified for your Keystone(s). You will be re-logged in now.", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:
            {
                (UIAlertAction)in Void.self
                
                var tokanid = self.defaults.valueForKey("deviceToken") as? String
                
                if(tokanid == nil){
                    tokanid = "h433897fg77873456fg868389"
                }
                
                //Clear defaults
                
               self.delObj.checkLogin()
                
                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idLoginViewController") as! ViewController
                self.navigationController?.pushViewController(nextView, animated: true)
                
                
        }))
        
//        alert.addAction(UIAlertAction (title: "Close", style: .Destructive, handler: { (UIAlertAction) in
//            
//        }))
        
        dispatch_async(dispatch_get_main_queue(), {
            // code here
            
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
        //print("Your Keystone status has changed. You will be re-logged in now.")
        
        
    }
    
    func LogoutForKeystoneStatusChange()
    {
        
        let alert = UIAlertController(title: "Your Keystone status has changed. You will be re-logged in now.", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:
            {
                (UIAlertAction)in Void.self
                
                var tokanid = self.defaults.valueForKey("deviceToken") as? String
                
                if(tokanid == nil){
                    tokanid = "h433897fg77873456fg868389"
                }
                
                
                self.delObj.checkLogin();
                
                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idLoginViewController") as! ViewController
                self.navigationController?.pushViewController(nextView, animated: true)
                
                
        }))
        
        dispatch_async(dispatch_get_main_queue(), {
            
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
        
    }


    
    func handleSingleTap(recognizer: UITapGestureRecognizer) {
        txtAddNewTask.layer.borderColor = UIColor.grayColor().CGColor;
        HUD.removeFromSuperViewOnHide = true
        self.view.endEditing(true)
    }
    
    func textViewShouldReturn(textView: UITextView!) -> Bool {
        self.view.endEditing(true);
        
        return false;
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool{
        txtAddNewTask.layer.borderColor = UIColor(red: 0.1, green: 0.4, blue: 0.6, alpha: 1.0).CGColor;
        //self.btAddNewTask.hidden = true
        return true
    }
    func textFieldDidEndEditing(textField: UITextField) {
        txtAddNewTask.layer.borderColor = UIColor.grayColor().CGColor;
        self.view.endEditing(true);
        
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        addNewORUpdateTask()
        return false
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let subString: NSString = NSString(string: textViewDuration.text!)
        if(subString.length == 4)
        {
            textViewDuration.deleteBackward()
        }
        return true
    }
    
    // set date as scedule-----------------------------------------------
    
    func datePickerChanged(datePicker:UIDatePicker) {
        
        let dateFormatter = NSDateFormatter();
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
        let strDate = dateFormatter.stringFromDate(datePicker.date)
        
        let date : NSDate = datePicker.date
        let enddateFmt = NSDateFormatter();
        enddateFmt.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let duration = Float(textViewDuration.text!)! * 60
        //var duration = (textViewDuration.text as? String).floatValue * 60  //.toInt()! * 60
        
        let dur:NSTimeInterval = NSTimeInterval(duration)
        let dt: NSDate = date.dateByAddingTimeInterval(dur)
        let endDate = dateFormatter.stringFromDate(dt)
        
        let parameters = ["name": arr[locButtonDate.row],
            "startDate": strDate,
            "endDate": endDate,
             "complete": "false",
            //> Change> "complete": taskComplete[locButtonDate.row],
            "assignedUser": assignuserData1[locButtonDate.row]
            
        ];
        
        ////println("user data : \(assignuserData1[locButtonDate.row])" )
        if(self.proxyUser == "PROXY")
        {
           
            delObj.headersAll =  ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
        }
        let headers = delObj.headersAll
        Alamofire.Manager.sharedInstance
            .request(.PUT, appDelegate.baseURL+"users/" + kID + "/tasks/"+taskId[locButtonDate.row], parameters: parameters,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    self.getTask(self.segmentView.indexOfSelectedSegment)
                }else
                {
                    self.appDelegate.networkConnError()
                }
        }
    }
    
    @IBAction func btSetDateDone(sender: AnyObject) {
        let str : NSCharacterSet = NSCharacterSet(charactersInString: " ")
        if(textViewDuration.text!.stringByTrimmingCharactersInSet(str) == "")
            //if(textViewDuration.text == "")
        {
            let alert = UIAlertController(title: "infoSAGE", message: "Please enter time duration!", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:handleCancel))
            self.presentViewController(alert, animated: true, completion: { })
            
        }else{
            self.datePickerChanged(datePicker)
            UIView.animateWithDuration(0.3, animations: {
                self.tbToDoLists.setContentOffset(CGPoint(x: 0,y: self.actualPos), animated: true)
                }, completion: nil)
            self.datePickerView.hidden = true
        }
    }
    
    @IBAction func cancelSetDate(sender: AnyObject) {
        self.datePickerView.hidden = true
        UIView.animateWithDuration(0.3, animations: {
            self.tbToDoLists.setContentOffset(CGPoint(x: 0,y: self.actualPos), animated: true)
            }, completion: nil)
    }
    
    //-------------------------------------------------------------------
    
    func configurationTextField(textField: UITextField!)
    {
        if let tField = textField {
            self.textFeild = tField
            self.textFeild.placeholder = "What needs to be done?"
        }
    }
    func handleCancel(alertView: UIAlertAction!){
    }
    
    // Delete all selected tasks(multiple Task)------
    @IBAction func deleteSelectedTask(sender: AnyObject) {

        if(!self.btTaskCountandDelete.hidden){
        
        let alert = UIAlertController(title: "Delete Tasks", message: "Are you sure that you want to delete all your completed tasks? They will be permanently removed from your Help List.", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler:{(UIAlertAction)in Void.self
        }))
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Destructive, handler:{
            (UIAlertAction)in Void.self
            
            self.deleteTask(self)
            alert.dismissViewControllerAnimated(true, completion: nil)
            
            
        }))
        self.presentViewController(alert, animated: true, completion: {})
        }
        
        
    }
    
    //Delete task
    
    func deleteTask(sender: AnyObject){
       
            if(self.index1 < self.arr.count)
            {
                if(self.taskComplete[self.index1] == "true" )
                {
                    if(self.proxyUser == "PROXY")
                    {
                        self.delObj.headersAll =  ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
                    }
                    
                    let headers = self.delObj.headersAll
                    Alamofire.Manager.sharedInstance
                        .request(.DELETE, self.appDelegate.baseURL+"users/" + self.kID + "/tasks/" + self.taskId[self.index1], parameters: nil,headers: headers, encoding: .JSON)
                        .responseJSON {
                            response in
                            
                            if(response.result.isSuccess)
                            {
                                self.index1 = self.index1 + 1
                                self.deleteTask(self)
                                
                                if(response.response?.statusCode == 403){
                                    
                                    self.LogoutForProxy()
                                    
                                }
                                
                            }else{self.appDelegate.networkConnError();}
                    }
                }else
                {
                    self.index1 = self.index1 + 1
                    self.deleteTask(self)}
            }else{
                self.index1 = 0
                self.getTask(self.segmentView.indexOfSelectedSegment)
            }
    }
    
    //selecting preference for get ALL/UNASSIGN/UPCOMING tasks
    
    func didSelectSegmentAtIndex(segmentIndex: Int) {
        self.txtFullMsg.hidden = true
        self.view.endEditing(true);
        self.getTask(segmentIndex)
    }
    
    func clearArray()
    {
        arr.removeAll(keepCapacity: false)
        fbtArr.removeAll(keepCapacity: false)
        taskOtherNote.removeAll(keepCapacity: false)
        sbtArr.removeAll(keepCapacity: false)
        taskId.removeAll(keepCapacity: false)
        taskComplete.removeAll(keepCapacity: false)
        tbToDoLists.reloadData()
        
    }
    
    //get all user/keystone data(task etc.)-----------------------------------
    func loadUserData(jsonData:JSON, nullMsg:String)
    {
        let count: Int? = jsonData.array?.count
        btTaskCountandDelete.hidden = true
        btTaskCountandDelete.userInteractionEnabled = false
        if(count != 0)
        {
            lblTaskCount.hidden = false
            if let ct = count {
                for index in 0...ct-1 {
                    
                    let logID = defaults.valueForKey("UID") as? String
                    
                    let userIsKeystone = jsonData[index]["user"]["isElder"].boolValue
                    
                    //print(userIsKeystone)
                    
                    if(!userIsKeystone){
                    
                        self.LogoutForKeystoneStatusChange()
                    }
                    
                    self.userDataAssign = jsonData[index]["assignedUser"]
                    self.assignuserData1.insert(self.userDataAssign.object , atIndex: index)
                    
                    self.timeDuration.insert(jsonData[index]["duration"].stringValue , atIndex: index)
                    
                    self.arr.insert(jsonData[index]["name"].stringValue, atIndex: index)
                    self.taskOtherNote.insert(jsonData[index]["otherNotes"].stringValue, atIndex: index)
                    self.taskId.insert(jsonData[index]["id"].stringValue, atIndex: index)
                    if(jsonData[index]["complete"].stringValue == "")
                    {
                        self.taskComplete.insert("false", atIndex: index)
                    }else{
                        self.taskComplete.insert(jsonData[index]["complete"].stringValue, atIndex: index)
                    }
                    if(jsonData[index]["complete"].stringValue == "true")
                    { indexC = indexC + 1
                    }
                    self.setAssignUserId.insert(jsonData[index]["assignedUser"]["target"]["id"].stringValue, atIndex: index)
                    if(jsonData[index]["assignedUser"]["target"]["firstName"].stringValue=="")
                    {
                        if(proxyUser == "PROXY" || logID == kID)
                        {
                            self.fbtArr.insert("Assign", atIndex: index)
                        }else
                        {
                            self.fbtArr.insert("Volunteer", atIndex: index)
                        }
                    }else{
                        
                        if(jsonData[index]["assignedUser"]["target"]["id"].stringValue == logID && proxyUser != "PROXY")
                        {
                            self.fbtArr.insert("Me", atIndex: index)
                        }else{
                            
                            let fName = jsonData[index]["assignedUser"]["target"]["firstName"].stringValue
                            let subString: NSString = NSString(string:  fName)
                            if(subString.length > 3)
                            {
                                let indexx  = fName.startIndex.advancedBy(4)
                                let iString = fName.substringToIndex(indexx)
                                self.fbtArr.insert(iString, atIndex: index)
                            }else{
                                self.fbtArr.insert(subString as String, atIndex: index)
                            }
                        }
                        
                    }
                    if(jsonData[index]["endDate"].stringValue=="")
                    {
                        self.startDateTime.insert("", atIndex: index)
                        self.endDateTime.insert("", atIndex: index)
                        self.dateTimeArr.insert("", atIndex: index)
                        self.sbtArr.insert("Schedule", atIndex: index)
                        
                    }else{
                        
                        self.endDateTime.insert(jsonData[index]["endDate"].stringValue, atIndex: index)
                        self.startDateTime.insert(jsonData[index]["startDate"].stringValue, atIndex: index)
                        
                        let dateString:NSTimeInterval =  jsonData[index]["startDate"].doubleValue / 1000
                        
                        self.dateTimeArr.insert("\(dateString)", atIndex: index)
                        let  dateformat : NSDateFormatter = NSDateFormatter()
                        dateformat.setLocalizedDateFormatFromTemplate("MM/dd")
                        
                        let stringDate = dateformat.stringFromDate(NSDate(timeIntervalSince1970: dateString))
                        
                        self.sbtArr.insert(stringDate, atIndex: index)
                        
                        
                        let dateformat1 = NSDateFormatter()
                        dateformat1.timeStyle = .ShortStyle
                        dateformat1.dateStyle = .MediumStyle
                        
                        var stringDate1 = dateformat1.stringFromDate(NSDate(timeIntervalSince1970: dateString))
                        
                    } }
                
                lblTaskCount.text = "\(arr.count - indexC) of \(arr.count) remaining"
                
                if(indexC > 0){
                    if(self.isEditablePTS != "false"){
                        btTaskCountandDelete.hidden = false
                        btTaskCountandDelete.userInteractionEnabled = true
                         btTaskCountandDelete.setTitle("Delete \(indexC) Items.", forState: UIControlState.Normal)
                        if(indexC == 1){
                            
                             btTaskCountandDelete.setTitle("Delete \(indexC) Item.", forState: UIControlState.Normal)
                        
                        }
                       }
                }else{ btTaskCountandDelete.hidden = true
                
                    btTaskCountandDelete.userInteractionEnabled = false
                }
                
                  self.nullMessage.hidden = true
                self.tbToDoLists.reloadData()
            }
        }else{
            self.tbToDoLists.reloadData()
            self.nullMessage.hidden = false
            lblTaskCount.hidden = true
            self.nullMessage.text = nullMsg
        }
        
        self.processIndiactor.hidden = true
    }
    
    func getTask(segIndex:Int)
    {
        
        processIndiactor.hidden = false
        self.indexC = 0
        clearArray()
        self.nullMessage.hidden = true
        switch(segIndex)
        {
        case 0:
            let headers = delObj.headersAll
            
            Alamofire.Manager.sharedInstance
                .request(.GET, appDelegate.baseURL+"users/" + kID + "/tasks?mode=ALL", parameters: nil , headers: headers,encoding: .JSON)
                .responseJSON {
                   response in
                    if(response.result.isSuccess)
                    {
                        let json1 = JSON(response.result.value!)
                        
                        if(json1["userMessage"].stringValue != "You must be logged in to do that."){
                            self.clearArray()
                            
                            //print(json1)
                            
                            self.loadUserData(json1,nullMsg: "There are no tasks")
                        }else
                        {
                            self.appDelegate.checkLogin()
                        }
                        if(json1["userMessage"].stringValue == "You don't have permission to do that."){
                            
                            self.processIndiactor.hidden = true
                            self.LogoutForProxy()
                        }
                        
                        
                    }else{
                        
                        self.processIndiactor.hidden = true
                        self.appDelegate.networkConnError();
                    }
            }
            break
        case 1:
            let headers = delObj.headersAll
            Alamofire.Manager.sharedInstance
                .request(.GET, appDelegate.baseURL+"users/" + kID + "/tasks?mode=UNASSIGNED", parameters: nil ,headers: headers, encoding: .JSON)
                .responseJSON {
                    response in
                    if(response.result.isSuccess)
                    {
                        self.clearArray()
                        let json1 = JSON(response.result.value!)
                        if(json1["userMessage"].stringValue != "You must be logged in to do that."){
                            self.loadUserData(json1,nullMsg: "There are no UNASSIGNED tasks")
                        }else
                        {
                            self.appDelegate.checkLogin()
                        }
                        
                        if(json1["userMessage"].stringValue == "You don't have permission to do that."){
                            
                            self.processIndiactor.hidden = true
                            self.LogoutForProxy()
                        }
                        
                    }else{
                        
                        self.processIndiactor.hidden = true
                        self.appDelegate.networkConnError();
                    }
            }
            break
        case 2:
            let headers = delObj.headersAll
            Alamofire.Manager.sharedInstance
                .request(.GET, appDelegate.baseURL+"users/" + kID + "/tasks?mode=UPCOMING", parameters: nil ,headers: headers, encoding: .JSON)
                .responseJSON {
                    response in
                    if(response.result.isSuccess)
                    {
                        let json1 = JSON(response.result.value!)
                        
                        
                        if(json1["userMessage"].stringValue != "You must be logged in to do that."){
                            self.clearArray()
                            self.loadUserData(json1,nullMsg: "There are no UPCOMING tasks")
                        }else
                        {
                            self.appDelegate.checkLogin()
                        }
                        
                        if(json1["userMessage"].stringValue == "You don't have permission to do that."){
                            
                            self.processIndiactor.hidden = true
                            self.LogoutForProxy()
                        }
                        
                    }else{
                        
                        self.processIndiactor.hidden = true
                        self.appDelegate.networkConnError();
                    }
            }
            break
        default:
            break
            
        }
    }
    
    //-------------------------------------------------------------------------
    
    //check and uncheck complete task ------------------------------
    @IBAction func checkTaskIsComplete(sender: UIButton, event: UIEvent) {
        
        self.txtFullMsg.hidden = true
        
        
        //checkPrivileges()
        
        self.view.endEditing(true);
        var touch : UITouch;
        touch = event.allTouches()!.first! // as! UITouch
        var loc : CGPoint
        loc = touch.locationInView(self.tbToDoLists)
        
        if let indexPath :NSIndexPath = self.tbToDoLists.indexPathForRowAtPoint(loc){
            
            var cell:myTableViewCell = myTableViewCell()
            cell = self.tbToDoLists .cellForRowAtIndexPath(indexPath) as! myTableViewCell
            
            if(cell.btCheck.titleLabel?.text == "0")
            {
                
                self.taskComplete[indexPath.row] = "true"
                
                self.indexC = self.indexC + 1
                
                cell.btCheck.setTitle("1", forState:  UIControlState.Normal)
                cell.btCheck.setImage(self.isSelected, forState: UIControlState.Normal)
                let strikeThroughAttributes = [NSStrikethroughStyleAttributeName : 1]
                let strikeThroughString = NSAttributedString(string: self.arr[indexPath.row], attributes: strikeThroughAttributes)
                cell.txtLabel.attributedText = strikeThroughString
                cell.txtLabel.clipsToBounds = true
                
                lblTaskCount.text = "\(arr.count - indexC) of \(arr.count) remaining"
                if(self.isEditablePTS != "false"){
                    self.btTaskCountandDelete.hidden = false
                    self.btTaskCountandDelete.userInteractionEnabled = true
                    self.btTaskCountandDelete.setTitle("Delete \(self.indexC) Items.", forState: UIControlState.Normal)
                
                    if(indexC == 1){
                      self.btTaskCountandDelete.setTitle("Delete \(self.indexC) Item.", forState: UIControlState.Normal)
                        
                    }
                }
                
                let textMessage = arr[indexPath.row]
                let parameters = ["name": textMessage,
                    "startDate": startDateTime[indexPath.row],
                    "endDate":endDateTime[indexPath.row],
                    "complete": "true",
                    "assignedUser": assignuserData1[indexPath.row]
                    
                ];
                if(self.proxyUser == "PROXY")
                {
                    /*Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]*/
                    delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
                }
                let headers = delObj.headersAll
                Alamofire.Manager.sharedInstance
                    .request(.PUT, appDelegate.baseURL+"users/" + kID + "/tasks/" + self.taskId[sender.tag], parameters: parameters, headers: headers,encoding: .JSON)
                    .responseJSON {
                       response in
                        if(response.result.isSuccess)
                        {
                            
                            if(response.response?.statusCode == 403){
                                
                                self.LogoutForProxy()
                                
                            }
                            // self.getTask(0)
                        }else{
                            
                            self.processIndiactor.hidden = true
                            self.appDelegate.networkConnError();
                        }
                }
                
            }else
            {
                self.taskComplete[indexPath.row] = "false"
                
                self.indexC = self.indexC - 1
                cell.txtLabel.text = self.arr[indexPath.row]
                cell.btCheck.setTitle("0", forState:  UIControlState.Normal)
                cell.btCheck.setImage(self.unselected, forState: UIControlState.Normal)
                if(self.indexC == 0)
                {
                    lblTaskCount.text = "\(arr.count) of \(arr.count) remaining"
                    self.btTaskCountandDelete.hidden = true
                    self.btTaskCountandDelete.userInteractionEnabled = false
                }else{
                    
                    lblTaskCount.text = "\(arr.count - indexC) of \(arr.count) remaining"
                    if(self.isEditablePTS != "false"){
                        self.btTaskCountandDelete.hidden = false
                        self.btTaskCountandDelete.userInteractionEnabled = true
                        self.btTaskCountandDelete.setTitle("Delete \(self.indexC) Items.", forState: UIControlState.Normal)
                        
                        if(indexC == 1){
                            
                             self.btTaskCountandDelete.setTitle("Delete \(self.indexC) Item.", forState: UIControlState.Normal)
                            
                        }
                        
                        
                    }
                    
                }
                
                let textMessage = arr[indexPath.row]
                let parameters = ["name": textMessage,
                    "startDate": startDateTime[indexPath.row],
                    "endDate":endDateTime[indexPath.row],
                    "complete": "false",
                    "assignedUser": assignuserData1[indexPath.row]
                    
                ];
                if(self.proxyUser == "PROXY")
                {
                   
                    delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
                }
                let headers = delObj.headersAll
                Alamofire.Manager.sharedInstance
                    .request(.PUT, appDelegate.baseURL+"users/" + kID + "/tasks/" + self.taskId[indexPath.row], parameters: parameters,  headers: headers,encoding: .JSON)
                    .responseJSON {
                      response in
                        
                        if(response.result.isSuccess)
                        {
                            if(response.response?.statusCode == 403){
                                
                                self.LogoutForProxy()
                                
                            }
                        }else{
                            
                            
                            self.processIndiactor.hidden = true
                            self.appDelegate.networkConnError();
                        }
                }
                
            }
        }
    }
    
    //Add or update task function
    func addNewORUpdateTask()
    {
        
        //print("\(self.secondView.frame.origin.y)  \(self.addNewTaskView.frame.size.height + 5) \(self.addNewTaskView.frame.origin.y)")
        if(addNewTaskView.hidden)
        {
            if(self.secondView.frame.origin.y == (self.addNewTaskView.frame.size.height + 5) + self.addNewTaskView.frame.origin.y){
            UIView.animateWithDuration(0.3, animations: {
                self.secondView.frame.origin.y = (self.addNewTaskView.frame.size.height + 5) + self.addNewTaskView.frame.origin.y
                }, completion: nil)
            }
            //self.btAddNewTask.setTitle("Save", forState: UIControlState.Normal)
            
        }
        else
        {
            self.btAddNewTask.hidden = false
            let str : NSCharacterSet = NSCharacterSet.whitespaceAndNewlineCharacterSet()
    
            var task = self.txtAddNewTask.text!.stringByTrimmingCharactersInSet(str)
          
            
            //print(task)
            
            
            if( task != "")
            {
                // if for update task and else for creating a new task
                if(editPos == 1)
                {
                    self.editPos = 0
                    let parameters = [
                        "name": task,
                        "startDate": self.startDateTime[indexPath.row],
                        "endDate":self.endDateTime[indexPath.row],
                        "complete": self.taskComplete[indexPath.row],
                        "assignedUser": self.assignuserData1[indexPath.row]];
                    if(proxyUser == "PROXY")
                    {
            
                        delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
                    }
                    
                    let headers = delObj.headersAll
                    Alamofire.Manager.sharedInstance
                        .request(.PUT, self.appDelegate.baseURL+"users/" + self.kID + "/tasks/" + self.taskId[indexPath.row], parameters: parameters,headers: headers, encoding: .JSON)
                        .responseJSON {
                            response in
                            if(response.result.isSuccess)
                            {
                                
                                if(response.response?.statusCode == 403){
                                    
                                    self.LogoutForProxy()
                                    
                                }

                                
                                self.txtAddNewTask.text = ""
                                UIView.animateWithDuration(0.3, animations: {
                                    self.secondView.frame.origin.y = self.tabPos
                                    self.addNewTaskView.hidden = true
                                    self.editPos = 0
                                    self.btAddNewTask.setTitle("Add", forState: UIControlState.Normal)
                                    self.btAddNewTask.hidden = false
                                    }, completion: { (finished) -> Void in
                                })
                            }else{
                                self.processIndiactor.hidden = true
                                self.appDelegate.networkConnError();
                            }
                    }
                    
                }else{
                    
                    let parameters = [
                        "user":self.userData.object,
                        "name": task,
                        "startDate": NSNull(),
                        "endDate": NSNull(),
                        "assignedUser": NSNull() ];
                    if(proxyUser == "PROXY")
                    {
                        delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
                    }
                    let headers = delObj.headersAll
                    Alamofire.Manager.sharedInstance
                        .request(.POST, self.appDelegate.baseURL+"users/" + self.kID + "/tasks", parameters: parameters, headers: headers,encoding: .JSON)
                        .responseJSON {
                           response in
                            if(response.result.isSuccess)
                            {
                                self.txtAddNewTask.text = ""
                                if(self.segmentView.indexOfSelectedSegment == 2){
                                     self.segmentView.selectSegmentAtIndex(0)
                                    //self.getTask(0)
                                }else{
                                    self.getTask(self.segmentView.indexOfSelectedSegment)
                                }
                                    UIView.animateWithDuration(0.3, animations: {
                                    self.secondView.frame.origin.y = self.tabPos
                                    self.addNewTaskView.hidden = true
                                    self.btAddNewTask.setTitle("Add", forState: UIControlState.Normal)
                                    self.btAddNewTask.hidden = false
                                    }, completion: { (finished) -> Void in
                                })
                                
                                if(response.response?.statusCode == 403){
                                    
                                    self.LogoutForProxy()

                                }
                                
                                
                            }else{
                                
                                self.appDelegate.networkConnError();
                            }
                    }
                }
                
            }else
            {
                self.addNewTaskView.hidden = true
                UIView.animateWithDuration(0.3, animations: {
                    self.secondView.frame.origin.y = self.tabPos
                    self.btAddNewTask.setTitle("Add", forState: UIControlState.Normal)
                    self.btAddNewTask.hidden = false
                    }, completion: { (finished) -> Void in
                })
            }
        }
        
    }
    
    @IBAction func addNewTask(sender: AnyObject) {
        if(addNewTaskView.hidden)
        {   self.addNewTaskView.hidden = false
            self.btAddNewTask.setTitle("Cancel", forState: UIControlState.Normal)
            UIView.animateWithDuration(0.3, animations: {
                self.secondView.frame.origin.y = (self.addNewTaskView.frame.size.height + 5) + self.addNewTaskView.frame.origin.y
                }, completion: { (finished) -> Void in
            })
        }
        else
        {
            self.addNewTaskView.hidden = true
            self.txtAddNewTask.text = ""
            view.endEditing(true)
            UIView.animateWithDuration(0.3, animations: {
                self.secondView.frame.origin.y = self.tabPos
                self.btAddNewTask.setTitle("Add", forState: UIControlState.Normal)
                self.btAddNewTask.hidden = false
                }, completion: { (finished) -> Void in
            })
        }
        
        let headers = delObj.headersAll
        
        Alamofire.Manager.sharedInstance
            .request(.GET, delObj.baseURL+"users/" + kID + "/profile", parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
               response in
                if(response.result.isSuccess)
                {
                    
                    let json1 = JSON(response.result.value!)
                    
                    if(json1["userMessage"].stringValue == "You don't have permission to do that."){
                        print("called from line :586")
                        self.LogoutForProxy()
                    }else{
                        
                        if(json1["user"]["isElder"].boolValue) {
                            
                            
                        }else{
                            print("called from line :594")
                            self.LogoutForKeystoneStatusChange()
                            
                        }
                        
                        
                    }
                    
                }
                //println(self.userData)
        }
        
        
        
        checkPrivileges()
        

    }
    
    //tab to click on msg to show message(task) -------------------
   
    @IBAction func showFullMessage(sender: AnyObject) {
        self.view.endEditing(true)
        
        if(!addNewTaskView.hidden)
        {
            if(txtAddNewTask.text == "")
            {
                addNewTask(0)
            }
        }
        
        oldTaskMsg = arr[sender.tag]
        otherNotes = taskOtherNote[sender.tag]
        taskAlert(sender.tag)
    }

    // Update task
    // Here in this method block, Editing and Viewing of Task with Other notes is added.
    
    func taskAlert(index:Int)
    {
        alertView.close()
        alertView.containerView1 = createContainerView(1)
        alertView.delegate = self
        if(isEditablePTS == "false")
        {
            alertView.buttonTitles = ["Cancel"]
        }else{
            alertView.buttonTitles = buttons
            alertView.onButtonTouchUpInside = { (alertView: CustomIOS7AlertView, buttonIndex: Int) -> Void in
                if(self.buttons[buttonIndex] == "Edit" || self.buttons[buttonIndex] == "Save")
                {
                    if(self.flagAlert == 0)
                    {
                        self.flagAlert = 1
                        self.buttons[1] = "Save"
                       // alertView.close()
                        self.taskAlert(index)
                    }else{
                        
                        let str : NSCharacterSet = NSCharacterSet.whitespaceAndNewlineCharacterSet()
                        
                        let task = self.txtTaskDetail.text!.stringByTrimmingCharactersInSet(str)
                        
                       // task = task.trunc(120)
                        
                        let notes = self.otherNotesTextView.text!.stringByTrimmingCharactersInSet(str)
                       
                      //  notes = notes.trunc(1024)
                        
                        //print("\(task)  >> \(notes)")
                        
                        if(notes.characters.count > 1000){
                            
                             self.lblMessage.text = "Other notes text is too long. Please try again with a shorter task name."
                            
                            if(self.lblMessage.hidden){
                                
                                UIView.animateWithDuration(0.3, animations: {
                              
                                self.lblNotes.frame.origin.y = self.lblMessage.frame.origin.y + self.lblMessage.frame.height + 2
                                self.otherNotesTextView.frame.origin.y = self.lblNotes.frame.origin.y + 20
                                self.otherNotesTextView.frame.size.height = self.otherNotesTextView.frame.height - 10;
                                //self.collectionView?.frame.size.height
                                }, completion: { (finished) -> Void in
                            })
                            
                            self.lblMessage.hidden = false
                            
                            }
                            //Other notes cannot be too long. Please try again with shorter text.
                            
                        }else if(task.characters.count > 255)
                        {
                           
                            self.lblMessage.text = "The task name is too long. Please try again with shorter text."
                            
                            if(self.lblMessage.hidden){
                                
                                UIView.animateWithDuration(0.3, animations: {
                                    
                                    self.lblNotes.frame.origin.y = self.lblMessage.frame.origin.y + self.lblMessage.frame.height + 2
                                    self.otherNotesTextView.frame.origin.y = self.lblNotes.frame.origin.y + 20
                                    self.otherNotesTextView.frame.size.height = self.otherNotesTextView.frame.height - 10;
                                    
                                    }, completion: { (finished) -> Void in
                                })
                                
                                self.lblMessage.hidden = false
                            }
                        }
                        else if( task != "")
                        {
                            self.flagAlert = 0;
                            self.buttons[1] = "Edit"
                            alertView.close()
                            
                            let parameters = ["name": task,
                                "startDate": self.startDateTime[index],
                                "endDate":self.endDateTime[index],
                                "complete": self.taskComplete[index],
                                "assignedUser": self.assignuserData1[index],
                                "otherNotes": notes
                            ];
                            
                            if(self.proxyUser == "PROXY")
                            {
                             self.delObj.headersAll =  ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
                            }
                            let headers = self.delObj.headersAll
                            Alamofire.Manager.sharedInstance
                                .request(.PUT, self.appDelegate.baseURL+"users/" + self.kID + "/tasks/" + self.taskId[index], parameters: parameters, headers: headers,encoding: .JSON)
                                .responseJSON {
                                    response in
                                    if(response.result.isSuccess)
                                    {
                                        let json1 = JSON(response.result.value!)
                                        
                                        if(json1["userMessage"].stringValue == "You don't have permission to do that."){
                                            //print("called from line :586")
                                            self.LogoutForProxy()
                                        }
                                        if(response.response?.statusCode == 403){
                                            
                                            self.LogoutForProxy()
                                            
                                        }
                                        
                                        self.txtAddNewTask.text = ""
                                        self.getTask(self.segmentView.indexOfSelectedSegment)
                                        
                                    }else{
                                        
                                        self.appDelegate.networkConnError();
                                    }
                            }
                            
                        }else if(task == "") {
                            
                            self.lblMessage.text = "The task name cannot be blank. Please try again."
                            
                            if(self.lblMessage.hidden){
                                
                                UIView.animateWithDuration(0.3, animations: {
                                    
                                    self.lblNotes.frame.origin.y = self.lblMessage.frame.origin.y + self.lblMessage.frame.height + 2
                                    self.otherNotesTextView.frame.origin.y = self.lblNotes.frame.origin.y + 20
                                    self.otherNotesTextView.frame.size.height = self.otherNotesTextView.frame.height - 10;
                                    
                                    }, completion: { (finished) -> Void in
                                })
                                
                                self.lblMessage.hidden = false
                            }

                            
                        
                        
                        }
                    }
                }
                
            }
        }
        
        alertView.show()
    }
    
    func createContainerView(flag:Int) -> UIView {
        
        //print(self.view.frame.width)
        
        let widhtOfView = self.view.frame.width
        
        var heightOfAlert = self.view.frame.width
        
        var widhtOfAlert = self.view.frame.width - self.view.frame.width//Just add and delete so that datatype will be assigned properly.
        
        if(widhtOfView == 387.00){
            
            widhtOfAlert = 350.00
        }else if(widhtOfView > 390.00){
            
            widhtOfAlert = 375.00
        }else{
            widhtOfAlert = 300
           
            txtTaskDetail.autocorrectionType = UITextAutocorrectionType.No
            otherNotesTextView.autocorrectionType = UITextAutocorrectionType.No
            
        }
        if(flagAlert == 0){
            
            heightOfAlert = 230
        
            
            
        }else{
        
              heightOfAlert = 270
            
            if(widhtOfAlert == 300){
                
                heightOfAlert = 230
            }

        
        }
        
         containerView = UIView(frame: CGRectMake(0, 0, widhtOfAlert, heightOfAlert))
         lblTitle = UILabel(frame: CGRectMake(10,10, containerView.frame.width, 13))
         txtTaskDetail = UITextView(frame: CGRectMake(10,30, containerView.frame.width - 20, 50))
        
        if(flagAlert == 0){
            
            lblMessage = UILabel(frame: CGRectMake(10,txtTaskDetail.frame.origin.y + 53, containerView.frame.width - 10 , 0))
            lblNotes = UILabel(frame: CGRectMake(10,lblMessage.frame.origin.y + lblMessage.frame.height + 2, containerView.frame.width, 13))
            otherNotesTextView = UITextView(frame: CGRectMake(10,lblNotes.frame.origin.y + 20, containerView.frame.width - 20, 120))
        
        }else{
        
            lblMessage = UILabel(frame: CGRectMake(10,txtTaskDetail.frame.origin.y + 53, containerView.frame.width - 15 , 35))
            lblNotes = UILabel(frame: CGRectMake(10,lblMessage.frame.origin.y  + 14, containerView.frame.width, 13))
            
            var heightOfNotes:CGFloat = 135.00;
            
            if(widhtOfAlert == 300){
                
                heightOfNotes = 100.00
            }
            
            
            otherNotesTextView = UITextView(frame: CGRectMake(10,lblNotes.frame.origin.y + 30, containerView.frame.width - 20, heightOfNotes))
            
            lblMessage.font = UIFont(name: "HelveticaNeue", size: 14)
            lblMessage.numberOfLines = 2
            lblMessage.textColor = UIColor.redColor()
           
        }

        
        txtTaskDetail.delegate = self
        otherNotesTextView.delegate = self
        otherNotesTextView.text = otherNotes
        
        if(flagAlert == 0){
            txtTaskDetail.editable = false
            txtTaskDetail.textColor = UIColor.darkGrayColor()
            lblTitle.text = "Task"
            otherNotesTextView.editable = false
            otherNotesTextView.textColor = UIColor.darkGrayColor()
            lblNotes.text = "Other Notes:"
            
        }else
        {
            txtTaskDetail.editable = true
            txtTaskDetail.textColor = UIColor.blackColor()
            otherNotesTextView.editable = true
            otherNotesTextView.textColor = UIColor.blackColor()
            lblTitle.text = "Edit Task"
        
            if(otherNotes.characters.count == 0){
                
                lblNotes.text = "Add Other Notes:"
            }else{
                
                lblNotes.text = "Edit Other Notes:"
            }
            containerView.addSubview(lblMessage)
            self.lblMessage.hidden = true
            
        }
        
        txtTaskDetail.layer.cornerRadius = 3
        txtTaskDetail.text = oldTaskMsg
        
        otherNotesTextView.layer.cornerRadius = 3
        otherNotesTextView.text = otherNotes
        
        txtTaskDetail.font = UIFont.systemFontOfSize(15)
        otherNotesTextView.font = UIFont.systemFontOfSize(13)
        
        

        containerView.addSubview(lblTitle)
        containerView.addSubview(lblNotes)
        containerView.addSubview(txtTaskDetail)
        containerView.addSubview(otherNotesTextView)

        return containerView
    }
   
    func customIOS7AlertViewButtonTouchUpInside(alertView: CustomIOS7AlertView, buttonIndex: Int) {
        
        if(self.buttons[buttonIndex] == "Cancel")
        {
            flagAlert = 0
            self.buttons[1] = "Edit"
            alertView.close()
        }
        if(self.buttons[buttonIndex] == "Save")
        {
            self.buttons[1] = "Edit"
            //alertView.close()
        }
    }
    
    //tab to double click on msg to edit task -------------------
    @IBAction func editTask(sender: UIButton, event: UIEvent) {
        self.view.endEditing(true);
        if(isEditablePTS != "false")
        {
            var touch : UITouch;
            touch = event.allTouches()!.first! // as! UITouch
            var loc : CGPoint
            loc = touch.locationInView(self.tbToDoLists)
            txtFullMsg.hidden = true
            indexPath = self.tbToDoLists.indexPathForRowAtPoint(loc)
            editPos = 1
            if(addNewTaskView.hidden)
            {
                txtAddNewTask.text = arr[indexPath.row]
                self.btAddNewTask.setTitle("Cancel", forState: UIControlState.Normal)
                self.btAddNewTask.hidden = false
                self.addNewTaskView.hidden = false
                UIView.animateWithDuration(0.3, animations: {
                    self.secondView.frame.origin.y = (self.addNewTaskView.frame.size.height + 5) + self.addNewTaskView.frame.origin.y
                    }, completion: { (finished) -> Void in
                })
            }else
            {
                txtAddNewTask.text = arr[indexPath.row]
                UIView.animateWithDuration(0.3, animations: {
                    self.secondView.frame.origin.y = self.tabPos
                    self.addNewTaskView.hidden = true
                    self.editPos = 0
                    self.txtAddNewTask.text = ""
                    self.btAddNewTask.setTitle("Add", forState: UIControlState.Normal)
                    self.btAddNewTask.hidden = false
                    }, completion: { (finished) -> Void in
                })
            }
        }
    }
    
    //----------------------------------------------------------------------------
    
    //set unassign user---------------------------------------------
    @IBAction func btSetUnassignUser(sender: AnyObject) {
        let textMessage = arr[indexPath.row]
        let parameters = ["name": textMessage,
            "startDate": startDateTime[indexPath.row],
            "endDate":endDateTime[indexPath.row],
            "complete": "false",//taskComplete[indexPath.row],
            "assignedUser": NSNull()
            
        ];
        if(self.proxyUser == "PROXY")
        {
           
            delObj.headersAll =  ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
        }
        let headers = delObj.headersAll
        Alamofire.Manager.sharedInstance
            .request(.PUT, appDelegate.baseURL+"users/" + kID + "/tasks/" + self.taskId[indexPath.row], parameters: parameters,headers: headers, encoding: .JSON)
            .responseJSON {
               response in
                if(response.result.isSuccess)
                {
                    
                    if(response.response?.statusCode == 403){
                        
                        self.LogoutForProxy()
                        
                    }
                    
                    self.viewUserName.hidden = true
                    self.tbUserName.reloadData()
                    self.getTask(self.segmentView.indexOfSelectedSegment)
                }else{
                    
                    self.appDelegate.networkConnError();
                }
        }
    }
    
    @IBAction func btCancelUserView(sender: AnyObject) {
        UIView.animateWithDuration(0.5, animations: {
            self.viewUserName.frame.origin.y = self.view.frame.size.height+50}, completion:{ (finished) -> Void in
        })
        
    }
    
    //set assinable user---------------------------------------------
    @IBAction func setUser(sender: UIButton, event: UIEvent) {
        self.view.endEditing(true);
        if(!addNewTaskView.hidden)
        {
        addNewTask(0)
        }
        datePickerView.hidden = true
        self.tbUserName.reloadData()
       /* var touch : UITouch;
        touch = event.allTouches()?.first as! UITouch
        locButton = touch.locationInView(self.tbToDoLists)
        indexPath = self.tbToDoLists.indexPathForRowAtPoint(locButton)!*/
        
        indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        locButton = indexPath
        userFirstName = setAssignUserId[indexPath.row]
        
       ////print("Assigned to: \(userFirstName)")
        //print("btn Name to: \(fbtArr[indexPath.row])")
        
        if(fbtArr[indexPath.row] == "Volunteer")
        {
            let textMessage = arr[indexPath.row]
            let parameters = ["name": textMessage,
                "startDate": startDateTime[indexPath.row],
                "endDate":endDateTime[indexPath.row],
                "complete": "false",//taskComplete[indexPath.row],
                "assignedUser": assignuserData[self.alUser]
            ];
            if(self.proxyUser == "PROXY")
            {
               
                delObj.headersAll =  ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
            }
            let headers = delObj.headersAll
            Alamofire.Manager.sharedInstance
                .request(.PUT, appDelegate.baseURL+"users/" + kID + "/tasks/" + self.taskId[indexPath.row], parameters: parameters,headers: headers, encoding: .JSON)
                .responseJSON {
                   response in
                    if(response.result.isSuccess)
                    {
                        if(response.response?.statusCode == 403){
                            
                            self.LogoutForProxy()
                            
                        }
                        self.viewUserName.hidden = true
                        self.tbUserName.reloadData()
                        self.getTask(self.segmentView.indexOfSelectedSegment)
                    }else{
                        
                        self.appDelegate.networkConnError();
                    }
            }
            
        }else if(fbtArr[indexPath.row] == "Me")
        {
        
          self.btSetUnassignUser(self)
        }else
        {
            self.viewUserName.hidden = false
            UIView.animateWithDuration(0.5, animations: {
                self.viewUserName.frame.origin.y = 0/*self.secondView.frame.origin.y*/}, completion:{ (finished) -> Void in
        })
        }
        
    }
    
    //schedule task------------------------------------------------------------
   
    @IBAction func setDate(sender: UIButton, event: UIEvent) {
        CheckPrev { response in
            if(response){
                self.view.endEditing(true)
                if(!self.addNewTaskView.hidden)
                {
                    self.addNewTask(0)
                }
                self.txtFullMsg.hidden = true
                
                var touch : UITouch;
                touch = event.allTouches()!.first! // as! UITouch
                var loc : CGPoint
                loc = touch.locationInView(self.tbToDoLists)
              
                //indexPath = self.tbToDoLists.indexPathForRowAtPoint(loc)!
                var cell:myTableViewCell = myTableViewCell()
                let indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
                self.locButtonDate = indexPath
                cell = self.tbToDoLists .cellForRowAtIndexPath(indexPath) as! myTableViewCell
                
                let a = loc
                
                
                
                if(self.timeDuration[indexPath.row] != ""){
                    self.textViewDuration.text = self.timeDuration[indexPath.row]}
                else{
                    self.textViewDuration.text = "60"
                }
                let str : NSCharacterSet = NSCharacterSet(charactersInString: " ")
                if( self.dateTimeArr[indexPath.row].stringByTrimmingCharactersInSet(str) != "")
                {
                    
                    let timeinterval : NSTimeInterval = NSTimeInterval( (self.dateTimeArr[indexPath.row] as NSString).doubleValue )
                    
                    // convert it in to NSTimeInteral
                    
                    let dateFromServer = NSDate(timeIntervalSince1970:timeinterval)
                    
                    self.datePicker.setDate(dateFromServer, animated: true)
                    
                    //print(dateFromServer)
                }else
                {
                    self.datePicker.setDate(NSDate(), animated: true)
                }
                
                //Scroll table to adjust DatePicker.
                
                if(self.datePickerView.hidden){
                    self.datePickerView.hidden = false
                    if(a.y > self.datePickerView.frame.origin.y - CGRectGetHeight(cell.btSecond.bounds))
                    {
                        let pos1 = a.y - (self.datePickerView.frame.origin.y )
                        UIView.animateWithDuration(0.3, animations: {
                            self.tbToDoLists.setContentOffset(CGPoint(x: 0,y: pos1+CGRectGetHeight(cell.btSecond.bounds)*3), animated: true)
                            
                            }, completion: { (finished) -> Void in
                        })
                    }
                }else{
                    self.datePickerView.hidden = true
                    self.tbToDoLists.setContentOffset(CGPoint(x: 0,y: self.actualPos), animated: true)
                }
            }else{
                print("Undable to load data")
            }
        }//Check prev ends
        
        
    }
    
    
    
    
    
    func CheckPrev( completion : (Bool) -> ()) {
        
        let headers = delObj.headersAll
        
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL + "users/" + kID + "/privileges/users/" + logUserId, parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
                 response in
                if(response.result.isSuccess)
                {
                    
                    var json1 = JSON(response.result.value!)
                    print("*json1 line1923 to do list \(json1)")
                    let count: Int? = json1.array?.count
                    if(count != 0)
                    {
                        var uploadphoto : String = String()
                        
                        self.isProxyInResponse = json1["PROXY"].stringValue
                        self.isEditablePTS = json1["EDIT_TASKS"].stringValue
                        self.isCalenderPTS = json1["VIEW_CALENDAR"].stringValue
                        self.isConnected = json1["CONNECTED"].stringValue
                        self.isMedicationVisible = json1["VIEW_MEDICATIONS"].stringValue
                        uploadphoto = json1["UPLOAD_PHOTOS"].stringValue
                        if(self.isConnected == "false" )
                        {
                            self.LogoutForProxy()
                            
                        }else{
                            
                            if(self.proxyUser == "PROXY")
                            {
                                if(self.isProxyInResponse == "false" )
                                {
                                    self.LogoutForProxy()
                                    
                                }
                            }
                            
                            if(self.isEditablePTS == "false")
                            {
                                self.btAddNewTask.enabled = false
                                self.btAddNewTask.alpha = 0.6
                                
                                if(uploadphoto == "true")
                                {
                                    print("Caregiver not delete!")
                                    print("Yea, Keep me here")
                                    
                                }
                                else
                                {
                                    print("Caregiver deleted!")
                                    self.LogoutForProxy()
                                }
                            }
                            else{
                                self.btAddNewTask.enabled = true
                                self.btAddNewTask.alpha = 1
                                //self.isParticipant = "false"
                                
                            }
                            
                        }
                        completion(true)
                    }
                }else{
                    completion(false)
                }
                
                
        }//ends
        
    }

    
    
    //MARK: checkPrivileges
    
    func checkPrivileges() {
        
        let headers = delObj.headersAll
        
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL + "users/" + kID + "/privileges/users/" + logUserId, parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    var json1 = JSON(response.result.value!)
                    let count: Int? = json1.array?.count
                    if(count != 0)
                    {
                        
                        self.isProxyInResponse = json1["PROXY"].stringValue
                        self.isEditablePTS = json1["EDIT_TASKS"].stringValue
                        self.isCalenderPTS = json1["VIEW_CALENDAR"].stringValue
                        self.isConnected = json1["CONNECTED"].stringValue
                        self.isMedicationVisible = json1["VIEW_MEDICATIONS"].stringValue
                        
                        if(self.isConnected == "false" )
                        {
                            self.LogoutForProxy()
                            
                        }else{
                            
                            if(self.proxyUser == "PROXY")
                            {
                                if(self.isProxyInResponse == "false" )
                                {
                                    self.LogoutForProxy()
                                    
                                }
                            }
                            
                            if(self.isEditablePTS == "false")
                            {
                                self.btAddNewTask.enabled = false
                                self.btAddNewTask.alpha = 0.6
                                
                                if(self.CGDeleted == "false")
                                {
                                    print("nt deleted yet")
                                }
                                else
                                {
                                    self.LogoutForProxy()
                                }

                                
                            }else{
                                self.btAddNewTask.enabled = true
                                self.btAddNewTask.alpha = 1
                                
                                
                            }
                            
                        }
                        
                    }
                }
                
                
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated..
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == tbUserName)
        {return assignUserName.count}else{return arr.count}
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:myTableViewCell = myTableViewCell()
        // user task table
        if(tableView == tbToDoLists)
        {
            
            cell = tbToDoLists.dequeueReusableCellWithIdentifier("cellAll") as! myTableViewCell
            
            cell.btFirst.layer.cornerRadius = 5
            cell.btSecond.layer.cornerRadius = 5
            cell.btCheck.tag = indexPath.row
            cell.btShowMsg.tag = indexPath.row
            cell.btSecond.tag = indexPath.row
            cell.btFirst.tag = indexPath.row
            
            if(taskComplete[indexPath.row] == "true")
            {
                cell.btCheck.setTitle("1", forState:  UIControlState.Normal)
                cell.btCheck.setImage(isSelected, forState: UIControlState.Normal)
                let strikeThroughAttributes = [NSStrikethroughStyleAttributeName : 1]
                let strikeThroughString = NSAttributedString(string: arr[indexPath.row], attributes: strikeThroughAttributes)
                cell.txtLabel.attributedText = strikeThroughString
                cell.txtLabel.clipsToBounds = true
            }
            else
            {
                cell.txtLabel.text = arr[indexPath.row]
                cell.btCheck.setTitle("0", forState:  UIControlState.Normal)
                cell.btCheck.setImage(unselected, forState: UIControlState.Normal)
            }
            if(fbtArr[indexPath.row] == "Assign")
            {
                cell.btFirst.setImage(bgAssign, forState: .Normal)
            }else{
                if(fbtArr[indexPath.row] == "Volunteer")
                {
                    cell.btFirst.setImage(btVolunteer, forState: .Normal)
                }
                else{
                    cell.btFirst.setImage(nil, forState: .Normal)
                    cell.btFirst.setTitle(fbtArr[indexPath.row], forState:  UIControlState.Normal)
                }
                
            }
            if(sbtArr[indexPath.row] == "Schedule")
            {
                cell.btSecond.setImage(btScedule, forState: .Normal)
            }else{
                cell.btSecond.setImage(nil, forState: .Normal)
                cell.btSecond.setTitle(sbtArr[indexPath.row], forState:  UIControlState.Normal)
            }
            
            if(fbtArr[indexPath.row] == "Volunteer" || fbtArr[indexPath.row] == "Assign")
            {
                cell.btFirst.backgroundColor = UIColor(red: 151/255, green: 174/255, blue: 30/255, alpha: 1.0)
            }
            else
            {
                cell.btFirst.backgroundColor = UIColor(red: 0.1, green: 0.4, blue: 0.6, alpha: 1.0)
            }
            if(sbtArr[indexPath.row] != "Schedule")
            {
                cell.btSecond.backgroundColor = UIColor(red: 26/255, green: 102/255, blue: 153/255, alpha: 1.0)
            }
            else
            {
                cell.btSecond.backgroundColor = UIColor(red: 5/255, green: 119/255, blue: 46/255, alpha: 1.0)
            }
            
            if(fbtArr[indexPath.row] != "Me"  && proxyUser != "PROXY")
            {
                cell.btFirst.enabled = false
                if(fbtArr[indexPath.row] == "Volunteer" || fbtArr[indexPath.row] == "Assign")
                {
                    cell.btFirst.enabled = true
                    cell.btSecond.enabled = true
                    cell.btFirst.backgroundColor = UIColor(red: 151/255, green: 174/255, blue: 30/255, alpha: 1.0)
                    
                }else{
                    cell.btFirst.backgroundColor = UIColor(red: 26/255, green: 102/255, blue: 170/255, alpha: 0.6)
                    if(sbtArr[indexPath.row] == "Schedule" )
                    {
                        cell.btSecond.backgroundColor = UIColor(red: 5/255, green: 119/255, blue: 46/255, alpha: 1.0)
                        cell.btSecond.enabled = true
                    }else{
                        cell.btSecond.backgroundColor = UIColor(red: 26/255, green: 102/255, blue: 170/255, alpha: 0.6)
                        cell.btSecond.enabled = false
                    }
                }
            }else
            {
                cell.btFirst.enabled = true
                cell.btSecond.enabled = true
            }
          
            
            if(isEditablePTS == "false")
            {
                if(fbtArr[indexPath.row] == "Me")
                {
                    cell.btSecond.enabled = true
                    cell.btCheck.enabled = true
                }else{
                    
                    cell.btCheck.enabled = false
                    
                    if([indexPath.row] == "Me" || fbtArr[indexPath.row] == "Volunteer")
                    {
                        cell.btSecond.enabled = true
                    }else{
                        cell.btSecond.enabled = false
                        cell.btSecond.backgroundColor = UIColor(red: 26/255, green: 102/255, blue: 170/255, alpha: 0.6)}
                }
                
                
                
            }else{
                //cell.btSecond.enabled = true
                cell.btCheck.enabled = true
            }
            
            if(assignUserName.count == 0)
            {
                cell.btFirst.enabled = false
                cell.btFirst.alpha = 0.7
            }
            
            cell.btShowMsg.addTarget(self, action: #selector(toDoList.editTask(_:event:)), forControlEvents: UIControlEvents.TouchDownRepeat)
            cell.btShowMsg.addTarget(self, action: #selector(toDoList.showFullMessage(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            
            cell.btCheck.addTarget(self, action: #selector(toDoList.checkTaskIsComplete(_:event:)), forControlEvents: UIControlEvents.TouchUpInside)
            cell.btSecond.addTarget(self, action: #selector(toDoList.setDate(_:event:)), forControlEvents: UIControlEvents.TouchUpInside)
            cell.btFirst.addTarget(self, action: #selector(toDoList.setUser(_:event:)), forControlEvents: UIControlEvents.TouchUpInside)
            return cell
        }
            
            // assignable user table
        else{
            
            var cell:tblUserCell = tblUserCell()
            cell = tableView.dequeueReusableCellWithIdentifier("cellUser", forIndexPath: indexPath) as! tblUserCell
            //var urlData = self.appDelegate.baseImageURL + "w50xh50-2/images/default_profile_image.png"
            let headers = delObj.headersAll
            Alamofire.request(.GET, keystoneProPic[indexPath.row],headers: headers).response() {
                (_, _, data, _) in
                
                let image = UIImage(data: data! as NSData)
                cell.imageView?.image = image
                if(self.assignUserId[indexPath.row] == self.userFirstName)
                {
                    cell.textLabel?.text = self.assignUserName[indexPath.row]
                    cell.backgroundColor = UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1)
                }
                else
                {
                    cell.textLabel?.text = self.assignUserName[indexPath.row]
                    cell.backgroundColor = UIColor.whiteColor()
                }
                
            }
            return cell
        }
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        //assign user to tab table cell
        if(tableView == tbUserName){
            self.viewUserName.endEditing(true);
            self.secondView.endEditing(false);
            
            if(isEditablePTS != "false")
            {
                let indexPathFirstButton = locButton
                let textMessage = arr[indexPathFirstButton.row]
                let parameters = ["name": textMessage,
                    "startDate": startDateTime[indexPathFirstButton.row],
                    "endDate": endDateTime[indexPathFirstButton.row],
                    "complete": taskComplete[indexPathFirstButton.row],
                    "assignedUser": assignuserData[indexPath.row]
                    
                ];
                if(self.proxyUser == "PROXY")
                {
                   
                    delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
                }
                let headers = delObj.headersAll
                Alamofire.Manager.sharedInstance
                    .request(.PUT, appDelegate.baseURL+"users/" + kID + "/tasks/" + self.taskId[indexPathFirstButton.row], parameters: parameters,headers: headers, encoding: .JSON)
                    .responseJSON {
                        response in
                        if(response.result.isSuccess)
                        {
                            if(response.response?.statusCode == 403){
                                
                                self.LogoutForProxy()
                                
                            }
                            self.viewUserName.hidden = true
                            self.tbUserName.reloadData()
                            self.getTask(self.segmentView.indexOfSelectedSegment)
                        }else{
                            
                            self.appDelegate.networkConnError();
                        }
                }
            }
        }
    }
   
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if(isEditablePTS != "false")
        {return true }
        else{ return false }
    }
}
    //delete task for swipe left to right as device angle
   /* func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]? {
        var deleteAction = UITableViewRowAction(style: .Default, title: "Delete") { (action, indexPath) -> Void in
            
            var alert = UIAlertController(title: "Confirm to Delete", message: "Are you sure you want to delete this task?", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler:{(UIAlertAction)in Void.self
                self.tbToDoLists.reloadData()}))
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler:{
                (UIAlertAction)in Void.self
                if(self.proxyUser == "PROXY")
                {
                    Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
                }
                
                Alamofire.Manager.sharedInstance
                    .request(.DELETE, self.appDelegate.baseURL+"users/" + self.kID + "/tasks/" + self.taskId[indexPath.row], parameters: nil, encoding: .JSON)
                    .responseJSON {
                        (request, response, data1, error) -> Void in
                        if(data1 != nil)
                        {
                            self.getTask(self.segmentView.indexOfSelectedSegment)
                        }else{
                            
                            self.appDelegate.networkConnError();
                        }
                }
                self.tbToDoLists.editing = false
            }))
            self.presentViewController(alert, animated: true, completion: { })
        }
        return [deleteAction]
    }*/
    
   
  /*  func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }*/


