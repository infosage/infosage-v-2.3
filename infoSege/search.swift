//
//  search.swift
//  infoSage
//
//  Created by Vijay Mac on 18/04/16.
//  Copyright © 2016 Pramod shirsath. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SafariServices


class search: UIViewController,UITableViewDataSource,UITextFieldDelegate,SFSafariViewControllerDelegate, MBProgressHUDDelegate{
    
    
    var HUD:MBProgressHUD = MBProgressHUD()
    @IBOutlet var txtFldSearch: UITextField!
    @IBOutlet var btndone: UIButton!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var btnPrevious: UIButton!
    @IBOutlet var lblNoSearchResult: UILabel!
    @IBOutlet var tblSearch: UITableView!
    
    @IBOutlet var btnShowHelp: UIButton!
    @IBOutlet var lblPageNumber: UILabel!
    @IBOutlet var headerView: UIView!
     @IBOutlet var footerView: UIView!
    @IBOutlet var progressBar: UIActivityIndicatorView!
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let defaults = NSUserDefaults.standardUserDefaults()
    
    var newCnt = 0;
    // var appleProducts,snippet,datatitle,displayLink,link = ["null"]
    
    var arrSnippet:[String] = []
    var arrTitle:[String] = []
    var arrDisplayLink:[String] = []
    var arrLink:[String] = []
    var arrAppleProducts:[String] = []
    var urlForNextPage = ""
    var nextBackCounter = 1
    var helpText = ""
    var filteredAppleProducts = [String]()
    
    @IBOutlet var txtHelp: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtFldSearch.layer.borderColor = UIColor(red: 0, green: 128/255, blue: 0, alpha: 1.0).CGColor
        txtFldSearch.layer.borderWidth = 0.5
        txtFldSearch.layer.cornerRadius = 5
        txtFldSearch.autocorrectionType = UITextAutocorrectionType.No
        txtFldSearch.placeholder = "Find health resources"
        self.tblSearch.tableFooterView = UIView()
        self.tblSearch.alpha = 1
        
        self.progressBar.hidden = true
        self.HUD.hidden = true
        self.progressBar.stopAnimating()
        self.btnNext.userInteractionEnabled = false
        self.btnPrevious.userInteractionEnabled = false
        self.btnNext.alpha = 0.6
        self.btnPrevious.alpha = 0.6
        self.lblNoSearchResult.hidden = true
        
        // defaults.setValue("NO", forKey:"helpText")
        
        if(self.defaults.valueForKey("helpText") != nil){
           helpText = self.defaults.valueForKey("helpText") as! String
            
        }else{
            helpText = "YES"
        }
        
        print(helpText)
        
        if(helpText == "NO"){
            self.txtHelp.hidden = true
            self.btnShowHelp.frame.origin.y = self.headerView.frame.origin.y + self.headerView.frame.height
            
            self.txtFldSearch.frame.origin.y = self.btnShowHelp.frame.origin.y + self.btnShowHelp.frame.height;
            self.btnShowHelp.setTitle("Show", forState: UIControlState.Normal)
            self.tblSearch.frame.origin.y = self.txtFldSearch.frame.origin.y +  self.txtFldSearch.frame.height + 5;
            
            self.tblSearch.frame.size.height = self.footerView.frame.origin.y - self.tblSearch.frame.origin.y + 5
            
            print(self.view.frame.height)
            print(self.tblSearch.frame.origin.y)
            print(self.footerView.frame.origin.y)
            //var n = self.txtFldSearch.frame.origin.y +  self.txtFldSearch.frame.height + 5
            // defaults.setValue("NO", forKey:"helpText")
            
        }else{
            self.txtHelp.hidden = false
            self.txtFldSearch.frame.origin.y =  self.txtHelp.frame.origin.y + self.txtHelp.frame.height + 10;
            self.tblSearch.frame.origin.y = self.txtFldSearch.frame.origin.y +  self.txtFldSearch.frame.height;
            self.btnShowHelp.frame.origin.y = 121;
            self.btnShowHelp.setTitle("Hide", forState: UIControlState.Normal)
            self.tblSearch.frame.size.height = self.footerView.frame.origin.y - self.tblSearch.frame.origin.y + 5
            // defaults.setValue("YES", forKey:"helpText")
        }
        
        //Analytics
        var proxiedUID = glob_logUserID
         print("Printing gloabally declared values: keyID: \(glob_keystoneID) , isProxy : \(glob_viewAsProxy), Loginuserkestone: \(glob_loginUserIsKeystone) ,loguserid:  \(glob_logUserID) ")
        
        if(glob_viewAsProxy){
        
            proxiedUID = glob_keystoneID
        
            appDelegate.headersAll =  ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": glob_keystoneID]
        }
         let headers = appDelegate.headersAll
         
         //let deviceType = self.defaults.valueForKey("deviceType") as! String
         
         let device = UIDevice.currentDevice().name
         let screenSize = self.defaults.valueForKey("screenSize") as! String
         let OS = self.defaults.valueForKey("OS") as! String
         
         let alertController = UIAlertController(title: "Device Info", message: "\(UIDevice.currentDevice().name)  \(screenSize) \(OS)  @home", preferredStyle: UIAlertControllerStyle.Alert)
         alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
         
         //self.presentViewController(alertController, animated: true, completion: nil)
         
         let parameters = ["string":"resources"]
         
         var url = self.appDelegate.baseURL+"users/\(glob_logUserID)/elasticLog/pageview/proxiedUser/\(proxiedUID)/mobile?operatingSystem=\(OS)&deviceType=\(device)&screenSize=\(screenSize)"
         url = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
         
         Alamofire.request(.POST, url,parameters: parameters,headers: headers,encoding: .JSON)
         .responseJSON {
         response in
         if(response.result.isSuccess)
         {
         print(response.response?.statusCode)
         print("Success")
         
         }else if(response.result.isFailure){
         
         print(response.response?.statusCode)
         print("failure")
         
         }else{
         
         self.appDelegate.networkConnError();
         
         }
         }
        
        //self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func evnHelpClicked(sender: AnyObject) {
        
        self.txtHelp.hidden = !self.txtHelp.hidden
        if(self.txtHelp.hidden){
            
             self.btnShowHelp.frame.origin.y = self.headerView.frame.origin.y + self.headerView.frame.height
            
            self.txtFldSearch.frame.origin.y = self.btnShowHelp.frame.origin.y + self.btnShowHelp.frame.height;
           
            
            self.tblSearch.frame.origin.y = self.txtFldSearch.frame.origin.y +  self.txtFldSearch.frame.height + 5;
            
            self.btnShowHelp.setTitle("Show", forState: UIControlState.Normal)
            self.tblSearch.frame.size.height = self.footerView.frame.origin.y - self.tblSearch.frame.origin.y + 5
            defaults.setValue("NO", forKey:"helpText")
            
        }else{
            
            self.txtFldSearch.frame.origin.y =  self.txtHelp.frame.origin.y + self.txtHelp.frame.height + 10;
            self.tblSearch.frame.origin.y = self.txtFldSearch.frame.origin.y +  self.txtFldSearch.frame.height;
            self.btnShowHelp.frame.origin.y = 121;
            self.tblSearch.frame.size.height = self.footerView.frame.origin.y - self.tblSearch.frame.origin.y + 5
            self.btnShowHelp.setTitle("Hide", forState: UIControlState.Normal)
            defaults.setValue("YES", forKey:"helpText")
        }
        
        //        let alert = UIAlertController (title: "Search help", message: "The resource guide provides search results from a list of curated websites. The Division of Aging at Beth Israel Deaconess Medical Center has filtered the websites thought be biased or duplicative and has selected websites considered to be reliable and trustworthy sources of information designed to be helpful to seniors and families.", preferredStyle: UIAlertControllerStyle.Alert)
        //        alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Cancel, handler:{ (UIAlertAction)in Void()  }))
        //        self.presentViewController(alert, animated: true, completion: {})
        
    }
    
    
    @IBAction func clickDone(sender: AnyObject) {
        tblSearch.reloadData()
    }
    
    
    func textFieldDidEndEditing(textField: UITextField){
        
        self.searchCall()
        
        if(textField.text != ""){
        HUD.labelText = ""
        HUD.square = false
        self.HUD.hidden = false
        self.view.addSubview(HUD)
        HUD.show(true)
        }
        self.btnNext.userInteractionEnabled = false;
        self.btnPrevious.userInteractionEnabled = false;
        
        self.lblNoSearchResult.hidden = true
        self.nextBackCounter = 1
    }
    
    
    @IBAction func btnNextClick(sender: AnyObject) {
        
        self.btnNext.userInteractionEnabled = false;
        self.btnPrevious.userInteractionEnabled = false;
                
        self.nextBackCounter += 10
        
        HUD.labelText = ""
        HUD.square = false
        self.HUD.hidden = false
        self.view.addSubview(HUD)
        HUD.show(true)
        //print("Next Hit\(self.nextBackCounter)")
        self.searchCall()
        
    }
    
    @IBAction func btnPreviousClick(sender: AnyObject) {
        
        self.btnNext.userInteractionEnabled = false;
        self.btnPrevious.userInteractionEnabled = false;
        
        self.btnPrevious.userInteractionEnabled = false;
        
        if(self.nextBackCounter != 1){
            self.nextBackCounter -= 10
            //print("pre Hit\(self.nextBackCounter)")
            HUD.labelText = ""
            HUD.square = false
            self.HUD.hidden = false
            self.view.addSubview(HUD)
            HUD.show(true)
            self.searchCall()
            
            if(self.nextBackCounter == 1){
                self.btnPrevious.userInteractionEnabled = false
                self.btnPrevious.alpha = 0.6
            }
        }
        
    }
    //Go button
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func searchCall(){
        
        let name = self.txtFldSearch.text!
        
        self.tblSearch.alpha = 0.6
        self.tblSearch.userInteractionEnabled = false
        self.progressBar.hidden = false
        self.progressBar.startAnimating()
        
        let i = "\(self.nextBackCounter)"
        //print(i)
        self.arrDisplayLink.removeAll(keepCapacity: false)
        self.arrSnippet.removeAll(keepCapacity: false)
        self.arrTitle.removeAll(keepCapacity: false)
        self.arrLink.removeAll(keepCapacity: false)
        var urlString1 = ""
        
        if (name.characters.count > 0) {
            
            //Searching query
            
            if(i == "1"){
                urlString1 = "https://www.googleapis.com/customsearch/v1?q=\(name)&key=AIzaSyCMGfdDaSfjqv5zYoS0mTJnOT3e9MURWkU&cx=008268151070442059713:wo6u-ggjryo"
                
            }else{
                urlString1 = "https://www.googleapis.com/customsearch/v1?q=\(name)&key=AIzaSyCMGfdDaSfjqv5zYoS0mTJnOT3e9MURWkU&cx=008268151070442059713:wo6u-ggjryo&start=\(i)"
            }
            
            urlString1 = urlString1.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            
            let headers = appDelegate.headersAll
            Alamofire.Manager.sharedInstance
                .request(.GET, urlString1, parameters: nil,headers: headers, encoding: .JSON)
                .responseJSON {
                  response in
                    if(response.result.isSuccess)
                    {
                        
                        self.arrDisplayLink = []
                        self.arrSnippet = []
                        self.arrTitle = []
                        self.arrLink = []
                        
                        let json1 = JSON(response.result.value!)
                        
                        //let myInt: Int? = Int(json1.dictionary!["searchInformation"]!["totalResults"].stringValue)
                       
                        if(json1.dictionary!["searchInformation"]!["totalResults"].stringValue != "0"){
                            let counter: Int? = json1.dictionary!["items"]!.count
                            if(counter != 0)
                            {
                                if let ct = counter{
                                    for index in 0...ct-1{
                                        self.arrTitle.insert(json1["items"][index]["title"].stringValue, atIndex: index)
                                        self.arrSnippet.insert(json1["items"][index]["snippet"].stringValue, atIndex: index)
                                        self.arrLink.insert(json1["items"][index]["link"].stringValue, atIndex: index)
                                        self.arrDisplayLink.insert(json1["items"][index]["displayLink"].stringValue, atIndex: index)
                                    }
                                    
                                    //UI Updating code here.
                                    self.tblSearch.reloadData();
                                    self.tblSearch.setContentOffset(CGPointZero, animated:true)
                                    
                                    self.tblSearch.alpha = 1
                                    self.tblSearch.userInteractionEnabled = true
                                    self.progressBar.hidden = true
                                    self.HUD.hidden = true
                                    self.lblPageNumber.hidden = false
                                    self.lblPageNumber.text = "Page \(self.nextBackCounter/10 + 1)"
                                    
                                    self.progressBar.stopAnimating()
                                    
                                    self.newCnt = ct;
                                    
                                    NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(search.enablePN), userInfo: nil, repeats: false)
                                    
                                    self.lblNoSearchResult.hidden = true
                                }
                                
                                
                            }
                        }else{
                            
                            if(self.nextBackCounter < 10){
                                self.btnPrevious.userInteractionEnabled = false
                                self.btnPrevious.alpha = 0.6
                                self.nextBackCounter = 1
                            }
                            self.btnNext.userInteractionEnabled = false
                            self.btnNext.alpha = 0.6
                            
                            self.tblSearch.alpha = 0
                            self.lblPageNumber.hidden = true
                            self.tblSearch.userInteractionEnabled = false
                            self.lblNoSearchResult.hidden = false
                            self.lblNoSearchResult.text = "Nothing found for \"\(name)\"."
                            self.progressBar.hidden = true
                            self.HUD.hidden = true
                            self.progressBar.stopAnimating()
                            
                            self.arrDisplayLink.removeAll(keepCapacity: false)
                            self.arrSnippet.removeAll(keepCapacity: false)
                            self.arrTitle.removeAll(keepCapacity: false)
                            self.arrLink.removeAll(keepCapacity: false)
                            
                            
                            //UI Updating code here.
                            self.tblSearch.reloadData();
                            self.tblSearch.setContentOffset(CGPointZero, animated:true)
                            
                            
                        }
                    }
                    else
                    {
                        //Searching failed
                        
                        self.appDelegate.networkConnError()
                        
                        self.hideView()
                        
                    }
            }
        }else{
            
            //Searching blank/ Empty search
            //UI Updating code here.
            self.tblSearch.reloadData();
            self.tblSearch.setContentOffset(CGPointZero, animated:true)
            
            self.hideView()
        }
        
        
    }
    
    func enablePN(){
        
        self.btnPrevious.userInteractionEnabled = true;
        
        //  Disable next button when no more results availble in next search
        if(self.newCnt > 9){
            self.btnNext.userInteractionEnabled = true
            self.btnNext.alpha = 1
        }else{
            self.btnNext.userInteractionEnabled = false
            self.btnNext.alpha = 0.6
        }
        if(self.btnNext.userInteractionEnabled){
            self.btnPrevious.userInteractionEnabled = true
            self.btnPrevious.alpha = 1
        }
        
        if(self.nextBackCounter == 1){
            self.btnPrevious.userInteractionEnabled = false
            self.btnPrevious.alpha = 0.6
        }
        
    }
    func hideView()
    {
        self.tblSearch.alpha = 0
        self.tblSearch.userInteractionEnabled = false
        self.progressBar.hidden = true
        self.HUD.hidden = true
        self.progressBar.stopAnimating()
        self.btnPrevious.userInteractionEnabled = false
        self.btnPrevious.alpha = 0.6
        self.btnNext.userInteractionEnabled = false
        self.btnNext.alpha = 0.6
        self.lblPageNumber.hidden = true
        self.lblNoSearchResult.hidden = true
        self.HUD.hidden = true
        
    }
    func setLayerDesign(object:AnyObject)
    {
        object.layer.borderColor = UIColor.grayColor().CGColor
        object.layer.borderWidth = 0.5
        object.layer.cornerRadius = 5
        object.titleLabel?!.adjustsFontSizeToFitWidth = true
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let counter: Int? = self.arrTitle.count;
        
        if let ct = counter{
            if(ct == 10){
                return 10;
            }
        }
        return 0;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        var cell:SearchTVC = SearchTVC()
        
        if(self.arrDisplayLink.count == 10 && self.arrSnippet.count ==  10 && self.arrTitle.count == 10 && self.arrLink.count == 10){
            cell = tblSearch.dequeueReusableCellWithIdentifier("SearchCell") as! SearchTVC
            //cell.lblData.text = self.appleProducts[indexPath.row]
            cell.lblDisplayLink.text = self.arrDisplayLink[indexPath.row]
            cell.lblSnippet.text = self.arrSnippet[indexPath.row]
            cell.lblSnippet.textColor = UIColor.darkGrayColor()
            cell.btnTitle.text = self.arrTitle[indexPath.row]
            cell.btnOpenLink.addTarget(self, action: #selector(search.openLink(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            cell.btnOpenLink.tag = indexPath.row;
            self.setLayerDesign(cell.dataView);
        }
        return cell
    }
    
    
    func openLink(sender:UIButton) {
        //print(sender.tag)
        //print(arrLink[sender.tag])
        
        if #available(iOS 9.0, *) {
            let safariVC = SFSafariViewController(URL:NSURL(string: arrLink[sender.tag])!)
            safariVC.delegate = self
            self.presentViewController(safariVC, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
        }
        tblSearch.reloadData()
    }
    
    @available(iOS 9.0, *)
    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     
     The resource guide provides search results from a list of curated websites. The Division of Aging at Beth Israel Deaconess Medical Center has filtered the websites thought be biased or duplicative and has selected websites considered to be reliable and trustworthy sources of information designed to be helpful to seniors and families.
     
     
     
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     
     print(json1.dictionary!["queries"]!["nextPage"])
     let safe = (json1.dictionary!["queries"]!["nextPage"][0]["safe"])
     let cx = (json1.dictionary!["queries"]!["nextPage"][0]["cx"])
     
     let count = (json1.dictionary!["queries"]!["nextPage"][0]["count"])
     let searchTerms = (json1.dictionary!["queries"]!["nextPage"][0]["searchTerms"])
     
     let startIndex = (json1.dictionary!["queries"]!["nextPage"][0]["startIndex"])
     
     
     /*  let title = (json1.dictionary!["queries"]!["nextPage"][0]["title"])
     let totalResults = (json1.dictionary!["queries"]!["nextPage"][0]["totalResults"])
     let inputEncoding = (json1.dictionary!["queries"]!["nextPage"][0]["inputEncoding"])
     let outputEncoding = (json1.dictionary!["queries"]!["nextPage"][0]["outputEncoding"])
     */
     self.urlForNextPage = "https://www.googleapis.com/customsearch/v1?q=\(searchTerms)&num=\(count)&start=\(startIndex)&lr=lang_ca&safe=\(safe)&cx=\(cx)&cref={cref?}&sort={sort?}&filter=0&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter=e&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType=image&fileType={fileType?}&rights={rights?}&imgSize=small&imgType=clipart&imgColorType=color&imgDominantColor=white&alt=json"
     
     //   self.nextPage(self.urlForNextPage)
     
     */
    
}


