//
//  rplyTableCell.swift
//  infoSege
//
//  Created by Pramod shirsath on 4/3/15.
//  Copyright (c) 2015 Pramod shirsath. All rights reserved.
//

import UIKit

class rplyTableCell: UITableViewCell {

    @IBOutlet weak var btShowFullCommentsMsg: UIButton!
    @IBOutlet weak var btEditMsg: UIButton!
    @IBOutlet weak var replyUserImage: UIImageView!
    @IBOutlet weak var replyUserName: UILabel!
    @IBOutlet weak var replyUserMessage: UILabel!
    @IBOutlet weak var replyUserDT: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
