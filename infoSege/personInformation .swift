//
//  personInformation.swift
//  infoSege
//
//  Created by Pramod shirsath on 3/5/15.
//  Copyright (c) 2015 Pramod shirsath. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CustomTextBoxField: UITextField
{
    
    override func textRectForBounds(bounds: CGRect) -> CGRect { return CGRectInset(bounds, 10.0, 5.0) };
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect { return self.textRectForBounds(bounds) }
}

class RDLabel: UILabel
{
    override func drawTextInRect(rect: CGRect) {
        let newRect = CGRectOffset(rect, 2, 0) // move text 10 points to the right
        super.drawTextInRect(newRect)
    }
}

class personInformation: UIViewController, UIGestureRecognizerDelegate, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UIApplicationDelegate
{
    @IBOutlet weak var viewForChangeAccount: UIView!
    @IBOutlet weak var btnMenuLogout: UIButton!
    @IBOutlet weak var btnMenuMedication: UIButton!
    @IBOutlet weak var firstViewMneu: UIView!
    @IBOutlet var btnMenuPhotos: UIButton!
    @IBOutlet weak var secondViewMneu: UIView!
    @IBOutlet var btHideNot: UIButton!
    @IBOutlet var btNewChangeAcc: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var tbAlert: UITableView!
    @IBOutlet weak var noteCount: UILabel!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var selectedKeystoneName: UILabel!
    @IBOutlet weak var btHome: UIButton!
    @IBOutlet weak var btHideNav: UIButton!
    @IBOutlet weak var btChangeAccOut: UIButton!
    @IBOutlet var tdSelectIcon: UIImageView!
    @IBOutlet var upSelectIcon: UIImageView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var btHelpList: UIButton!
    @IBOutlet weak var btCanHelpList: UIButton!
    @IBOutlet var viewForParticipant: UIView!
    @IBOutlet var viewForPostLine: UIView!
    var pos = 0
    var buttonIndex = 0
    var alertUserName:[String] = []
    var alertUserMessage:[String] = []
    var alertUserProPic:[String] = []
    var alertUserID:[String] = []
    var resolvedStatus:[String] = []
    var forwordLinkId:[String] = []
    var keystoneName:[String] = []
    var keyIDArr:[String] = []
    var keystoneProPic:[String] = []
    var unResolvedNotfID:[String] = []
    var alertNoteDate:[String] = []
    var indexNotification = 0
    var unresolvedCount = "0"
    var isMedicationVisible : String = String()
    var isProxyUser : String = String()
    var isConnected : String = String()
    var VIEW_PHOTOS : String = String()
    var EDIT_PHOTOS : String = String()
    var UPLOAD_PHOTOS : String = String()
    
    var isLoginUserKeystone : Bool = Bool()
    var HUD:MBProgressHUD = MBProgressHUD()
    
    let delObj = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBOutlet var newImgView: UIImageView!
    
    
    var name: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("name")
        }
        set{
            NSUserDefaults.standardUserDefaults().setObject(newValue!, forKey: "name")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    var proPic: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("proPic")
        }
        set{
            NSUserDefaults.standardUserDefaults().setObject(newValue!, forKey: "proPic")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    var keyStoneID_PROXY: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("keyID")
        }
        set{
            NSUserDefaults.standardUserDefaults().setObject(newValue!, forKey: "keyID")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    let defaults = NSUserDefaults.standardUserDefaults()
    var alertconnURL: String = String()
    var proxyUser = ""
    var keystoneID = ""
    
    var CallAlertShows:Bool = Bool()
    
    
    var stringOne = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //print("Printing gloabally declared values: keyID: \(glob_keystoneID) , isProxy : \(glob_viewAsProxy), Loginuserkestone: \(glob_loginUserIsKeystone) ,loguserid:  \(glob_logUserID) ")
        CallAlertShows = true
        
        if((glob_logUserID == glob_keystoneID)||(glob_viewAsProxy)){
            
            //
            
            btCanHelpList.hidden = true;
            btCanHelpList.userInteractionEnabled = false
            
            btHelpList.hidden = false;
            btHelpList.userInteractionEnabled = true
            
            
        }else{
            
            // txtHeaderText.text = "Can You Help With This?"
            btHelpList.hidden = true;
            btHelpList.userInteractionEnabled = false
            btCanHelpList.hidden = false;
            btCanHelpList.userInteractionEnabled = true
        }
        
        
    }
    
    
    //Any Push notification received for the App will refresh the Notifications.
    
    func methodOfReceivedNotification(notification: NSNotification){
        
        alertShows()
        
    }
    
    func updateProfilePhoto(notification: NSNotification){
        
        var imageURL = proPic as! String
        
        let logUserProPic = defaults.valueForKey("logUserProPic") as! String
        
        if(imageURL == "" || imageURL == " ")
        {
            imageURL = logUserProPic
        }
        
        let headers = delObj.headersAll
        
        Alamofire.request(.GET,imageURL,headers:headers).response() {
            (_, _, data, _) in
            
            let image = UIImage(data: data! as NSData)
            
            self.newImgView.image = image
            
        }
        
        
    }
    override func viewWillAppear(animated: Bool) {
        
        self.firstViewMneu.hidden = true
         self.delObj.swipeFlag = false
        glob_currentPageIndex = 0
        
        let viewSwipeLeft: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(personInformation.swipViewLeft))
        
        viewSwipeLeft.direction = UISwipeGestureRecognizerDirection.Right
        
        let viewSwipeRight: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(personInformation.swipViewRight))
        
        viewSwipeRight.direction = UISwipeGestureRecognizerDirection.Left
        
        self.view.addGestureRecognizer(viewSwipeLeft)
        self.view.addGestureRecognizer(viewSwipeRight)
        
        
        //From ViewDidLoad
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(personInformation.methodOfReceivedNotification(_:)), name:"NotificationIdentifier", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(personInformation.updateProfilePhoto(_:)), name:"ProfilePhotoupdated", object: nil)
        
        let checkState = defaults.valueForKey("POST") as! String
        let logUserProPic = defaults.valueForKey("logUserProPic") as! String
        let logUserName = defaults.valueForKey("logUserName") as! String
        let madeState = defaults.valueForKey("MADE") as! String
        
        if(checkState == "true" || madeState == "true")
        {
            btTodoUpdateSegment(self)
        }
        
        
        stringOne = defaults.valueForKey("UID") as! String
        
        proxyUser = defaults.valueForKey("PROXY") as! String
        
        //print("\(proxyUser)");
        
        keystoneID = keyStoneID_PROXY as! String
        
        if(keystoneID == "")
        {
            keystoneID = stringOne
        }
        
        tbAlert.tableFooterView = UIView(frame: CGRectZero)
        
        
        // Visible change account or not
        
        btChangeAccOut.enabled = false
        btChangeAccOut.alpha = 0
        
        btNewChangeAcc.hidden = true
        
        if(proxyUser == "PROXY" && stringOne != keystoneID)
        {
            
            btHome.enabled = false
            btHome.alpha = 0
            viewForChangeAccount.frame.origin.y = btChangeAccOut.frame.origin.y
            btChangeAccOut.frame.origin.y = btHome.frame.origin.y
            btNewChangeAcc.hidden = false
            
        }else
        {
            btHome.enabled = true
            btHome.alpha = 1
            
        }
        
        self.navigationController?.navigationBar.hidden = true
        self.navigationController?.interactivePopGestureRecognizer!.enabled = false;
        self.navigationController?.interactivePopGestureRecognizer!.delegate = self;
        
        
        // Visible Medication for non participnt or not
        
        self.isMedicationVisible = "true"
        
        let headers = delObj.headersAll
        
        Alamofire.Manager.sharedInstance
            .request(.GET, delObj.baseURL + "users/" + keystoneID + "/privileges/users/" + stringOne, parameters: nil, encoding: .JSON, headers:headers)
            .responseJSON {
               response in
                if(response.result.isSuccess)
                {
                    
                    var json1 = JSON(response.result.value!)
                    
                    let count: Int? = json1.array?.count
                    if(count != 0)
                    {
                        self.isMedicationVisible = json1["VIEW_MEDICATIONS"].stringValue as String
                        self.isProxyUser = json1["PROXY"].stringValue as String
                        self.isConnected = json1["CONNECTED"].stringValue as String
                        self.VIEW_PHOTOS = json1["VIEW_PHOTOS"].stringValue as String
                        self.EDIT_PHOTOS = json1["EDIT_PHOTOS"].stringValue as String
                        self.UPLOAD_PHOTOS = json1["UPLOAD_PHOTOS"].stringValue as String
                        self.defaults.setValue(self.VIEW_PHOTOS, forKey:"VIEW_PHOTOS" )
                        self.defaults.setValue(self.EDIT_PHOTOS, forKey:"EDIT_PHOTOS" )
                        self.defaults.setValue(self.UPLOAD_PHOTOS, forKey:"UPLOAD_PHOTOS" )
                        self.defaults.setValue(self.isProxyUser, forKey:"isProxyUser")
                        
                        if(self.isConnected == "true"){
                            if(json1["VIEW_MEDICATIONS"].stringValue == "false")
                            {
                                self.btnMenuMedication.enabled = false
                                self.btnMenuMedication.alpha = 0
                                self.viewForParticipant.frame.origin.y = self.btnMenuMedication.frame.origin.y - 3
                                self.viewForPostLine.hidden = true
                                
                                glob_viewAsParticipant = true
                            }else{
                                glob_viewAsCaregiver = true
                                
                            }
                        }else{
                            print("called from line :308")
                            self.LogoutForProxy()
                            
                            
                        }
                    }
                }else{
                    
                }
        }
        
        
        //check loged user have x-proxy user or not and hide the chng account.
        
        var connURL = delObj.baseURL+"users/"+stringOne+"/privileges/PROXY/outgoing"
        
        Alamofire.Manager.sharedInstance
            .request(.GET, connURL, parameters: nil,  encoding: .JSON,headers:headers)
            .responseJSON {
               response in
                
                //print("Privilege Profile URL:\(request)")
                
                if(response.result.isSuccess)
                {
                    let json1 = JSON(response.result.value!)
                    let count: Int? = json1.array?.count
                    if(count != 0)
                    {
                        //self.btChangeAccOut.alpha = 1
                        self.btNewChangeAcc.hidden = false
                        self.btNewChangeAcc.userInteractionEnabled = true
                        
                    }
                }
                
                self.alertShows()
        }
        
        // set keystone profile pic---------------------------------------------
        
        var imageURL = proPic as! String
        
        if(imageURL == "" || imageURL == " ")
        {
            imageURL = logUserProPic
        }
        
        Alamofire.request(.GET,imageURL,headers:headers).response() {
            (_, _, data, _) in
            
            let image = UIImage(data: data! as NSData)
            
            self.newImgView.image = image
        }
        // get network
        connURL = delObj.baseURL+"users/"+stringOne+"/connections/"
        
        Alamofire.Manager.sharedInstance
            .request(.GET, connURL, parameters: nil , encoding: .JSON, headers:headers)
            .responseJSON {
               response in
                
                //  NSLog("dispatch_group_leave(group)")
                if(response.result.isSuccess)
                {
                    let json1 = JSON(response.result.value!)
                    if(json1.count != 0)
                    {
                        let count: Int? = json1.array?.count
                        if(count != 0)
                        {
                            self.keystoneName.removeAll(keepCapacity: false)
                            self.keyIDArr.removeAll(keepCapacity: false)
                            self.keystoneProPic.removeAll(keepCapacity: false)
                            
                            var indexCount = 0
                            if(json1[0]["owner"]["isElder"].boolValue)
                            {
                                indexCount = 1
                                self.isLoginUserKeystone = true
                                
                                self.keystoneName.insert(json1[0]["owner"]["firstName"].stringValue + " " + json1[0]["owner"]["lastName"].stringValue, atIndex: 0)
                                
                                if(json1[0]["owner"]["profileImage"]["image"]["path"].stringValue == "")
                                {
                                    self.keystoneProPic.insert(self.delObj.baseImageURL + "w168xh168-2/images/default_profile_image.png", atIndex: 0)
                                    
                                }else
                                {
                                    let keyProPic = self.delObj.baseImageURL + "w168xh168-2/images/"+(json1[0]["owner"]["profileImage"]["image"]["path"].stringValue)
                                    self.keystoneProPic.insert(keyProPic, atIndex: 0)
                                }
                                
                                self.keyIDArr.insert(json1[0]["owner"]["id"].stringValue, atIndex: 0)
                                
                            }
                            if let ct = count {
                                for  index in 0...ct-1 {
                                    
                                    
                                    if(json1[index]["target"]["isElder"].boolValue  && (json1[index]["subscribed"].boolValue))
                                    {
                                        
                                        self.keystoneName.insert(json1[index]["target"]["firstName"].stringValue + " " + json1[index]["target"]["lastName"].stringValue, atIndex: indexCount)
                                        self.keyIDArr.insert(json1[index]["target"]["id"].stringValue, atIndex: indexCount)
                                        if(json1[index]["target"]["profileImage"]["image"]["path"].stringValue == "")
                                        {
                                            self.keystoneProPic.insert(self.delObj.baseImageURL + "w168xh168-2/images/default_profile_image.png", atIndex: indexCount)
                                            
                                        }else
                                        {
                                            let keyProPic = self.delObj.baseImageURL + "w168xh168-2/images/"+(json1[index]["target"]["profileImage"]["image"]["path"].stringValue)
                                            self.keystoneProPic.insert(keyProPic, atIndex: indexCount)
                                        }
                                        
                                        indexCount++
                                    }else{
                                        
                                    }
                                }
                            }
                            if(self.keystoneName.count > 1){
                                if(self.proxyUser == "PROXY" && self.stringOne != self.keystoneID)
                                {
                                    // btHome.hidden = true
                                    self.btHome.enabled = false
                                    self.btHome.alpha = 0
                                    
                                }else
                                {
                                    self.btHome.enabled = true
                                    self.btHome.alpha = 1
                                    
                                }
                                
                                
                            }else{
                                self.btHome.enabled = false
                                self.btHome.alpha = 0
                                
                            }
                            
                        }
                    }else
                    {
                        self.btHome.enabled = false
                        self.btHome.alpha = 0
                        /*  self.viewForChangeAccount.frame.origin.y = self.btChangeAccOut.frame.origin.y
                         self.btChangeAccOut.frame.origin.y = self.btHome.frame.origin.y*/
                    }
                }else{
                    
                    self.delObj.networkConnError();
                }
        }
        
        
        Alamofire.Manager.sharedInstance
            .request(.GET, delObj.baseURL+"users/" + keystoneID + "/profile", parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    
                    let json1 = JSON(response.result.value!)
                    
                    if(json1["userMessage"].stringValue == "You don't have permission to do that."){
                        print("called from line :476")
                        self.LogoutForProxy()
                    }else{
                        
                        if(json1["user"]["isElder"].boolValue) {
                            
                            
                        }else{
                            print("called from line :484")
                            self.LogoutForKeystoneStatusChange()
                            
                        }
                        
                        
                    }
                    
                }
                //println(self.userData)
        }
        
        
        
        let strName = name as! String
        if(strName == " " || strName == "")
        {
            selectedKeystoneName.text = logUserName
        }else{
            selectedKeystoneName.text = name as? String
        }
        
        headerView.layer.masksToBounds = true
        headerView.layer.borderColor = UIColor.grayColor().CGColor;
        headerView.layer.borderWidth = 0.5;
        alertView.layer.masksToBounds = true
        alertView.layer.borderColor = UIColor.grayColor().CGColor;
        alertView.layer.borderWidth = 0.5;
        
        btHideNotification(self)
        
    }
    
    
    //MARK: Open menu
    
    func swipViewLeft() {
        
        view.endEditing(true)
        btHideNotification(self)
        self.delObj.swipeFlag = true
        
        //check loged user have x-proxy user or not and Hide/Show the Change Account button.
        
        let connURL = delObj.baseURL+"users/"+glob_logUserID+"/privileges/PROXY/outgoing"
        let headers = delObj.headersAll
        
        
        Alamofire.Manager.sharedInstance
            .request(.GET, connURL, parameters: nil,  encoding: .JSON,headers:headers)
            .responseJSON {
               response in
                
                if(response.result.isSuccess)
                {
                    let json1 = JSON(response.result.value!)
                    let count: Int? = json1.array?.count
                    
                    if(count != 0)
                    {
                        //self.btChangeAccOut.alpha = 1
                        self.btNewChangeAcc.hidden = false
                        self.btNewChangeAcc.userInteractionEnabled = true
                        
                    }else{
                        
                        //                        self.defaults.setValue("", forKey:"isProxyUser")
                        //                        self.defaults.setValue("", forKey:"PROXY")
                        self.btNewChangeAcc.hidden = true
                        self.btNewChangeAcc.userInteractionEnabled = false
                        
                    }
                }
        }
        
        // get priviliges by user to Keystone
        
        if(keystoneID !=  stringOne){
            Alamofire.Manager.sharedInstance
                .request(.GET, delObj.baseURL + "users/" + keystoneID + "/privileges/users/" + stringOne, parameters: nil, encoding: .JSON, headers:headers)
                .responseJSON {
                   response in
                    if(response.result.isSuccess)
                    {
                        
                        var json1 = JSON(response.result.value!)
                        let count: Int? = json1.array?.count
                        if(count != 0)
                        {
                            self.isMedicationVisible = json1["VIEW_MEDICATIONS"].stringValue as String
                            let isProxyUser = json1["PROXY"].stringValue as String
                            
                            //***
                            let isViewAsProxy = self.defaults.valueForKey("PROXY") as! String
                            
                            if(isViewAsProxy == "PROXY"){
                                
                                if(isProxyUser == "false"){
                                    
                                    self.LogoutForProxy()
                                    print("called from line :585")
                                    
                                }
                            }
                            //****
                            
                            //  self.defaults.setValue(self.isProxyUser, forKey:"isProxyUser")
                            
                            let isConnected = json1["CONNECTED"].stringValue
                            
                            if(isConnected == "false" )
                            {
                                print("called from line :549")
                                self.LogoutForProxy()
                                
                            }
                            
                            if(self.isMedicationVisible == "false")
                            {
                                self.btnMenuMedication.enabled = false
                                self.btnMenuMedication.alpha = 0
                                self.viewForParticipant.frame.origin.y = self.btnMenuMedication.frame.origin.y - 4
                                self.viewForPostLine.hidden = true
                            }else{
                                
                                self.btnMenuMedication.enabled = true
                                self.btnMenuMedication.alpha = 1
                                self.viewForParticipant.frame.origin.y = self.btnMenuMedication.frame.origin.y + self.btnMenuMedication.frame.height + 1
                                self.viewForPostLine.hidden = false
                                
                                // Visible Medication for non participnt or not
                            }
                        }
                    }else{
                        
                    }
            }
        }
        
        //Fetch Profile of KEYSTONE  to check its keystone status
        
        Alamofire.Manager.sharedInstance
            .request(.GET, delObj.baseURL+"users/" + keystoneID + "/profile", parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    
                    let json1 = JSON(response.result.value!)
                    
                    if(json1["userMessage"].stringValue != "You must be logged in to do that." ){
                        if(json1["userMessage"].stringValue == "You don't have permission to do that."){
                            print("called from line :586")
                            self.LogoutForProxy()
                        }else{
                            
                            if(json1["user"]["isElder"].boolValue) {
                                
                                
                            }else{
                                print("called from line :594")
                                self.LogoutForKeystoneStatusChange()
                                
                            }
                            
                        }
                    }else
                    {
                        self.delObj.checkLogin()
                    }
                    
                }
        }
        
        
        
        // get complte network
        
        let  connURL1 = delObj.baseURL+"users/"+stringOne+"/connections/"
        
        Alamofire.Manager.sharedInstance
            .request(.GET, connURL1, parameters: nil , encoding: .JSON, headers:headers)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    let json1 = JSON(response.result.value!)
                    if(json1.count != 0)
                    {
                        let count: Int? = json1.array?.count
                        if(count != 0)
                        {
                            self.keystoneName.removeAll(keepCapacity: false)
                            self.keyIDArr.removeAll(keepCapacity: false)
                            self.keystoneProPic.removeAll(keepCapacity: false)
                            
                            var indexCount = 0
                            if(json1[0]["owner"]["isElder"].boolValue)
                            {
                                indexCount = 1
                                
                                self.isLoginUserKeystone = true
                                
                                glob_loginUserIsKeystone = true
                                
                                self.keystoneName.insert(json1[0]["owner"]["firstName"].stringValue + " " + json1[0]["owner"]["lastName"].stringValue, atIndex: 0)
                                
                                if(json1[0]["owner"]["profileImage"]["image"]["path"].stringValue == "")
                                {
                                    self.keystoneProPic.insert(self.delObj.baseImageURL + "w168xh168-2/images/default_profile_image.png", atIndex: 0)
                                    
                                }else
                                {
                                    let keyProPic = self.delObj.baseImageURL + "w168xh168-2/images/"+(json1[0]["owner"]["profileImage"]["image"]["path"].stringValue)
                                    self.keystoneProPic.insert(keyProPic, atIndex: 0)
                                }
                                
                                self.keyIDArr.insert(json1[0]["owner"]["id"].stringValue, atIndex: 0)
                                
                            }else{
                                
                                if(glob_loginUserIsKeystone){
                                    
                                    glob_loginUserIsKeystone = false
                                    
                                    print("called from line :656")
                                    self.LogoutForKeystoneStatusChange()
                                }
                                
                            }
                            
                            if let ct = count {
                                for  index in 0...ct-1 {
                                    if(json1[index]["target"]["isElder"].boolValue  && (json1[index]["subscribed"].boolValue))
                                    {
                                        
                                        self.keystoneName.insert(json1[index]["target"]["firstName"].stringValue + " " + json1[index]["target"]["lastName"].stringValue, atIndex: indexCount)
                                        self.keyIDArr.insert(json1[index]["target"]["id"].stringValue, atIndex: indexCount)
                                        if(json1[index]["target"]["profileImage"]["image"]["path"].stringValue == "")
                                        {
                                            self.keystoneProPic.insert(self.delObj.baseImageURL + "w168xh168-2/images/default_profile_image.png", atIndex: indexCount)
                                            
                                        }else
                                        {
                                            let keyProPic = self.delObj.baseImageURL + "w168xh168-2/images/"+(json1[index]["target"]["profileImage"]["image"]["path"].stringValue)
                                            self.keystoneProPic.insert(keyProPic, atIndex: indexCount)
                                        }
                                        
                                        indexCount += 1
                                    }else{
                                        
                                    }
                                }
                            }
                            if(self.keystoneName.count > 1){
                                if(self.proxyUser == "PROXY" && self.stringOne != self.keystoneID)
                                {
                                    // btHome.hidden = true
                                    self.btHome.enabled = false
                                    self.btHome.alpha = 0
                                    
                                }else
                                {
                                    self.btHome.enabled = true
                                    self.btHome.alpha = 1
                                    
                                }
                                
                                
                            }else{
                                self.btHome.enabled = false
                                self.btHome.alpha = 0
                                
                            }
                            
                        }
                    }else
                    {
                        self.btHome.enabled = false
                        self.btHome.alpha = 0
                    }
                    
                    
                    if(!(self.firstViewMneu.frame.origin.x == 0)){
                        UIView.animateWithDuration(0.5, animations: {
                            self.firstViewMneu.hidden = false
                            self.firstViewMneu.frame.origin.x = 0
                            self.btHideNav.alpha = 0.5
                            self.secondViewMneu.frame.origin.x = self.firstViewMneu.frame.size.width
                            }, completion: { (finished) -> Void in
                        })
                    }
                }else{
                    
                    if(!(self.firstViewMneu.frame.origin.x == 0)){
                        UIView.animateWithDuration(0.5, animations: {
                            self.firstViewMneu.hidden = false
                            self.firstViewMneu.frame.origin.x = 0
                            self.btHideNav.alpha = 0.5
                            self.secondViewMneu.frame.origin.x = self.firstViewMneu.frame.size.width
                            }, completion: { (finished) -> Void in
                        })
                    }
                    
                    self.delObj.networkConnError();
                }
        }
        
    }
    
    //MARK: Close menu
    
    func swipViewRight() {
        
        if(self.delObj.swipeFlag){
            self.delObj.swipeFlag = false
        btHideNotification(self)
        view.endEditing(true)
        
        if((self.firstViewMneu.frame.origin.x == 0)){
            UIView.animateWithDuration(0.5, animations: {
                //   self.firstViewMneu.hidden = true
                self.firstViewMneu.frame.origin.x = -(self.firstViewMneu.frame.size.width)
                self.secondViewMneu.frame.origin.x = 0
                self.btHideNav.alpha = 0.0
                }, completion: { (finished) -> Void in
            })
        }
        
        print(glob_currentPageIndex)
        
        
        
        let viewControllerIdentifiers = ["toDo", "upDate","idActivInactiveMedication","idphotosView",
                                         "idSearchView"]
        // storyboard identifiers for the child view controllers
        
        let newController = storyboard!.instantiateViewControllerWithIdentifier(viewControllerIdentifiers[glob_currentPageIndex])
        
        let oldController = childViewControllers.last
        newController.view.frame = oldController!.view.frame
        oldController!.willMoveToParentViewController(nil)
        addChildViewController(newController)
        
        transitionFromViewController(oldController!, toViewController: newController, duration: 0, options: .TransitionCrossDissolve, animations:{ () -> Void in
            // nothing needed here
            }, completion: { (finished) -> Void in
                oldController!.removeFromParentViewController()
                newController.didMoveToParentViewController(self)
        })
        }else{
            print("Ohhh..>! ")
        }
    }
    @IBAction func btnEVMenuOption(sender: AnyObject) {
        
        self.swipViewLeft()
        
        
    }
    @IBAction func btnEVClosLeftView(sender: AnyObject) {
        
       self.swipViewRight()
    }
    
    //shows user alerts ----------------------------------------------------
    func LogoutForProxy()
    {
        
        let alert = UIAlertController(title: "Your privileges have been modified for your Keystone(s). You will be re-logged in now.", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:
            {
                (UIAlertAction)in Void.self
                
                var tokanid = self.defaults.valueForKey("deviceToken") as? String
                
                if(tokanid == nil){
                    tokanid = "h433897fg77873456fg868389"
                }
                
                
                defer{
                    self.doLogout(tokanid!, stringUserID: self.stringOne);
                }
                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idLoginViewController") as! ViewController
                self.navigationController?.pushViewController(nextView, animated: true)
                
                
        }))
        
        //        alert.addAction(UIAlertAction (title: "Close", style: .Destructive, handler: { (UIAlertAction) in
        //
        //        }))
        
        dispatch_async(dispatch_get_main_queue(), {
            
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
        
    }
    
    //shows user alerts ----------------------------------------------------
    func LogoutForKeystoneStatusChange()
    {
        
        let alert = UIAlertController(title: "Your Keystone status has changed. You will be re-logged in now.", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:
            {
                (UIAlertAction)in Void.self
                
                var tokanid = self.defaults.valueForKey("deviceToken") as? String
                
                if(tokanid == nil){
                    tokanid = "h433897fg77873456fg868389"
                }
                
                
                defer{
                    self.doLogout(tokanid!, stringUserID: self.stringOne);
                }
                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idLoginViewController") as! ViewController
                self.navigationController?.pushViewController(nextView, animated: true)
                
                
        }))
        
        dispatch_async(dispatch_get_main_queue(), {
            
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
        
    }
    
    
    func alertShows()
    {
        if(CallAlertShows){
            
            CallAlertShows = false;
            
            alertUserName.removeAll(keepCapacity: false)
            alertUserMessage.removeAll(keepCapacity: false)
            alertUserProPic.removeAll(keepCapacity: false)
            alertUserID.removeAll(keepCapacity: false)
            resolvedStatus.removeAll(keepCapacity: false)
            forwordLinkId.removeAll(keepCapacity: false)
            alertNoteDate.removeAll(keepCapacity: false)
            //   self.proForNotification.hidden = false
            
            self.notificationView.hidden = true
            if(proxyUser == "PROXY")
            {
                alertconnURL = delObj.baseURL + "users/"+keystoneID+"/notifications"
                
                //print("URL keystoneID: \(keystoneID)")
                
                if((keystoneID) != ""){
                    
                    let headers = delObj.headersAll
                    Alamofire.Manager.sharedInstance
                        .request(.GET, alertconnURL, parameters: nil ,headers: headers, encoding: .JSON)
                        .responseJSON {
                           response in
                            
                            //print("notifications url2: \(request)")
                            if(response.result.isSuccess)
                            {
                                //background thread
                                let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
                                dispatch_async(dispatch_get_global_queue(priority, 0)) {
                                    
                                    let json1 = JSON(response.result.value!)
                                    
                                    let count1: Int? = json1["notifications"].array?.count
                                    if(count1 != 0)
                                    {
                                        self.alertUserName = []
                                        self.alertUserMessage = []
                                        self.alertUserProPic = []
                                        self.alertUserID = []
                                        self.resolvedStatus = []
                                        self.forwordLinkId = []
                                        self.alertNoteDate = []
                                        
                                        var indexCount = 0
                                        if let ct = count1 {
                                            for  index in 0...ct-1 {
                                                if(!json1["notifications"][index]["resolved"].boolValue)
                                                {  self.unResolvedNotfID.insert(json1["notifications"][index]["id"].stringValue, atIndex: indexCount)
                                                }
                                                else{ self.unResolvedNotfID.insert("0", atIndex: indexCount)
                                                }
                                                
                                                let dateString:NSTimeInterval =  json1["notifications"][index]["date"].doubleValue / 1000
                                                let  dateformat : NSDateFormatter = NSDateFormatter()
                                                dateformat.setLocalizedDateFormatFromTemplate("EEE, dd MMM yyyy hh:mm a")
                                                
                                                let stringDate = dateformat.stringFromDate(NSDate(timeIntervalSince1970: dateString))
                                                
                                                self.alertNoteDate.insert("\(stringDate)", atIndex: index)
                                                
                                                self.alertUserName.insert(json1["notifications"][index]["name"].stringValue , atIndex: indexCount)
                                                
                                                self.resolvedStatus.insert(json1["notifications"][index]["resolved"].stringValue , atIndex: indexCount)
                                                
                                                self.alertUserMessage.insert(json1["notifications"][index]["text"].stringValue , atIndex: indexCount)
                                                
                                                let linkID = json1["notifications"][index]["forwardLink"].stringValue
                                                
                                                var str : NSCharacterSet = NSCharacterSet(charactersInString: "/public/")
                                                
                                                let stringlength = (linkID.characters.count)
                                                
                                                var ierror: NSError?
                                                
                                                let regex = try! NSRegularExpression(pattern: "([-]|[ /]|[A-Z]|[a-z])",
                                                    options: [.CaseInsensitive])
                                                
                                                
                                                let modString = regex.stringByReplacingMatchesInString(linkID, options: NSMatchingOptions(), range: NSMakeRange(0, stringlength), withTemplate: "")
                                                
                                                self.forwordLinkId.insert(modString, atIndex: indexCount)
                                                if(json1["notifications"][index]["image"]["path"].stringValue == "")
                                                {
                                                    self.alertUserProPic.insert(self.delObj.baseImageURL + "w50xh50-2/images/default_profile_image.png", atIndex: indexCount)
                                                    
                                                }else
                                                {
                                                    let userProPic = self.delObj.baseImageURL + "w50xh50-2/images/"+(json1["notifications"][index]["image"]["path"].stringValue)
                                                    self.alertUserProPic.insert(userProPic, atIndex: indexCount)
                                                }
                                                
                                                self.alertUserID.insert(json1["notifications"][index]["id"].stringValue, atIndex: indexCount)
                                                // }
                                                indexCount++
                                            }
                                            
                                            //main
                                            dispatch_async(dispatch_get_main_queue()) {
                                                let count2 = json1["numUnresolved"].stringValue
                                                //    //println("count is \(count2)")
                                                if(count2 != "0")
                                                {
                                                    self.unresolvedCount = count2
                                                    self.notificationView.hidden = false
                                                    //   self.proForNotification.hidden = true
                                                    self.noteCount.text = count2 }
                                                else{
                                                    self.notificationView.hidden = true
                                                    //  self.proForNotification.hidden = true
                                                }
                                                self.tbAlert.reloadData()
                                                
                                                NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: #selector(personInformation.canCallAlertShows), userInfo: nil, repeats: false)
                                                //    self.proForNotification.hidden = true
                                                
                                            }
                                            //end main
                                        }
                                    }
                                } //end background thread
                                
                            }
                            else{
                                NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(personInformation.canCallAlertShows), userInfo: nil, repeats: false)
                                self.delObj.networkConnError();
                            }
                    }
                    
                }else{
                    NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(personInformation.canCallAlertShows), userInfo: nil, repeats: false)
                    
                }
                
            }else
            {
                alertconnURL = delObj.baseURL + "users/"+stringOne+"/notifications"
                
                //print("URL stringOne: \(stringOne)")
                
                if((stringOne) != ""){
                    
                    let headers = delObj.headersAll
                    Alamofire.Manager.sharedInstance
                        .request(.GET, alertconnURL, parameters: nil ,headers: headers, encoding: .JSON)
                        .responseJSON {
                            response in
                            
                            //print("notifications url2: \(request)")
                            if(response.result.isSuccess)
                            {
                                //background thread
                                let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
                                dispatch_async(dispatch_get_global_queue(priority, 0)) {
                                    
                                    let json1 = JSON(response.result.value!)
                                    
                                    let count1: Int? = json1["notifications"].array?.count
                                    if(count1 != 0)
                                    {
                                        self.alertUserName = []
                                        self.alertUserMessage = []
                                        self.alertUserProPic = []
                                        self.alertUserID = []
                                        self.resolvedStatus = []
                                        self.forwordLinkId = []
                                        self.alertNoteDate = []
                                        
                                        var indexCount = 0
                                        if let ct = count1 {
                                            for  index in 0...ct-1 {
                                                if(!json1["notifications"][index]["resolved"].boolValue)
                                                {  self.unResolvedNotfID.insert(json1["notifications"][index]["id"].stringValue, atIndex: indexCount)
                                                }
                                                else{ self.unResolvedNotfID.insert("0", atIndex: indexCount)
                                                }
                                                
                                                let dateString:NSTimeInterval =  json1["notifications"][index]["date"].doubleValue / 1000
                                                let  dateformat : NSDateFormatter = NSDateFormatter()
                                                dateformat.setLocalizedDateFormatFromTemplate("EEE, dd MMM yyyy hh:mm a")
                                                
                                                let stringDate = dateformat.stringFromDate(NSDate(timeIntervalSince1970: dateString))
                                                
                                                self.alertNoteDate.insert("\(stringDate)", atIndex: index)
                                                
                                                self.alertUserName.insert(json1["notifications"][index]["name"].stringValue , atIndex: indexCount)
                                                
                                                self.resolvedStatus.insert(json1["notifications"][index]["resolved"].stringValue , atIndex: indexCount)
                                                
                                                self.alertUserMessage.insert(json1["notifications"][index]["text"].stringValue , atIndex: indexCount)
                                                
                                                let linkID = json1["notifications"][index]["forwardLink"].stringValue
                                                
                                                var str : NSCharacterSet = NSCharacterSet(charactersInString: "/public/")
                                                
                                                let stringlength = (linkID.characters.count)
                                                
                                                var ierror: NSError?
                                                
                                                let regex = try! NSRegularExpression(pattern: "([-]|[ /]|[A-Z]|[a-z])",
                                                    options: [.CaseInsensitive])
                                                
                                                
                                                let modString = regex.stringByReplacingMatchesInString(linkID, options: NSMatchingOptions(), range: NSMakeRange(0, stringlength), withTemplate: "")
                                                
                                                self.forwordLinkId.insert(modString, atIndex: indexCount)
                                                if(json1["notifications"][index]["image"]["path"].stringValue == "")
                                                {
                                                    self.alertUserProPic.insert(self.delObj.baseImageURL + "w50xh50-2/images/default_profile_image.png", atIndex: indexCount)
                                                    
                                                }else
                                                {
                                                    let userProPic = self.delObj.baseImageURL + "w50xh50-2/images/"+(json1["notifications"][index]["image"]["path"].stringValue)
                                                    self.alertUserProPic.insert(userProPic, atIndex: indexCount)
                                                }
                                                
                                                self.alertUserID.insert(json1["notifications"][index]["id"].stringValue, atIndex: indexCount)
                                                // }
                                                indexCount += 1
                                            }
                                            
                                            //main
                                            dispatch_async(dispatch_get_main_queue()) {
                                                let count2 = json1["numUnresolved"].stringValue
                                                //    //println("count is \(count2)")
                                                if(count2 != "0")
                                                {
                                                    self.unresolvedCount = count2
                                                    self.notificationView.hidden = false
                                                    //   self.proForNotification.hidden = true
                                                    self.noteCount.text = count2 }
                                                else{
                                                    self.notificationView.hidden = true
                                                    //  self.proForNotification.hidden = true
                                                }
                                                self.tbAlert.reloadData()
                                                NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: #selector(personInformation.canCallAlertShows), userInfo: nil, repeats: false)
                                                //    self.proForNotification.hidden = true
                                                
                                            }
                                            //end main
                                        }
                                    }
                                } //end background thread
                                
                            }
                            else{
                                
                                NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: #selector(personInformation.canCallAlertShows), userInfo: nil, repeats: false)
                                
                                self.delObj.networkConnError();
                            }
                    }
                    
                }else{
                    //print("Null userID")
                    
                    NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: #selector(personInformation.canCallAlertShows), userInfo: nil, repeats: false)
                }
                
            }
        }
        
    }
    
    
    // NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(photosView.loadAllUserPhoto), userInfo: nil, repeats: false)
    
    func canCallAlertShows()
    {
        
        self.CallAlertShows = true;
        
    }
    
    @IBAction func btnEVBackViewController(sender: AnyObject) {
        
        
    }
    
    @IBAction func btHideNotification(sender: AnyObject) {
        pos = 0
        UIView.animateWithDuration(0.3, animations: {
            
            self.alertView.frame.origin.y = -self.alertView.frame.size.height
            self.alertView.hidden = true
            }, completion:nil)
        
    }
    
    func resolveNotification()
    {
        if(self.unresolvedCount != "0")
        {
            if(unResolvedNotfID.count > indexNotification)
            {
                if(unResolvedNotfID[indexNotification] != "0")
                {
                    if(proxyUser == "PROXY")
                    {  alertconnURL = delObj.baseURL+"users/"+keystoneID+"/notifications/"+unResolvedNotfID[indexNotification]
                    }else
                    {  alertconnURL = delObj.baseURL+"users/"+stringOne+"/notifications/"+unResolvedNotfID[indexNotification]
                    }
                    //   //println(alertconnURL)
                    let headers = delObj.headersAll
                    Alamofire.Manager.sharedInstance
                        .request(.PUT, alertconnURL, parameters: nil ,headers: headers, encoding: .JSON)
                        .responseJSON{
                           response in
                            self.indexNotification = self.indexNotification + 1
                            self.resolveNotification()
                    }
                }else{
                    if(unResolvedNotfID.count > indexNotification)
                    {self.indexNotification = self.indexNotification + 1
                        resolveNotification()}
                }
            }
        }
    }
    
    @IBAction func btNotification(sender: AnyObject) {
        
        if(alertUserID.count != 0)
        {
            resolveNotification()
            self.notificationView.hidden = true
            if(pos == 0)
            {
                pos = 1
                UIView.animateWithDuration(0.3, animations: {
                    self.alertView.frame.origin.y = self.headerView.frame.origin.y + self.headerView.frame.size.height - 0.5
                    self.alertView.hidden = false
                    }, completion:nil)
            }else
            {
                pos = 0
                UIView.animateWithDuration(0.3, animations: {
                    
                    self.alertView.frame.origin.y = -self.alertView.frame.size.height
                    self.alertView.hidden = true
                    }, completion:nil)
            }
        }else
        {
            // self.proForNotification.hidden = true
            let alertController = UIAlertController(title: "You have no Notifications", message: "", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    //------------------------------------------------------------------------------
    
    //MARK:button for go to home page------------------------------------------------
    @IBAction func goHomePage(sender: AnyObject) {
        
        delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
        defaults.setValue("", forKey:"PROXY" )
        
        let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idHomePage") as! homePage
        self.navigationController?.pushViewController(nextView, animated: true)
        
        
        
        
    }
    
    //MARK:button for change loged user or go to proxy user----------------------------------
    @IBAction func changeAccount(sender: AnyObject) {
        
        // change as X-proxy user
        
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setValue("PROXY", forKey:"PROXY" )
        let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idHomePage") as! homePage
        self.navigationController?.pushViewController(nextView, animated: true)
        
    }
    
    //MARK: button for logout loged user------------------------------------------------
    @IBAction func btLogout(sender: AnyObject) {
        
        let headers = delObj.headersAll
        stringOne = defaults.valueForKey("UID") as! String
        
        let alert = UIAlertController(title: "Confirm to Logout", message: "Are you sure you want to Logout?", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler:nil))
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler:
            {
                (UIAlertAction)in Void.self
                
                let tokanid = self.defaults.valueForKey("deviceToken") as? String
                //print("Takanid\(tokanid)")
                if(tokanid == nil){
                    
                    
                    let loginmanager:FBSDKLoginManager=FBSDKLoginManager()
                    let signIn = GPPSignIn.sharedInstance()
                    
                    loginmanager.logOut()
                    signIn?.signOut()
                    
                    Alamofire.request(.POST, self.delObj.baseURL+"logout/"+self.stringOne,headers:headers)
                        .responseJSON {
                           response in
                            
                            //print("Logout: \(request)  ")
                            
                            let st = response.response?.statusCode
                            
                            if(st == 204)
                            {
                                self.delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
                                
                                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idLoginViewController") as! ViewController
                                self.navigationController?.pushViewController(nextView, animated: true)
                            }else
                            {
                                self.showLogoutAlert()
                            }
                    }
                    
                    self.defaults.setValue("", forKey:"PROXY" )
                    self.defaults.setValue("", forKey:"UserName" )
                    self.defaults.setValue("", forKey:"UID" )
                    self.defaults.setValue("", forKey:"Password" )
                    self.defaults.setValue("Logout", forKey:"fbG+accessToken" )
                    
                    
                }else
                {
                    
                    Alamofire.request(.POST, self.delObj.baseURL + "users/" + self.stringOne + "/push-notifications/apns/unregister", parameters: ["string": tokanid!], headers: headers,encoding: .JSON)
                        .responseJSON {
                            response in
                            
                            let loginmanager:FBSDKLoginManager=FBSDKLoginManager()
                            let signIn = GPPSignIn.sharedInstance()
                            
                            loginmanager.logOut()
                            signIn?.signOut()
                            
                            Alamofire.request(.POST, self.delObj.baseURL+"logout/"+self.stringOne,headers:headers)
                                .responseJSON {
                                    response in
                                    
                                    //print("Logout: \(request)  ")
                                    
                                    let st = response.response?.statusCode
                                    
                                    if(st == 204)
                                    {
                                        self.delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
                                        
                                        let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idLoginViewController") as! ViewController
                                        self.navigationController?.pushViewController(nextView, animated: true)
                                    }else
                                    {
                                        self.showLogoutAlert()
                                    }
                            }
                            
                            self.defaults.setValue("", forKey:"PROXY" )
                            self.defaults.setValue("", forKey:"UserName" )
                            self.defaults.setValue("", forKey:"UID" )
                            self.defaults.setValue("", forKey:"Password" )
                            self.defaults.setValue("Logout", forKey:"fbG+accessToken" )
                    }
                }
                
        }))
        
        self.presentViewController(alert, animated: true, completion: { })
        
    }
    
    func showLogoutAlert() {
        let alertController1 = UIAlertController(title: "Logout Failed..!!", message: "InfoSAGE failed to logout. Please try after some time.", preferredStyle: UIAlertControllerStyle.Alert)
        alertController1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController1, animated: true, completion: {})
    }
    
    
    //Mark: Conditional logout
    
    func doLogout(let tokanid:String, stringUserID:String)
    {
        
        delObj.checkLogin();
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated..
    }
    
    //
    
    //MARK:segements controller for todo and update list-----------------------------------------
    @IBAction func btTodoUpdateSegment(sender: AnyObject) {
        self.view.endEditing(true);
        
        self.btHideNotification(self)
        
        UIView.animateWithDuration(0.5, animations: {
            self.firstViewMneu.frame.origin.x = -(self.firstViewMneu.frame.size.width)
            self.secondViewMneu.frame.origin.x = 0
            self.btHideNav.alpha = 0.0
            }, completion: nil)
        
        
        //self.alertShows()
        
        let checkState = defaults.valueForKey("POST") as! String
        let madeState = defaults.valueForKey("MADE") as! String
        
        if(checkState == "true")
        {
            buttonIndex = 1
            self.defaults.setValue("false", forKey:"POST" )
            //Update List
        }else if(madeState == "true")
        {
            buttonIndex = 2
            self.defaults.setValue("false", forKey:"MADE" )
            //Medication
        }else
        {
            
            buttonIndex = sender.tag
            
            //Todo list,Photos,Search.
            
        }
        
        if(buttonIndex > 4){
            let notificationType = UIApplication.sharedApplication().currentUserNotificationSettings()!.types
            
            if notificationType == UIUserNotificationType.None {
                //self.alertShows()
                
            }
            buttonIndex = 0
        }
        
        let viewControllerIdentifiers = ["toDo", "upDate","idActivInactiveMedication","idphotosView",
                                         "idSearchView"]
        // storyboard identifiers for the child view controllers
        glob_currentPageIndex = buttonIndex;
        self.delObj.swipeFlag = false
        
        let newController = storyboard!.instantiateViewControllerWithIdentifier(viewControllerIdentifiers[buttonIndex])
        //as! UIViewController
        //print("CLICKED > \(viewControllerIdentifiers[buttonIndex])")
        let oldController = childViewControllers.last //as! UIViewController
        newController.view.frame = oldController!.view.frame
        oldController!.willMoveToParentViewController(nil)
        addChildViewController(newController)
        
        transitionFromViewController(oldController!, toViewController: newController, duration: 0, options: .TransitionCrossDissolve, animations:{ () -> Void in
            // nothing needed here
            }, completion: { (finished) -> Void in
                oldController!.removeFromParentViewController()
                newController.didMoveToParentViewController(self)
        })
    }
    
    //all tables methos and delegets and load all notifiaction ---------------------------
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return alertUserName.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let headers = delObj.headersAll
        var cell:myAlertViewCell = myAlertViewCell()
        cell = tbAlert.dequeueReusableCellWithIdentifier("alertCell") as! myAlertViewCell
        cell.alrtUserName.text = alertUserName[indexPath.row]
        cell.alrtUserMessage.text = alertUserMessage[indexPath.row]
        cell.lblNoteDate.text = alertNoteDate[indexPath.row]
        if(resolvedStatus[indexPath.row] == "true")
        {
            cell.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 1)
        }else
        {
            cell.backgroundColor = UIColor(red: 223/255, green: 240/255, blue: 238/255, alpha: 1)
        }
        Alamofire.request(.GET, alertUserProPic[indexPath.row], headers:headers).response() {
            (_, _, data, _) in
            
            let image = UIImage(data: data! as NSData)
            cell.alrtUserImage.image = image
            
        }
        /*if(alertUserName.count < 5 && alertUserName.count-1 == indexPath.row && pos == 0)
         {
         alertView.frame.size.height = cell.contentView.frame.size.height * 7 + btHideNot.frame.size.height * 2
         btHideNot.frame.origin.y = alertView.frame.size.height - btHideNot.frame.size.height
         }
         else{
         alertView.frame.size.height = alertView.frame.size.height
         btHideNot.frame.origin.y = btHideNot.frame.origin.y
         }*/
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        var alert = UIAlertController()
        if(keyIDArr.contains(self.forwordLinkId[indexPath.row]))
        {
            for index in 0...keyIDArr.count-1
            {
                if(self.forwordLinkId[indexPath.row] == keyIDArr[index])
                {
                    self.name = keystoneName[index]
                    self.proPic = keystoneProPic[index]
                }else{
                    
                }
            }
            
            self.keyStoneID_PROXY = self.forwordLinkId[indexPath.row]
            let id = self.forwordLinkId[indexPath.row]
            //println("forwordLinkId id:-\(id)")
            let statePost = self.alertUserName[indexPath.row].rangeOfString("post")
            let statePost1 = self.alertUserName[indexPath.row].rangeOfString("posts")
            let medState1 = self.alertUserName[indexPath.row].rangeOfString("medication")
            let medState = self.alertUserName[indexPath.row].rangeOfString("medications")
            
            //print("Notifications: \(alertUserName)")
            //print("Home button hidden \(self.btHome.alpha)")
            
            
            
            let isElder = self.defaults.valueForKey("isElderStatus") as! String
            
            if(proxyUser == "PROXY")
            { alertconnURL = delObj.baseURL+"users/"+keystoneID+"/notifications/"+alertUserID[indexPath.row]
                //print("alertconnURL as proxy:\(alertconnURL)")
            }else
            { alertconnURL = delObj.baseURL+"users/"+stringOne+"/notifications/"+alertUserID[indexPath.row]
                //print("alertconnURL not as proxy:\(alertconnURL)")
                
            }
            //println("Contains")
            let headers = delObj.headersAll
            Alamofire.Manager.sharedInstance
                .request(.PUT, alertconnURL, parameters: nil , headers: headers,encoding: .JSON)
                .responseJSON {
                    response in
                    //print("notifications url1: \(request)")
                    if(response.result.isSuccess)
                    {
                        self.pos = 0
                        
                        //Commented by vijay
                        
                        self.alertShows()
                        if(id != "")
                        {
                            if(id != self.stringOne || isElder == "true" )
                            {
                                if(statePost != nil || statePost1 != nil)
                                {
                                    self.defaults.setValue("true", forKey:"POST" )
                                }else
                                {
                                    self.defaults.setValue("false", forKey:"POST" )
                                }
                                if((medState != nil || medState1 != nil) && self.isMedicationVisible == "true")
                                {
                                    self.defaults.setValue("true", forKey:"MADE" )
                                    //  self.btTodoUpdateSegment(self)
                                }else
                                {
                                    self.defaults.setValue("false", forKey:"MADE" )
                                    
                                }
                                
                                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idPersonalInforamation") as! personInformation
                                self.navigationController?.pushViewController(nextView, animated: true)
                                /**/
                                
                            }
                            else{
                                /*Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]*/
                                
                                self.delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
                                
                                self.defaults.setValue("", forKey:"PROXY" )
                                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idHomePage") as! homePage
                                self.navigationController?.pushViewController(nextView, animated: true)
                            }
                        }else{
                            /* Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]*/
                            self.delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
                            
                            self.defaults.setValue("", forKey:"PROXY" )
                            let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idHomePage") as! homePage
                            self.navigationController?.pushViewController(nextView, animated: true)
                        }
                        UIView.animateWithDuration(0.3, animations: {
                            
                            self.alertView.frame.origin.y = -self.alertView.frame.size.height
                            self.alertView.hidden = true
                            }, completion:nil)
                        
                    }
                    else{
                        self.delObj.networkConnError();
                    }
            }
            
        }else if((self.forwordLinkId[indexPath.row]) == self.stringOne)
        {
            //print("Forwrd link id: \(self.forwordLinkId[indexPath.row]) == \(self.stringOne)")
            
            //print("Keystones num: \(keyIDArr.count)")
            
            let headers = delObj.headersAll
            if(keyIDArr.count > 1 ){
                for index in 0...keyIDArr.count-1
                {
                    if(self.forwordLinkId[indexPath.row] == keyIDArr[index])
                    {
                        self.name = keystoneName[index]
                        self.proPic = keystoneProPic[index]
                    }else{
                        //println("is Participant")
                    }
                }
            }else{
                
            }
            
            self.keyStoneID_PROXY = self.forwordLinkId[indexPath.row]
            let id = self.forwordLinkId[indexPath.row]
            //print("forwordLinkId id:-\(id)")
            let statePost = self.alertUserName[indexPath.row].rangeOfString("post")
            let statePost1 = self.alertUserName[indexPath.row].rangeOfString("posts")
            let medState1 = self.alertUserName[indexPath.row].rangeOfString("medication")
            let medState = self.alertUserName[indexPath.row].rangeOfString("medications")
            
            //println("Notifications: \(alertUserName)")
            
            let isElder = self.defaults.valueForKey("isElderStatus") as! String
            
            if(proxyUser == "PROXY")
            { alertconnURL = delObj.baseURL+"users/"+keystoneID+"/notifications/"+alertUserID[indexPath.row]
                //println("alertconnURL as proxy:\(alertconnURL)")
            }else
            { alertconnURL = delObj.baseURL+"users/"+stringOne+"/notifications/"+alertUserID[indexPath.row]
                //println("alertconnURL not as proxy:\(alertconnURL)")
                
            }
            //println("Contains")
            Alamofire.Manager.sharedInstance
                .request(.PUT, alertconnURL, parameters: nil ,headers:headers, encoding: .JSON)
                .responseJSON {
                    response in
                    
                    if(response.result.isSuccess)
                    {
                        //println("response-\(response)")
                        //println("data1-\(data1)")
                        
                        self.pos = 0
                        //Commented for testing by Vijay 29Jan
                        self.alertShows()
                        if(id != "")
                        {
                            if(id != self.stringOne || isElder == "true" )
                            {
                                if(statePost != nil || statePost1 != nil)
                                {
                                    self.defaults.setValue("true", forKey:"POST" )
                                }else
                                {
                                    self.defaults.setValue("false", forKey:"POST" )
                                }
                                if((medState != nil || medState1 != nil) && self.isMedicationVisible == "true")
                                {
                                    self.defaults.setValue("true", forKey:"MADE" )
                                    
                                }else
                                {
                                    self.defaults.setValue("false", forKey:"MADE" )
                                }
                                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idPersonalInforamation") as! personInformation
                                self.navigationController?.pushViewController(nextView, animated: true)
                                
                            }
                            else{
                                
                                self.delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
                                
                                self.defaults.setValue("", forKey:"PROXY" )
                                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idHomePage") as! homePage
                                self.navigationController?.pushViewController(nextView, animated: true)
                            }
                        }else{
                            
                            
                            self.delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
                            
                            self.defaults.setValue("", forKey:"PROXY" )
                            let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idHomePage") as! homePage
                            self.navigationController?.pushViewController(nextView, animated: true)
                        }
                        UIView.animateWithDuration(0.3, animations: {
                            
                            self.alertView.frame.origin.y = -self.alertView.frame.size.height
                            self.alertView.hidden = true
                            }, completion:nil)
                        
                    }
                    else{
                        self.delObj.networkConnError();
                    }
            }
        }else{
            
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            // self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
            //println("no: keyIDArr:  \(keyIDArr), Forwrd link id: \(self.forwordLinkId[indexPath.row])")
            alert = UIAlertController(title: "You are not connected to this user.", message: nil ,preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Cancel, handler:{ (UIAlertAction)in Void()  }))
            self.presentViewController(alert, animated: true, completion: {})
            //println("No Contains")
        } //End of Inner else
    }
    
    //    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
    //
    //        if (tableView == tbAlert) {
    //        }
    //
    //        let deleteAction = UITableViewRowAction(style: .Default, title: "Delete") { (action, indexPath) -> Void in
    //        }
    //        return [deleteAction]
    //    }
    //
    //    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    //
    //        if (tableView == tbAlert) {
    //            return false
    //        }
    //        return true
    //    }
    //------------------------------------------------------------------------------------------
}


