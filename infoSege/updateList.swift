//
//  updateList.swift
//  infoSege
//
//  Created by Pramod shirsath on 3/29/15.
//  Copyright (c) 2015 Pramod shirsath. All rights reserved.

import UIKit
import Alamofire
import SwiftyJSON


class fullMsgLabel: UILabel
{
    override func drawTextInRect(rect: CGRect) {
        let newRect = CGRectOffset(rect, 7, 0) // move text 10 points to the right
        super.drawTextInRect(newRect)
    }
}
class updateList: UIViewController, UITableViewDataSource, UITableViewDelegate,UITextViewDelegate, UITextFieldDelegate,CustomIOS7AlertViewDelegate,MBProgressHUDDelegate{
    
    let delObj = UIApplication.sharedApplication().delegate as! AppDelegate
    var generalBool : Bool = Bool()

    //Outlets.............
    @IBOutlet weak var lblRplyTextLenghtError: UILabel!
    @IBOutlet weak var lblTextLenghtError: UILabel!
    @IBOutlet weak var txtViewFullMsg: UITextView!
    @IBOutlet weak var viewFullRplyMsg: UIView!
    @IBOutlet weak var lblFullPostMsg: UITextView!
    @IBOutlet weak var viewFullPostMsg: UIView!
    @IBOutlet weak var btSendComments: UIButton!
    @IBOutlet weak var lblTapToShow: UILabel!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var btSendPost: UIButton!
    @IBOutlet weak var btSendEditMessage: UIButton!
    @IBOutlet weak var btHideFullMessage: UIButton!
    @IBOutlet weak var txtUpdateMsg: UITextField!
    @IBOutlet weak var secondTableView: UITableView!
    @IBOutlet weak var firstView: UIView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var txtRepluUserMsgBox: UITextView!
    @IBOutlet weak var replyUserTable: UITableView!
    @IBOutlet weak var replyView: UIView!
    
    // @IBOutlet weak var btSend: UIButton!
    @IBOutlet weak var txtMessageBox: UITextView!
    @IBOutlet weak var todoListView1: UIView!
    
    @IBOutlet weak var txtEditMsg: UITextField!
    @IBOutlet weak var updateView: UIView!
    @IBOutlet weak var tbMessageList: UITableView!
    
    //Array variables........................
    var tbOldPos:CGFloat = CGFloat()
    var textFeild : UITextField = UITextField (frame:CGRectMake(0, 0, 10, 10));
    var sampleLabel = UITextView()
    var messageArr:[String] = []
    var userName:[String] = []
    var sendDate:[String] = []
    var userProPic:[String] = []
    var postId:[String] = []
    var commentId:[String] = []
    var countComment:Int = Int()
    var userID:[String] = []
    var postUserID:[String] = []
    var rplyMessageArr:[String] = []
    var rplyUserName:[String] = []
    var rplySendDate:[String] = []
    var rplyUserProPic:[String] = []
    var sectionIndex:[String] = []
    //Other variavle...........
    var oldTaskMsg = ""
  //  var sendBT = "b"
    var currentLoc : CGPoint = CGPoint()
    var scrollPos : CGPoint = CGPoint()
    var jsonData:JSON = JSON("")
    var userComments1:[String] = []
    var delCommentsIndex: Int = Int()
    var postIndex = 0
    var commentIndex = 0
    var commentCountArr:[AnyObject] = []
    var indexPath :NSIndexPath = NSIndexPath()
    var mainJsonIndexValArr:[AnyObject] = []
    let COMMENTS_LIMIT = 1000
    var imgSent:UIImage = UIImage()
    var HUD:MBProgressHUD = MBProgressHUD()
  //
    var isProxyUser : String = String()
    var isConnected : String = String()
    var isMedicationVisible : String = String()
    var CanWriteUpdate : String = String()
    var canUploadPhotos = ""
    
    var lbllongposterrmsg: UILabel!
    
    var stringOne = ""
    
    var alert1: CustomIOS7AlertView!
    var keystoneID: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("keyID")
        }
    }
    let buttons = [
        "Cancel",
        "Save"
    ]
    var kID : String = String()
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let defaults = NSUserDefaults.standardUserDefaults()
    var logUserId = ""
    var proxyUser = ""
     var proxyIDForAnalytics = ""
    let vc : personInformation = personInformation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label.textColor = UIColor.darkGrayColor()
        stringOne = defaults.valueForKey("UID") as! String
              
    }
    
    
    func LogoutForKeystoneStatusChange()
    {
        
        let alert = UIAlertController(title: "Your Keystone status has changed. You will be re-logged in now.", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:
            {
                (UIAlertAction)in Void.self
                
                var tokanid = self.defaults.valueForKey("deviceToken") as? String
                
                if(tokanid == nil){
                    tokanid = "h433897fg77873456fg868389"
                }
                
                
                self.delObj.checkLogin();
                
                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idLoginViewController") as! ViewController
                self.navigationController?.pushViewController(nextView, animated: true)
                
                
        }))
        
        dispatch_async(dispatch_get_main_queue(), {
            
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
        
    }
    

    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        generalBool = false
        //From view Did Load
        
        lblTextLenghtError.text = "1000 characters of 1000 remaining"
        lblTextLenghtError.textColor = UIColor.grayColor()
        
        lblRplyTextLenghtError.text = "1000 characters of 1000 remaining"
        lblRplyTextLenghtError.textColor = UIColor.grayColor()
        
        txtMessageBox.text = "Share an update"
        txtMessageBox.textColor = UIColor.lightGrayColor()
        
        txtRepluUserMsgBox.textColor = UIColor.lightGrayColor()
        txtRepluUserMsgBox.text = "Reply…"
        
       // sampleLabel.delegate = self
        
      
        appDelegate.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "Content-Type" : "application/json"]
        
        proxyUser = defaults.valueForKey("PROXY") as! String
        
        if(proxyUser == "PROXY")
        {
            logUserId = keystoneID as! String
            
            
        }else{
            logUserId = defaults.valueForKey("UID") as! String
        }
        
        kID = keystoneID as! String
        if(kID == "")
        {
            kID = logUserId
        }
        
        if(proxyUser == "PROXY")
        {
            self.proxyIDForAnalytics = kID
            
        }else{
            
            self.proxyIDForAnalytics = logUserId
        }
        //set border and corner radius to view -_--------------------------------------------
        secondTableView.layer.borderColor = UIColor.grayColor().CGColor;
        secondTableView.layer.borderWidth = 0.5;
        secondTableView.layer.cornerRadius = 5
        
        txtMessageBox.layer.borderColor = UIColor.grayColor().CGColor;
        txtMessageBox.layer.borderWidth = 0.5;
        txtMessageBox.layer.cornerRadius = 5
        
        replyView.layer.borderColor = UIColor.grayColor().CGColor;
        replyView.layer.borderWidth = 0.5;
        replyView.layer.cornerRadius = 5
        
        txtRepluUserMsgBox.layer.borderColor = UIColor.grayColor().CGColor;
        txtRepluUserMsgBox.layer.borderWidth = 0.5;
        txtRepluUserMsgBox.layer.cornerRadius = 5
        
        lblFullPostMsg.layer.borderColor = UIColor.grayColor().CGColor;
        lblFullPostMsg.layer.borderWidth = 0.5;
        lblFullPostMsg.layer.cornerRadius = 5
        
        txtViewFullMsg.layer.borderColor = UIColor.grayColor().CGColor;
        txtViewFullMsg.layer.borderWidth = 0.5;
        txtViewFullMsg.layer.cornerRadius = 5
        
        btSendPost.layer.cornerRadius = 5
        //-------------------------------------------------------------------------------------
        //get all user/keystone post
        getAllPost()
        
        tbOldPos = self.tbMessageList.frame.origin.y
               let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(updateList.handleSingleTap(_:)))
        tapRecognizer.numberOfTapsRequired = 1
    
        //Analytics
        
      //  let deviceType = self.defaults.valueForKey("deviceType") as! String
        
        let device = UIDevice.currentDevice().name
        let screenSize = self.defaults.valueForKey("screenSize") as! String
        let OS = self.defaults.valueForKey("OS") as! String
        
        let alertController = UIAlertController(title: "Device Info", message: "\(UIDevice.currentDevice().name)  \(screenSize) \(OS)  @home", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
        
        let headers = appDelegate.headersAll
        
        //self.presentViewController(alertController, animated: true, completion: nil)
        
        let parameters = ["string":"updates"]
        
        var url = self.appDelegate.baseURL+"users/\(stringOne)/elasticLog/pageview/proxiedUser/\(self.proxyIDForAnalytics)/mobile?operatingSystem=\(OS)&deviceType=\(device)&screenSize=\(screenSize)"
        url = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        //print("\(parameters) \(url)")
        //print("user \(stringOne)")
        
        Alamofire.request(.POST, url,parameters: parameters,headers: headers,encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    //print(response?.statusCode)
                    //print("Success")
                    
                }else if(response.result.isFailure){
                    
                    //print(response?.statusCode)
                    //print("failure")
                    
                }else{
                    
                    self.appDelegate.networkConnError();
                    
                }
        }
        
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL+"users/" + self.kID + "/profile", parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
               response in
                if(response.result.isSuccess)
                {
                    
                    let json1 = JSON(response.result.value!)
                    
                    if(json1["userMessage"].stringValue == "You don't have permission to do that."){
                        //print("called from line :586")
                        self.LogoutForProxy()
                    }else{
                        
                        if(json1["user"]["isElder"].boolValue) {
                            
                            
                        }else{
                            print("called from todo line :287")
                            
                            //self.LogoutForProxy()
                            self.LogoutForKeystoneStatusChange()
                            
                        }
                        
                        
                    }
                    
                }
                //println(self.userData)
        }
        
        
        //Check participants user rights
        
        
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL + "users/" + self.kID + "/privileges/users/" + stringOne, parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                
                print("Req in post : \(response.request)")
                
                if(response.result.isSuccess)
                {
                    var json1 = JSON(response.result.value!)
                    
                    
                    let count: Int? = json1.array?.count
                    if(count != 0)
                    {
                        
                      let isProxyInResponse = json1["PROXY"].stringValue
                      let isConnected = json1["CONNECTED"].stringValue
                        if(isConnected == "false" )
                        {
                            self.LogoutForProxy()
                            
                        }
                        
                        if(self.proxyUser == "PROXY")
                        {
                            if(isProxyInResponse == "false" )
                            {
                                //print("Not Proxy Account")
                                
                                self.LogoutForProxy()
                                
                            }
                        }
                        
                     
                    }
                }
                
                
        }


        
        self.view.addGestureRecognizer(tapRecognizer)
        checkprivileges()

    }
    
    func checkprivileges()
    {
        
        
        let headers = appDelegate.headersAll
        
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL + "users/" + self.kID + "/privileges/users/" + stringOne, parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                
               // print("Req in post : \(request)")
                
                if(response.result.isSuccess)
                {
                    var json1 = JSON(response.result.value!)
                    print("*** json1: 381:: \(json1)")
                    
                    let count: Int? = json1.array?.count
                    if(count != 0)
                    {
                        self.CanWriteUpdate = json1["WRITE_UPDATES"].stringValue as String
                        self.isMedicationVisible = json1["VIEW_MEDICATIONS"].stringValue as String
                        self.isConnected = json1["CONNECTED"].stringValue as String
                        self.isProxyUser = json1["PROXY"].stringValue as String
                        self.canUploadPhotos = json1["UPLOAD_PHOTOS"].stringValue
                        if(self.isMedicationVisible == "true")
                        {
                            if(self.isConnected == "true")
                            {
                                print("\n called from updatesList line393")
                            }
                            else
                            {
                                self.LogoutForProxy()
                                print("\n called from updatesList line398")
                                
                            }
                        }
                        else{
                            //Jchange 25/8
                            //if(self.CanWriteUpdate == "true")//to stop msg
                            if(self.canUploadPhotos == "true")
                            {
                                if(self.CanWriteUpdate == "true")
                                {
                                    print("Can write update 411")
                                    
                                }
                                else
                                {
                                    print("\n called from updatesList line417")
                                    self.LogoutForProxy()
                                    
                                }
                            }
                            else
                            {
                                print("called from updatesList line423")
                                self.LogoutForProxy()
                                
                                //                                //if(self.canUploadPhotos == "true")
                                //                                if(self.CanWriteUpdate == "true")
                                //                                {
                                //                                    print("\n called from updatesList line413")
                                //
                                //                                }
                                //                                else
                                //                                {
                                //                                      print("\n called from updatesList line419")
                                //                                    self.LogoutForProxy()
                                //                                }
                            }
                            
                            
                        }
                    }
                }
        }
        
        
    }

        
        
      /*  let headers = appDelegate.headersAll
        
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL + "users/" + self.kID + "/privileges/users/" + stringOne, parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                
                print("Req in post : \(response.request)")
                
                if(response.result.isSuccess)
                {
                    var json1 = JSON(response.result.value!)
                    
                    
                    let count: Int? = json1.array?.count
                    if(count != 0)
                    {
                         self.isMedicationVisible = json1["VIEW_MEDICATIONS"].stringValue as String
                        self.isConnected = json1["CONNECTED"].stringValue as String
                        self.isProxyUser = json1["PROXY"].stringValue as String
                        if(self.isMedicationVisible == "true")
                        {
                        if(self.isConnected == "true")
                        {
                            print("\n called from updatesList line354")
                        }
                        else
                        {
                            self.LogoutForProxy()
                            print("\n called from updatesList line359")
                            
                        }
                        }
                        else{
                            self.LogoutForProxy()
                            print("\n called from updatesList line369")
                        }
                    }
                }
        }
        
        
    }*/
    
    

    
    func LogoutForProxy()
    {
        let alert = UIAlertController(title: "Your privileges have been modified for your Keystone(s). You will be re-logged in now.", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:
            {
                (UIAlertAction)in Void.self
                
                var tokanid = self.defaults.valueForKey("deviceToken") as? String
                
                if(tokanid == nil){
                    tokanid = "h433897fg77873456fg868389"
                }
                
                //Clear defaults
                
                self.appDelegate.checkLogin()
                
                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idLoginViewController") as! ViewController
                self.navigationController?.pushViewController(nextView, animated: true)
                
                
        }))
        
        dispatch_async(dispatch_get_main_queue(), {
            // code here
            
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
        //print("Your Keystone status has changed. You will be re-logged in now.")
        
        
    }

    
    func handleSingleTap(recognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
        let str : NSCharacterSet = NSCharacterSet(charactersInString: " ")
        if( txtMessageBox.text.stringByTrimmingCharactersInSet(str) == "")
        {
            txtMessageBox.textColor = UIColor.lightGrayColor()
            txtMessageBox.text = "Share an update"
        }
        if( txtRepluUserMsgBox.text.stringByTrimmingCharactersInSet(str) == "")
        {
            txtRepluUserMsgBox.textColor = UIColor.lightGrayColor()
            txtRepluUserMsgBox.text = "Reply…"
        }
        if(!viewFullPostMsg.hidden){
           UIView.animateWithDuration(0.3, animations: {
                self.viewFullPostMsg.hidden = true
                self.viewFullPostMsg.frame.origin.x = self.updateView.frame.size.width+20}, completion:nil)}
        if(!viewFullRplyMsg.hidden){
            UIView.animateWithDuration(0.3, animations: {
                self.viewFullRplyMsg.hidden = true
                self.viewFullRplyMsg.frame.origin.x = self.updateView.frame.size.width+20}, completion:nil)}
    }
    
    func textViewShouldReturn(textView: UITextView!) -> Bool {
        self.view.endEditing(true);
        let str : NSCharacterSet = NSCharacterSet(charactersInString: " ")
        if( txtMessageBox.text.stringByTrimmingCharactersInSet(str) == "")
        {
            txtMessageBox.textColor = UIColor.lightGrayColor()
            txtMessageBox.text = "Share an update"
        }
        if( txtRepluUserMsgBox.text.stringByTrimmingCharactersInSet(str) == "")
        {
            txtRepluUserMsgBox.textColor = UIColor.lightGrayColor()
            txtRepluUserMsgBox.text = "Reply…"
        }
        return false;
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        viewFullPostMsg.hidden = true
        viewFullRplyMsg.hidden = true
        
        self.btSendPost.enabled = true
        self.btSendComments.enabled = true
        
        if(textView == txtMessageBox){
            if( txtMessageBox.text == "Share an update")
            {
                txtMessageBox.text = ""
                txtMessageBox.textColor = UIColor.darkGrayColor()
            }
        }else{
            if( txtRepluUserMsgBox.text == "Reply…")
            {
                txtRepluUserMsgBox.text = ""
                txtRepluUserMsgBox.textColor = UIColor.darkGrayColor()
            }
        }
        
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(textView == txtMessageBox)
        {
            if(txtMessageBox.text.characters.count + (text.characters.count - range.length) <= COMMENTS_LIMIT){
                lblTextLenghtError.text = "\(1000 -  (txtMessageBox.text.characters.count + (text.characters.count - range.length))) characters of 1000 remaining";
                lblTextLenghtError.textColor = UIColor.grayColor()
                //return txtMessageBox.text.characters.count + (text.characters.count - range.length) <= COMMENTS_LIMIT
                self.btSendPost.enabled = true
                 self.btSendPost.alpha = 1
            }
            else{
                lblTextLenghtError.text = "The post is too long. Cannot be more than 1000 characters.";
                lblTextLenghtError.textColor = UIColor.redColor()
                //return txtMessageBox.text.characters.count + (text.characters.count - range.length) <= COMMENTS_LIMIT
                self.btSendPost.enabled = false
                self.btSendPost.alpha = 0.6
            }
            return true
        }
        if(textView == sampleLabel)
        {
            if(sampleLabel.text.characters.count + (text.characters.count - range.length) <= COMMENTS_LIMIT){
                lbllongposterrmsg.text = "\(1000 -  (sampleLabel.text.characters.count + (text.characters.count - range.length))) characters of 1000 remaining";
                
                lbllongposterrmsg.textColor = UIColor.darkGrayColor()
                alert1.setButtonEnabled(true, buttonName: "Save")
              //  self.sendBT = "b"
                
                
            }
            else{
                lbllongposterrmsg.text = "The post is too long. Cannot be more than 1000 characters.";
                lbllongposterrmsg.textColor = UIColor.redColor()
              //  self.sendBT = "a"
                alert1.setButtonEnabled(false, buttonName: "Save")
            }
            return true
        }
        else
        {
            if(txtRepluUserMsgBox.text.characters.count + (text.characters.count - range.length) <= COMMENTS_LIMIT)
                
            {
                lblRplyTextLenghtError.text = "\(1000 -  (txtRepluUserMsgBox.text.characters.count + (text.characters.count - range.length))) characters of 1000 remaining";
                lblRplyTextLenghtError.textColor = UIColor.grayColor()
                //return txtRepluUserMsgBox.text.characters.count + (text.characters.count - range.length) <= COMMENTS_LIMIT
                btSendComments.enabled = true
                btSendComments.alpha = 1
            }
            else{
                lblRplyTextLenghtError.text = "The reply is too long. Cannot be more than 1000 characters.";
                lblRplyTextLenghtError.textColor = UIColor.redColor()
               // return txtRepluUserMsgBox.text.characters.count + (text.characters.count - range.length) <= COMMENTS_LIMIT
                
                btSendComments.enabled = false
                btSendComments.alpha = 0.6
            }
            return true
            
        }
        
        
    }
    
    @IBAction func cancelPostSend(sender: AnyObject) {
        txtMessageBox.textColor = UIColor.lightGrayColor()
        txtMessageBox.text = "Share an update"
    }
    
    //get all user/keystone post-------------------------------
    func getAllPost()
    {
        activityIndicator.hidden = false
        
              
        let headers = appDelegate.headersAll
        Alamofire.Manager.sharedInstance
            .request(.GET, self.appDelegate.baseURL+"users/" + self.kID + "/posts", parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    self.userName = []
                    self.messageArr = []
                    self.sendDate = []
                    self.userProPic = []
                    self.commentCountArr = []

                    
                    
                    self.jsonData = JSON(response.result.value!)
                   // var dt = self.jsonData["userMessage"].stringValue
                  
                    if(self.jsonData["userMessage"].stringValue != "You must be logged in to do that." ){
                        
                        self.activityIndicator.hidden = true
                        
                        let count: Int? = self.jsonData.array?.count
                        if(count != 0)
                        {
                            self.label.text = "This user has no updates"
                            if let ct = count {
                                
                                var indexNo = 0;
                                for index in 0...ct-1 {
                                    
                                    
                                    if(!self.jsonData[index]["isDeleted"].boolValue){
                                        
                                        self.label.text = ""
                                        
                                        var commentCnt:JSON = JSON("")
                                        
                                        commentCnt = self.jsonData[index]["comments"]
                                        
                                        let cnt = commentCnt.array?.count
                                        var commCnt = 0
                                        
                                        if (cnt > 0) {
                                            
                                            for idx in 0...cnt!-1
                                            {
                                                if(!commentCnt[idx]["isDeleted"].boolValue){
                                                    
                                                    commCnt = commCnt + 1
                                                }
                                            }
                                        }
                                        self.commentCountArr.insert(commCnt, atIndex: indexNo)
                                        self.userID.insert(self.jsonData[index]["user"]["id"].stringValue, atIndex: indexNo)
                                        
                                        self.postId.insert(self.jsonData[index]["postId"].stringValue, atIndex: indexNo)
                                        
                                        self.mainJsonIndexValArr.insert(index, atIndex: indexNo)
                                        
                                        self.userName.insert(self.jsonData[index]["user"]["firstName"].stringValue, atIndex: indexNo)
                                        self.messageArr.insert(self.jsonData[index]["postText"].stringValue, atIndex: indexNo)
                                        
                                        let dateString: String = self.jsonData[index]["postTimeEdited"].stringValue // change to your date format
                                        
                                        let dateFormatter = NSDateFormatter()
                                        dateFormatter.locale = NSLocale(localeIdentifier: "us")
                                        dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss zzz"
                                        let date = dateFormatter.dateFromString(dateString)
                                        
                                        if(date == nil)
                                        {
                                            self.sendDate.insert(dateString , atIndex: indexNo)
                                        }else{
                                            //dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss zzz"
                                            dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm a"
                                            let localDate = dateFormatter.stringFromDate(date!)
                                           
                                            self.sendDate.insert(localDate, atIndex: indexNo)
                                        }
                                        
                                        
                                        if(self.jsonData[index]["user"]["profileImage"]["image"]["path"].stringValue == "") {
                                            self.userProPic.insert(self.appDelegate.baseImageURL + "w168xh168-2/images/default_profile_image.png", atIndex: indexNo)
                                        }
                                        else
                                        {self.userProPic.insert(self.appDelegate.baseImageURL + "w100xh100-2/images/"+self.jsonData[index]["user"]["profileImage"]["image"]["path"].stringValue, atIndex: indexNo)}
                                        
                                        indexNo = indexNo + 1;
                                        
                                    }
                                    
                                }
                                
                                self.tbMessageList.reloadData()
                              
                                
                                self.getUserComments(self.postIndex)
                                
                            }
                        }else{
                            
                            self.messageArr.removeAll()
                            self.tbMessageList.reloadData()
                            
                        }
                    }else
                    {
                        self.appDelegate.checkLogin()
                    }
                    if(self.jsonData["userMessage"].stringValue == "You don't have permission to do that."){
                        
                        self.activityIndicator.hidden = true
                        self.LogoutForProxy()
                    }
                    
                    
                }else{
                    
                    self.activityIndicator.hidden = true
                    self.appDelegate.networkConnError();
                }
        }
        
    }
    
    //get all user/keystone comments-------------------------------
    func getUserComments(btindex: Int)
    {
        
        rplyUserName.removeAll(keepCapacity: false)
        rplyMessageArr.removeAll(keepCapacity: false)
        rplySendDate.removeAll(keepCapacity: false)
        rplyUserProPic.removeAll(keepCapacity: false)
        commentId.removeAll(keepCapacity: false)
        postUserID.removeAll(keepCapacity: false)
        
        self.replyUserTable.reloadData()
        
        var userComments = jsonData[btindex]["comments"]
        let count: Int? = userComments.count
        
        if(count != 0)
        {
            
            if let ct = count {
                var indexNo = 0;
                for index in 0...ct-1 {
                    
                    if(!userComments[index]["isDeleted"].boolValue)
                    {
                        self.commentId.insert(userComments[index]["commentId"].stringValue, atIndex: indexNo)
                        self.rplyUserName.insert(userComments[index]["user"]["firstName"].stringValue, atIndex: indexNo)
                        self.postUserID.insert(userComments[index]["user"]["id"].stringValue, atIndex: indexNo)
                  
                        self.rplyMessageArr.insert(userComments[index]["commentText"].stringValue, atIndex: indexNo)
                        
                        let dateString: String = userComments[index]["commentTimeEdited"].stringValue // change to your date format
                        
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.locale = NSLocale(localeIdentifier: "us")
                        dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss zzz"
                        let date = dateFormatter.dateFromString(dateString)
                        
                        if(date != nil)
                        {
                            // dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss zzz"
                            dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm a"
                            let localDate = dateFormatter.stringFromDate(date!)
                            self.rplySendDate.insert(localDate, atIndex: indexNo)
                        }else
                        {
                            self.rplySendDate.insert(userComments[index]["commentTimeEdited"].stringValue, atIndex: indexNo)
                        }
                        if(userComments[index]["user"]["profileImage"]["image"]["path"].stringValue == "")
                        {
                            self.rplyUserProPic.insert(self.appDelegate.baseImageURL + "w168xh168-2/images/default_profile_image.png", atIndex: indexNo)
                        }
                        else
                        {
                            self.rplyUserProPic.insert(self.appDelegate.baseImageURL + "w100xh100-2/images/"+userComments[index]["user"]["profileImage"]["image"]["path"].stringValue, atIndex: indexNo)
                            
                        }
                        
                        
                        indexNo = indexNo+1
                        
                    }
                }
                self.replyUserTable.reloadData()
                if(postUserID.count >= 4)
                { reverseCommentArry() }
                
            }
        }else{
            self.replyUserTable.reloadData()
        }
    }
    
    func reverseCommentArry()
    {
        self.replyUserTable.scrollToRowAtIndexPath(NSIndexPath(forRow: postUserID.count - 1, inSection: 0), atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
    }
  /*  func configurationTextField(textField: UITextField!)
    {
        if let tField = textField {
            
            self.textFeild = textField!
            self.textFeild.text = oldTaskMsg
            //self.textFeild.placeholder = "What needs to be done?"
        }
    }*/
    
    // update post message-------------------------------
    func createContainerView(flag:Int) -> UIView {
        let containerView = UIView(frame: CGRectMake(0, 0, 290, 190))
        let lblTitle = UILabel(frame: CGRectMake(0,10, containerView.frame.width, 20))
       
        
        lblTitle.textAlignment = NSTextAlignment.Center
        
         let strcnt = oldTaskMsg.characters.count
        
        lbllongposterrmsg = UILabel(frame: CGRectMake(10,150, containerView.frame.width - 10, 40))
        lbllongposterrmsg.textAlignment = NSTextAlignment.Left
        lbllongposterrmsg.font = UIFont.systemFontOfSize(14)
        lbllongposterrmsg.lineBreakMode = NSLineBreakMode.ByWordWrapping
        lbllongposterrmsg.numberOfLines = 2
        
        lbllongposterrmsg.text = ""
        
        lbllongposterrmsg.text = " \(1000 - strcnt) characters of 1000 remaining."
        lbllongposterrmsg.textColor = UIColor.darkGrayColor()
        
        containerView.addSubview(lblTitle)
        
        
        if(flag == 0)
        {lblTitle.text = "Edit Post"}
        else if(flag == 2)
        {lblTitle.text = "View Post"}
        else if(flag == 3)
        {lblTitle.text = "View Comment"}
        else{lblTitle.text = "Edit Comment"}
        
        if(flag == 0)
        {
            sampleLabel = UITextView(frame: CGRectMake(10,40, containerView.frame.width - 20, 110))
            sampleLabel.layer.cornerRadius = 5
            sampleLabel.text = oldTaskMsg
            sampleLabel.textColor = UIColor.darkGrayColor()
            sampleLabel.font = UIFont.systemFontOfSize(15)
            sampleLabel.delegate = self
        
            containerView.addSubview(sampleLabel)
            containerView.addSubview(lbllongposterrmsg)
        }
        else if(flag == 2 || flag == 3)
        {
           
            sampleLabel = UITextView(frame: CGRectMake(10,40, containerView.frame.width - 20, 140))
            sampleLabel.layer.cornerRadius = 5
            sampleLabel.text = oldTaskMsg
            sampleLabel.textColor = UIColor.darkGrayColor()
            sampleLabel.font = UIFont.systemFontOfSize(15)
            sampleLabel.delegate = self
           
            containerView.addSubview(sampleLabel)
            sampleLabel.editable = false
            
        }else{
            
            sampleLabel = UITextView(frame: CGRectMake(10,40, containerView.frame.width - 20, 110))
            sampleLabel.layer.cornerRadius = 5
            sampleLabel.text = oldTaskMsg
            sampleLabel.textColor = UIColor.darkGrayColor()
            sampleLabel.font = UIFont.systemFontOfSize(15)
            sampleLabel.delegate = self
            
            containerView.addSubview(sampleLabel)
            containerView.addSubview(lbllongposterrmsg)
        }

    
        return containerView
    }
    
    
    func customIOS7AlertViewButtonTouchUpInside(alertView: CustomIOS7AlertView, buttonIndex: Int) {
        
        if(buttonIndex == 1){
        
            alertView.close()
            
            let image : UIImage = UIImage(named:"37x-Checkmark")!
            self.HUD.customView = UIImageView(image: image)
            
            self.HUD.mode = MBProgressHUDMode.CustomView
            self.HUD.labelText = "Post has been edited."
            self.view.addSubview(self.HUD)
            self.HUD.show(true)
            self.HUD.hide(true, afterDelay: 3)
        }else
        {
            alertView.close()
        }
    }
    
    @IBAction func showFullPostMsg(sender: UIButton, event: UIEvent) {
        
        self.view.endEditing(true);
        viewFullPostMsg.hidden = true
        if(!tbMessageList.editing){
            oldTaskMsg = messageArr[sender.tag]//cell.txtUpdatedMessage.text!
            
            // Create a new AlertView instance
            let alertView = CustomIOS7AlertView()
            alert1 = alertView
            
            // Set the button titles array
            alertView.buttonTitles = ["Close"]
            
            // Set a custom container view
            alertView.containerView1 = createContainerView(2)
            
            alertView.delegate = self
            
            alertView.onButtonTouchUpInside = { (alertView: CustomIOS7AlertView, buttonIndex: Int) -> Void in
                if(self.buttons[buttonIndex] == "Close")
                {
                    
                    
                }
                else{
                    
                }
            }
            
            alertView.show()
            
            
        }
        
        
        
    }

    
    @IBAction func editPostMsg(sender: UIButton, event: UIEvent) {
        
        self.view.endEditing(true);
        viewFullPostMsg.hidden = true
        if(!tbMessageList.editing){
            oldTaskMsg = messageArr[sender.tag]//cell.txtUpdatedMessage.text!
            
            // Create a new AlertView instance
            let alertView = CustomIOS7AlertView()
            alert1 = alertView
           
            // Set the button titles array
            alertView.buttonTitles = buttons
            
            
            // Set a custom container view
            alertView.containerView1 = createContainerView(0)
            
            alertView.delegate = self
            
            alertView.onButtonTouchUpInside = { (alertView: CustomIOS7AlertView, buttonIndex: Int) -> Void in
                if(self.buttons[buttonIndex] == "Save" && (self.lbllongposterrmsg.textColor == UIColor.darkGrayColor()))
                {
                    
                    if(self.proxyUser == "PROXY")
                    {
                        self.appDelegate.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
                    }
                    let str : NSCharacterSet = NSCharacterSet(charactersInString: " ")
                    if( self.sampleLabel.text.stringByTrimmingCharactersInSet(str) != "")
                    {
                        let headers = self.appDelegate.headersAll
                        Alamofire.Manager.sharedInstance
                            .request(.PUT, self.appDelegate.baseURL+"users/" + self.kID + "/posts/" + self.postId[sender.tag], parameters: ["string":self.sampleLabel.text],headers: headers, encoding: .JSON)
                            .responseJSON {
                                response in
                                if(response.result.isSuccess)
                                {
                                    self.messageArr[sender.tag] = self.sampleLabel.text
                                    
                                    self.tbMessageList.reloadData()
                                    
                                    
                                    
                                }else{
                                    
                                    self.appDelegate.networkConnError();
                                }
                        }
                        
                    }
                    
                }
                else{
                    
                }
            }
            
            alertView.show()
        
            
        }
    }
    
    // send/update comments-------------------------------
    @IBAction func sendReplyMessage(sender: AnyObject) {
        
        self.btSendComments.enabled = false
        
        let str1 : NSCharacterSet = NSCharacterSet(charactersInString: "\n")
        let str : NSCharacterSet = NSCharacterSet(charactersInString: " ")
     
        if( txtRepluUserMsgBox.text.stringByTrimmingCharactersInSet(str) == "" || self.txtRepluUserMsgBox.textColor == UIColor.lightGrayColor() || txtRepluUserMsgBox.text.stringByTrimmingCharactersInSet(str1) == "")
        {
            self.btSendComments.enabled = true
            let alertController = UIAlertController(title: "Please enter your message!", message: "", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
            self.presentViewController(alertController, animated: true, completion: nil)
            
        }
        else{
            if(proxyUser == "PROXY")
            {
             appDelegate.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
            }
            let headers = appDelegate.headersAll
            Alamofire.Manager.sharedInstance
                .request(.POST, self.appDelegate.baseURL+"users/" + self.kID + "/posts/"+self.postId[self.delCommentsIndex]+"/comments", parameters:["string":txtRepluUserMsgBox.text],headers: headers, encoding: .JSON)
                .responseJSON {
                    response in
                    if(response.result.isSuccess)
                    {
                        self.view.endEditing(true)
                        self.txtRepluUserMsgBox.text = "Reply…"
                        self.lblRplyTextLenghtError.text = "1000 characters of 1000 remaining"
                        self.btSendComments.enabled = true
                        
                        let image : UIImage = UIImage(named:"37x-Checkmark")!
                        self.HUD.customView = UIImageView(image: image)
                        
                        self.HUD.mode = MBProgressHUDMode.CustomView
                        self.HUD.labelText = "Comment has been sent"
                        self.view.addSubview(self.HUD)
                        self.HUD.show(true)
                        self.HUD.hide(true, afterDelay: 3)
                        
                        self.getAllPost()
                        
                        self.txtRepluUserMsgBox.textColor = UIColor.lightGrayColor()
                    }else{
                        
                        self.txtRepluUserMsgBox.text = "Reply…"
                        self.btSendComments.enabled = true
                        self.appDelegate.networkConnError();
                    }
            }
        }
        
    }
    
    // cancel comments box-------------------------------
    @IBAction func calcelReplyMessage(sender: AnyObject) {
      
        generalBool = false
       
       
        viewFullRplyMsg.hidden = true
        lblTextLenghtError.hidden = false
        
        lblRplyTextLenghtError.text = "1000 characters of 1000 remaining"
        lblRplyTextLenghtError.textColor = UIColor.grayColor()
        tbMessageList.userInteractionEnabled = true
        btSendPost.userInteractionEnabled = true
        txtMessageBox.userInteractionEnabled = true
        let str : NSCharacterSet = NSCharacterSet(charactersInString: " ")
        if( txtMessageBox.text.stringByTrimmingCharactersInSet(str) == "")
        {
            txtMessageBox.textColor = UIColor.lightGrayColor()
            txtMessageBox.text = "Share an update"
        }
        
        txtRepluUserMsgBox.text = "Reply…"
        txtRepluUserMsgBox.textColor = UIColor.lightGrayColor()
        view.endEditing(true)
        
        var cell:myUpdateTableViewCell = myUpdateTableViewCell()
        cell = self.tbMessageList .cellForRowAtIndexPath(indexPath) as! myUpdateTableViewCell
     
        UIView.animateWithDuration(0.5, animations: {
            self.tbMessageList.frame.origin.y =  self.tbOldPos
            cell.btEditPost.hidden = false
           
            cell.btSendReplyMsg.hidden = false
            }, completion:nil)
        
        UIView.animateWithDuration(0.5, animations: {
            self.replyView.frame.origin.x = self.view.frame.size.width+50
            //self.replyView.hidden = true
            }, completion:nil)
        
        self.tbMessageList.reloadData()
        self.secondTableView.reloadData()
    }
    
    
    func CheckPrev( completion : (Bool) -> ()) {
        
        let headers = appDelegate.headersAll
        
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL + "users/" + self.kID + "/privileges/users/" + stringOne, parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
                  response in
                
               // print("Req in post : \(request)")
                
                if(response.result.isSuccess)
                {
                    var json1 = JSON(response.result.value!)
                    print("*** json1: 381:: \(json1)")
                    
                    let count: Int? = json1.array?.count
                    if(count != 0)
                    {
                        self.CanWriteUpdate = json1["WRITE_UPDATES"].stringValue as String
                        self.isMedicationVisible = json1["VIEW_MEDICATIONS"].stringValue as String
                        self.isConnected = json1["CONNECTED"].stringValue as String
                        self.isProxyUser = json1["PROXY"].stringValue as String
                        self.canUploadPhotos = json1["UPLOAD_PHOTOS"].stringValue
                        if(self.isMedicationVisible == "true")
                        {
                            if(self.isConnected == "true")
                            {
                                print("\n called from updatesList line393")
                            }
                            else
                            {
                                self.LogoutForProxy()
                                print("\n called from updatesList line398")
                                
                            }
                        }
                        else{
                            //Jchange 25/8
                            //if(self.CanWriteUpdate == "true")//to stop msg
                            if(self.canUploadPhotos == "true")
                            {
                                if(self.CanWriteUpdate == "true")
                                {
                                    print("Can write update 411")
                                    
                                }
                                else
                                {
                                    print("\n called from updatesList line417")
                                    self.LogoutForProxy()
                                    
                                }
                            }
                            else
                            {
                                print("called from updatesList line423")
                                self.LogoutForProxy()
                                
                                //                                //if(self.canUploadPhotos == "true")
                                //                                if(self.CanWriteUpdate == "true")
                                //                                {
                                //                                    print("\n called from updatesList line413")
                                //
                                //                                }
                                //                                else
                                //                                {
                                //                                      print("\n called from updatesList line419")
                                //                                    self.LogoutForProxy()
                                //                                }
                            }
                            
                            
                        }
                        completion(true)
                    }
                }else{
                    completion(false)
                }
        }//ends
        
        
    }

    
    // send/update post
    @IBAction func btSendMessage(sender: AnyObject) {
        CheckPrev { response in
            if(response){
                self.view.endEditing(true);
                self.btSendPost.enabled = false
                
                let str : NSCharacterSet = NSCharacterSet(charactersInString: " ")
                let str1 : NSCharacterSet = NSCharacterSet(charactersInString: "\n")
                if( self.txtMessageBox.text.stringByTrimmingCharactersInSet(str) == "" || self.txtMessageBox.text.stringByTrimmingCharactersInSet(str1) == "" ||  self.txtMessageBox.textColor == UIColor.lightGrayColor())
                {
                    self.btSendPost.enabled = true
                    let alertController = UIAlertController(title: "Please enter your message!", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                    
                }
                else{
                    if(self.proxyUser == "PROXY")
                    {
                        self.appDelegate.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
                    }
                    if(self.canUploadPhotos == "true"){
                        let headers = self.appDelegate.headersAll
                        Alamofire.Manager.sharedInstance
                            .request(.POST, self.appDelegate.baseURL+"users/" + self.kID + "/posts", parameters:["string": self.txtMessageBox.text], headers: headers,encoding: .JSON)
                            .responseJSON {
                                response in
                                if(response.result.isSuccess)
                                {   self.txtMessageBox.text = "Share an update"
                                    self.lblTextLenghtError.text = "1000 characters of 1000 remaining"
                                    self.lblTextLenghtError.textColor = UIColor.lightGrayColor()
                                    self.btSendPost.enabled = true
                                    self.txtMessageBox.textColor = UIColor.lightGrayColor()
                                    
                                    let image : UIImage = UIImage(named:"37x-Checkmark")!
                                    self.HUD.customView = UIImageView(image: image)
                                    
                                    self.HUD.mode = MBProgressHUDMode.CustomView
                                    self.HUD.labelText = "Post has been sent"
                                    self.view.addSubview(self.HUD)
                                    self.HUD.show(true)
                                    self.HUD.hide(true, afterDelay: 3)
                                    
                                    self.getAllPost()
                                }else{
                                    
                                    self.activityIndicator.hidden = true
                                    self.btSendPost.enabled = true
                                    self.appDelegate.networkConnError();
                                }
                        }
                    }else{
                        print("Hurrreyyyy...!")
                    }
                }
            }else{
                print("You have no data ")
            }
            
            
        }
        
    }

        
        
    //alerts message
    func alertPermission(msg: String)
    {
        let alertController = UIAlertController(title: msg, message: "", preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: self.reloAdtbMessageList))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    // reload post and comments-------------------------------
    func reloAdtbMessageList(alertView: UIAlertAction!)
    {
        tbMessageList.reloadData()
        // replyUserTable.reloadData()
    }
    
    // open comments box-------------------------------
    @IBAction func sendReplyMsg(sender: UIButton, event: UIEvent) {
       
        generalBool = true
        
        lblTextLenghtError.hidden = true
        viewFullPostMsg.hidden = true
        btSendPost.userInteractionEnabled = false
        txtMessageBox.userInteractionEnabled = false
        if(!tbMessageList.editing){
        indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
       
        delCommentsIndex = indexPath.row
        
        var cell:myUpdateTableViewCell = myUpdateTableViewCell()
        cell = self.tbMessageList.cellForRowAtIndexPath(indexPath) as! myUpdateTableViewCell
        tbMessageList.userInteractionEnabled = false
        
        let h:CGFloat = CGFloat(indexPath.row)
        UIView.animateWithDuration(0.3, animations: {
            
            cell.btEditPost.hidden = true
            cell.btSendReplyMsg.hidden = true
            
            self.tbMessageList.setContentOffset(CGPoint(x: 0,y: CGRectGetHeight(cell.bounds)*h), animated: true)
            self.tbMessageList.frame.origin.y = 0
            }, completion: nil)
            
        postIndex = mainJsonIndexValArr[indexPath.row].integerValue
        
        getUserComments(postIndex)
        
        UIView.animateWithDuration(0.5, animations: {
            self.replyView.hidden = false
            self.replyView.frame.origin.x = self.updateView.frame.origin.x}, completion:nil)
        }
        
    }
    
    
    // reusable button for show full post message comments -------------------------------
    
    
    @IBAction func hideFullMsgView(sender: AnyObject) {
        if(sender.tag == 0)
        {
            UIView.animateWithDuration(0.3, animations: {
                self.viewFullPostMsg.hidden = true
                self.viewFullPostMsg.frame.origin.x = self.updateView.frame.size.width+20 }, completion:nil)
        }
        else{
            UIView.animateWithDuration(0.3, animations: {
                self.viewFullRplyMsg.hidden = true
                self.viewFullRplyMsg.frame.origin.x = self.updateView.frame.size.width+20 }, completion:nil)
        }
    }
    // reusable button for show full message comments -------------------------------
    @IBAction func showFullCommentsMsg(sender: UIButton, event: UIEvent) {
        
        let indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        var cell:rplyTableCell = rplyTableCell()
        cell = self.replyUserTable .cellForRowAtIndexPath(indexPath) as! rplyTableCell
        let h:CGFloat = CGFloat(indexPath.row) + 0.99
        txtViewFullMsg.text = rplyMessageArr[indexPath.row]
        viewFullRplyMsg.frame.origin.y = (cell.frame.size.height) * h
       
        self.view.endEditing(true);
        commentIndex = sender.tag
       // viewFullRplyMsg.hidden = true
        oldTaskMsg = rplyMessageArr[sender.tag]
        
        let alertView = CustomIOS7AlertView()
        
        // Set the button titles array
        alertView.buttonTitles = ["Close"]
        
        // Set a custom container view
        alertView.containerView1 = createContainerView(3)
        
        alert1 = alertView
        // Set self as the delegate
        alertView.delegate = self
        
        // Or, use a closure
        alertView.onButtonTouchUpInside = { (alertView: CustomIOS7AlertView, buttonIndex: Int) -> Void in
            if(self.buttons[buttonIndex] == "Close")
            {
              alertView.close()
            }
        }
        alertView.show()
        
        
    }
    
    // reusable button for update  comments-------------------------------
    @IBAction func editCommentMsg(sender: UIButton, event: UIEvent) {
        self.view.endEditing(true);
        commentIndex = sender.tag
        viewFullRplyMsg.hidden = true
        oldTaskMsg = rplyMessageArr[sender.tag]
        
        let alertView = CustomIOS7AlertView()
        
        // Set the button titles array
        alertView.buttonTitles = buttons
        
        // Set a custom container view
        alertView.containerView1 = createContainerView(1)
        
        alert1 = alertView
        // Set self as the delegate
        alertView.delegate = self
        
        // Or, use a closure
        alertView.onButtonTouchUpInside = { (alertView: CustomIOS7AlertView, buttonIndex: Int) -> Void in
            if(self.buttons[buttonIndex] == "Save")
            {
                if(self.proxyUser == "PROXY")
                {
                   /* Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]*/
                    self.appDelegate.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
                }
             
                let str : NSCharacterSet = NSCharacterSet(charactersInString: " ")
                if( self.sampleLabel.text.stringByTrimmingCharactersInSet(str) != "")
                {
                    let headers = self.appDelegate.headersAll
                Alamofire.Manager.sharedInstance
                    .request(.PUT, self.appDelegate.baseURL+"users/" + self.kID + "/posts/"+self.postId[self.delCommentsIndex]+"/comments/"+self.commentId[self.commentIndex], parameters: ["string":self.sampleLabel.text], headers: headers,encoding: .JSON)
                    .responseJSON {
                       response in
                        if(response.result.isSuccess)
                        {
                            
                            self.rplyMessageArr[self.commentIndex] = self.sampleLabel.text
                            self.replyUserTable.reloadData()
                            self.getAllPost()
                            
                        }else{
                            
                            self.appDelegate.networkConnError();
                        }
                }
                }
            }
            else{
                   }
        }
        alertView.show()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated..
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == replyUserTable)
        {
            return postUserID.count
        }
        else{
          
            //print(self.messageArr.count)
          
            return self.messageArr.count
           
        }
    }
    func scrollToRowAtIndexPath(indexPath: NSIndexPath, atScrollPosition scrollPosition: UITableViewScrollPosition, animated: Bool)
    {
        
    }
    //assign data to table view
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if(tableView == tbMessageList){
            var cell : myUpdateTableViewCell = myUpdateTableViewCell()
            cell = tableView.dequeueReusableCellWithIdentifier("messageCell", forIndexPath: indexPath) as! myUpdateTableViewCell
            cell.txtUserName.text = userName[indexPath.row]
            cell.txtUpdatedMessage.text = messageArr[indexPath.row]
            
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            
            cell.txtDates.text = sendDate[indexPath.row]
            cell.btEditPost.tag = indexPath.row
            cell.btShowFullPostMsg.tag = indexPath.row
            cell.btSendReplyMsg.tag = indexPath.row
            
           
            if(!generalBool){
                if(userID[indexPath.row] != logUserId)
                {
                    cell.btEditPost.hidden = true
                }else{
                    cell.btEditPost.hidden = false
                }
            }else{
                cell.btEditPost.hidden = true
            }
            //mainJsonIndexValArr[indexPath.row].integerValue
            let headers = appDelegate.headersAll
            Alamofire.request(.GET, userProPic[indexPath.row],headers: headers).response() {
                (_, _, data, _) in
                
                let image = UIImage(data: data! as NSData)
                cell.userProfilePic.image = image
                
            }
            
            cell.commentCount.text = "\(commentCountArr[indexPath.row]) comments"
            cell.btSendReplyMsg.addTarget(self, action: #selector(updateList.sendReplyMsg(_:event:)), forControlEvents: UIControlEvents.TouchUpInside)
            cell.btEditPost.addTarget(self, action: #selector(updateList.editPostMsg(_:event:)), forControlEvents: UIControlEvents.TouchUpInside)
            cell.btShowFullPostMsg.addTarget(self, action: #selector(updateList.showFullPostMsg(_:event:)), forControlEvents: UIControlEvents.TouchUpInside)
            
            return cell
        }else
        {
            var cell : rplyTableCell = rplyTableCell()
            cell = tableView.dequeueReusableCellWithIdentifier("replyCell", forIndexPath: indexPath) as! rplyTableCell
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            
            cell.replyUserName.text = rplyUserName[indexPath.row]
            cell.replyUserMessage.text = rplyMessageArr[indexPath.row]
            cell.replyUserDT.text = rplySendDate[indexPath.row]
            
            cell.btEditMsg.tag = indexPath.row
            cell.btShowFullCommentsMsg.tag = indexPath.row
            
            if(postUserID[indexPath.row] != logUserId)
            {
                cell.btEditMsg.hidden = true
            }else
            {cell.btEditMsg.hidden = false}
            Alamofire.request(.GET, rplyUserProPic[indexPath.row]).response() {
                (_, _, data, _) in
                
                let image = UIImage(data: data! as NSData)
                cell.replyUserImage.image = image
                
            }
            cell.btShowFullCommentsMsg.addTarget(self, action: #selector(updateList.showFullCommentsMsg(_:event:)), forControlEvents: UIControlEvents.TouchUpInside)
            cell.btEditMsg.addTarget(self, action: #selector(updateList.editCommentMsg(_:event:)), forControlEvents: UIControlEvents.TouchUpInside)
            
            return cell
        }
    }
    
    //delete post for swipe left to right as device angle
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath delIndexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .Default, title: "Delete") { (action, delIndexPath) -> Void in
           
            tableView.editing = true
            
            if(tableView == self.tbMessageList)
            {
              //print("Post count in Table : \(self.messageArr.count)")
                
                self.viewFullPostMsg.hidden = true
                
                let alert = UIAlertController(title: "Confirm to Delete", message: "Are you sure you want to delete this Post?", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler:{(UIAlertAction)in Void.self
                    self.tbMessageList.reloadData()}))
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler:{
                    (UIAlertAction)in Void.self
                    if(self.proxyUser == "PROXY")
                    {
                     
                        self.appDelegate.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
                    }
                    
                    let headers = self.appDelegate.headersAll
                    Alamofire.Manager.sharedInstance
                        .request(.DELETE, self.appDelegate.baseURL+"users/" + self.kID + "/posts/"+self.postId[delIndexPath.row], parameters:nil,headers: headers, encoding: .JSON)
                        .responseJSON {
                            response in
                           
                            if(response.result.isSuccess)
                            {
                                var json1 = JSON(response.result.value!)
                                if(json1["userMessage"].stringValue == "" )
                                {
                                    let image : UIImage = UIImage(named:"37x-Checkmark")!
                                    self.HUD.customView = UIImageView(image: image)
                                    
                                    self.HUD.mode = MBProgressHUDMode.CustomView
                                    self.HUD.labelText = "Post has been deleted"
                                    self.view.addSubview(self.HUD)
                                    self.HUD.show(true)
                                    self.HUD.hide(true, afterDelay: 3)
                                    self.getAllPost()
                                    
                                  
                                    
                                    self.tbMessageList.reloadData()
                                    
                                }
                            }else{
                                self.appDelegate.networkConnError();
                            }
                    }
                }))
                self.presentViewController(alert, animated: true, completion: {})
                
            }
            else
            {
                let alert = UIAlertController(title: "Confirm to Delete", message: "Are you sure you want to delete this Comment?", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler:{(UIAlertAction)in Void.self
                    self.replyUserTable.reloadData()}))
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default, handler:{
                    (UIAlertAction)in Void.self
                    
                    if(self.proxyUser == "PROXY")
                    {
                      
                        self.appDelegate.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
                    }
                    
                    let headers = self.appDelegate.headersAll
                   
                    Alamofire.Manager.sharedInstance
                        .request(.DELETE, self.appDelegate.baseURL+"users/" + self.kID + "/posts/"+self.postId[self.delCommentsIndex]+"/comments/"+self.commentId[delIndexPath.row], parameters:nil,headers: headers, encoding: .JSON)
                        .responseJSON {
                            response in
                            
                            if(response.result.isSuccess)
                            {
                                var json1 = JSON(response.result.value!)
                                if(json1["userMessage"].stringValue == "" )
                                {
                                    let image : UIImage = UIImage(named:"37x-Checkmark")!
                                    self.HUD.customView = UIImageView(image: image)
                                    
                                    self.HUD.mode = MBProgressHUDMode.CustomView
                                    self.HUD.labelText = "Comment has been deleted"
                                    self.view.addSubview(self.HUD)
                                    self.HUD.show(true)
                                    self.HUD.hide(true, afterDelay: 3)
                                    self.replyUserTable.reloadData()
                                    self.getAllPost()
                                }
                            }else{
                                
                                self.appDelegate.networkConnError();
                            }
                    }
                }))
                self.presentViewController(alert, animated: true, completion: {
                })
            }
        }
        return [deleteAction]
    }
    
    func handleCancel(alertView: UIAlertAction!)
    {
    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        NSLog("deleting row")
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath1: NSIndexPath) -> Bool {
       
        if((kID == logUserId) || (proxyUser == "PROXY")){
            
            return true
        }else{
        
        if(tableView == tbMessageList)
        {
            
            if(userID[indexPath1.row] == logUserId)
            {
                return true
            }else
            {
                return false
            }
        }else{
            
            if(tableView == replyUserTable)
            {
             if(postUserID[indexPath1.row] == logUserId)
                {
                    return true
                }
                else
                {
                    return false
                }
            
            }else{
                return false
            
            }
        }
        }
    }
}


