//
//  ViewController.swift
//  infoSege
//
//  Created by Pramod shirsath on 3/4/15.
//  Copyright (c) 2015 Pramod shirsath. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AddressBook
import MediaPlayer
import AssetsLibrary
import CoreLocation
import CoreMotion
import SafariServices
import SystemConfiguration


var glob_logUserID :String = String()
var glob_keystoneID:String = String()

var glob_loginUserIsKeystone : Bool = Bool ()
var glob_viewAsParticipant:Bool = Bool()
var glob_viewAsProxy:Bool = Bool()
var glob_viewAsCaregiver:Bool = Bool()
var glob_currentPageIndex:Int = Int()

// Custom text field class for textfield of desired size and editing space.

class CustomTextField: UITextField, MBProgressHUDDelegate,UIGestureRecognizerDelegate {
    
    override func textRectForBounds(bounds: CGRect) -> CGRect { return CGRectInset(bounds, 10.0, 0) };
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect { return self.textRectForBounds(bounds) }
}

// String comparison extension

extension String {
    func isEqualToString(find: String) -> Bool {
        return String(format: self) == find
    }
}

//Starts 22 July
public class Reachability {
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}

// Ends  22 july

class ViewController: UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate, GPPSignInDelegate  ,GIDSignInUIDelegate ,GIDSignInDelegate,SFSafariViewControllerDelegate{
    
    var name: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("name")
        }
        set{
            NSUserDefaults.standardUserDefaults().setObject(newValue!, forKey: "name")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    var proPic: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("proPic")
        }
        set{
            NSUserDefaults.standardUserDefaults().setObject(newValue!, forKey: "proPic")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    var keystoneID: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("keyID")
        }
        set{
            NSUserDefaults.standardUserDefaults().setObject(newValue!, forKey: "keyID")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }

    
    // Outlets
    @IBOutlet weak var btsignin: UIButton!
    @IBOutlet weak var txtUserName: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var logScrollView: UIScrollView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var signInButton: GIDSignInButton!
   
    let loginmanager : FBSDKLoginManager = FBSDKLoginManager()
    var signIn : GPPSignIn?
    var fbAccessToken = "406062389550689"
    let delObj = UIApplication.sharedApplication().delegate as! AppDelegate
    var loginData : JSON = JSON("")
    var chk_flag = ""
    let defaults = NSUserDefaults.standardUserDefaults()
    var HUD:MBProgressHUD = MBProgressHUD()
    
    
    var alert = UIAlertController()
    
    var alert1 = UIAlertController()
    
    var loadAlert = UIAlertView()
    var userID = ""
    var tmpUserName : String = String()
    var tmpUserPwd : String = String()
    var SMaccesstoken : String = ""
    
    var actionToEnable : UIAlertAction?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        userID = self.defaults.valueForKey("UID") as! String
        HUD.labelText = ""
        
//        GIDSignIn.sharedInstance().delegate = self
//        GIDSignIn.sharedInstance().uiDelegate = self
        
        var tmpUserName : String = String()
        var tmpUserPwd : String = String()
        
        // Google login variables
        
       /* signIn = GPPSignIn.sharedInstance()
        signIn?.shouldFetchGooglePlusUser = true
        signIn?.shouldFetchGoogleUserID=true
        signIn?.shouldFetchGoogleUserEmail=true
        signIn?.clientID = "69758586256-rp3p8764ia9tij7qbglh95lie58fs8j3.apps.googleusercontent.com"
        signIn?.scopes = [kGTLAuthScopePlusLogin]
        signIn?.delegate = self*/
        
        let signIn: GIDSignIn = GIDSignIn.sharedInstance()
        // if self.fetchEmailToggle.isEnabled {
         signIn.shouldFetchBasicProfile = true
         //}
         signIn.clientID = "69758586256-rp3p8764ia9tij7qbglh95lie58fs8j3.apps.googleusercontent.com"
         signIn.scopes = ["profile", "email"]
         signIn.delegate = self
         signIn.uiDelegate = self
         //self.statusField.text = "Initialized auth2..."
        
        self.btsignin.setTitle("Sign In", forState: UIControlState.Normal)
        
        // Check for default values of Login type flag. If nil put values.
        
        if(defaults.valueForKey("fbG+accessToken") != nil){
            SMaccesstoken = self.defaults.valueForKey("fbG+accessToken") as! String
            
        }else{
            SMaccesstoken = "cleared"
            defaults.setValue("cleared", forKey:"fbG+accessToken" )
            
        }
        if(defaults.valueForKey("UserName") != nil){
            tmpUserName  = defaults.valueForKey("UserName") as! String
            
        }else{
            tmpUserName = ""
        }
        
        if(defaults.valueForKey("Password") != nil){
            tmpUserPwd  = defaults.valueForKey("Password") as! String
            
        }else{
            tmpUserPwd = ""
        }
        
        keystoneID = ""
        proPic = ""
        name = ""
        
        //       //print("userID at LoginView:: \(userID)")
        //       //print("SMaccesstoken at LoginView:: \(SMaccesstoken)")
        //       //print("tmpUserName:- \(tmpUserName)")
        //       //print("tmpUserPwd:- \(tmpUserPwd)")
        
        // All are available then autologin
        
        // Local Notification to inform that User did pressed home/or awaked app while login with social media.
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(ViewController.methodOfReceivedNotificationGplus(_:)), name:
            UIApplicationDidBecomeActiveNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(ViewController.enableRetry), name:"enableRetry", object: nil)
        
        //Alert message to indicate Autologin with infoSAGE or SocialAccount.
        
        
        //  if(userID != "" && (SMaccesstoken == "InfoSAGE"  || SMaccesstoken == "Facebook" || SMaccesstoken == "Google+") && (tmpUserName != "" && tmpUserPwd != "" ))
        
        if(userID != "" && SMaccesstoken == "InfoSAGE" && (tmpUserName != "" && tmpUserPwd != "" ))
        {
            
            glob_logUserID = userID
            glob_viewAsProxy = false
            
            alert = UIAlertController(title: " Logging in. Please Wait…", message: nil ,preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Cancel, handler:{ (UIAlertAction)in Void()  }))
            
            
            let action = UIAlertAction(title: "Retry", style: UIAlertActionStyle.Default, handler:{ (UIAlertAction)in Void()
                self.delObj.checkLogin()  })
            
            alert.addAction(action)
            
            self.actionToEnable = action
            action.enabled = false
            dispatch_async(dispatch_get_main_queue(),{
                self.presentViewController(self.alert, animated: true, completion: {})
                
            });
            
        }else if(userID != "" && (SMaccesstoken == "Facebook" || SMaccesstoken == "Google+") && (tmpUserName == "" && tmpUserPwd == "" ))
        {
            
             glob_logUserID = userID
            glob_viewAsProxy = false
            
            //print("auto login with social media:")
            alert = UIAlertController(title: "Logging in. Please Wait…", message: nil ,preferredStyle: UIAlertControllerStyle.Alert)
            
            alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.Cancel, handler:{ (UIAlertAction)in Void()  }))
            
            let action = UIAlertAction(title: "Retry", style: UIAlertActionStyle.Default, handler:{ (UIAlertAction)in Void()
                self.delObj.checkLogin()
            })
            
            alert.addAction(action)
            self.actionToEnable = action
            action.enabled = false
            
            self.presentViewController(alert, animated: true, completion: {})
        }
        else
        {
            // //print("No auto login")
            
        }
        
        
        self.activityIndicator.hidden = true
        self.navigationController?.navigationBar.hidden = true
        self.navigationController?.interactivePopGestureRecognizer!.enabled = false;
        self.navigationController?.interactivePopGestureRecognizer!.delegate = self;
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.handleSingleTap(_:)))
        tapRecognizer.numberOfTapsRequired = 1
        
        btsignin.layer.cornerRadius = 5
        
        //self.view.addGestureRecognizer(tapRecognizer)
        txtUserName.layer.masksToBounds = true
        txtUserName.layer.borderColor = UIColor.grayColor().CGColor;
        txtUserName.layer.borderWidth = 0.5;
        txtUserName.layer.cornerRadius = 5
        txtPassword.layer.masksToBounds = true
        txtPassword.layer.borderColor = UIColor.grayColor().CGColor;
        txtPassword.layer.borderWidth = 0.5;
        txtPassword.layer.cornerRadius = 5
    }
    
   
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        //myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func signIn(signIn: GIDSignIn!,
                presentViewController viewController: UIViewController!) {
        self.presentViewController(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func signIn(signIn: GIDSignIn!,
                dismissViewController viewController: UIViewController!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
                withError error: NSError!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
                        //print("User ID : \(userId) \n Token: \(idToken) \n FullName \(fullName) \n GivenNAme: \(givenName) \n FamilyName \(familyName) \n Email : \(email)")
          
            defaults.setValue("Google+", forKey:"fbG+accessToken" )
            
            socialMediaLogin(email!, accessToken: idToken!)
            // ...
        } else {
            //print("\(error.localizedDescription)")
        }
    }
    
    //MARK: google login
    
    @IBAction func signInClicked(sender: AnyObject) {
       // self.statusField.text = "Clicked sign in!"
        
     // Mod Suraj 22 July
        
        if(Reachability.isConnectedToNetwork()){
            
           
            HUD.labelText = "Login with Google"
            HUD.square = false
            self.view.addSubview(HUD)
            self.HUD.hidden = false
            HUD.show(true)
            
            
            GIDSignIn.sharedInstance().signIn()
        }else{
            self.delObj.networkConnError()
        }
        
         //End 22 july
    }
    
    @IBAction func signOutClicked(sender: AnyObject) {
       // self.statusField.text = "Clicked sign out!"
        GIDSignIn.sharedInstance().signOut()
    }
    
    @IBAction func disconnectClicked(sender: AnyObject) {
       // self.statusField.text = "Clicked disconnect!"
        GIDSignIn.sharedInstance().disconnect()
    }
    
    @IBAction func emailToggled(sender: AnyObject) {
        
         GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
        /*
        if self.fetchEmailToggle.isEnabled {
            GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
        }
        else {
            GIDSignIn.sharedInstance().shouldFetchBasicProfile = false
        }*/
    }

    override func viewWillAppear(animated: Bool) {
        //Push not setting
        
        super.viewWillAppear(true)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.postDeviceTokenToDB(_:)), name:"postTokenToController", object: nil)
        
    }
    
    func enableRetry()
    {
        self.actionToEnable?.enabled = true
        
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        
        if(self.userID != "" && SMaccesstoken != "" && (tmpUserName == "" && tmpUserPwd == "" ))
        {
            
            if(SMaccesstoken == "Facebook")
            {
                self.btFacebooklogin(self)
            }else if(SMaccesstoken == "Google+")
            {
                self.signInClicked(self)
            }else{
                
                
            }
            
        }
        
    }
    
    override func viewWillDisappear(animated:Bool) {
        //print("Disappearing..")
        super.viewWillDisappear(animated)
        
        self.alert.dismissViewControllerAnimated(true){}
    }
    
    
    // Scroll Username and Password textfield when start typing.
    
    func textFieldDidBeginEditing(textField: UITextField) {
        logScrollView.setContentOffset(CGPoint(x: 0, y:  CGRectGetHeight(txtPassword.bounds)+10), animated: true)
    }
    func textFieldDidEndEditing(textField: UITextField) {
        logScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    
    //MARK: FB
    @IBAction func btFacebooklogin(sender: AnyObject) {
        
        // let accessToken = self.defaults.valueForKey("fbG+accessToken") as! String
        //print("accessToken in FB: \(accessToken)")
        
        HUD.labelText = "Login with Facebook"
        HUD.square = false
        self.HUD.hidden = false
        self.view.addSubview(HUD)
        HUD.show(true)
        
        self.activityIndicator.hidden = false
        
        if (FBSDKAccessToken.currentAccessToken() == nil)
        {
            let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "name,first_name, last_name,email"])
            
            graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
                if ((error) != nil){}else{}   })
            
            self.loginmanager.logInWithReadPermissions(["public_profile", "email"], handler: { (result, error) -> Void in
                if (error == nil) {
                    if (result.isCancelled) {
                        self.activityIndicator.hidden = true
                        self.HUD.hidden = true
                    }
                    else
                    {
                        if (result.grantedPermissions.contains("email")) {
                            if (FBSDKAccessToken.currentAccessToken() != nil)
                            {
                                self.fbAccessToken = FBSDKAccessToken.currentAccessToken().tokenString
                                self.returnUserData()
                            }
                        }
                    }
                }
            })
        }
        else
        {
            fbAccessToken = FBSDKAccessToken.currentAccessToken().tokenString
            self.returnUserData()
        }
    }
    
    func returnUserData()
    {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "name,gender,picture, first_name, last_name,location, email"])
        
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            
            if ((error) != nil)
            {}
            else
            {
                let userEmail : NSString = result.valueForKey("email") as! NSString
                if(userEmail != "")
                {
                    self.socialMediaLogin(userEmail as String, accessToken: self.fbAccessToken)
                    self.defaults.setValue("Facebook", forKey:"fbG+accessToken" )
                }
            }
        })
    }
    
    //MARK: G+
    
    @IBAction func btGooglepluslogin(sender: AnyObject) {
        
       // self.signIn?.authenticate()
//        HUD.labelText = "Login with google+"
//        HUD.square = false
//        self.view.addSubview(HUD)
//        self.HUD.hidden = false
//        HUD.show(true)
        
        //self.statusField.text = "Clicked sign in!"
      //  GIDSignIn.sharedInstance().signIn()

        
        
    }
    
    
    
    func methodOfReceivedNotificationGplus(notification: NSNotification)
    {
        if (((GPPSignIn.sharedInstance().authentication)) != nil) {
            //print(" The user has  signed in properly")
        }
        else
        {
            if(HUD.labelText == "Login with google")
            {
                self.HUD.hidden = true
                //print("The user has  not  signed in properly")
            }
        }
    }
    
    func finishedWithAuth(auth: GTMOAuth2Authentication!, error: NSError!) {
        if(error == nil)
        {
            
            let email = self.signIn?.authentication.userEmail
            let accessToken = auth.accessToken
            
         //   defaults.setValue("Google+", forKey:"fbG+accessToken" )
            
            if(email != nil && accessToken != nil)
            {
                //self.alert.dismissViewControllerAnimated(true, completion: { () -> Void in})
                socialMediaLogin(email!, accessToken: accessToken!)
            }else{
                
                self.activityIndicator.hidden = true
                self.HUD.hidden = true
            }
            
        }else{
            
            self.activityIndicator.hidden = true
            self.HUD.hidden = true
            
        }
    }
    
    func didDisconnectWithError(error: NSError!)
    {
        if ((error) != nil) {} else {
            
        }
    }
    
    // Login with Social Media : email and access token.
    
    func socialMediaLogin(emailAddress: String , let accessToken : String )
    {
        let headers = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
        
        Alamofire.Manager.sharedInstance
            .request(.POST,delObj.baseURL + "login/oauth", parameters: ["emailAddress": emailAddress ,
                "authorizationToken": accessToken],encoding: .JSON,headers: headers).responseJSON {
               response in
                    
                print("request:\(response.request)")
                print("response:\(response.response)")
                print("result:\(response.result.value)")
                if(response.result.isSuccess)
                {
                    var json1 = JSON(response.result.value!)
                    
                    print("json1:\(json1)")
                    if(json1["userMessage"].stringValue == "")
                    {
                        self.loginData = JSON(response.result.value!)
                        self.userLoginData()
                        self.activityIndicator.hidden = false
                    }
                    else
                    {
                        
                        //print(json1["userMessage"].stringValue)
                        
                        let alert = UIAlertController(title: "You will need InfoSAGE credentials in order to sign-in", message: "Please sign-up at infosagehealth.org", preferredStyle: UIAlertControllerStyle.Alert)
                        
                        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Cancel, handler:{ (UIAlertAction)in Void()
                            let signIn = GPPSignIn.sharedInstance()
                            signIn?.signOut()
                        }))
                        alert.addAction(UIAlertAction(title: "Sign Up", style: UIAlertActionStyle.Default, handler:{
                            (UIAlertAction)in Void()
                            let url:NSURL? = NSURL(string: "http://infosagehealth.org")
                            UIApplication.sharedApplication().openURL(url!)
                            
                        }))
                        self.presentViewController(alert, animated: true, completion: {                        })
                        self.HUD.hidden = true
                        self.activityIndicator.hidden = true
                        self.delObj.networkConnError();
                    }
                    
                }
                else if(response.response?.statusCode == 401 && self.SMaccesstoken == "Google+")
                {
                    let signIn = GPPSignIn.sharedInstance()
                    signIn?.signOut()
                    if(self.userID != "" && self.SMaccesstoken != "" && (self.tmpUserName == "" && self.tmpUserPwd == "" ))
                    {
                        if(self.SMaccesstoken == "Google+")
                        {
                            self.signInClicked(self)
                        }
                    }
                    
                }
                else if(response.result.isFailure)
                {
                    self.actionToEnable?.enabled = true
                    
                    if(response.response == nil)
                    {
                        self.HUD.hidden = true
                        self.activityIndicator.hidden = true
                        self.delObj.networkConnError();
                        
                        
                        
                    }else{
                        //print(response?.statusCode);
                        
                        self.defaults.setValue("cleared", forKey:"fbG+accessToken" )
                        let alert = UIAlertController(title: "You will need InfoSAGE credentials in order to sign-in", message: "Please sign-up at infosagehealth.org", preferredStyle: UIAlertControllerStyle.Alert)
                        
                        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Cancel, handler:{ (UIAlertAction)in Void()
                            let signIn = GPPSignIn.sharedInstance()
                            signIn?.signOut()
                        }))
                        alert.addAction(UIAlertAction(title: "Sign Up", style: UIAlertActionStyle.Default, handler:{
                            (UIAlertAction)in Void()
                            let url:NSURL? = NSURL(string: "http://infosagehealth.org")
                            UIApplication.sharedApplication().openURL(url!)
                            
                        }))
                        self.presentViewController(alert, animated: true, completion: {
                        })
                        self.HUD.hidden = true
                        self.activityIndicator.hidden = true
                    }
                }
                else
                {
                    self.HUD.hidden = true
                    self.activityIndicator.hidden = true
                    self.delObj.networkConnError();
                }
                
        }
        
    }
    
    
    // Login with InfoSAGE username and password
    
    @IBAction func btSignin(sender: UIButton) {
        delObj.chkVal = ""
        delObj.validateTextField(txtUserName)
        delObj.validateTextField(txtPassword)
        
        if(userID != ""){
            let vc : personInformation = personInformation()
            var tokanid = self.defaults.valueForKey("deviceToken") as? String
            
            if(tokanid == nil){
                tokanid = "h433897fg77873456fg868389"
            }
            
            vc.doLogout(tokanid!, stringUserID: self.userID)
            
        }
        
        if(delObj.chkVal == "")
        {
            self.activityIndicator.hidden = false
            
            let headers = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
            
            Alamofire.Manager.sharedInstance
                .request(.POST, delObj.baseURL + "login", parameters: ["username": txtUserName.text!,
                    "password": txtPassword.text!],headers: headers, encoding: .JSON)
                .validate()
                .responseJSON {
                    response in
                    
                    if(response.result.isSuccess)
                    {
                        
                        self.loginData = JSON(response.result.value!)
                        
                        // Load logged in user data to proceed
                        self.userLoginData()
                        //Set defaults login type flag to infoSAGE.
                        
                        self.defaults.setValue("InfoSAGE", forKey:"fbG+accessToken" )
                        
                    }else if(response.result.isFailure)
                    {
                        self.txtPassword.layer.borderColor = UIColor.redColor().CGColor
                        self.txtUserName.layer.borderColor = UIColor.redColor().CGColor
                        
                        let alertController = UIAlertController(title: "Login Failed", message: "Are you sure your username and password were entered correctly?", preferredStyle: UIAlertControllerStyle.Alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Destructive,handler: nil))
                        
                        self.presentViewController(alertController, animated: true, completion: nil)
                        self.HUD.hidden = true
                        self.activityIndicator.hidden = true
                        // self.delObj.networkConnError();
                        
                    }else
                    {
                        self.defaults.setValue("cleared", forKey:"fbG+accessToken" )
                        self.HUD.hidden = true
                        self.activityIndicator.hidden = true
                        self.delObj.networkConnError();
                    }
            }
        }
    }
    
    // Store defaults for logged in user.
    func userLoginData()
    {
        
        defaults.setValue("", forKey:"UID" )
        
        //Search page should display help text.
        
        if(self.defaults.valueForKey("helpText") != nil){
            let helpText = self.defaults.valueForKey("helpText") as! String
            //print("Help Text: \(helpText)")
        }else{
            defaults.setValue("YES", forKey:"helpText")
        }
        
        self.HUD.hidden = true
        self.activityIndicator.hidden = true
        
        
        // Check for user session expired or not
        
        if(self.loginData["userMessage"].stringValue == "" )
        {
            
            let myData = NSKeyedArchiver.archivedDataWithRootObject(loginData["user"].object)
            
            defaults.setValue(myData, forKey:"USEROBJECT")
            
            defaults.setValue(loginData["user"]["id"].stringValue, forKey:"UID" )
            
            glob_logUserID = loginData["user"]["id"].stringValue
            
            defaults.setValue(loginData["user"]["firstName"].stringValue + " " + loginData["user"]["lastName"].stringValue, forKey:"logUserName" )
            
            //Store user entered InfoSAGE Username and Password from Textfield  to use them at Autologin
            
            defaults.setValue(txtUserName.text, forKey:"UserName" )
            defaults.setValue(txtPassword.text, forKey:"Password" )
            
            if(loginData["user"]["profileImage"]["image"]["path"].stringValue == "")
            {
                defaults.setValue(self.delObj.baseImageURL + "w168xh168-2/images/default_profile_image.png", forKey:"logUserProPic" )
                // defaults.setValue( "default_profile_image.png", forKey:"logUserProPic" )
            }else{
                let tempPath = self.delObj.baseImageURL + "w168xh168-2/images/" + loginData["user"]["profileImage"]["image"]["path"].stringValue
                
                defaults.setValue(tempPath, forKey:"logUserProPic" )
                
                // defaults.setValue(loginData["user"]["profileImage"]["image"]["path"].stringValue, forKey:"logUserProPic" )
            }
            
            // Remove defaults if any so update new.
            
            defaults.setValue("", forKey:"getOneKeystone" )
            defaults.setValue("", forKey:"name" )
            defaults.setValue("", forKey:"proPic" )
            defaults.setValue("", forKey:"keystoneID" )
            defaults.setValue("", forKey:"PROXY" )
            defaults.setValue("false", forKey:"POST")
            
            
            
            let userID = defaults.valueForKey("UID") as? String
            
            let connURL = self.delObj.baseURL+"users/" + userID! + "/connections/"
            
            let headers = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
            
            // Fetch network of Logged in user
            
            Alamofire.request(.GET, connURL, parameters: nil ,headers: headers, encoding: .JSON)
                .responseJSON {
                    
                    response in
                    
                    if(response.result.isSuccess)
                    {
                        let json1 = JSON(response.result.value!)
                        
                        let count: Int? = json1.array?.count
                        
                        self.defaults.setValue(json1[0]["owner"]["isElder"].stringValue, forKey:"isElderStatus" )
                        
                        // If Logged in user(owner) is a Keystone, redirect him to  --> PersonalPage i.e. ToDoList
                        
                        glob_loginUserIsKeystone = json1[0]["owner"]["isElder"].boolValue
                        
                        if(json1[0]["owner"]["isElder"].boolValue)
                        {
                            self.name = json1[0]["owner"]["firstName"].stringValue + " " + json1[0]["owner"]["lastName"].stringValue
                            
                            if(json1[0]["owner"]["profileImage"]["image"]["path"].stringValue == "")
                            {
                                self.proPic = self.delObj.baseImageURL + "w168xh168-2/images/default_profile_image.png"
                                
                            }else
                            {
                                self.proPic = self.delObj.baseImageURL + "w168xh168-2/images/"+(json1[0]["owner"]["profileImage"]["image"]["path"].stringValue)
                                
                            }
                            
                            self.keystoneID = json1[0]["owner"]["id"].stringValue
                            self.defaults.setValue(count, forKey:"getOneKeystone" )
                            self.defaults.setValue(self.name, forKey:"name" )
                            self.defaults.setValue(self.proPic, forKey:"proPic" )
                            self.defaults.setValue(self.keystoneID, forKey:"keystoneID" )
                            
                            glob_keystoneID = json1[0]["owner"]["id"].stringValue
                            glob_loginUserIsKeystone = true
                            self.defaults.setValue("", forKey:"PROXY" )
                            
                            //Changed 18/12/2015 by Vj   self.defaults.setValue("PROXY", forKey:"PROXY" )
                            
                            //
                            let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idPersonalInforamation") as! personInformation
                            self.navigationController?.pushViewController(nextView, animated: true)
                            
                        }
                        else
                        {
                            if(count == 1)
                            {
                                
                                self.name = json1[0]["target"]["firstName"].stringValue + " " + json1[0]["target"]["lastName"].stringValue
                                
                                if(json1[0]["target"]["profileImage"]["image"]["path"].stringValue == "")
                                {
                                    self.proPic = self.delObj.baseImageURL + "w168xh168-2/images/default_profile_image.png"
                                    
                                }else
                                {
                                    self.proPic = self.delObj.baseImageURL + "w168xh168-2/images/"+(json1[0]["target"]["profileImage"]["image"]["path"].stringValue)
                                    
                                }
                                self.keystoneID = json1[0]["target"]["id"].stringValue
                                self.defaults.setValue(count, forKey:"getOneKeystone" )
                                self.defaults.setValue(self.name, forKey:"name" )
                                self.defaults.setValue(self.proPic, forKey:"proPic" )
                                self.defaults.setValue(self.keystoneID, forKey:"keystoneID" )
                                
                                glob_keystoneID = json1[0]["target"]["id"].stringValue
                                glob_loginUserIsKeystone = false
                                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idPersonalInforamation") as! personInformation
                                self.navigationController?.pushViewController(nextView, animated: false)
                                
                                
                            }
                            else
                            {
                                
                                if(json1.count != 0)
                                {
                                    // For multiple keystones redirect to home
                                    glob_loginUserIsKeystone = false
                                    let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idHomePage") as! homePage
                                    self.navigationController?.pushViewController(nextView, animated: true)
                                }else
                                {
                                    
                                    // For any other case: if no one keystone logged out that user with alert
                                    let chkTmp : Bool = self.loginData["user"]["isElder"].boolValue as Bool
                                    
                                    if(chkTmp){
                                        let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idPersonalInforamation") as! personInformation
                                        self.navigationController?.pushViewController(nextView, animated: false)
                                    }else{
                                        
                                        self.alert.dismissViewControllerAnimated(true){}
                                        
                                        
                                        let alertController = UIAlertController(title: "You have no Keystones. Please have a Keystone invite you to their network via the website.", message: "", preferredStyle: UIAlertControllerStyle.Alert)
                                        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
                                        
                                        self.presentViewController(alertController, animated: true, completion: nil)
                               
                                        var tokanid = self.defaults.valueForKey("deviceToken") as? String
                                        let userID = self.defaults.valueForKey("UID") as! String
                                        if(tokanid == nil){
                                            tokanid = "h433897fg77873456fg868389"
                                        }
                                        
                                        self.doLogout(tokanid!, stringUserID: userID)
                                        self.HUD.hidden = true
                                        self.activityIndicator.hidden = true
                                       
                                        
                                    }
                                    
                                }
                                
                            }
                        }
                        
                    }else{
                        self.HUD.hidden = true
                        self.activityIndicator.hidden = true
                        self.delObj.networkConnError();
                    }
            }
            // register for push notification
            let tokanid = defaults.valueForKey("deviceToken") as? String
            if(tokanid != nil){
                let headers = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
                Alamofire.Manager.sharedInstance
                    .request(.POST, delObj.baseURL + "users/" + userID! + "/push-notifications/apns/register", parameters: ["string": tokanid!],headers: headers, encoding: .JSON)
                    .responseJSON {
                       response in
                }
            }
            loadAlert.dismissWithClickedButtonIndex(0, animated: true)
        }
        else
        {
            loadAlert.dismissWithClickedButtonIndex(0, animated: true)
            self.HUD.hidden = true
            self.activityIndicator.hidden = true
            self.delObj.networkConnError();
            let alertController = UIAlertController(title: loginData["userMessage"].stringValue, message: "", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Destructive,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
    }
    
    func doLogout(let tokanid:String, stringUserID:String)
    {
        
        self.delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"];
        
        let headers = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"];
        
        
        //Clear defaults
       
        self.defaults.setValue("Logout", forKey:"fbG+accessToken" )
        self.defaults.setValue("", forKey:"UserName" )
        self.defaults.setValue("", forKey:"Password" )
        self.defaults.setValue("", forKey:"PROXY" )
        self.defaults.setValue("", forKey: "UID")
        
        
        
        if(tokanid == "h433897fg77873456fg868389"){
            
            let loginmanager:FBSDKLoginManager=FBSDKLoginManager()
            let signIn = GPPSignIn.sharedInstance()
            loginmanager.logOut()
            signIn?.signOut()
            
            self.delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
            
            Alamofire.request(.POST, self.delObj.baseURL+"logout/" + stringUserID,headers:headers)
                .responseJSON {
                    response in
                    if(response.response?.statusCode == 204 ){
                        
                        
                        
                    }
            }
            
        }else{
            Alamofire.Manager.sharedInstance
                .request(.POST, self.delObj.baseURL + "users/" + stringUserID + "/push-notifications/apns/unregister", parameters: ["string": tokanid], encoding: .JSON, headers:headers)
                .responseJSON {
                    response in
                    
                    // Fb/ g+ authentication logout.
                    
                    let loginmanager:FBSDKLoginManager=FBSDKLoginManager()
                    let signIn = GPPSignIn.sharedInstance()
                    loginmanager.logOut()
                    signIn?.signOut()
                    
                    Alamofire.request(.POST, self.delObj.baseURL+"logout/" + stringUserID,headers:headers)
                        .responseJSON {
                            response in
                            
                            //print(result)
                            //print(response)
                            
                            if(response.response?.statusCode == 204 ){
                                
                                
                            }
                            
                    }
                    
                    
                    
            }
        }
        
        
    }
    
    
    func postDeviceTokenToDB(notification:NSNotification){
        defaults.setValue(delObj.deviceTokenStr, forKey: "deviceToken")
        // defaults.synchronize()
    }
    
    
    
    
    // Sign-Up for new user.
    
    @IBAction func btNewUser(sender: AnyObject) {
        
        let url:NSURL? = NSURL(string: "https://www.infosagehealth.org")
      //  UIApplication.sharedApplication().openURL(url!)
        let safariVC = SFSafariViewController(URL:url!, entersReaderIfAvailable: false)
        safariVC.delegate = self
        self.presentViewController(safariVC, animated: true, completion: nil)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func handleSingleTap(recognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
        //alert.dismissViewControllerAnimated(true, completion:nil)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    func didFailWithOAuthIOError(error: NSError!) {
        //self.status_label.text = "Could not login with Facebook"
    }
    func didFailAuthenticationServerSide(body: String!, andResponse response: NSURLResponse!, andError error: NSError!) {
        
    }
    
    func didAuthenticateServerSide(body: String!, andResponse response: NSURLResponse!) {
        
    }
    
    func didReceiveOAuthIOCode(code: String!) {
        
    }
    
    
}
