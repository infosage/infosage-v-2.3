import UIKit
import Alamofire
import SwiftyJSON
import CoreFoundation


@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate,UIGestureRecognizerDelegate {
    
    
    let FACEBOOK_SCHEME = "fb406062389550689"
    let GOOGLEPLUS_SCHEME = "org.infosagehealth.app"
    var window: UIWindow?
    var chkVal = ""
    var state = false
    var notState = false
    let height = UIScreen.mainScreen().bounds.size.height
    var storyboadName = ""
    var deviceTokenStr = ""
    var devicetype : String = String()
    var kIDHeader : String = String()
    var headersAll : Dictionary<String,String> = Dictionary<String,String>()
      var medEditflag : Bool = Bool()
    var swipeFlag : Bool = Bool()
    
    // Select either of API to switch production or staging API.
    
    // let baseURL = "https://infosagehealth.org/api/"
    // var baseImageURL = "http://infosagehealth.org/cache/"
    
    // let baseURL = "http://infosage-stage-apps.hsl.harvard.edu:80/api/"
    //var baseImageURL = "http://infosage-stage-apps.hsl.harvard.edu:80/cache/"
    
    let baseURL = "http://ec2-52-36-249-33.us-west-2.compute.amazonaws.com/api/"
    var baseImageURL = "http://ec2-52-36-249-33.us-west-2.compute.amazonaws.com/cache/"
    
    // Defaults:
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    var name: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("name")
        }
        set{
            NSUserDefaults.standardUserDefaults().setObject(newValue!, forKey: "name")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    var proPic: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("proPic")
        }
        set{
            NSUserDefaults.standardUserDefaults().setObject(newValue!, forKey: "proPic")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    var keystoneID: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("keyID")
        }
        set{
            NSUserDefaults.standardUserDefaults().setObject(newValue!, forKey: "keyID")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    var mainViewController:UIViewController = UIViewController()
    
    func anAsyncMethod(resultHandler: (result: AnyObject) -> Void) { }
    
    func anotherAsyncMethod(resultHandler: (result: AnyObject) -> Void) {}
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        swipeFlag = false
        Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
        
        headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
        
        [NSThread .sleepForTimeInterval(2)]
        
        let anotherCharacter: CGFloat = height
        
        
        switch anotherCharacter
        {
        case 568:
            storyboadName = "Main"
        case 480:
            storyboadName = "Main_iPhone4"
        case 667:
            storyboadName = "Main_iPhone6"
        case 736:
            storyboadName = "Main_iPhone6_plus"
        case 1024:
            storyboadName = "Main_iPad"
        default:
            storyboadName = "Main"
            
            
        }
        
        
        enum UIUserInterfaceIdiom : Int
        {
            case Unspecified
            case Phone
            case Pad
        }
        
        struct ScreenSize
        {
            static let SCREEN_WIDTH         = UIScreen.mainScreen().bounds.size.width
            static let SCREEN_HEIGHT        = UIScreen.mainScreen().bounds.size.height
            static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
            static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        }
        
        if(UIScreen.mainScreen().respondsToSelector(#selector(UIScreen.displayLinkWithTarget(_:selector:))) && (UIScreen.mainScreen().scale == 2.0))
        {
            
            //print("Retina @2x")
            
        }else if(UIScreen.mainScreen().respondsToSelector(#selector(UIScreen.displayLinkWithTarget(_:selector:))) && (UIScreen.mainScreen().scale == 3.0)){
            
            //print("Retina @3x")
            
        }else{
            
            //print("No retina")
            
        }
        
        struct IS_RETINA
        {
            
            static let retina2x = UIScreen.mainScreen().respondsToSelector(#selector(UIScreen.displayLinkWithTarget(_:selector:))) && (UIScreen.mainScreen().scale == 2.0)
            
            static let retina3x = UIScreen.mainScreen().respondsToSelector(#selector(UIScreen.displayLinkWithTarget(_:selector:))) && (UIScreen.mainScreen().scale == 3.0)
            
        }
        struct DeviceType
        {
            static let IS_IPHONE_4_OR_LESS  = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
            static let IS_IPHONE_5          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
            static let IS_IPHONE_6          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
            static let IS_IPHONE_6P         = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
            static let IS_IPHONE_6S          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
            static let IS_IPHONE_6SP         = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
            static let IS_IPAD              = UIDevice.currentDevice().userInterfaceIdiom == .Pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
            static let IS_IPAD_PRO          = UIDevice.currentDevice().userInterfaceIdiom == .Pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
        }
        
        
        if DeviceType.IS_IPHONE_6P {
            //print("IS_IPHONE_6P")
            self.defaults.setValue("IPHONE_6P", forKey:"deviceType" )
            self.defaults.setValue("1920x1080", forKey:"screenSize" )
            
        }else if DeviceType.IS_IPHONE_6 {
            self.defaults.setValue("IPHONE_6", forKey:"deviceType" )
            self.defaults.setValue("1334x750", forKey:"screenSize" )
            //print("IS_IPHONE_6")
            
        }else if DeviceType.IS_IPHONE_5 {
            self.defaults.setValue("IPHONE_5", forKey:"deviceType" )
            self.defaults.setValue("1136x640", forKey:"screenSize" )
            //print("IS_IPHONE_5")
            
        }else if DeviceType.IS_IPHONE_4_OR_LESS {
            //print("IS_IPHONE_4_OR_LESS")
            self.defaults.setValue("IPHONE_4_OR_LESS", forKey:"deviceType" )
            self.defaults.setValue("960x640", forKey:"screenSize" )
        }else if DeviceType.IS_IPAD {
            //print("IS_IPAD")
            self.defaults.setValue("IPAD", forKey:"deviceType" )
            self.defaults.setValue("2048x1536", forKey:"screenSize" )
            
        }else if DeviceType.IS_IPAD_PRO {
            //print("IS_IPAD_PRO")
            self.defaults.setValue("IPAD_PRO", forKey:"deviceType" )
            self.defaults.setValue("2732×2048", forKey:"screenSize" )
            
        }else{
            
            //print("IS_Simulator")
        }
        
        
        struct Version{
            static let SYS_VERSION_FLOAT = (UIDevice.currentDevice().systemVersion as NSString).floatValue
            static let iOS7 = (Version.SYS_VERSION_FLOAT < 8.0 && Version.SYS_VERSION_FLOAT >= 7.0)
            static let iOS8 = (Version.SYS_VERSION_FLOAT >= 8.0 && Version.SYS_VERSION_FLOAT < 9.0)
            static let iOS9 = (Version.SYS_VERSION_FLOAT >= 9.0 && Version.SYS_VERSION_FLOAT < 10.0)
        }
        
        if Version.iOS8 {
            //print("iOS8")
            
            self.defaults.setValue("iOS8", forKey:"OS" )
        }else if Version.iOS7 {
            //print("iOS7")
            self.defaults.setValue("iOS8", forKey:"OS" )
        }
        else if Version.iOS9 {
            self.defaults.setValue("iOS9", forKey:"OS" )
            //print("iOS9")
        }else {
            //print("iOS10")
            
            self.defaults.setValue("iOS10", forKey:"OS" )
        }
        
        //let device = UIDevice.currentDevice()
        //print(device.name)
        
        UIApplication.sharedApplication().registerForRemoteNotifications()
        self.defaults.setValue(0, forKey:"segIndex" )
        self.defaults.setValue("false", forKey:"MADE" )
        
        
        // Call to the Autologin
        
        checkLogin()
        
        //Remote Notification Variable
        
        let types: UIUserNotificationType = [UIUserNotificationType.Badge, UIUserNotificationType.Alert, UIUserNotificationType.Sound]
        let settings: UIUserNotificationSettings = UIUserNotificationSettings( forTypes: types, categories: nil )
        
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        
        // Initialize sign-in
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        //return true
    }
    
    
    
    @available(iOS 8.0,*)
    func application(application: UIApplication,
                     openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        print("sign in with annotation")
        
        if(url.scheme == FACEBOOK_SCHEME)
        {
            return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        
        return GIDSignIn.sharedInstance().handleURL(url,
                                                    sourceApplication: sourceApplication,
                                                    annotation: annotation)
    }
    @available(iOS 9.0, *)
    func application(app: UIApplication, openURL url: NSURL, options: [String : AnyObject]) -> Bool {
        print("sign in with options")
        
        if(url.scheme == FACEBOOK_SCHEME)
        {
            let sourceApplication: String? = options[UIApplicationOpenURLOptionsSourceApplicationKey] as? String
            return FBSDKApplicationDelegate.sharedInstance().application(app, openURL: url, sourceApplication: sourceApplication, annotation: nil)
        }else{
            
            return GIDSignIn.sharedInstance().handleURL(url,
                                                        sourceApplication: options[UIApplicationOpenURLOptionsSourceApplicationKey] as! String?,
                                                        annotation: options[UIApplicationOpenURLOptionsAnnotationKey])
        }
        
       
    }
    
    
   //  FB scheme
    
 /*   func application(application: UIApplication, openURL url: NSURL,sourceApplication: String?,
                     annotation: AnyObject) -> Bool {
        
        print("Social Login: \(url.scheme)")
        
        if(url.scheme == FACEBOOK_SCHEME)
        {
            return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
        }
        else
        {
            return GPPURLHandler.handleURL(url, sourceApplication: sourceApplication, annotation: annotation)
        }
        
    }*/
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject])
    {
        NSNotificationCenter.defaultCenter().postNotificationName("NotificationIdentifier", object: nil)
        let appState:UIApplicationState = application.applicationState
        if(appState == UIApplicationState.Inactive)
        {
            checkLogin()
        }
    }
    
    //Alert for network Connection Error
    
    func networkConnError(){
        let refreshAlert = UIAlertView()
        refreshAlert.title = "Error"
        refreshAlert.message = "Please check data connection!"
        refreshAlert.addButtonWithTitle("Dismiss")
        refreshAlert.show()
    }
    
    func displayeMessage(title: String,msg : String){
        let dispAlert = UIAlertView()
        dispAlert.title = title
        dispAlert.message = msg
        dispAlert.addButtonWithTitle("Dismiss")
        dispAlert.show()
    }
    
    // Remote Notfication Service
    func application( application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData ) {
        let characterSet: NSCharacterSet = NSCharacterSet( charactersInString: "<>" )
        
        let deviceTokenString: String = ( deviceToken.description as NSString )
            .stringByTrimmingCharactersInSet( characterSet )
            .stringByReplacingOccurrencesOfString( " ", withString: "" ) as String
        
        deviceTokenStr = deviceTokenString
        
        //print("deviceTokenString: \(deviceTokenString)")
        NSNotificationCenter.defaultCenter().postNotificationName("postTokenToController", object: nil)
        
        defaults.setValue(deviceTokenString, forKey:"deviceToken" )
        
    }
    
    func application( application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError ) {
        
    }
    // Remote notificaton Service Ends ..
    
    
    // On app launch, check for autologin. works only for infoSAGE creadantials.
    
    func checkLogin()
    {
        // create viewController code...
        keystoneID = ""
        proPic = ""
        name = ""
        let storyboard = UIStoryboard(name: storyboadName, bundle: nil)
        
        let userID = defaults.valueForKey("UID") as? String
        let userName = defaults.valueForKey("UserName") as? String
        let password = defaults.valueForKey("Password") as? String
        let accTk = defaults.valueForKey("fbG+accessToken") as? String
        
        
        if(userID == nil){
            self.defaults.setValue("", forKey:"UID")
        }
        if(userName == nil){
            self.defaults.setValue("", forKey:"UID")
        }
        if(password == nil){
            self.defaults.setValue("", forKey:"UID")
        }
        if(accTk == nil){
            self.defaults.setValue("cleared", forKey:"fbG+accessToken")
        }
        
        //       //print("userID in appdelgts: \(userID)")
        //       //print("accessToken in appdelgts: \(accTk)")
        //       //print("userName in appdelgts: \(userName)")
        //       //print("passWord in appdelgts: \(password)")
        
        self.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
        mainViewController = storyboard.instantiateViewControllerWithIdentifier("idLoginViewController") as! ViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        self.window?.rootViewController = nvc
        self.window?.makeKeyAndVisible()
        
        if(userName != "" && userName != nil && accTk == "InfoSAGE")
        {
            let  user = UITextField()
            let  pass = UITextField()
            
            user.text = userName
            pass.text = password
            
            let request = NSMutableURLRequest(URL: NSURL(string: baseURL+"login")!)
            let session = NSURLSession.sharedSession()
            request.HTTPMethod = "POST"
            
            let params = ["username":user.text!, "password":pass.text!] as Dictionary<String, String>
            
            //var err: NSError?
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue("0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", forHTTPHeaderField: "X-Vendor-Id")
            
            do {
                request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(params, options: [])
            } catch let error as NSError {
                //err = error
                //print(error)
                request.HTTPBody = nil
            }
            
            let task = session.dataTaskWithRequest(request) { data, response, error in
                guard data != nil else {
                    //print("no data found: \(error?.localizedDescription)")
                    NSNotificationCenter.defaultCenter().postNotificationName("enableRetry", object: nil)
                    return
                }
                
                do {
                    if let json = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? NSDictionary {
                        let success = json["success"] as? Int
                        
                        if let parseJSON = json as? NSDictionary {
                            if let userMessage = parseJSON["userMessage"] as? String {
                                
                                NSNotificationCenter.defaultCenter().postNotificationName("enableRetry", object: nil)
                                
                                let mainViewController = storyboard.instantiateViewControllerWithIdentifier("idLoginViewController") as! ViewController
                                
                                let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                                
                                self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
                                self.window?.rootViewController = nvc
                                self.window?.makeKeyAndVisible()
                                
                            }else{
                                let userID = self.defaults.valueForKey("UID") as? String
                                let tokanid = self.defaults.valueForKey("deviceToken") as? String
                                if(tokanid != nil){
                                    
                                    Alamofire.Manager.sharedInstance
                                        .request(.POST, self.baseURL + "users/" + userID! + "/push-notifications/apns/register", parameters: ["string": self.deviceTokenStr],headers: self.headersAll, encoding: .JSON)
                                        .responseJSON {
                                           response in
                                            
                                            
                                    }
                                }
                                
                                self.defaults.setValue("", forKey:"PROXY" )
                                self.defaults.setValue("", forKey:"getOneKeystone" )
                                self.defaults.setValue("", forKey:"name" )
                                self.defaults.setValue("", forKey:"proPic" )
                                self.defaults.setValue("", forKey:"keystoneID" )
                                self.defaults.setValue("", forKey:"PROXY" )
                                self.defaults.setValue("false", forKey:"POST" )
                                
                                let connURL = self.baseURL+"users/" + userID! + "/connections/"
                                
                                Alamofire.Manager.sharedInstance
                                    .request(.GET, connURL, parameters: nil , headers: self.headersAll,encoding: .JSON)
                                    .responseJSON {
                                        response in
                                        if(response.result.isSuccess)
                                        {
                                            let json1 = JSON(response.result.value!)
                                            
                                            let count: Int? = json1.array?.count
                                            self.defaults.setValue(json1[0]["owner"]["isElder"].stringValue, forKey:"isElderStatus" )
                                            if(json1[0]["owner"]["isElder"].boolValue)
                                            {
                                                
                                                self.name = json1[0]["owner"]["firstName"].stringValue + " " + json1[0]["owner"]["lastName"].stringValue
                                                
                                                if(json1[0]["owner"]["profileImage"]["image"]["path"].stringValue == "")
                                                {
                                                    self.proPic = self.baseImageURL + "w168xh168-2/images/default_profile_image.png"
                                                    
                                                }else
                                                {
                                                    self.proPic = self.baseImageURL + "w168xh168-2/images/"+(json1[0]["owner"]["profileImage"]["image"]["path"].stringValue)
                                                    
                                                }
                                                self.keystoneID = json1[0]["owner"]["id"].stringValue
                                                self.defaults.setValue(count, forKey:"getOneKeystone" )
                                                self.defaults.setValue(self.name, forKey:"name" )
                                                self.defaults.setValue(self.proPic, forKey:"proPic" )
                                                self.defaults.setValue(self.keystoneID, forKey:"keystoneID" )
                                                //self.defaults.setValue("PROXY", forKey:"PROXY" )
                                                
                                                //Logged in user is keystone so goto self Help List.
                                                glob_loginUserIsKeystone = true;
                                                glob_keystoneID = json1[0]["owner"]["id"].stringValue;
                                                glob_logUserID = json1[0]["owner"]["id"].stringValue
                                                
                                                self.mainViewController = storyboard.instantiateViewControllerWithIdentifier("idPersonalInforamation") as! personInformation
                                                
                                            }else{
                                                
                                                if(count <= 1)
                                                {
                                                    self.name = json1[0]["target"]["firstName"].stringValue + " " + json1[0]["target"]["lastName"].stringValue
                                                    
                                                    if(json1[0]["target"]["profileImage"]["image"]["path"].stringValue == "")
                                                    {
                                                        self.proPic = self.baseImageURL + "w168xh168-2/images/default_profile_image.png"
                                                        
                                                    }else
                                                    {
                                                        self.proPic = self.baseImageURL + "w168xh168-2/images/"+(json1[0]["target"]["profileImage"]["image"]["path"].stringValue)
                                                        
                                                    }
                                                    self.keystoneID = json1[0]["target"]["id"].stringValue
                                                    self.defaults.setValue(count, forKey:"getOneKeystone" )
                                                    self.defaults.setValue(self.name, forKey:"name" )
                                                    self.defaults.setValue(self.proPic, forKey:"proPic" )
                                                    self.defaults.setValue(self.keystoneID, forKey:"keystoneID" )
                                                    
                                                    glob_keystoneID = json1[0]["target"]["id"].stringValue
                                                    glob_loginUserIsKeystone = false
                                                    
                                                    self.mainViewController = storyboard.instantiateViewControllerWithIdentifier("idPersonalInforamation") as! personInformation
                                                    
                                                }else{
                                                    
                                                    glob_loginUserIsKeystone = false
                                                    glob_keystoneID = "0"
                                                    self.mainViewController = storyboard.instantiateViewControllerWithIdentifier("idHomePage") as! homePage
                                                    
                                                }
                                                
                                            }
                                            
                                            let nvc: UINavigationController = UINavigationController(rootViewController: self.mainViewController)
                                            
                                            self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
                                            self.window?.rootViewController = nvc
                                            self.window?.makeKeyAndVisible()
                                            
                                        }else{
                                            NSNotificationCenter.defaultCenter().postNotificationName("enableRetry", object: nil)
                                            
                                            self.networkConnError();
                                        }
                                        
                                }
                            }
                            
                        }
                        
                    } else {
                        let jsonStr = NSString(data: data!, encoding: NSUTF8StringEncoding)    // No error thrown, but not NSDictionary
                        //print("Error could not parse JSON: \(jsonStr)")
                    }
                } catch let parseError {
                    
                    let jsonStr = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    //print("Error could not parse JSON: '\(jsonStr)' '\(parseError)'")
                }
            }
            
            task.resume()
        }
        else
        {
            mainViewController = storyboard.instantiateViewControllerWithIdentifier("idLoginViewController")as! ViewController
            let nvc1: UINavigationController = UINavigationController(rootViewController: mainViewController)
            self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
            self.window?.rootViewController = nvc1
            self.window?.makeKeyAndVisible()
            
        }
        
    }
    
    // Text Field validation
    func validateTextField(textField: UITextField){
        
        if(textField.text == ""){textField.layer.borderColor = UIColor.redColor().CGColor
        }else{
            textField.layer.borderColor = UIColor.lightGrayColor().CGColor }
        
        if(textField.text == "" && chkVal == ""){chkVal = "Yes"}
        
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        
        //print("applicationWillEnterForeground")
        
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        //print("applicationDidBecomeActive")
        
        
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        //print("App is terminated")
    }
    
    
    
}

