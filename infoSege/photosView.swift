//
//  photosViewViewController.swift
//  infoSage
//
//  Created by Vijay MAC on 02/10/15.
//  Copyright © 2015 Pramod shirsath. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
import AssetsLibrary

extension UIImageView {
    public func imageFromUrl(urlString: String) {
        if let url = NSURL(string: urlString) {
            let request = NSURLRequest(URL: url)
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
                (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
                if let imageData = data as NSData? {
                    self.image = UIImage(data: imageData)
                }
            }
        }
    }
}



class photosView: UIViewController,UIAlertViewDelegate,UICollectionViewDelegateFlowLayout, UICollectionViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate , UIGestureRecognizerDelegate,MBProgressHUDDelegate {
    
    var HUD:MBProgressHUD = MBProgressHUD()
    
    let defaults = NSUserDefaults.standardUserDefaults()
    let imagePicker = UIImagePickerController()
    let delObj = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBOutlet var HeaderView: UIView!
    @IBOutlet var BigImageView: UIView!
    @IBOutlet var bigImage: UIImageView!
  
   // @IBOutlet var bigScrollView: UIScrollView!
    @IBOutlet var ViewOfCollVw: UIView!
    @IBOutlet weak var btClose: UIButton!
    @IBOutlet var btnFrwrd: UIButton!
    @IBOutlet var slider: UISlider!
    @IBOutlet var btnBckwrd: UIButton!
    @IBOutlet var btnOtionClicked: UIButton!
    @IBOutlet var btnUpload: UIButton!
    @IBOutlet var collectionView: UICollectionView?
    
    @IBOutlet var lblNoPhotos: UILabel!
    @IBOutlet var rightSwipe: UISwipeGestureRecognizer!
    
    @IBOutlet var leftSwipe: UISwipeGestureRecognizer!
    
    @IBOutlet var OptionView: UIView!
    
    @IBOutlet var setProfilePhoto: UIButton!
    
    @IBOutlet var deletePhoto: UIButton!
    
    var scrollView: UIScrollView!
    var NewBigImg: UIImageView = UIImageView()
    var idImages: [String] = []
    var profilePicId = -1
    var ProfileImages: [String] = []
    var uploadedImage:[String] = []
    var indexofimage = 0
    var keystoneID: String = String()
    var image: NSData = NSData()
    var strBase64 : NSString = NSString()
//    var image: UIImage = UIImage()
    var imageType : String = String()
    var imageToUpload: UIImage = UIImage()
    var imageString : NSString = NSString()
    var imgData : NSData = NSData()
    var stringOne:String = String()
    var UserName:String = String()
    var proPic: String = String()
    var UserPicDeleted: Bool = Bool()
    var VIEW_PHOTOS : String = String()
    var EDIT_PHOTOS : String = String()
    var UPLOAD_PHOTOS : String = String()
    var proxyUser : String = String()
    var loginasproxyUser : String = String()
    var proxyIDForAnalytics = ""
    var uploadingPhoto: Bool = false
    var isMedicationVisible : String = String()
    var isProxyUser : String = String()
    var isConnected : String = String()
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        self.bigImage.layer.contentsAreFlipped()
//        self.bigImage.layer.cornerRadius = 10
       // self.bigImage.clipsToBounds = true
        self.bigImage.userInteractionEnabled = true
        self.ViewOfCollVw.frame.origin.y = self.HeaderView.frame.height + 2
        stringOne = defaults.valueForKey("UID") as! String
        keystoneID = defaults.valueForKey("keyID") as! String
        VIEW_PHOTOS = defaults.valueForKey("VIEW_PHOTOS") as! String
        EDIT_PHOTOS = defaults.valueForKey("EDIT_PHOTOS") as! String
        UPLOAD_PHOTOS = defaults.valueForKey("UPLOAD_PHOTOS") as! String
        proxyUser = defaults.valueForKey("isProxyUser") as! String
        loginasproxyUser = defaults.valueForKey("PROXY") as! String
        UserName = defaults.valueForKey("name") as! String
        proPic = defaults.valueForKey("proPic") as! String
        UserPicDeleted = false
        // print("login user :\(stringOne) >> Keystone id \(keystoneID)")
        if(keystoneID == "")
        {
            keystoneID = stringOne
        }
                // Do any additional setup after loading the view.
        setProfilePhoto.setTitle("Set as profile photo", forState: UIControlState.Normal)
        setProfilePhoto.titleLabel!.lineBreakMode = NSLineBreakMode.ByWordWrapping;
        imagePicker.delegate = self
        //btnOtionClicked.hidden = true
        if(EDIT_PHOTOS == "true" || proxyUser == "true"){
            let tap = UITapGestureRecognizer(target: self, action: #selector(photosView.btnOtionClickedEv(_:)))
            bigImage.addGestureRecognizer(tap)
            bigImage.userInteractionEnabled = true
            //btnOtionClicked.hidden = false
        }
        
        
        if(UPLOAD_PHOTOS == "false"){
            btnUpload.hidden = true
        }
        
        checkPrivileges()
        
        if(self.loginasproxyUser == "PROXY")
        {
            delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.keystoneID]
            
            self.proxyIDForAnalytics = self.keystoneID
        }else{
            delObj.headersAll = ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ"]
            
            self.proxyIDForAnalytics = stringOne
        }
        
        let headers = delObj.headersAll
        
        if(!uploadingPhoto){
            
            loadAllUserPhoto()
            let device = UIDevice.currentDevice().name
            let screenSize = self.defaults.valueForKey("screenSize") as! String
            let OS = self.defaults.valueForKey("OS") as! String
            
            let alertController = UIAlertController(title: "Device Info", message: "\(UIDevice.currentDevice().name)  \(screenSize) \(OS)  @home", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: nil))
            
            //self..presentViewController(alertController, animated: true, completion: nil)
            
            let parameters = ["string":"photos"]
            
            var url = self.delObj.baseURL+"users/\(stringOne)/elasticLog/pageview/proxiedUser/\(self.proxyIDForAnalytics)/mobile?operatingSystem=\(OS)&deviceType=\(device)&screenSize=\(screenSize)"
            url = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            
           
            
            Alamofire.request(.POST, url,parameters: parameters,headers: headers,encoding: .JSON)
                .responseJSON {
                   response in
                    if(response.result.isSuccess)
                    {
                        
                    }else if(response.result.isFailure){
                        
                        //Succesfully analytics recorded.
                        
                    }else{
                        
                        self.delObj.networkConnError();
                        
                    }
            }
            
        }
        
        Alamofire.Manager.sharedInstance
            .request(.GET, delObj.baseURL+"users/" + keystoneID + "/profile", parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    
                    let json1 = JSON(response.result.value!)
                    
                    if(json1["userMessage"].stringValue == "You don't have permission to do that."){
                        //print("called from line :586")
                        self.LogoutForProxy()
                    }else{
                        
                        if(json1["user"]["isElder"].boolValue) {
                            
                            
                        }else{
                            print("called from todo line :287")
                            
                            self.LogoutForProxy()
                            //self.LogoutForKeystoneStatusChange()
                            
                        }
                        
                        
                    }
                    
                }
                //println(self.userData)
        }
        
        
        //Check participants user rights
        
        Alamofire.Manager.sharedInstance
            .request(.GET, delObj.baseURL + "users/" + keystoneID + "/privileges/users/" + stringOne, parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    var json1 = JSON(response.result.value!)
                    
                    
                    let count: Int? = json1.array?.count
                    if(count != 0)
                    {
                        
                        let isProxyInResponse = json1["PROXY"].stringValue
                        let isConnected = json1["CONNECTED"].stringValue
                        
                        
                        if(isConnected == "false" )
                        {
                            self.LogoutForProxy()
                            
                        }
                        
                        if(self.loginasproxyUser == "PROXY")
                        {
                            if(isProxyInResponse == "false" )
                            {
                                //print("Not Proxy Account")
                                
                                self.LogoutForProxy()
                                
                            }
                        }
                        
                        
                    }
                }
                
                
        }

        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView = UIScrollView()
        
        scrollView.scrollEnabled = true;
        scrollView.userInteractionEnabled = true
        NewBigImg = UIImageView()
        
        scrollView.contentSize = NewBigImg.bounds.size
        scrollView.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        
        scrollView.addSubview(NewBigImg)
        view.addSubview(scrollView)
        scrollView.hidden = true
        
       // self.setLayerDesign(collectionView!)
    }
    
    func checkPrivileges(){
    
    
        let headers = delObj.headersAll
        
        Alamofire.Manager.sharedInstance
            .request(.GET, delObj.baseURL + "users/" + keystoneID + "/privileges/users/" + stringOne, parameters: nil, encoding: .JSON, headers:headers)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    
                    var json1 = JSON(response.result.value!)
                    
                    let count: Int? = json1.array?.count
                    if(count != 0)
                    {
                        self.isMedicationVisible = json1["VIEW_MEDICATIONS"].stringValue as String
                        self.isProxyUser = json1["PROXY"].stringValue as String
                        self.isConnected = json1["CONNECTED"].stringValue as String
                        self.VIEW_PHOTOS = json1["VIEW_PHOTOS"].stringValue as String
                        self.EDIT_PHOTOS = json1["EDIT_PHOTOS"].stringValue as String
                        self.UPLOAD_PHOTOS = json1["UPLOAD_PHOTOS"].stringValue as String
                        self.defaults.setValue(self.VIEW_PHOTOS, forKey:"VIEW_PHOTOS" )
                        self.defaults.setValue(self.EDIT_PHOTOS, forKey:"EDIT_PHOTOS" )
                        self.defaults.setValue(self.UPLOAD_PHOTOS, forKey:"UPLOAD_PHOTOS" )
                        //self.defaults.setValue(self.isProxyUser, forKey:"isProxyUser")
                        
                        if(self.isConnected == "true"){
                            if(self.VIEW_PHOTOS == "true")
                            {
                                if(self.UPLOAD_PHOTOS == "true"){ }else{
                                
                                    self.LogoutForProxy()
                                    print("called from line :288")
                                }
                               
                            }else{
                                
                                  self.LogoutForProxy()
                            }
                        }else{
                            print("called from line :297")
                            self.LogoutForProxy()
                            
                            
                        }
                    }
                }else{
                    
                }
        }

        
        
    }
    
    func LogoutForProxy()
    {
        let alert = UIAlertController(title: "Your privileges have been modified for your Keystone(s). You will be re-logged in now.", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:
            {
                (UIAlertAction)in Void.self
                
                var tokanid = self.defaults.valueForKey("deviceToken") as? String
                
                if(tokanid == nil){
                    tokanid = "h433897fg77873456fg868389"
                }
                
                //Clear defaults
                defer{
                    
                    self.delObj.checkLogin()
                }
                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idLoginViewController") as! ViewController
                self.navigationController?.pushViewController(nextView, animated: true)
                
                
        }))
        
        dispatch_async(dispatch_get_main_queue(), {
            // code here
            
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
        //print("Your Keystone status has changed. You will be re-logged in now.")
        
        
    }

    //MARK: Load All images
    
    func loadAllUserPhoto()
    {
        let headers = delObj.headersAll
        self.proPic = defaults.valueForKey("proPic") as! String
        self.ProfileImages.removeAll(keepCapacity: false)
        self.idImages.removeAll(keepCapacity: false)
        Alamofire.request(.GET, self.delObj.baseURL+"users/"+self.keystoneID+"/images", headers:headers , encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    var json1 = JSON(response.result.value!)
                    if(json1["userMessage"].stringValue != "You must be logged in to do that."){
                        
                        let count: Int? = json1.array?.count
                        
                        if(count != 0 && count != nil)
                        {
                            var indexFlag = 0
                            self.ProfileImages = []
                            self.idImages = []
                            for index in 0...count!-1
                            {
                                /*
                                 let n = self.delObj.baseImageURL+"w168xh168-2/images/" + json1[index]["image"]["path"].stringValue
                                 let m = self.proPic
                                 self.ProfileImages.insert(self.delObj.baseImageURL+"w168xh168-2/images/" + json1[index]["image"]["path"].stringValue, atIndex: indexFlag)
                                 if n == m{
                                 self.profilePicId = indexFlag
                                 }else{
                                 }
                                 self.idImages.insert(json1[index]["id"].stringValue, atIndex: indexFlag)
                                 indexFlag += 1

                                 */
                                 let n = self.delObj.baseImageURL+"w168xh168-2/images/" + json1[index]["image"]["path"].stringValue
                                let m = self.proPic
                               
                                if n == m{
                                    self.profilePicId = indexFlag
                                }
                                
                                //http://ec2-52-36-249-33.us-west-2.compute.amazonaws.com/cache/
                                
                                
                                self.ProfileImages.insert(self.delObj.baseImageURL+"w300xh200-2/images/" + json1[index]["image"]["path"].stringValue, atIndex: indexFlag)
                                

                                
                                self.idImages.insert(json1[index]["id"].stringValue, atIndex: indexFlag)
                                indexFlag += 1
                            }
                            if(self.UserPicDeleted){
                                //self.UserPicDeleted = false
                                
                                self.forwardBtnClicked(self)
                                
                            }
                            self.lblNoPhotos.hidden = true
                            self.ViewOfCollVw.hidden = false
                            self.BigImageView.hidden = false
                            self.loadUploadersForKeystone()
                            //print(self.ProfileImages)
                            self.collectionView?.reloadData()
                        }else{
                            
                            
                            if(self.UserName == "" || self.UserName == " " || self.keystoneID == self.stringOne){
                                
                                self.lblNoPhotos.text = "You have not uploaded any photos."
                                
                            }else{
                                
                                self.lblNoPhotos.text = " \(self.UserName) has not uploaded any photos."
                                
                            }
                            
                            self.lblNoPhotos.hidden = false
                            self.ViewOfCollVw.hidden = true
                            self.BigImageView.hidden = true
                        }
                    }else{
                        self.delObj.checkLogin()
                    }
                }
                else
                {
                    print("Network Error....!!")
                }
        }
    }
    
    //MARK: Uploaders
    
    func loadUploadersForKeystone(){
        self.uploadedImage.removeAll(keepCapacity: false)
        let headers = delObj.headersAll
        Alamofire.request(.GET, self.delObj.baseURL+"users/"+self.keystoneID+"/images/uploaders", headers:headers , encoding: .JSON)
            .responseJSON {
               response in
                if(response.result.isSuccess)
                {
                    var json1 = JSON(response.result.value!)
                    let count: Int? = json1.array?.count
                    if(count != 0 && count != nil)
                    {
                        var indexFlag = 0
                        for index in 0...count!-1
                        {
                            if(json1[index]["id"].stringValue == self.stringOne)
                            {
                                self.uploadedImage.insert("1", atIndex: indexFlag)
                                indexFlag += 1
                            }else{
                                self.uploadedImage.insert("0", atIndex: indexFlag)
                                indexFlag += 1
                            }
                        }
                        //print("uploadedImage :> \(self.uploadedImage)")
                    }else{
                    }
                }
                else
                {
                    print("Network Error....!!")
                }
        }
        
    }
    
    func setLayerDesign(object:AnyObject)
    {
        object.layer.borderColor = UIColor.grayColor().CGColor
        object.layer.borderWidth = 0.5
        object.layer.cornerRadius = 5
        object.titleLabel?!.adjustsFontSizeToFitWidth = true
    }
    
    //MARK: Upload Photo
    
    @IBAction func clickUploadPhoto(sender: UIButton) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        presentViewController(imagePicker, animated: true, completion: nil)
        self.uploadingPhoto = true
        self.btnEvClose(self)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        print("info:\(info)")
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            self.image = UIImagePNGRepresentation(pickedImage)!
             //self.image = UIImageJPEGRepresentation(pickedImage, 0.98)!
            //let imageData : NSData = UIImagePNGRepresentation(pickedImage)!
            
           // self.strBase64  = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
            self.actionUpload()
        /*    if let referenceUrl = info[UIImagePickerControllerReferenceURL] as? NSURL {
                
                ALAssetsLibrary().assetForURL(referenceUrl, resultBlock: { asset in
                    
                    var fileName = asset.defaultRepresentation().filename()
                    //do whatever with your file name
                    print("fileName:\(fileName)")
                    fileName = fileName.lowercaseString
                    if fileName.containsString(".jp") {
                        print("jpg type file")
                        self.imageType = "jpeg"
                        self.image = UIImageJPEGRepresentation(pickedImage, 1.0)!
                    }else if( fileName.containsString(".pn")){
                        self.imageType = "png"
                        self.image = UIImagePNGRepresentation(pickedImage)!
                    }else{
                        self.imageType = "png"
                        self.image = UIImagePNGRepresentation(pickedImage)!
                    }
                    
                   
                    self.actionUpload()
                    }, failureBlock: nil)
            }*/
        
            
//            let imgData: NSData = NSData(data: UIImageJPEGRepresentation((image), 1)!)
//            let imageSize: Int = imgData.length
//            
//            let db = imageSize/1024000;
//            
//            print("size of image in MB:  \(db)")
            
            
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    class func bytesInImage(image: UIImage) -> Int {
        let imageRef: CGImageRef = image.CGImage!
        return CGImageGetBytesPerRow(imageRef) * CGImageGetHeight(imageRef)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    //Custome
    
/*    func imageType(imgData : NSData) -> String
    {
        var c = [UInt8](count: 1, repeatedValue: 0)
        imgData.getBytes(&c, length: 1)
        
        let ext : String
        
        switch (c[0]) {
        case 0xFF:
          
            ext = "jpg"
            self.imageType = "jpeg"
              print("ext:\(ext)")
        case 0x89:
            
            ext = "png"
            self.imageType = "png"
              print("ext:\(ext)")
        case 0x47:
            
            ext = "gif"
              print("ext:\(ext)")
        case 0x49, 0x4D :
            ext = "tiff"
              print("ext:\(ext)")
        default:
            ext = "" //unknown
        }
        
        return ext
    }*/
    //Ends
    
    
    
   // --------------------------------------------------
    func actionUpload() {
        let URL = delObj.baseURL+"users/"+self.keystoneID+"/images"
        let headers = delObj.headersAll
       
        //let image1 = image
        let imgCheck : UIImage = UIImage(named:"Checkmark.png")!
        let imgCross : UIImage = UIImage(named:"comClose.png")!
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.Indeterminate
        loadingNotification.labelText = "Calculating size..."
        
        
        // Start
        
        
        let imgData1 = image
        
     //  imageType(imgData1)
        // var imgData: NSData = UIImagePNGRepresentation(image)
        // you can also replace UIImageJPEGRepresentation with UIImagePNGRepresentation.
        let imageSize: Int = imgData1.length
        let calculatedSize = (Double(imageSize) / 1024.0)/1024.0
        print("size of image in MB: %f \(calculatedSize)")
        // ends
        
        if(calculatedSize <= 10){
            loadingNotification.labelText = "Uploading photo..."
            
        Alamofire.upload(.POST, URL,headers:headers, multipartFormData: {
            multipartFormData in
            if let imageData = imgData1 as? NSData //imgData1
                //UIImageJPEGRepresentation(image1, 1)
            {   multipartFormData.appendBodyPart(data: imageData, name: "file", mimeType: "image/png")}
            },encodingCompletion: {
                encodingResult in
                switch encodingResult {
                case .Success(let upload, _, _):
                    upload.responseJSON { response in
                        if(response.result.isSuccess){
                            
                            print("response:\(response)")
                            
                            //  To dismiss the ProgressHUD:
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            
                            self.ProfileImages.removeAll(keepCapacity: false)
                            self.idImages.removeAll(keepCapacity: false)
                            
                            self.collectionView?.reloadData()
                            
                            //You cannot upload this file. Please make sure your image is smaller than 10 MB.
                            
                            NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(photosView.loadAllUserPhoto), userInfo: nil, repeats: false)
                            self.HUD.customView = UIImageView(image: imgCheck)
                            self.HUD.mode = MBProgressHUDMode.CustomView
                            self.HUD.labelText = "Photo has been uploaded."
                            self.view.addSubview(self.HUD)
                            self.HUD.show(true)
                            self.HUD.hide(true, afterDelay: 4)
                            self.uploadingPhoto = false
                        }else{
                            
//                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
//                            self.HUD.customView = UIImageView(image: imgCross)
//                            self.HUD.mode = MBProgressHUDMode.CustomView
//                            self.HUD.labelText = "You cannot upload this file. Please make sure your image is smaller than 10 MB."
//                            self.view.addSubview(self.HUD)
//                            self.HUD.show(true)
//                            self.HUD.hide(true, afterDelay: 2)
                            self.uploadingPhoto = false
                            
                            let alert = UIAlertController(title: "Failed", message: "You cannot upload this file. Please make sure your image is smaller than 10 MB.", preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.Cancel, handler:{(UIAlertAction)in Void.self
                               }))
                            
                            
                            print(encodingResult)
                            print(response)
                            print(response.result)
                            
                        }
                    }
                    break
                case .Failure(let encodingError):
                    print(encodingError)
                    self.uploadingPhoto = false
                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                    
                }
        })
        }else{
            //Image size is exceed tha 10 MB
            
           self.delObj.displayeMessage("Failed", msg: "You cannot upload this file. Please make sure your image is smaller than 10 MB.")
             MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
        }
        
    }
    
    
    @IBAction func changeImageViewScale(sender: AnyObject) {
       bigImage.transform = CGAffineTransformMakeScale(CGFloat(slider.value), CGFloat(slider.value))
    }
    
    
    //MARK: Set Image to Profile
    
    @IBAction func evnSetAsProfilePhoto(sender: AnyObject) {
        let alert = UIAlertController(title: "Confirm to Update Profile Image", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler:{(UIAlertAction)in Void.self
            self.collectionView?.reloadData()}))
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Destructive, handler:{
            (UIAlertAction)in Void.self
        }))
        // self..presentViewController(alert, animated: true, completion: { self.OptionView.hidden = true})
        let headers = delObj.headersAll
        let imageID = self.idImages[self.indexofimage]
        self.profilePicId = self.indexofimage
        self.setProfilePhoto.enabled = false
        self.setProfilePhoto.alpha = 0.7
        let myString: NSString = imageID
        let myStringToInt: Int = Int(myString.intValue)
        let parameters = ["wrappedInteger": myStringToInt];
        Alamofire.request(.PUT,self.delObj.baseURL+"users/"+self.keystoneID+"/images/profile/mobile", parameters: parameters, headers:headers , encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    var json1 = JSON(response.result.value!)
                    //print(json1)
                    
                    if(json1["userMessage"].stringValue != "You must be logged in to do that."){
                        
                        let tempPath = self.delObj.baseImageURL + "w168xh168-2/images/" + json1["image"]["path"].stringValue
                        if(self.keystoneID == self.stringOne){
                            self.defaults.setValue(tempPath, forKey:"logUserProPic")
                        }
                        self.defaults.setValue(tempPath, forKey:"proPic")
                        NSNotificationCenter.defaultCenter().postNotificationName("ProfilePhotoupdated", object: nil)
                        
                        
                    }else{
                        self.delObj.checkLogin()
                    }
                }
                self.OptionView.hidden = true
                //   self.loadAllUserPhoto()
                //   self.collectionView?.reloadData()
        }
    }
    
    //MARK: Delete image
    
    @IBAction func evnDeletePhoto(sender: AnyObject) {
        let alert = UIAlertController(title: "Confirm to Delete Image", message: "Are you sure that you want to delete this Image ?", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.Cancel, handler:{(UIAlertAction)in Void.self
            self.collectionView?.reloadData()}))
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.Destructive, handler:{
            (UIAlertAction)in Void.self
            let headers = self.delObj.headersAll
            if (self.indexofimage == self.profilePicId) {
                self.profilePicId = -1
                self.defaults.setValue(self.delObj.baseImageURL + "w168xh168-2/images/default_profile_image.png", forKey:"proPic" )
                NSNotificationCenter.defaultCenter().postNotificationName("ProfilePhotoupdated", object: nil)
            }
            ///http://ec2-52-36-249-33.us-west-2.compute.amazonaws.com/api/users/:userId/images/:userImageId
            Alamofire.request(.DELETE, self.delObj.baseURL+"users/"+self.keystoneID+"/images/"+self.idImages[self.indexofimage], headers:headers , encoding: .JSON)
                .responseJSON {
                  response in
                    if(response.result.isSuccess){
                    
                        self.loadAllUserPhoto()
                        self.UserPicDeleted = true
                    }else{
                        
                        self.delObj.networkConnError()
                        
                    }
            }
        }))
        
        
        alert.popoverPresentationController?.sourceView = self.BigImageView
        alert.popoverPresentationController?.sourceRect = self.BigImageView.bounds
        
        self.presentViewController(alert, animated: true, completion: { self.OptionView.hidden = true})
    }
    
    //MARK: Photo Views Options
    
    @IBAction func swipedSctionLeft(sender: UISwipeGestureRecognizer) {
    
       // bigImage.zoomIn() // here the magic
        forwardBtnClicked(UISwipeGestureRecognizer)
    }
    @IBAction func swipedAction(sender: UISwipeGestureRecognizer) {
        
       // bigImage.zoomIn()
        backBtnClicked(UISwipeGestureRecognizer)
    }
    @IBAction func backBtnClicked(sender: AnyObject)  {
        if(!btnFrwrd.hidden){
        btnFrwrd.hidden = false
        }
        self.OptionView.hidden = true
        self.indexofimage -= 1
        //print(" On backwrd :\(indexofimage)")
        if(indexofimage < 0)
        {
            indexofimage = ProfileImages.count - 1
        }
        checkAvailabilityOfPhotosOption()
        if (indexofimage == self.profilePicId) {
            self.setProfilePhoto.enabled = false
            self.setProfilePhoto.alpha = 0.7
        }else{
            self.setProfilePhoto.enabled = true
            self.setProfilePhoto.alpha = 1
        }
        let indexPath = NSIndexPath(forRow: self.indexofimage, inSection: 0)
        self.collectionView!.scrollToItemAtIndexPath(indexPath, atScrollPosition: UICollectionViewScrollPosition.Bottom, animated: true)
        let headers = delObj.headersAll
        if(indexofimage > -1)
        {
            
//            let newUrl = ProfileImages[self.indexofimage].stringByReplacingOccurrencesOfString("cache/w300xh200-2", withString:"");
//           self.bigImage.imageFromUrl(newUrl)
          
            var newUrl = ProfileImages[self.indexofimage].stringByReplacingOccurrencesOfString("w300xh200-2", withString:"w1000");
          /*  let widhtOfView = self.view.frame.width
          
            if(widhtOfView == 387.00){
                newUrl = ProfileImages[self.indexofimage].stringByReplacingOccurrencesOfString("w300xh200-2", withString:"w800xh800-1");
                
            }else if(widhtOfView > 390.00){
                
                newUrl = ProfileImages[self.indexofimage].stringByReplacingOccurrencesOfString("w300xh200-2", withString:"w1000xh1000-1");
            }*/
            /*else{
               newUrl = ProfileImages[indexPath.row].stringByReplacingOccurrencesOfString("w300xh200-2", withString:"w1600xh1400-2");
            }*/
            
             print("**newUrl:\(newUrl)")
            //self.bigImage.af_setImageWithURL(NSURL(string: newUrl)!)
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                //All stuff here
                print("Run on background thread")
                self.bigImage.sd_setImageWithURL(NSURL(string: newUrl), placeholderImage: nil, options: SDWebImageOptions.RefreshCached)
            })

            self.bigImage.contentMode = .ScaleAspectFit
            
          //  self.bigImage.image = image
            self.bigImage!.alpha = 0.4
          //  self.bigImage.contentMode = .Center
            //code to animate bg with delay 2 and after completion it recursively calling animateImage method
            UIView.animateWithDuration(0.5, delay: 0, options:UIViewAnimationOptions.CurveEaseOut, animations: {() in
                self.bigImage!.alpha = 1.0;
                },
                                       completion: {(Bool) in
            })
            
            
           /*  Alamofire.request(.GET, newUrl,headers:headers).response()
                {
                    (request, response, data, result) in
                    let image = UIImage(data: data! as NSData)
                    //Jchange
                    
                    //self.bigImage.contentMode = UIViewContentMode.ScaleAspectFit
                    self.bigImage.contentMode = .ScaleAspectFit
                    self.bigImage.image = image
                    self.bigImage!.alpha = 0.4
                    //code to animate bg with delay 2 and after completion it recursively calling animateImage method
                    UIView.animateWithDuration(0.5, delay: 0, options:UIViewAnimationOptions.CurveEaseOut, animations: {() in
                        self.bigImage!.alpha = 1.0;
                        },
                        completion: {(Bool) in
                    })
            }*/
        }else{
            indexofimage += 1
        }
    }
    @IBAction func forwardBtnClicked(sender: AnyObject) {
        self.OptionView.hidden = true
        if(self.UserPicDeleted){
            self.UserPicDeleted = false
            self.indexofimage -= 1
        }
        self.indexofimage += 1
        if(self.indexofimage >  self.ProfileImages.count - 1)
        {
            self.indexofimage = 0
        }
        checkAvailabilityOfPhotosOption()
        // print(" On forwardd :\(self.indexofimage)  > \(self.idImages)")
        if (self.indexofimage == self.profilePicId) {
            self.setProfilePhoto.enabled = false
            self.setProfilePhoto.alpha = 0.7
        }else{
            self.setProfilePhoto.enabled = true
            self.setProfilePhoto.alpha = 1
        }
        let indexPath = NSIndexPath(forRow: self.indexofimage, inSection: 0)
        self.collectionView!.scrollToItemAtIndexPath(indexPath, atScrollPosition: UICollectionViewScrollPosition.Bottom, animated: true)
        let headers = delObj.headersAll
        if(self.indexofimage < self.ProfileImages.count)
        {
            var checkUrl = ProfileImages[self.indexofimage]
            print("*** checkUrl \(checkUrl)")
            
//            let newUrl = ProfileImages[self.indexofimage].stringByReplacingOccurrencesOfString("cache/w300xh200-2", withString:"");
//            self.bigImage.imageFromUrl(newUrl)
//
            var newUrl = ProfileImages[self.indexofimage].stringByReplacingOccurrencesOfString("w300xh200-2", withString:"w1000");
            
            //let newUrl = ProfileImages[self.indexofimage]
            print("**newUrl:\(newUrl)")
            
            
          /*  let widhtOfView = self.view.frame.width
            if(widhtOfView == 387.00){
                newUrl = ProfileImages[self.indexofimage].stringByReplacingOccurrencesOfString("w300xh200-2", withString:"w800xh800-1");
                
            }else if(widhtOfView > 390.00){
                
                newUrl = ProfileImages[self.indexofimage].stringByReplacingOccurrencesOfString("w300xh200-2", withString:"w1000xh1000-1");
            }*/
            /*else{
                  newUrl = ProfileImages[indexPath.row].stringByReplacingOccurrencesOfString("w300xh200-2", withString:"w1600xh1400-2");
            }*/
            
            print("**newUrl:\(newUrl)")
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                //All stuff here
                print("Run on background thread")
                self.bigImage.sd_setImageWithURL(NSURL(string: newUrl), placeholderImage: nil, options: SDWebImageOptions.RefreshCached)
            })

            
            self.bigImage.contentMode = .ScaleAspectFit
            
            self.bigImage!.alpha = 0.4
           // self.bigImage.contentMode = .Center
            //code to animate bg with delay 2 and after completion it recursively calling animateImage method
            UIView.animateWithDuration(0.5, delay: 0, options:UIViewAnimationOptions.CurveEaseOut, animations: {() in
                self.bigImage!.alpha = 1.0;
                },
                        completion: {(Bool) in
            })
        
            /*Alamofire.request(.GET, newUrl,headers:headers).response()
                {
                    (_, _, data, _) in
                  //  print("Data: \(data)")
                    let image = UIImage(data: data!)                 
                    print("Image: \(image)")
                  // print("Data: \(data)")
                   //self.bigImage.contentMode = UIViewContentMode.ScaleAspectFit
                    //let h = image?.size.height
                    let h = image?.size.height
                    let w = image?.size.width
                    //let w = image?.size.width
                    
                    print("height \(h)")
                    print("width \(w)")

                   /* if(h > 196 || w > 305)
                    {
                        self.bigImage.hidden = true
                        self.scrollView.hidden = false
                        self.NewBigImg.image = image
                    }
                    else
                    {*/
                    self.bigImage.image = image
                    self.bigImage!.alpha = 0.4
                    self.bigImage.contentMode = .ScaleAspectFit
                    //code to animate bg with delay 2 and after completion it recursively calling animateImage method
                    UIView.animateWithDuration(0.5, delay: 0, options:UIViewAnimationOptions.CurveEaseOut, animations: {() in
                        self.bigImage!.alpha = 1.0;
                        },
                        completion: {(Bool) in
                    })}*/
            //}
        }else
        {
            self.indexofimage -= 1
        }
    }
    
    
   
    
    @IBAction func btnOtionClickedEv(sender: AnyObject) {
        self.OptionView.hidden = !self.OptionView.hidden
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        let setProfilePhoto = UIAlertAction(title: "Set as profile photo", style: .Default) { (action) in
            // ...
            self.evnSetAsProfilePhoto(self)
            self.OptionView.hidden = true
        }
        if(EDIT_PHOTOS == "true" || proxyUser == "true"){
            if(self.profilePicId == self.indexofimage){ }
            else{  alertController.addAction(setProfilePhoto)  }
        }
        
        let destroyAction = UIAlertAction(title: "Delete photo", style: .Destructive) { (action) in
            self.evnDeletePhoto(self)
            self.OptionView.hidden = true
        }
        alertController.addAction(destroyAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            self.OptionView.hidden = true
            // ...
        }
        alertController.addAction(cancelAction)
        
        alertController.popoverPresentationController?.sourceView = self.BigImageView
        alertController.popoverPresentationController?.sourceRect = self.BigImageView.bounds
        self.presentViewController(alertController, animated: true) {
            // ...
        }
    }
    @IBAction func btnEvClose(sender: AnyObject) {
        self.OptionView.hidden = true
        UIView.animateWithDuration(0.3, animations: {
            self.ViewOfCollVw.frame.origin.y = self.HeaderView.frame.height + 2
            self.collectionView?.frame.size.height = self.view.frame.height - (self.HeaderView.frame.height + 10)
            self.bigImage.image = UIImage(named: "")
            self.OptionView.hidden = true
        })
    }
    
    //MARK: Collection view delegates.
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int{
        return 1
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let count = ProfileImages.count
        if(count == 1 ){
        
            self.btnFrwrd.hidden = true;
            self.btnBckwrd.hidden = true;
        
        }else{ self.btnFrwrd.hidden = false;
            self.btnBckwrd.hidden = false;  }
        return ProfileImages.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("CollectionViewCell", forIndexPath: indexPath) as! CollectionViewCell
         cell.backgroundColor = UIColor(red: 236/255, green: 236/255, blue: 236/255, alpha: 1.0)
        let headers =  delObj.headersAll
        let newUrl = ProfileImages[indexPath.row].stringByReplacingOccurrencesOfString("w300xh200-2", withString:"w500");
        
       // let image = UIImage()
        //Jchange
       // cell.imageView.contentMode = .ScaleToFill
        //cell.imageView.contentMode = .ScaleAspectFill
        cell.imageView.contentMode = .ScaleAspectFit
        cell.imageView.image = nil
        
        cell.imageView.sd_setImageWithURL(NSURL(string: newUrl), placeholderImage: nil, options: SDWebImageOptions.RefreshCached)
        //cell.imageView.contentMode = .ScaleToFill
       /* Alamofire.request(.GET, newUrl,headers:headers).response()
        {
                (_, _, data, _) in
                    
                    let image = UIImage(data: data! as NSData)
            //Jchange
                   cell.imageView.contentMode = .ScaleToFill
                    //cell.imageView.contentMode = .ScaleAspectFill
            
                    cell.imageView.image = image
            
        }*/
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath){
        self.indexofimage = indexPath.row
        checkAvailabilityOfPhotosOption()
        
      //  var newUrl = ProfileImages[indexPath.row].stringByReplacingOccurrencesOfString("w300xh200-2", withString:"w300xh200-1");
     
         var newUrl = ProfileImages[indexPath.row].stringByReplacingOccurrencesOfString("w300xh200-2", withString:"w1000");
        
     /*   let widhtOfView = self.view.frame.width
        if(widhtOfView == 387.00){
             newUrl = ProfileImages[indexPath.row].stringByReplacingOccurrencesOfString("w300xh200-2", withString:"w800xh800-1");
        
        }else if(widhtOfView > 390.00){
            
          newUrl = ProfileImages[indexPath.row].stringByReplacingOccurrencesOfString("w300xh200-2", withString:"w1000xh1000-1");
        }*/
        /*else{
             newUrl = ProfileImages[indexPath.row].stringByReplacingOccurrencesOfString("w300xh200-2", withString:"w1600xh1400-2");
        }*/
        
   
        print("**newUrl:\(newUrl)")
        //self.bigImage.contentMode = .Center
        let headers =  delObj.headersAll
      
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            //All stuff here
            print("Run on background thread")
            self.bigImage.sd_setImageWithURL(NSURL(string: newUrl), placeholderImage: nil, options: SDWebImageOptions.RefreshCached)
        })
        
       // self.bigImage.af_setImageWithURL(NSURL(string: newUrl)!)
          self.bigImage.contentMode = .ScaleAspectFit
        //self.bigImage.clipsToBounds = true
      //  self.bigImage.contentMode = .ScaleAspectFit
        
         self.bigImage!.alpha = 1.0;
        
        
      /* Alamofire.request(.GET, newUrl,headers:headers).response()
            {
                (_, _, data, _) in
                let image = UIImage(data: data! as NSData)
                //Jchange
              

                //self.bigImage.contentMode = UIViewContentMode.ScaleAspectFit
                self.bigImage.autoresizingMask = [UIViewAutoresizing.FlexibleBottomMargin , UIViewAutoresizing.FlexibleHeight, UIViewAutoresizing.FlexibleRightMargin ,UIViewAutoresizing.FlexibleLeftMargin , UIViewAutoresizing.FlexibleTopMargin , UIViewAutoresizing.FlexibleWidth]

                
             // self.bigImage.contentMode = .ScaleToFill
                self.bigImage.clipsToBounds = true
               self.bigImage.contentMode = .ScaleAspectFit
                print("*****image dimensino:\(image!.size.width) height:\(image!.size.height)")
               // self.bigScrollView.contentSize = CGSizeMake(image!.size.width, image!.size.height)
               // self.bigImage.frame = CGRectMake(0, 0, image!.size.width, image!.size.height)
                 print("*****bigImage dimensino:\(self.bigImage.frame.size.width) height:\(self.bigImage.frame.size.height)")
               self.bigImage.image = image
                self.bigImage!.alpha = 0.7
                               //code to animate bg with delay 2 and after completion it recursively calling animateImage method
                UIView.animateWithDuration(0.5, delay: 0, options:UIViewAnimationOptions.TransitionCurlUp, animations: {() in
                    self.bigImage!.alpha = 1.0;
                    },
                    completion: {(Bool) in
                })
                
        }*/
        
        UIView.animateWithDuration(0.5, animations: {
            self.ViewOfCollVw.frame.origin.y = self.BigImageView.frame.origin.y + self.BigImageView.frame.height + 2
            self.collectionView?.frame.size.height = self.view.frame.height - self.ViewOfCollVw.frame.origin.y
            
            self.collectionView!.scrollToItemAtIndexPath(indexPath, atScrollPosition: UICollectionViewScrollPosition.Bottom, animated: false)
        })
    }
    
    func collectionView(collectionView: UICollectionView,
                        willDisplayCell cell: UICollectionViewCell,
                                        forItemAtIndexPath indexPath: NSIndexPath){
    }
    
    //    func collectionView(collectionView: UICollectionView, didHighlightItemAtIndexPath indexPath: NSIndexPath) {
    //        let cell = collectionView.cellForItemAtIndexPath(indexPath)
    //        cell?.backgroundColor = UIColor.redColor()
    //    }
    //
    //    // change background color back when user releases touch
    //    func collectionView(collectionView: UICollectionView, didUnhighlightItemAtIndexPath indexPath: NSIndexPath) {
    //        let cell = collectionView.cellForItemAtIndexPath(indexPath)
    //        cell?.backgroundColor = UIColor.greenColor()
    //    }
    
    func checkAvailabilityOfPhotosOption(){
        
        if(!(self.uploadedImage.isEmpty)){
            if(self.uploadedImage[self.indexofimage] == "1" || (EDIT_PHOTOS == "true" || proxyUser == "true")){
                let tap = UITapGestureRecognizer(target: self, action: #selector(photosView.btnOtionClickedEv(_:)))
                bigImage.addGestureRecognizer(tap)
                bigImage.userInteractionEnabled = true
                //btnOtionClicked.hidden = false
            }else{
                bigImage.userInteractionEnabled = false
                //btnOtionClicked.hidden = true
            }
            
        }
    }
    
//    func scaleImage(orginalImage: UIImage) -> UIImage {
//        var widthFactor: CGFloat = bigImage.frame.size.width / image.size.width
//        var destinationSize: CGSize = CGSizeMake(orginalImage.size.width * widthFactor, image.size.height * widthFactor)
//        UIGraphicsBeginImageContext(destinationSize)
//        orginalImage.drawInRect(CGRectMake(0, 0, destinationSize.width, destinationSize.height))
//        var scaledImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        bigImage.image = scaledImage
//        return scaledImage
//    }
//    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
