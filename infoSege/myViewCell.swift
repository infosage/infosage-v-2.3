//
//  myViewCell.swift
//  infoSege
//
//  Created by Pramod shirsath on 3/5/15.
//  Copyright (c) 2015 Pramod shirsath. All rights reserved.
//

import UIKit

class myViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var keystoneName: UILabel!
}
