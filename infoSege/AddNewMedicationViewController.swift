//
//  AddNewMedicationViewController.swift
//  infoSage
//
//  Created by Kunal MAC1 on 23/07/15.
//  Copyright (c) 2015 Pramod shirsath. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AddNewMedicationViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var touchCloseView: UIView!
    @IBOutlet weak var progressBar: UIActivityIndicatorView!
    @IBOutlet weak var tblSerachMedName: UITableView!
    @IBOutlet weak var btnSetSymptoms: UIButton!
    @IBOutlet weak var btnSetRoute: UIButton!
    @IBOutlet weak var btnSetDosage: UIButton!
    @IBOutlet weak var btnSetFrequency: UIButton!
    @IBOutlet weak var btnSetType: UIButton!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var txtNoteBox: UITextView!
    @IBOutlet weak var txtSearchMedicine: UITextField!
    @IBOutlet weak var btnClearMedication: UIButton!
    @IBOutlet weak var btnSaveMedication: UIButton!
    @IBOutlet weak var addSymptomView: UIView!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var unClickableView: UIView!
    
    var alertRoute:UIAlertView = UIAlertView()
    var alertType:UIAlertView = UIAlertView()
    var alertDosage:UIAlertView = UIAlertView()
    var alertFrequency:UIAlertView = UIAlertView()
    var alertSymptoms:UIAlertView = UIAlertView()
    var arrfrqList:NSDictionary = NSDictionary()
    var arrsympList:NSDictionary = NSDictionary()
    var drugID1 = ""
    var dosageName = "Unknown"
    var typeName = "Unknown"
    var routeName = "Unknown"
    var userData : JSON = JSON("")
    var isSelected: UIImage = UIImage()
    var unselected: UIImage = UIImage()
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let defaults = NSUserDefaults.standardUserDefaults()
    var arrMedicationName:[String] = []
    var arrDrugImoCode:[String] = []
    var arrRouts:[String] = []
    var arrFrequency = ["Unknown",
        "As needed",
        "One time",
        "Once daily",
        "Twice daily",
        "Three times daily",
        "Four times daily",
        "At bed time",
        "Every other day",
        "Once a week",
        "Every four (4) hours",
        "Every six (6) hours",
        "Every eight (8) hours",
        "Every twelve (12) hours",
        "Every month"]
    
    var  arrFrequencyObject:[String] = []
    var  arrSymptomObject:[String] = []
    var  arrType:[String] = []
    var arrDosage:[String] = []
    var arrSymptom = ["Unknown",
        "Chest Pain",
        "Constipation",
        "Cough",
        "Fever",
        "Headache",
        "Heartburn",
        "Indigestion",
        "Insomnia",
        "Nausea",
        "Pain",
        "Shortness of Breath",
        "Other"]
    
    var arrButtonDosage:[String] = []
    var arrButtonType:[String] = []
    var arrButtonRoute:[String] = []
    
    var editData:NSArray = NSArray()
    var proxyUser: String =  String()
    var kID: String =  String()
    var logUserId = ""
    var nextLine = 0
    var oldTag = 0
    
    var stringOne = ""
    
    var isMedicationVisible : String = String()
    
    var isProxyUser : String = String()
   
    
    let vc : personInformation = personInformation()
    
    let hvc : homePage = homePage()
    
    var keystoneID: AnyObject?{
        get{
            return NSUserDefaults.standardUserDefaults().objectForKey("keyID")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         stringOne = defaults.valueForKey("UID") as! String
        
        
        txtNoteBox.text = "Other Notes"
        txtNoteBox.textColor = UIColor.lightGrayColor()
        txtNoteBox.delegate = self
        txtSearchMedicine.autocorrectionType = UITextAutocorrectionType.No
        //txtNoteBox.becomeFirstResponder()
        btnSetSymptoms.hidden = true
       
        proxyUser = defaults.valueForKey("PROXY") as! String!
       
        kID =  keystoneID as! String
        if(proxyUser == "PROXY")
        {
            logUserId = keystoneID as! String
        }else{
            logUserId = defaults.valueForKey("UID") as! String  }
        btnClearMedication.setTitle("Clear", forState: UIControlState.Normal)
        
        if(kID == "" || kID == " ")
        {
            kID = logUserId
            let userLogObject = defaults.valueForKey("USEROBJECT") as! NSObject
            let freData: AnyObject =  NSKeyedUnarchiver.unarchiveObjectWithData(userLogObject as! NSData)!
            self.userData = JSON(freData)
        }
        
        arrsympList = ["id":0,"name":"N/A"]
        arrfrqList = ["allowsSymptom":0,"id":1,"name":"Unknown"]
        setLayerDesign(tblSerachMedName)
        setLayerDesign(btnSetSymptoms)
        setLayerDesign(btnSetRoute)
        setLayerDesign(btnSetDosage)
        setLayerDesign(btnSetFrequency)
        setLayerDesign(btnSetType)
        //setLayerDesign(btnCheckBox)
        setLayerDesign(txtNoteBox)
        setLayerDesign(txtSearchMedicine)
        btnClearMedication.layer.cornerRadius = 5
        btnSaveMedication.layer.cornerRadius = 5
        unClickableView.alpha = 0.4
        unClickableView.userInteractionEnabled = false
        btnCheckBox.tag = 0
        
        editData = defaults.valueForKey("AllForEditData") as! NSArray!
       
        if (appDelegate.height == 568 || appDelegate.height == 480) {
            isSelected = UIImage(named: "check20")!
            unselected = UIImage(named: "un-check20")!
        }
        else{
            isSelected = UIImage(named: "check40")!
            unselected = UIImage(named: "un-check40")!
        }
        if(editData.count != 0)
        {
            oldTag = 1
            
        let msg = "There has been a change in the way this medication is listed in our system. Now, to edit the medication, you will need to delete this medication and re-enter it on your list. Also, you might need to use the non-brand name (written on the label) of the medication. In case you need help, please give us a call (617-278-8158)."
            
            
            //print(editData[0])
            
            var urlString = appDelegate.baseURL+"users/\(kID)/drugImo/drugFormExists/\(editData[0])"
            
            urlString = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
            
            let headers = appDelegate.headersAll
            Alamofire.Manager.sharedInstance
                .request(.GET, urlString, parameters: nil,headers: headers, encoding: .JSON)
                .responseJSON {
                   response in
                    //print(result)
                   let json1 = JSON(response.result.value!)
                    print("json1 191 addmed:\(json1)")
                    if(!json1.boolValue){
                        self.appDelegate.medEditflag = true

                        let alert = UIAlertController(title: nil, message: msg, preferredStyle: UIAlertControllerStyle.Alert)
                        
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:{(UIAlertAction)in Void.self
                        }))
                        
                        self.presentViewController(alert, animated: true, completion: {})
                    
                    }
                    else
                    {
                        self.appDelegate.medEditflag = false
                    }
                    
            }
            
            let freData: AnyObject =  NSKeyedUnarchiver.unarchiveObjectWithData(editData[11] as! NSData)!
            let sympData: AnyObject =  NSKeyedUnarchiver.unarchiveObjectWithData(editData[12] as! NSData)!
            arrsympList = sympData as! NSDictionary
            arrfrqList = freData as! NSDictionary
          
            btnClearMedication.setTitle("Cancel", forState: UIControlState.Normal)
            getMade(editData[10] as! String)
            unClickableView.alpha = 1
            unClickableView.userInteractionEnabled = true
            
            txtSearchMedicine.text = editData[1] as? String
            routeName = (editData[2] as? String)!
            btnSetRoute.setTitle(editData[2] as? String, forState: UIControlState.Normal)
            typeName = (editData[3] as? String)!
            btnSetType.setTitle(editData[3] as? String, forState: UIControlState.Normal)
            dosageName = (editData[4] as? String)!
            btnSetDosage.setTitle(editData[4] as? String, forState: UIControlState.Normal)
            btnSetFrequency.setTitle(editData[5] as? String, forState: UIControlState.Normal)
            if(editData[5] as! String == "As needed")
            {
                btnSetSymptoms.hidden = false
                UIView.animateWithDuration(0.3, animations: {
                    self.addSymptomView.frame.origin.y = self.btnSetFrequency.frame.origin.y + self.btnSetFrequency.frame.size.height
                    
                    }, completion:nil)
            }
            if(editData[6] as! String != "N/A")
            {
                btnSetSymptoms.setTitle(editData[6] as? String, forState: UIControlState.Normal)
            }
            if(editData[8] as! String != ""){
                txtNoteBox.text = editData[8] as! String
                txtNoteBox.textColor = UIColor.darkGrayColor()}
            if(editData[7] as! String == "false")
            {
                btnCheckBox.tag = 1
                btnCheckBox.setImage(self.isSelected, forState: UIControlState.Normal)
            }else
            {
                btnCheckBox.tag = 0
                btnCheckBox.setImage(self.unselected, forState: UIControlState.Normal)
            }
            
        }
        
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddNewMedicationViewController.handleSingleTap(_:)))
        tapRecognizer.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapRecognizer)
        getUserDate()
        
        //NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: Selector("timerCall"), userInfo: nil, repeats: true)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        
        if(userData == nil)
        {getUserDate()}
    }
    func getUserDate()
    {
        let headers = appDelegate.headersAll
        
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL+"users/" + kID + "/profile", parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    
                    let json1 = JSON(response.result.value!)
                    
                    if(json1["userMessage"].stringValue == "You don't have permission to do that."){
                        
                    
                        self.LogoutForProxy()
                    }

                    
                    self.userData = json1["user"]
                    
                    
                    //println(self.userData)
                }
        }
    }
    
    func LogoutForProxy()
    {
        let alert = UIAlertController(title: "Your privileges have been modified for your Keystone(s). You will be re-logged in now.", message: "", preferredStyle: UIAlertControllerStyle.Alert)

        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:
            {
                (UIAlertAction)in Void.self
                
                var tokanid = self.defaults.valueForKey("deviceToken") as? String
                
                if(tokanid == nil){
                    tokanid = "h433897fg77873456fg868389"
                }
                
                //Clear defaults
                defer{
                
                self.appDelegate.checkLogin()
                }
                let nextView = self.storyboard?.instantiateViewControllerWithIdentifier("idLoginViewController") as! ViewController
                self.navigationController?.pushViewController(nextView, animated: true)
                
                
        }))
        
        dispatch_async(dispatch_get_main_queue(), {
            // code here
            
            self.presentViewController(alert, animated: true, completion: nil)
            
        })
        //print("Your Keystone status has changed. You will be re-logged in now.")
        
        
    }

    func handleSingleTap(recognizer: UITapGestureRecognizer) {
        self.view.endEditing(true)
        nextLine = 0
        txtNoteBox.resignFirstResponder()
        alertType.dismissWithClickedButtonIndex(0, animated: true)
    }
    func setLayerDesign(object:AnyObject)
    {
        object.layer.borderColor = UIColor.grayColor().CGColor
        object.layer.borderWidth = 0.5
        object.layer.cornerRadius = 5
        object.titleLabel?!.adjustsFontSizeToFitWidth = true
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if (textView.text == "") {
            textView.text = "Other Notes"
            textView.textColor = UIColor.lightGrayColor()
        }
        textView.resignFirstResponder()
    }
    
    func textViewDidBeginEditing(textView: UITextView){
        if (textView.text == "Other Notes"){
            textView.text = ""
            textView.textColor = UIColor.darkGrayColor()
        }
        textView.becomeFirstResponder()
    }
    // med name text field
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        var name = ""
        let countString = txtSearchMedicine.text!.characters.count + (string.characters.count - range.length)
        if(string == "")
        {
            name = txtSearchMedicine.text!.substringToIndex(txtSearchMedicine.text!.endIndex.predecessor())
        }else{
            name = txtSearchMedicine.text!+string
        }
        
        let aString: String = name
        let newString = aString.stringByReplacingOccurrencesOfString("/", withString: "-", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        self.btnSaveMedication.enabled = true
        
        oldTag = 0
        if(countString != 0)
        {
            var urlString = appDelegate.baseURL+"drugImo/search/" + newString
            
            urlString = urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
           
            //print(urlString)
           
            let headers = appDelegate.headersAll
            Alamofire.Manager.sharedInstance
                .request(.GET, urlString, parameters: nil,headers: headers, encoding: .JSON)
                .responseJSON {
                    response in
                    
                    self.arrDrugImoCode.removeAll(keepCapacity: true)
                    self.arrMedicationName.removeAll(keepCapacity: true)
                  //print(result)
                    if(response.result.isSuccess)
                    {
                        self.progressBar.hidden = true
                        
                        let json1 = JSON(response.result.value!)
                        //print(json1)
                        let count: Int? = json1.array?.count
                        if(count != 0)
                        {
                            
                            
                            if let ct = count {
                                for index in 0...ct-1
                                {
                                    self.arrDrugImoCode.insert(json1[index]["code"].stringValue, atIndex: index)
                                    self.arrMedicationName.insert(json1[index]["title"].stringValue, atIndex: index)
                                }
                                if(self.arrMedicationName.count > 0)
                                {   self.progressBar.hidden = true
                                    self.tblSerachMedName.reloadData()
                                    UIView.animateWithDuration(0.3, animations: {
                                        self.tblSerachMedName.frame.origin.y = self.txtSearchMedicine.frame.origin.y + self.txtSearchMedicine.frame.size.height
                                        
                                        }, completion:nil)
                                }  else
                                {
                                    self.progressBar.hidden = true
                                    self.tblSerachMedName.reloadData()
                                    UIView.animateWithDuration(0.3, animations: {
                                        self.tblSerachMedName.frame.origin.y = -self.tblSerachMedName.frame.size.height
                                        
                                        }, completion:nil)
                                }
                            }
                        }
                        else
                        {
                            self.progressBar.hidden = true
                            self.tblSerachMedName.reloadData()
                            UIView.animateWithDuration(0.3, animations: {
                                self.tblSerachMedName.frame.origin.y = -self.tblSerachMedName.frame.size.height
                                
                                }, completion:nil)
                        }
                        
                    }
                    else
                    {
                        self.progressBar.hidden = true
                        self.appDelegate.networkConnError()
                    }
            }
        }else
        {
            self.arrDrugImoCode.removeAll(keepCapacity: false)
            self.arrMedicationName.removeAll(keepCapacity: false)
            self.progressBar.hidden = true
            tblSerachMedName.reloadData()
            UIView.animateWithDuration(0.3, animations: {
                self.tblSerachMedName.frame.origin.y = -self.tblSerachMedName.frame.size.height
                
                }, completion:nil)
        }
        
        return true
    }
    // notebox
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        return false
    }
    
    //Handle return key.
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n")
        {
            self.view.endEditing(true)
            nextLine = 0
            txtNoteBox.resignFirstResponder()
            return false
            
        }
        
        return true
    }
    
    @IBAction func btnEVSetRoute(sender: AnyObject) {
        view.endEditing(true)
        
        
        let alertController = UIAlertController(title: "Set Route", message: nil, preferredStyle: .Alert)
        let closure = { (buttonIndex: Int) in
            { (action: UIAlertAction!) -> Void in
                //println("Index: \(buttonIndex)")
                self.routeName = self.arrButtonRoute[buttonIndex]
                self.btnSetRoute.setTitle(self.arrButtonRoute[buttonIndex], forState: UIControlState.Normal)
            }
        }
       
        arrButtonRoute.removeAll(keepCapacity: true)
        var oldValue = ""
        var index = 0
        var index1 = 0
       
        for btn in arrRouts
        {
            
            if((btnSetType.currentTitle!) != "Type")
            {
                if(arrType[index] == typeName || typeName == "Unknown" || arrType[index] == "Unknown")
                {
                    if((btnSetDosage.currentTitle!) != "Dosage")
                    {
                        if(arrDosage[index] == dosageName || dosageName == "Unknown" || arrDosage[index] == "Unknown")
                        {
                            if(oldValue != btn && !arrButtonRoute.contains(btn))
                            {
                                arrButtonRoute.append(btn)
                                oldValue = btn
                                //alertRoute.addButtonWithTitle(btn)
                                alertController.addAction(UIAlertAction(title: btn, style: .Default, handler: closure(index1)))
                                index1 += 1
                            }
                            
                        }
//                        else
//                        {
//                            if(oldValue != btn && !arrButtonRoute.contains(btn))
//                            {
//                                arrButtonRoute.append(btn)
//                                oldValue = btn
//                                //alertRoute.addButtonWithTitle(btn)
//                                alertController.addAction(UIAlertAction(title: btn, style: .Default, handler: closure(index1)))
//                                index1 += 1
//                            }
//                        }
                    }
                    else{
                        if(oldValue != btn && !arrButtonRoute.contains(btn))
                        {
                            arrButtonRoute.append(btn)
                            oldValue = btn
                            //alertType.addButtonWithTitle(arrType[index])
                            alertController.addAction(UIAlertAction(title: arrRouts[index], style: .Default, handler: closure(index1)))
                            index1 += 1
                        }
                        
                    }
                }
            }
            else
            {
                if((btnSetDosage.currentTitle!) != "Dosage")
                {
                    if(arrDosage[index] == dosageName || dosageName == "Unknown" || arrDosage[index] == "Unknown")
                    {
                        if((btnSetType.currentTitle!) != "Type")
                        {
                            if(arrType[index] == typeName || typeName == "Unknown" || arrType[index] == "Unknown")
                            {
                                if(oldValue != btn && !arrButtonRoute.contains(btn))
                                {
                                    arrButtonRoute.append(btn)
                                    oldValue = btn
                                    //alertRoute.addButtonWithTitle(btn)
                                    alertController.addAction(UIAlertAction(title: btn, style: .Default, handler: closure(index1)))
                                    index1 += 1
                                }
                            }
                        }
                        else
                        {
                            if(oldValue != btn && !arrButtonRoute.contains(btn))
                            {
                                arrButtonRoute.append(btn)
                                oldValue = btn
                                //alertRoute.addButtonWithTitle(btn)
                                alertController.addAction(UIAlertAction(title: btn, style: .Default, handler: closure(index1)))
                                index1 += 1
                            }
                        }
                    }
                }
                else
                {
                    if(oldValue != btn && !arrButtonRoute.contains(btn))
                    {
                        arrButtonRoute.append(btn)
                        oldValue = btn
                        //alertRoute.addButtonWithTitle(btn)
                        alertController.addAction(UIAlertAction(title: btn, style: .Default, handler: closure(index1)))
                        index1 += 1
                    }
                }
            }
            index += 1
            
            
        }
        
        let buttonCancel = UIAlertAction(title: "Cancel", style: .Destructive) { (action) -> Void in
            //println("Cancel Button Pressed")
        }
        alertController.addAction(buttonCancel)
        
        presentViewController(alertController, animated: true, completion: nil)
        //alertRoute.show()
    }
    
    @IBAction func btnEVSetType(sender: AnyObject) {
        view.endEditing(true)
        
        let alertController = UIAlertController(title: "Set Type", message: nil, preferredStyle: .Alert)
        let closure = { (buttonIndex: Int) in
            { (action: UIAlertAction!) -> Void in
                //println("Index: \(buttonIndex)")
                self.typeName = self.arrButtonType[buttonIndex]
                self.btnSetType.setTitle(self.arrButtonType[buttonIndex], forState: UIControlState.Normal)
            }
        }
    
        var oldValue = ""
        var index = 0
        var index1 = 0
        arrButtonType.removeAll(keepCapacity: true)
        for btn in arrType
        {
            if((btnSetRoute.currentTitle!) != "Route")
            {
                if(arrRouts[index] == routeName || routeName == "Unknown" || arrRouts[index] == "Unknown")
                {
                    if((btnSetDosage.currentTitle!) != "Dosage")
                    {
                        if(arrDosage[index] == dosageName || dosageName == "Unknown" || arrDosage[index] == "Unknown")
                        {
                            if(oldValue != btn && !arrButtonType.contains(btn))
                            {
                                arrButtonType.append(btn)
                                oldValue = btn
                                //alertType.addButtonWithTitle(arrType[index])
                                alertController.addAction(UIAlertAction(title: arrType[index], style: .Default, handler: closure(index1)))
                                index1 += 1
                            }
                            
                        }
                    }
                    else{
                        if(oldValue != btn && !arrButtonType.contains(btn))
                        {
                            arrButtonType.append(btn)
                            oldValue = btn
                            //alertType.addButtonWithTitle(arrType[index])
                            alertController.addAction(UIAlertAction(title: arrType[index], style: .Default, handler: closure(index1)))
                            index1 += 1
                        }
                        
                    }
                    
                }
            }
            else
            {
                if((btnSetDosage.currentTitle!) != "Dosage")
                {
                    if(arrDosage[index] == dosageName || dosageName == "Unknown" || arrDosage[index] == "Unknown")
                    {
                        if((btnSetRoute.currentTitle!) != "Route")
                        {
                            if(arrRouts[index] == routeName || routeName == "Unknown" || arrRouts[index] == "Unknown")
                            {
                                
                                if(oldValue != btn && !arrButtonType.contains(btn))
                                {
                                    arrButtonType.append(btn)
                                    oldValue = btn
                                    //alertType.addButtonWithTitle(arrType[index])
                                    alertController.addAction(UIAlertAction(title: arrType[index], style: .Default, handler: closure(index1)))
                                    index1 += 1
                                }
                            }
                        }else{
                            if(oldValue != btn && !arrButtonType.contains(btn))
                            {
                                arrButtonType.append(btn)
                                oldValue = btn
                                //alertType.addButtonWithTitle(arrType[index])
                                alertController.addAction(UIAlertAction(title: arrType[index], style: .Default, handler: closure(index1)))
                                index1 += 1
                            }
                            
                        }
                        
                    }
                }else{
                    if(oldValue != btn && !arrButtonType.contains(btn))
                    {
                        arrButtonType.append(btn)
                        oldValue = btn
                        //alertType.addButtonWithTitle(arrType[index])
                        alertController.addAction(UIAlertAction(title: arrType[index], style: .Default, handler: closure(index1)))
                        index1 += 1
                    }
                    
                }
            }
            index += 1
        }
        
        let buttonCancel = UIAlertAction(title: "Cancel", style: .Destructive) { (action) -> Void in
            //println("Cancel Button Pressed")
        }
        alertController.addAction(buttonCancel)
        
        presentViewController(alertController, animated: true, completion: nil)
        //alertType.show()
    }
    
    @IBAction func btnEVSetDosage(sender: AnyObject) {
        view.endEditing(true)
        
        let alertController = UIAlertController(title: "Set Dosage", message: nil, preferredStyle: .Alert)
        let closure = { (buttonIndex: Int) in
            { (action: UIAlertAction!) -> Void in
                //println("Index: \(buttonIndex)")
                self.dosageName = self.arrButtonDosage[buttonIndex]
                self.btnSetDosage.setTitle(self.arrButtonDosage[buttonIndex], forState: UIControlState.Normal)
            }
        }
        
        arrButtonDosage.removeAll(keepCapacity: true)
        // alertDosage = UIAlertView(title: "Set Dosage", message: nil, delegate: self, cancelButtonTitle: "Cancel")
        var oldValue = ""
        var index = 0
        var index1 = 0
        arrButtonDosage.removeAll(keepCapacity: true)
        for btn in arrDosage
        {
            if((btnSetType.currentTitle!) != "Type")
            {
                if(arrType[index] == typeName || typeName == "Unknown" || arrType[index] == "Unknown")
                {
                    
                    if((btnSetRoute.currentTitle!) != "Route")
                    {
                        if(arrRouts[index] == routeName || routeName == "Unknown" || arrRouts[index] == "Unknown")
                        {
                            if(oldValue != btn && !arrButtonDosage.contains(btn))
                            {
                                arrButtonDosage.append(btn)
                                oldValue = btn
                                // alertDosage.addButtonWithTitle(arrDosage[index])
                                alertController.addAction(UIAlertAction(title:arrDosage[index], style: .Default, handler: closure(index1)))
                                index1 += 1
                            }
                            
                        }
                       
                    }
                    else{
                        if(oldValue != btn && !arrButtonDosage.contains(btn))
                        {
                            arrButtonDosage.append(btn)
                            oldValue = btn
                            //alertType.addButtonWithTitle(arrType[index])
                            alertController.addAction(UIAlertAction(title: arrDosage[index], style: .Default, handler: closure(index1)))
                            index1 += 1
                        }
                        
                    }
                }
            }
            else
            {
                if((btnSetRoute.currentTitle!) != "Route")
                {
                    if(arrRouts[index] == routeName || routeName == "Unknown" || arrRouts[index] == "Unknown")
                    {
                        if((btnSetType.currentTitle!) != "Type")
                        {
                            if(arrType[index] == typeName || typeName == "Unknown" || arrType[index] == "Unknown")
                            {
                                if(oldValue != btn && !arrButtonDosage.contains(btn))
                                {
                                    arrButtonDosage.append(btn)
                                    oldValue = btn
                                    //alertDosage.addButtonWithTitle(arrDosage[index])
                                    alertController.addAction(UIAlertAction(title: arrDosage[index], style: .Default, handler: closure(index1)))
                                    index1 += 1
                                }
                            }
                        }
                        else
                        {
                            if(oldValue != btn && !arrButtonDosage.contains(btn))
                            {
                                arrButtonDosage.append(btn)
                                oldValue = btn
                                //alertRoute.addButtonWithTitle(btn)
                                alertController.addAction(UIAlertAction(title: btn, style: .Default, handler: closure(index1)))
                                index1 += 1
                            }
                        }
                    }
                }
                else
                {
                    if(oldValue != btn && !arrButtonDosage.contains(btn))
                    {
                        arrButtonDosage.append(btn)
                        oldValue = btn
                        //alertDosage.addButtonWithTitle(arrDosage[index])
                        alertController.addAction(UIAlertAction(title: arrDosage[index], style: .Default, handler: closure(index1)))
                        index1 += 1
                    }
                }
            }
            index += 1
            
        }
        
        let buttonCancel = UIAlertAction(title: "Cancel", style: .Destructive) { (action) -> Void in
            //println("Cancel Button Pressed")
        }
        alertController.addAction(buttonCancel)
        
        presentViewController(alertController, animated: true, completion: nil)
        //alertDosage.show()
    }
    
    @IBAction func btnEVsetFrequency(sender: AnyObject) {
        view.endEditing(true)
        //alertFrequency = UIAlertView(title: "Set Frequency", message: nil, delegate: self, cancelButtonTitle: "Cancel")
        
        let alertController = UIAlertController(title: "Set Frequency", message: nil, preferredStyle: .Alert)
        let closure = { (buttonIndex: Int) in
            { (action: UIAlertAction!) -> Void in
                //println("Index: \(buttonIndex)")
                self.arrfrqList = ["allowsSymptom":0,"id":buttonIndex+1,"name":self.arrFrequency[buttonIndex]]
                
                //println(self.arrfrqList)
                self.btnSetFrequency.setTitle(self.arrFrequency[buttonIndex], forState: UIControlState.Normal)
                
                if(self.arrFrequency[buttonIndex] == "As needed")
                {   self.btnSetSymptoms.hidden = false
                    self.arrfrqList = ["allowsSymptom":1,"id":buttonIndex+1,"name":self.arrFrequency[buttonIndex]]
                    //println(self.arrfrqList)
                    UIView.animateWithDuration(0.3, animations: {
                        self.addSymptomView.frame.origin.y = self.btnSetFrequency.frame.origin.y + self.btnSetFrequency.frame.size.height
                        
                        }, completion:nil)
                }else{
                    UIView.animateWithDuration(0.3, animations: {
                        self.btnSetSymptoms.hidden = true
                        self.addSymptomView.frame.origin.y = self.btnSetDosage.frame.origin.y + self.btnSetDosage.frame.size.height
                        self.arrsympList = ["id":0,"name":"N/A"]
                        self.btnSetSymptoms.setTitle("Associated Symptom", forState: UIControlState.Normal)
                        
                        }, completion:nil)
                }
            }
        }
        
        var index = 0
        for btn in arrFrequency
        {
            //alertFrequency.addButtonWithTitle(btn)
            alertController.addAction(UIAlertAction(title: btn, style: .Default, handler: closure(index)))
            index += 1
        }
        
        let buttonCancel = UIAlertAction(title: "Cancel", style: .Destructive) { (action) -> Void in
            //println("Cancel Button Pressed")
        }
        alertController.addAction(buttonCancel)
        
        presentViewController(alertController, animated: true, completion: nil)
        //alertFrequency.show()
        
    }
    
    @IBAction func btnEVSetSymptoms(sender: AnyObject) {
        view.endEditing(true)
        //alertSymptoms = UIAlertView(title: "Set Symptom", message: nil, delegate: self, cancelButtonTitle: "Cancel")
        
        let alertController = UIAlertController(title: "Set Symptom", message: nil, preferredStyle: .Alert)
        let closure = { (buttonIndex: Int) in
            { (action: UIAlertAction!) -> Void in
                //println("Index: \(buttonIndex)")
                self.arrsympList = ["id":buttonIndex+1,"name":self.arrSymptom[buttonIndex]]
                self.btnSetSymptoms.setTitle(self.arrSymptom[buttonIndex], forState: UIControlState.Normal)
            }
        }
        
        var index = 0
        for btn in arrSymptom
        {
            // alertSymptoms.addButtonWithTitle(btn)
            alertController.addAction(UIAlertAction(title: btn, style: .Default, handler: closure(index)))
            index += 1
        }
        
        let buttonCancel = UIAlertAction(title: "Cancel", style: .Destructive) { (action) -> Void in
            //println("Cancel Button Pressed")
        }
        alertController.addAction(buttonCancel)
        
        presentViewController(alertController, animated: true, completion: nil)
        //alertSymptoms.show()
    }
    
    @IBAction func btnEVSetCheckUncheck(sender: UIButton) {
        view.endEditing(true)
        if(btnCheckBox.tag == 0)
        {
            btnCheckBox.tag = 1
            btnCheckBox.setImage(self.isSelected, forState: UIControlState.Normal)
        }else{
            btnCheckBox.tag = 0
            btnCheckBox.setImage(self.unselected, forState: UIControlState.Normal)
        }
    }
    
    //MARK: Add medication clicked
    func saveUpdatedData()
    {
        
        self.progressBar.hidden = false
        var shareStat:String = String()
        var notes = ""
        if(txtNoteBox.text != "Other Notes")
        {
            notes = txtNoteBox.text
        }
        if(btnCheckBox.tag == 1)
        {
            shareStat = "false"
        }else
        {
            shareStat = "true"
        }
        
        
        appDelegate.headersAll =  ["X-Vendor-Id": "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ", "X-Proxy-User": self.kID]
        
        let str : NSCharacterSet = NSCharacterSet(charactersInString: " ")
        
       
        if( self.txtSearchMedicine.text!.stringByTrimmingCharactersInSet(str) != "" && self.oldTag == 1)
        {
            if( self.editData.count != 0)
            {
               
                let parameters = [
                    "id":self.editData[0],
                    "user":self.userData.object,
                    "name":self.txtSearchMedicine.text!,
                    "drugImoCode":self.editData[10],
                    "dosage":self.dosageName,
                    "route":self.routeName,
                    "type":self.typeName,
                    "drugFrequency":self.arrfrqList,
                    "drugSymptom":self.arrsympList,
                    "otherNotes":notes,
                    "isPublic":shareStat
                ];
                let headers = self.appDelegate.headersAll
                Alamofire.Manager.sharedInstance
                    .request(.PUT, self.appDelegate.baseURL+"users/" + self.kID + "/drugImo", parameters: parameters, headers: headers,encoding: .JSON)
                    .responseJSON {
                       response in
                        if(response.result.isSuccess)
                        {
                            //print(request)
                            //print(response)
                            //println(error)
                            
                            self.progressBar.hidden = true
                            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("idMedicationList") as! MedicationList
                            
                            
                            self.addChildViewController(vc)
                            vc.view.frame = self.container.frame
                            self.container.addSubview(vc.view)
                            vc.didMoveToParentViewController(self)
                            
                        }else
                        {
                            self.appDelegate.networkConnError()
                        }
                }
              
            }
            else
            {
                
                let parameters = [
                    "user":self.userData.object,
                    "name":self.txtSearchMedicine.text!,
                    "drugImoCode":self.drugID1,
                    "dosage":self.dosageName,
                    "route":self.routeName,
                    "type":self.typeName,
                    "drugFrequency":self.arrfrqList,
                    "drugSymptom":self.arrsympList,
                    "otherNotes":notes,
                    "isPublic":shareStat
                ];
                let headers = self.appDelegate.headersAll
                
                Alamofire.Manager.sharedInstance
                    .request(.POST, self.appDelegate.baseURL+"users/" + self.kID + "/drugImo", parameters: parameters,headers: headers, encoding: .JSON)
                    .responseJSON {
                        response in
                        if(response.result.isSuccess)
                        {
                        
                            self.progressBar.hidden = true
                            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("idMedicationList") as! MedicationList
                            
                            
                            self.addChildViewController(vc)
                            vc.view.frame = self.container.frame
                            self.container.addSubview(vc.view)
                            vc.didMoveToParentViewController(self)
                            
                        }else
                        {
                            self.appDelegate.networkConnError()
                        }
                }
                
                
            }
        }else
        {
            self.progressBar.hidden = true
            self.btnSaveMedication.enabled = true
            let alert = UIAlertController(title: "infoSAGE", message: "Please select valid medication name", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        
        
  }
  

    @IBAction func btnEVSaveMedication(sender: AnyObject) {
        
        self.btnSaveMedication.enabled = false
        
        let headers = appDelegate.headersAll
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL + "users/" + kID + "/privileges/users/" + stringOne, parameters: nil, encoding: .JSON, headers:headers)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    
                    var json1 = JSON(response.result.value!)
                    let count: Int? = json1.array?.count
                    if(count != 0)
                    {
                        self.appDelegate.medEditflag = false
                        self.isMedicationVisible = json1["VIEW_MEDICATIONS"].stringValue as String
                        self.isProxyUser = json1["PROXY"].stringValue as String
                        self.defaults.setValue(self.isProxyUser, forKey:"isProxyUser")
                        
                        let isConnected = json1["CONNECTED"].stringValue
                        
                        if(isConnected == "false" )
                        {
                            self.LogoutForProxy()
                            
                        }else{
                            
                            if(self.isProxyUser == "true"){
                                
                                self.saveUpdatedData()
                               
                                
                            }else{
                            
                            
                                 self.LogoutForProxy()
                            }
                            
                        }
                        
                    }
                }}

        
        
    }
    @IBAction func btnBackToPreviousPage(sender: AnyObject) {
        
        self.defaults.setObject([], forKey:"AllForEditData" )
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("idMedicationList") as! MedicationList
        self.addChildViewController(vc)
        vc.view.frame = self.container.frame
        self.container.addSubview(vc.view)
        vc.didMoveToParentViewController(self)
    }
    @IBAction func btnEVClearMedication(sender: AnyObject) {
         self.appDelegate.medEditflag = false
        if(editData.count != 0)
        {
            self.defaults.setObject([], forKey:"AllForEditData" )
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("idMedicationList") as! MedicationList
            self.addChildViewController(vc)
            vc.view.frame = self.container.frame
            self.container.addSubview(vc.view)
            vc.didMoveToParentViewController(self)
        }else{
            let vc = storyboard?.instantiateViewControllerWithIdentifier("idAddNewMedication") as! AddNewMedicationViewController
            self.addChildViewController(vc)
            vc.view.frame = self.container.frame
            self.container.addSubview(vc.view)
            vc.didMoveToParentViewController(self)
        }
    }
    func alertView(View: UIAlertView!, clickedButtonAtIndex buttonIndex: Int)
    {
        
        if(View == alertRoute)
        {
            if(buttonIndex != 0)
            {
                routeName = arrButtonRoute[buttonIndex-1]
                btnSetRoute.setTitle(arrButtonRoute[buttonIndex-1], forState: UIControlState.Normal)
            }
        }
        if(View == alertType)
        {
            if(buttonIndex != 0)
            {
                typeName = arrButtonType[buttonIndex-1]
                btnSetType.setTitle(arrButtonType[buttonIndex-1], forState: UIControlState.Normal)
            }
        }
        if(View == alertDosage)
        {
            if(buttonIndex != 0)
            {
                dosageName = arrButtonDosage[buttonIndex-1]
                btnSetDosage.setTitle(arrButtonDosage[buttonIndex-1], forState: UIControlState.Normal)
            }
        }
        if(View == alertFrequency)
        {
            if(buttonIndex != 0)
            {
                arrfrqList = ["allowsSymptom":0,"id":buttonIndex,"name":arrFrequency[buttonIndex-1]]
                //println(arrfrqList)
                btnSetFrequency.setTitle(arrFrequency[buttonIndex-1], forState: UIControlState.Normal)
                if(arrFrequency[buttonIndex-1] == "As needed")
                {   btnSetSymptoms.hidden = false
                    arrfrqList = ["allowsSymptom":1,"id":buttonIndex,"name":arrFrequency[buttonIndex-1]]
                    //println(arrfrqList)
                    UIView.animateWithDuration(0.3, animations: {
                        self.addSymptomView.frame.origin.y = self.btnSetFrequency.frame.origin.y + self.btnSetFrequency.frame.size.height
                        
                        }, completion:nil)
                }else{
                    UIView.animateWithDuration(0.3, animations: {
                        self.btnSetSymptoms.hidden = true
                        self.addSymptomView.frame.origin.y = self.btnSetDosage.frame.origin.y + self.btnSetDosage.frame.size.height
                        self.arrsympList = ["id":0,"name":"N/A"]
                        self.btnSetSymptoms.setTitle("Associated Symptom", forState: UIControlState.Normal)
                        
                        }, completion:nil)
                }
            }
        }
        if(View == alertSymptoms)
        {
            if(buttonIndex != 0)
            {
                arrsympList = ["id":buttonIndex,"name":arrSymptom[buttonIndex-1]]
                //println(arrsympList)
                btnSetSymptoms.setTitle(arrSymptom[buttonIndex-1], forState: UIControlState.Normal)
            }
        }
    }
    func backToPreviousPage()
    {
        let vc = storyboard?.instantiateViewControllerWithIdentifier("idMedicationList") as! MedicationList
        self.addChildViewController(vc)
        vc.view.frame = self.container.frame
        self.container.addSubview(vc.view)
        vc.didMoveToParentViewController(self)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMedicationName.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:searchMedicationCell = searchMedicationCell()
        cell = tblSerachMedName.dequeueReusableCellWithIdentifier("searchMedicationCell") as! searchMedicationCell
        cell.btnSelectMedication.tag = indexPath.row
        cell.lblMedicationName.text = arrMedicationName[indexPath.row]
        cell.btnSelectMedication.addTarget(self, action: #selector(AddNewMedicationViewController.getMedication(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        return cell
        
    }
    func getMedication(sender:UIButton)
    {
        oldTag = 1
        view.endEditing(true)
        arrRouts.removeAll(keepCapacity: true)
        arrDosage.removeAll(keepCapacity: true)
        arrType.removeAll(keepCapacity: true)
        self.progressBar.hidden = false
        txtSearchMedicine.text = arrMedicationName[sender.tag]
        self.btnSetRoute.setTitle("Route", forState: UIControlState.Normal)
        self.btnSetType.setTitle("Type", forState: UIControlState.Normal)
        self.btnSetDosage.setTitle("Dosage", forState: UIControlState.Normal)
        self.btnSetFrequency.setTitle("Frequency", forState: UIControlState.Normal)
        arrfrqList = ["allowsSymptom":0,"id":1,"name":"Unknown"]
        UIView.animateWithDuration(0.3, animations: {
            self.btnSetSymptoms.hidden = true
            self.addSymptomView.frame.origin.y = self.btnSetDosage.frame.origin.y + self.btnSetDosage.frame.size.height
            self.arrsympList = ["id":0,"name":"N/A"]
            self.btnSetSymptoms.setTitle("Associated Symptom", forState: UIControlState.Normal)
            
            }, completion:nil)
        
        
        UIView.animateWithDuration(0.3, animations: {
            self.tblSerachMedName.frame.origin.y = -self.tblSerachMedName.frame.size.height
            
            }, completion:nil)
        drugID1 = arrDrugImoCode[sender.tag]
        let headers = appDelegate.headersAll
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL+"drugImo/details/" + arrDrugImoCode[sender.tag], parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
               response in
                if(response.result.isSuccess)
                {
                    let json1 = JSON(response.result.value!)
                    if(json1["userMessage"].stringValue != "You must be logged in to do that."){
                     //print(json1)
                        var json = json1["forms"]
                        let count: Int? = json.array?.count
                     //print(json)
                        
                        if(count != 0)
                        {
                            
                            if let ct = count {
                                self.arrRouts.insert("Unknown", atIndex: 0)
                                self.arrDosage.insert("Unknown", atIndex: 0)
                                self.arrType.insert("Unknown", atIndex: 0)
                                for index in 0...ct-1 {
                                    self.arrRouts.insert(json[index]["route"].stringValue, atIndex: index + 1)
                                    self.arrDosage.insert(json[index]["dosage"].stringValue, atIndex: index + 1)
                                    self.arrType.insert(json[index]["type"].stringValue, atIndex: index + 1)
                                }
                            }
                            self.progressBar.hidden = true
                            self.unClickableView.alpha = 1
                            self.unClickableView.userInteractionEnabled = true
                        }else
                        {
                            self.arrRouts.insert("Unknown", atIndex: 0)
                            self.arrDosage.insert("Unknown", atIndex: 0)
                            self.arrType.insert("Unknown", atIndex: 0)
                            self.progressBar.hidden = true
                            self.unClickableView.alpha = 1
                            self.unClickableView.userInteractionEnabled = true
                            
                        }
                      
                        //print("Routs Array : \(self.arrRouts)")
                        //print("Dosage Array : \(self.arrDosage)")
                        //print("Type Array : \(self.arrType)")
                    }
                    else
                    {
                        self.progressBar.hidden = true
                        self.appDelegate.checkLogin()
                    }
                }else{
                    self.progressBar.hidden = true
                    self.appDelegate.networkConnError();
                }
        }
        
        
    }
    
    func getMade(drugID:String)
    {
        oldTag = 1
        arrRouts.removeAll(keepCapacity: true)
        arrDosage.removeAll(keepCapacity: true)
        arrType.removeAll(keepCapacity: true)
        self.progressBar.hidden = false
        //println(drugID)
        let headers = appDelegate.headersAll
        Alamofire.Manager.sharedInstance
            .request(.GET, appDelegate.baseURL+"drugImo/details/" + drugID, parameters: nil,headers: headers, encoding: .JSON)
            .responseJSON {
                response in
                if(response.result.isSuccess)
                {
                    let json1 = JSON(response.result.value!)
                    if(json1["userMessage"].stringValue != "You must be logged in to do that."){
                        var json = json1["forms"]
                        let count: Int? = json.array?.count
                        if(count != 0)
                        {
                            
                            if let ct = count {
                                self.arrRouts.insert("Unknown", atIndex: 0)
                                self.arrDosage.insert("Unknown", atIndex: 0)
                                self.arrType.insert("Unknown", atIndex: 0)
                                for index in 0...ct-1 {
                                    self.arrRouts.insert(json[index]["route"].stringValue, atIndex: index + 1)
                                    self.arrDosage.insert(json[index]["dosage"].stringValue, atIndex: index + 1)
                                    self.arrType.insert(json[index]["type"].stringValue, atIndex: index + 1)
                                }
                                
                            }
                            self.progressBar.hidden = true
                            self.unClickableView.alpha = 1
                            self.unClickableView.userInteractionEnabled = true
                            
                        }else
                        {
                            self.arrRouts.insert("Unknown", atIndex: 0)
                            self.arrDosage.insert("Unknown", atIndex: 0)
                            self.arrType.insert("Unknown", atIndex: 0)
                            self.progressBar.hidden = true
                            if(!self.appDelegate.medEditflag)
                            {
                                print("Ohhh..Its false")
                                self.appDelegate.checkLogin()
                            }else{
                                print("Ohhh..Its true")
                            }


                           // self.appDelegate.checkLogin()
                        }
                    }else{
                        self.progressBar.hidden = true
                        self.appDelegate.networkConnError();
                    }
                }
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
