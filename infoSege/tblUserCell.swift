//
//  tblUserCell.swift
//  infoSege
//
//  Created by Pramod shirsath on 4/1/15.
//  Copyright (c) 2015 Pramod shirsath. All rights reserved.
//

import UIKit

class tblUserCell: UITableViewCell {

    
    @IBOutlet weak var btUserName: UILabel!
    @IBOutlet weak var btAssignUser: UIButton!
    @IBOutlet weak var checkActiveStatus: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
