//
//  activInactiveMedicationCell.swift
//  infoSage
//
//  Created by Kunal MAC1 on 22/07/15.
//  Copyright (c) 2015 Pramod shirsath. All rights reserved.
//

import UIKit

class activInactiveMedicationCell: UITableViewCell {

    @IBOutlet weak var btnShowFullName: UIButton!
    @IBOutlet weak var btnInformation: UIButton!
    @IBOutlet weak var lblMedicationName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
