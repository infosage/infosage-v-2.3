//
//  myAlertViewCell.swift
//  infoSege
//
//  Created by Pramod shirsath on 4/10/15.
//  Copyright (c) 2015 Pramod shirsath. All rights reserved.
//

import UIKit

class myAlertViewCell: UITableViewCell {

    @IBOutlet weak var lblNoteDate: UILabel!
    @IBOutlet weak var alrtUserImage: UIImageView!
    @IBOutlet weak var alrtUserName: UILabel!
    @IBOutlet weak var alrtUserMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
