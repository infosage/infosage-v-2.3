//
//  myUpdateTableViewCell.swift
//  infoSege
//
//  Created by Pramod shirsath on 3/6/15.
//  Copyright (c) 2015 Pramod shirsath. All rights reserved.
//

import UIKit

class myUpdateTableViewCell: UITableViewCell {

    
    @IBOutlet weak var userProfilePic: UIImageView!
    @IBOutlet weak var txtUserName: UILabel!
    @IBOutlet weak var txtUpdatedMessage: UILabel!
    @IBOutlet weak var txtDates: UILabel!
    @IBOutlet weak var btSendReplyMsg: UIButton!
    @IBOutlet weak var replyMsgText: UITextField!
    @IBOutlet weak var commentCount: UILabel!
    @IBOutlet weak var btShowFullPostMsg: UIButton!
    
    @IBOutlet weak var btEditPost: UIButton!
    @IBOutlet weak var txtRepluUserMsgBox: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
